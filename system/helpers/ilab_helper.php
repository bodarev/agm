<?
function create_form_by_array($form,$data,$checker=false) {
	foreach($form as $row) {
		$value=@$data[$row['name']];
		if (isset($checker) && @in_array($row['name'], $checker)) {
			echo '<tr'.@$row['dop_tr'].'>
			<td width="200">'.$row['descr'].' *</td>
			<td>';
		} else {
			echo '<tr'.@$row['dop_tr'].'>
			<td width="200">'.$row['descr'].'</td>
			<td>';
		}

		if($row['name']=='phone' || $row['name'] == 'resp_phone' || $row['name'] == 'book_phone') {
			$pattern = 'pattern="^\+373(\s+)?\(?(79|78|69|68|67|60)\)?(\s+)?[0-9]{3}-?[0-9]{3}$"';
		} else {
			$pattern = '';
		}
		if (is_numeric($value) && ($row['name']=='PricePromo' || $row['name']=='PriceAngro' ))
		{
			$value = $value + 0;
		}

		if ($row['type']=='text') {
			echo '<input '.$pattern.' name="data['.$row['name'].']" type="text" id="'.$row['name'].'" class="'.$row['class'].'" value="'.$value.'" />';
		} elseif ($row['type']=='textarea') {
			echo '<textarea name="data['.$row['name'].']" id="'.$row['name'].'" class="'.$row['class'].'"'.@$row['dop_style'].'>'.$value.'</textarea>';
		} elseif ($row['type']=='select') {
			echo '<select class="'.$row['class'].'" name="data['.$row['name'].']" id="'.$row['name'].'">';
				addSelect($row['source'],$row['sourcename'],$value,$row['dop_tr']);
			echo '</select>';
		} elseif ($row['type']=='checkbox') {
			addCheckbox($row['source'], $row['sourcename'],$value);
		} elseif ($row['type']=='checkbox2') {
			addCheckbox2($row['source'], $row['sourcename'],$value);
			// echo '<input name="data['.$row['name'].']"'.$md.' type="checkbox" id="'.$row['name'].'" class="'.$row['class'].'" value="1" />';
		} elseif ($row['type']=='file') {
			echo '<input type="file" name="'.$row['name'].'">';
			if (!empty($value)) {
				echo '<img src="/public/'.$row['path'].'/'.$value.'" style="float:right;max-width:200px;">';
				if ($row['name']=='Banner') {
					echo '<br clear="all"><a style="float:right;margin-top:10px;" href="javascript:void(0);" id="bandel" onclick="delbanner()">Удалить</a>';
				}
			}
		} elseif ($row['type']=='catlist') {
			$cats=get_categories();
			echo '<select class="'.$row['class'].'" name="data['.$row['name'].']">';
				echo '<option value="0"></option>';
				build_cat_select($cats,0,$value,$row['exclude']);

			echo '</select>';
		}
		echo @$row['dop_data'];
		echo '</td>
		</tr>';
	}
}

function admin_form_text($translate=false,$name='',$descr='',$class='',$dop_style='',$dop_tr='',$dop_data='') {
	$ci=&get_instance();
	$langs=$ci->_langs();

	if (!empty($class)) $class=' '.$class;
	if (!$translate) {
		$return[]=array(
			'dop_tr'=>$dop_tr,
			'dop_style'=>$dop_style,
			'type'=>'text',
			'class'=>'form-control'.$class,
			'descr'=>$descr,
			'name'=>$name,
			'dop_data'=> $dop_data
		);
	} else {
		foreach($langs as $lang) {
			$return[]=array(
				'dop_tr'=>$dop_tr,
				'dop_style'=>$dop_style,
				'type'=>'text',
				'class'=>'form-control'.$class,
				'descr'=>$descr.' '.$lang,
				'name'=>$name.$lang
			);
		}
	}

	return $return;
}

function admin_form_select($name='',$descr='',$source='',$sourcename='',$class='',$dop_style='',$dop_tr='') {
	$ci=&get_instance();
	$langs=$ci->_langs();

	if (!empty($class)) $class=' '.$class;
	$return[]=array(
		'dop_tr'=>$dop_tr,
		'dop_style'=>$dop_style,
		'type'=>'select',
		'class'=>'form-control'.$class,
		'descr'=>$descr,
		'name'=>$name,
		'source'=>$source,
		'sourcename'=>$sourcename
	);

	return $return;
}

function admin_form_checkbox($name='', $descr='', $source='', $sourcename='', $object_type='', $class='', $dop_style='', $dop_tr='')
{
	$ci=&get_instance();
	$langs=$ci->_langs();

	$class = (!empty($class)) ? ' '.$class : '';
	$return[]= array(
		'dop_tr'	=> $dop_tr,
		'dop_style'	=> $dop_style,
		'type'		=> 'checkbox',
		'class'		=> 'form-control'.$class,
		'descr'		=> $descr,
		'name'		=> $name,
		'source'	=> $source,
		'sourcename'=> $sourcename,
		'object_type'=> $object_type
	);

	return $return;
}
function admin_form_textarea($translate=false,$name='',$descr='',$class='',$dop_style='',$dop_tr='', $dop_data='') {
	$ci=&get_instance();
	$langs=$ci->_langs();

	if (!empty($class)) $class=' '.$class;
	if (!$translate) {
		$return[]=array(
			'dop_tr'=>$dop_tr,
			'dop_style'=>$dop_style,
			'type'=>'textarea',
			'class'=>'form-control'.$class,
			'descr'=>$descr,
			'name'=>$name,
            'dop_data'=>$dop_data
		);
	} else {
		foreach($langs as $lang) {
			$return[]=array(
				'dop_tr'=>$dop_tr,
				'dop_style'=>$dop_style,
				'type'=>'textarea',
				'class'=>'form-control'.$class,
				'descr'=>$descr.' '.$lang,
				'name'=>$name.$lang,
				'dop_data'=>$dop_data
			);
		}
	}

	return $return;
}

function admin_form_file($translate=false,$name='',$descr='',$path='',$class='',$dop_style='',$dop_tr='') {
	$ci=&get_instance();
	$langs=$ci->_langs();

	if (!empty($class)) $class=' '.$class;
	if (!$translate) {
		$return[]=array(
			'dop_tr'=>$dop_tr,
			'dop_style'=>$dop_style,
			'type'=>'file',
			'path'=>$path,
			'class'=>'form-control'.$class,
			'descr'=>$descr,
			'name'=>$name
		);
	} else {
		foreach($langs as $lang) {
			$return[]=array(
				'dop_tr'=>$dop_tr,
				'dop_style'=>$dop_style,
				'type'=>'file',
				'path'=>$path,
				'class'=>'form-control'.$class,
				'descr'=>$descr.' '.$lang,
				'name'=>$name.$lang
			);
		}
	}

	return $return;
}

function convert_form($form) {
	$form1=array();

	foreach($form as $ext) {
		foreach($ext as $in) {
			$form1[]=$in;
		}
	}

	return $form1;
}

function save_data($input=array(),$checker,$files,$tblname,$form) {
	$ci=&get_instance();

	$uri3=intval($ci->uri->segment(3));

	$erflag=false;
	$err='';
	foreach($checker as $val) {
		if (empty($input[$val])) $erflag=true;
	}

	foreach($form as $row) {
		if ($row['type']=='checkbox') {
			if (empty($input[$row['name']])) $input[$row['name']]=0; else $input[$row['name']]=1;
		}
	}

	if (!$erflag) {

		$data_array=$input;
		if (empty($uri3)) {
			$ci->db->insert($tblname,$data_array);
			$id=$ci->db->insert_id();
		} else {
			$id=$uri3;
			$ci->db->where('ID',$id)->update($tblname,$data_array);
		}

		foreach($files as $filename) {

			if(!empty($_FILES[$filename]['name'])) {
				$ci->upload->do_upload($filename);
				$resarr = $ci->upload->data();
				$file = $resarr['file_name'];

				if(strtolower($resarr['file_ext']) == '.jpg' || strtolower($resarr['file_ext']) == '.jpeg' || strtolower($resarr['file_ext']) == '.gif' || strtolower($resarr['file_ext']) == '.png')
				{
					$ci->db->where('ID',$id)->update($tblname,array($filename=>$file));
				}
			}
		}

		if (!empty($id)) {
			header("Location: /".ADM_CONTROLLER."/$tblname/");
			exit();
		}
	} else {
		$err.='<div style="padding:10px 0;color:#ff0000;">Все поля отмеченные * обязательны для заполения</div>';
	}

	return $err;
}

function save_data_for_blog($input=array(),$checker,$files,$tblname,$form) {
	// Подключаем стандартные модули
	$ci=&get_instance();

	// Получаем идентификатор одной записи
	$uri3=intval($ci->uri->segment(3));

	// Переменные для определения и вывода ошибок
	$erflag=false;
	$err='';

	// Проверяем если поля, отмеченные как важные заполнены
	foreach($checker as $val) {
		if (empty($input[$val])) {
			$erflag=true;
		}
	}

	// Форматируем полученную дату
	$date = date("Y-m-d",strtotime($input['date']));
	$input['date'] = $date;

	// Изменяем входящее значения для чекбокса
	foreach($form as $row) {
		if ($row['type']=='checkbox') {
			if (empty($input[$row['name']])) $input[$row['name']]=0; else $input[$row['name']]=1;
		}
	}

	// Получаем внесенные Тэги
	$tags 		= $input['tags'];
	// Удаляем тэги из основного массива, так как они не относятся к таблицу БЛОГА
	unset($input['tags']);

	// Делаем новую запись в блог
	if (!$erflag) {
		$data_array=$input;
		// Проверяем, если есть запрос на изменение одной записи
		if (empty($uri3)) {
			$ci->db->insert($tblname,$data_array);
			$id=$ci->db->insert_id();
		} else {
			$id=$uri3;
			$ci->db->where('ID',$id)->update($tblname,$data_array);
		}

		// Работаем с файлами
		foreach($files as $filename) {

			if(!empty($_FILES[$filename]['name'])) {
				$ci->upload->do_upload($filename);
				$resarr = $ci->upload->data();
				$file = $resarr['file_name'];

				if(strtolower($resarr['file_ext']) == '.jpg' || strtolower($resarr['file_ext']) == '.jpeg' || strtolower($resarr['file_ext']) == '.gif' || strtolower($resarr['file_ext']) == '.png')
				{
					$ci->db->where('ID',$id)->update($tblname,array($filename=>$file));
				}
			}
		}

		// Работаем с тэгами
		if (!empty($tags)) {
			$tags_array = explode(',', $tags);
			$tags_array = array_unique($tags_array);
			// Объявляем массив с идентификаторами тэгов
			$tags_id 	= array();
			// Формируем текст для SQL запроса
			$tags_to_find = "'".str_replace(",","','",$tags)."'";
			// Делаем запрос в БД для поиска тэгов, которые уже были внесены
			$tags_in_db	= $ci->db->select('id,titleRU,titleRO')->from('tags')->where("titleRU IN (".$tags_to_find.") or titleRO IN (".$tags_to_find.") or titleEN IN (".$tags_to_find.")")->get()->result_array();
			// В случае, если есть совпадения, ищем какие тэги уже есть в базе
			if (count($tags_in_db) > 0) {
				// Производим поиск значения
				foreach ($tags_in_db as $row) {
					$key = array_search($row['titleRU'], $tags_array);
					if ($key < 0) {
						$key = array_search($row['titleRO'], $tags_array);
					}
					if ($key < 0)  {
						$key = array_search($row['titleEN'], $tags_array);
					}
					// Получив ключ тэга, который есть в базе, удаляем его из массива тэгов и заносим ID в массив @tags_id для дальнейшего добавления связи в БД
					if (isset($tags_array[$key])) {
						unset($tags_array[$key]);
					}
					$tags_id[] = $row['id'];
				}
			}
			// Производим запис Тэгов, которые до этого не были добавлены в БД
			if (count($tags_array) > 0) {
				foreach ($tags_array as $value) {
					// Переводим весь текст на латиницу
					$tag = from_cyr_to_lat($value);
					// Создаем URl для записи в БД
					$tag_url = str_replace(' ', '_', $tag);
					// Чистим URL от лишних символов
					$tag_url = make_clean_url($tag_url);
					// Формируем массив для добавления в базу
					$data = array(
						'url_name'	=> $tag_url,
						'titleRU'	=> $value,
						'titleRO'	=> $tag,
						'titleEN'	=> $tag
					);
					// Добавляем в Базу
					$ci->db->insert('tags', $data);
					$tag_id = $ci->db->insert_id();
					// Добавляем в главный массив с ID*шниками тэгов
					$tags_id[]	= $tag_id;
				}
			}
			// Связываем запись в блоге с идентификаторами
			if (count($tags_id) > 0) {
				// Очищаем прошлые тэги
				$ci->db->delete("blog_tag", array('blog' => $id));
				// Записываем новые тэги
				foreach($tags_id as $value) {
					$data = array(
						'blog'	=> $id,
						'tag'	=> $value
					);
					$ci->db->insert('blog_tag', $data);
				}
			}
		}
		// В случае, если все прошло ок, возвращаем пользователя на страницу блога
		if (!empty($id)) {
			header("Location: /".ADM_CONTROLLER."/$tblname/");
			exit();
		}
	} else {
		$err.='<div style="padding:10px 0;color:#ff0000;">Все поля отмеченные * обязательны для заполения</div>';
	}
	return $err;
}

function save_data_for_broker($input=array(),$checker,$files,$tblname,$form) {
	$ci=&get_instance();
	// Получаем ID брокера
	$uri3=intval($ci->uri->segment(3));
	// Флаг
	$erflag=false;
	$err='';

	// Проверяем если обязательные поля заполнены верно
	foreach($checker as $val) {
		if (empty($input[$val])) $erflag=true;
	}

	// Работаем с чекбоксом
	foreach($form as $row) {
		if ($row['type']=='checkbox') {
			if (empty($input[$row['name']])) $input[$row['name']]=0; else $input[$row['name']]=1;
		}
	}

	// Главный скрипт
	if (!$erflag) {
		// Переменная для пароля, в дальнейшем для проверки
		$password = $input['Pass'];
		// Формируем массивы для занесения в БД
		// Формируем массив для нового пользователя
		$user_data	= array();
		$user_data['Login'] 	= $input['Login'];
		$user_data['Pass'] 		= md5($input['Pass'].$ci::AUTH_SALT);
		$user_data['UserType']  = '3';
		$user_data['Status']	= '1';
		// Формируем данные для Брокера
		unset($input['Login']);
		unset($input['Pass']);
		$data_array=$input;

		// Исполняющий скрипт
		if (empty($uri3)) {
			// Проверяем если Login не занят
			$login = $ci->db->where('Login', $user_data['Login'])->get('User')->row_array();
			if ($login != null) {
				$err .= 'Данный логин уже занят другим пользователем, просим изменить данные';
				return $err;
			}
			// Записываем нового пользователя в БД
			$ci->db->insert('User', $user_data);
			// Получаем идентификатор нового пользователя
			$user_id = $ci->db->insert_id();
			// Записываем индекатор нового пользователя в массив Брокера
			$data_array['user_id'] = $user_id;
			// Записываем нового брокера
			$ci->db->insert($tblname,$data_array);
			$id =$ci->db->insert_id();
		} else {
			// Получаем ID Брокера
			$id=$uri3;
			// Получаем информацию по Брокеру
			$broker = $ci->db->where('ID', $id)->get($tblname)->row_array();
			// Получаем ID пользователя
			$user_id = $broker['user_id'];
			// Проверяем, если есть запрос на изменение логина пользователя
			if (!empty($user_data['Login'])) {
				// Проверяем если Логин не занят
				$login = $ci->db->where('Login', $user_data['Login'])->where('ID <>', $user_id)->get('User')->row_array();
				if ($login != null) {
					$err .= 'Данный логин уже занят другим пользователем, просим изменить данные';
					return $err;
				}
				$ci->db->where('ID', $user_id)->update('User', array('Login' => $user_data['Login']));
			}
			// Проверяем, если есть запрос на изменения пароля
			if (!empty($password)) {
				$ci->db->where('ID', $user_id)->update('User', array('Pass' => $user_data['Pass']));
			}
			// Записываем изменения по брокеру
			$ci->db->where('ID',$id)->update($tblname,$data_array);
		}

		foreach($files as $filename) {

			if(!empty($_FILES[$filename]['name'])) {
				$ci->upload->do_upload($filename);
				$resarr = $ci->upload->data();
				$file = $resarr['file_name'];

				if(strtolower($resarr['file_ext']) == '.jpg' || strtolower($resarr['file_ext']) == '.jpeg' || strtolower($resarr['file_ext']) == '.gif' || strtolower($resarr['file_ext']) == '.png')
				{
					$ci->db->where('ID',$id)->update($tblname,array($filename=>$file));
				}
			}
		}

		if (!empty($id) && !empty($user_id)) {
			header("Location: /".ADM_CONTROLLER."/$tblname/");
			exit();
		}
	} else {
		$err.='<div style="padding:10px 0;color:#ff0000;">Все поля отмеченные * обязательны для заполения</div>';
	}

	return $err;
}

function standart_form_script($tblname) {
	?>
	<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		function toggleb() {
			$("#newb").toggle();
		}

		function localsort() {
			counter=1;
			data='';
			$.each($('.sorthold'),function() {
				$(this).html(counter);
				if (counter<2) breaker=''; else breaker='<>';
				data+=breaker+$(this).attr('oid')+':'+counter;
				counter++;
			});
			$.post('/<?=ADM_CONTROLLER?>/edit_table_order/',{data:data,table:'<?=$tblname?>'},function(ret) {

			});
		}
	</script>
	<?
}

function addSelect($table,$sourcename='Name',$value=0,$param='') {
	$dopcond='';
	$ci=&get_instance();
	//$q=$ci->db->order_by('Sorder ASC,ID DESC')->get($table)->result_array();
	if ($table=='TopMenu') {
      echo '<option value="0"></option>';
      $q = $ci->db->where('ParentID', 0)->order_by('Sorder ASC,ID DESC')->get($table)->result_array();
  } elseif ($table=='Category') {
		echo '<option value="0"></option>';
		if($param=='Catalog') {
			$q=$ci->db->where('ParentCod !=',0)->order_by('Sorder ASC,ID DESC')->get($table)->result_array();
		} else {
			$q=$ci->db->where('ParentCod',0)->order_by('Sorder ASC,ID DESC')->get($table)->result_array();
		}
	} elseif ($table=='Gallery') {
		echo '<option value="0"></option>';
		$q=$ci->db->where('ShowInMenu',1)->order_by('Sorder ASC,ID DESC')->get($table)->result_array();
	} elseif ($table == 'regions') {
		echo '<option value="0"></option>';
		$q=$ci->db->where('ParentID',0)->order_by('Sorder ASC,ID DESC')->get($table)->result_array();
	} elseif ($table == 'type_of_deal') {
		$q=$ci->db->order_by('ID DESC')->get($table)->result_array();
	} elseif ($table == 'sectors') {
		echo '<option value="0"></option>';
		$q=$ci->db->where('ParentID <>',0)->order_by('Sorder ASC,ID DESC')->get('regions')->result_array();
	} elseif ($table == 'ap_state') {
		echo '<option value="0"></option>';
		$q=$ci->db->order_by('nameRU ASC')->get($table)->result_array();
	} elseif ($table == 'parking_place') {
		echo '<option value="0"></option>';
		$q=$ci->db->order_by('nameRU ASC')->get($table)->result_array();
	} elseif ($table == 'brokers') {
		$q=$ci->db->order_by('nameRU ASC')->get($table)->result_array();
	} elseif ($table == 'housing_stock') {
		echo '<option value="0"></option>';
		$q=$ci->db->order_by('nameRU')->get($table)->result_array();
	} elseif ($table == 'com_property_type') {
		$q=$ci->db->order_by('nameRU ASC')->get($table)->result_array();
	} elseif ($table == 'area_type') {
		$q=$ci->db->order_by('nameRU ASC')->get($table)->result_array();
	} elseif ($table == 'land_property_type') {
		$q=$ci->db->order_by('nameRU ASC')->get($table)->result_array();
	} elseif ($table == 'number_of_rooms') {
		$q=$ci->db->order_by('ID ASC')->get($table)->result_array();
	} elseif ($table == 'object_type') {
		$q=$ci->db->order_by('ID ASC')->get($table)->result_array();
	} elseif ($table == 'when_pu') {
		$q=$ci->db->order_by('ID ASC')->get($table)->result_array();
	} elseif ($table == 'frequency_pu') {
		$q=$ci->db->order_by('ID ASC')->get($table)->result_array();
	} elseif ($table == 'position_pu') {
		$q=$ci->db->order_by('ID ASC')->get($table)->result_array();
	} elseif ($table == 'Districts') {
		$q=$ci->db->order_by('ID ASC')->get($table)->result_array();
	}
	else {
		// $q=$ci->db->order_by('Sorder ASC,ID DESC')->get($table)->result_array();
		$q=$ci->db->order_by('Sorder ASC,ID DESC')->get($table)->result_array();
	}
	if ($table == 'sectors') {
		$q1=$ci->db->where('ParentID',0)->order_by('Sorder ASC,ID DESC')->get('regions')->result_array();
		foreach ($q1 as $arr1) {
			echo '<optgroup label="'.$arr1['nameRU'].'">';
			$q=$ci->db->where('ParentID', $arr1['ID'])->order_by('Sorder ASC,ID DESC')->get('regions')->result_array();
			foreach($q as $arr) {
				if ($arr['ID']==$value) $mod=' selected'; else $mod='';
				echo '<option'.$mod.' value="'.$arr['ID'].'">'.$arr[$sourcename].'</option>';
			}
			echo '</optgroup>';
		}
	} else if ($table == 'Status') {
		foreach($q as $arr) {
			if ($arr['ID']==$value) $mod=' selected'; else $mod='';
			echo '<option'.$mod.' value="'.$arr['Value'].'">'.$arr[$sourcename].'</option>';
		}
	} else if ($table == 'Category') {
		foreach($q as $arr) {
			if ($arr['Cod']==$value) $mod=' selected'; else $mod='';
			echo '<option'.$mod.' value="'.$arr['Cod'].'">'.$arr[$sourcename].'</option>';
		}
	} else {
		foreach($q as $arr) {
			if ($arr['ID']==$value) $mod=' selected'; else $mod='';
			echo '<option'.$mod.' value="'.$arr['ID'].'">'.$arr[$sourcename].'</option>';
		}
	}

}

function addCheckbox($table,$sourcename='Name',$value=0)
{
	$ci=&get_instance();
	$valarray = json_decode($value, true);
	$q = $ci->db->order_by('Sorder ASC, ID DESC')->get($table)->result();
	foreach ($q as $arr) {
		$md = '';
		if (!empty($value) && (in_array($arr->ID, $valarray)) ) {
			if (in_array($arr->ID, $valarray)) $md=' checked'; else $md='';
		}
		echo '<div>
			<input name="checkbox_a['.$arr->ID.']" '.$md.' type="checkbox" id="'.$arr->ID.'" class="form-control" value="'.$arr->ID.'" />
			<label for="'.$arr->ID.'">'.$arr->NameRU.'</label>
		</div>';
	}
}

function addCheckbox2($table,$sourcename='Name',$value=0)
{
	$ci=&get_instance();
	$q = $ci->db->order_by('Sorder ASC, ID DESC')->get($table)->result();
	foreach ($q as $arr) {
		$md = '';

    if (!empty($value) && ($arr->ID == $value or $value == 3) ) {
      $md=' checked';
    }

		echo '<input name="checkbox_a['.$arr->ID.']" '.$md.' type="checkbox" id="'.$arr->ID.'" class="form-control" value="'.$arr->ID.'" />
					<label for="'.$arr->ID.'">'.$arr->Name.'</label>';
	}
}


function newthumbs($photo='',$dir='',$width=0,$height=0,$version=0,$zc=0, $watermark = false)
{
	require_once(realpath('public').'/imres/phpthumb.class.php');
	//echo $dir;
	$result=is_dir(realpath('public/'.$dir).'/thumbs');
	if ($result) {
		$prevdir=$dir.'/thumbs';
	} else {
		if (mkdir(realpath('public/'.$dir).'/thumbs')) {
			$prevdir=$dir.'/thumbs';
		} else {
			return 'error 1';
		}
	}

	if (!empty($version)) {
		$result=is_dir(realpath('public/'.$dir).'/thumbs/version_'.$version);
		if ($result) {
			$prevdir=$dir.'/thumbs/version_'.$version;
		} else {
			if (mkdir(realpath('public/'.$dir).'/thumbs/version_'.$version)) {
				$prevdir=$dir.'/thumbs/version_'.$version;
			} else {
				return 'error 1';
			}
		}
	}
	$va1=explode('.',$photo);
	$ext=end($va1);

	$timg=realpath('public/'.$dir).'/'.$photo;
    $catimg=realpath('public/'.$prevdir).'/'.$photo;

	if (is_file($timg) && !is_file($catimg)) {
		$opath1=realpath('public/'.$dir).'/';
		$opath2=realpath('public/'.$prevdir).'/';
		$dest=$opath2.$photo;
		$source=$opath1.$photo;
		$phpThumb = new phpThumb();
		$phpThumb->setSourceFilename($source);
		if (!empty($width)) $phpThumb->setParameter('w', $width);
		if (!empty($height)) $phpThumb->setParameter('h', $height);
		if (!empty($height)) $phpThumb->setParameter('q', 90);
		if ($ext=='png') $phpThumb->setParameter('f', 'png');
		if (!empty($zc)) {
			$phpThumb->setParameter('zc', '1');
		}
		if ($watermark) {
			$phpThumb->setParameter('fltr', 'wmi|'.realpath('public/i/remark_watermark.png').'|C|50');
		}
		$phpThumb->setParameter('q', 90);
		if ($phpThumb->GenerateThumbnail())
		{
			if ($phpThumb->RenderToFile($dest))
			{
				$img='/public/'.$prevdir.'/'.$photo;
			} else {
				return 'error 3';
			}
		}

	} elseif(is_file($catimg)) {
		$img='/public/'.$prevdir.'/'.$photo;
	} else {
		return 'error 2';
	}

	return $img;
}

function init_load_img($dir) {
	if (!is_dir(realpath('public').'/'.$dir)) {
		mkdir(realpath('public').'/'.$dir);
	}
	$ci=&get_instance();
	$config['upload_path'] = realpath("public").'/'.$dir;
	$config['allowed_types'] = 'jpg|jpeg|gif|png';
	$config['encrypt_name'] = TRUE;
	$ci->load->library('upload', $config);
}

function clear($str,$type='0') {
	$str = trim($str);
	if ($type=='email') {
		if (filter_var($str, FILTER_VALIDATE_EMAIL) === false) {
		  	$str = "";
		}
	} else if ($type==1 or $type=='int') {
		$str = intval($str);
	} else if ($type==2 or $type=='float') {
		$str = str_replace(",",".",$str);
		$str = floatval($str);
	} else if ($type==3 or $type=='regx') {
		$str = preg_replace("/[^a-zA-ZА-Яа-я0-9.,!\s]/", "",$str);
	} else if ($type=='uri') {
		$str = preg_replace("/[^a-zA-Z0-9-_\s]/", "",$str);
	} else if ($type==4 or $type=='text') {
		$str = str_replace("'","&#8242;",$str);
		$str = str_replace("\"","&#34;",$str);
		$str = htmlspecialchars($str);
	} else {
		$str = strip_tags($str);
		$str = str_replace("\n"," ",$str);
		$str = str_replace("'","&#8242;",$str);
		$str = str_replace("\"","&#34;",$str);
		$str = preg_replace('!\s+!', ' ', $str);
		$str = stripslashes($str);
		$str = htmlspecialchars($str);
	}
	return $str;
}

// Функция для изменения текста, кирилические символы на латинские
function from_cyr_to_lat($textcyr) {
	$cyr = [
		'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
		'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
		'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
		'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
	];
	$lat = [
		'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
		'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
		'A','B','V','G','D','E','Io','Zh','Z','I','Y','K','L','M','N','O','P',
		'R','S','T','U','F','H','Ts','Ch','Sh','Sht','A','I','Y','e','Yu','Ya'
	];
	return str_replace($cyr, $lat, $textcyr);
}

function transliteration($str)
{
    $tr = array(
        "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
        "Д"=>"D","Е"=>"E","Ё"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
        "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",'ă'=>'a','â'=>'a','Â'=>'A','Ă'=>'A','ţ'=>'t','Ţ'=>'T','ş'=>'s','Ş'=>'S',
        "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
        "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH","î"=>"i","Î"=>"I",","=>"","№"=>"","ț"=>"t","Ț"=>"T","ș"=>"s","Ș"=>"S",
        "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
        "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y","?"=>"",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya"," "=>"_"," ("=>"",")"=>"",
        "."=>"","\\"=>"","/"=>"-","'"=>"","»"=>"","«"=>"","&quot;"=>"","'"=>"","\""=>"","&"=>"i","%"=>""
    );
    return strtolower(strtr($str,$tr));
}

// Функция для получения строки URL из текста
function make_clean_url($string) {
	$symbols = array('&','#',';','%','?',':','(',')','-','=','+','[',']',',','.','/','\\', '!');
	return str_replace($symbols, '', $string);
}

// Функция для сохранения данных по апартаментам
function save_data_for_apartments($input=array(),$checker,$files,$tblname,$form,$multifiles) {
	// Подключаем стандартные модули
	$ci=&get_instance();

	// Получаем идентификатор одной записи
	$uri3=intval($ci->uri->segment(3));

	// Переменные для определения и вывода ошибок
	$erflag=false;
	$err='';

	// Проверяем если поля, отмеченные как важные заполнены
	foreach($checker as $val) {
		if (empty($input[$val])) {
			$erflag=true;
		}
	}

	// Делаем новую запись в объекты и апартаменты
	if (!$erflag) {
			if (empty($uri3)) {
			// Формируем URI объекта
			$region 		= $ci->db->where('ID', $input['region_id'])->get('regions')->row_array();
			$sector 		= $ci->db->where('ID', $input['sector_id'])->get('regions')->row_array();
			$ru_uri_data 	= array();
			$ro_uri_data 	= array();
			$en_uri_data 	= array();
			if ($input['nameRU']) $ru_uri_data[] = trim($input['nameRU']);
			if ($input['nameRO']) $ro_uri_data[] = trim($input['nameRO']);
			if ($input['nameEN']) $en_uri_data[] = trim($input['nameEN']);
			if ($sector) {
				$ru_uri_data[] = $sector['nameRU'];
				$ro_uri_data[] = $sector['nameRO'];
				$en_uri_data[] = $sector['nameEN'];
			}
			if ($region) {
				$ru_uri_data[] = $region['nameRU'];
				$ro_uri_data[] = $region['nameRO'];
				$en_uri_data[] = $region['nameEN'];
			}
			if ($input['object_code']) {
				$ru_uri_data[] = $input['object_code'];
				$ro_uri_data[] = $input['object_code'];
				$en_uri_data[] = $input['object_code'];
			}
			$uriRU	= implode('_',$ru_uri_data);
			$uriRO 	= implode('_',$ro_uri_data);
			$uriEN 	= implode('_',$en_uri_data);
		} else {
			$uriRU = trim($input['uriRU']);
			$uriRO = trim($input['uriRO']);
			$uriEN = trim($input['uriEN']);
		}
		$uriRU 	= make_clean_url(transliteration($uriRU));
		$uriRO 	= make_clean_url(transliteration($uriRO));
		$uriEN 	= make_clean_url(transliteration($uriEN));
		// Формируем массивы данных
		$data_object = array(
			'uriRU'				=> $uriRU,
			'uriRO'				=> $uriRO,
			'uriEN'				=> $uriEN,
			'object_type_id'	=> $input['object_type_id'],
			'nameRU'			=> $input['nameRU'],
			'nameRO'			=> $input['nameRO'],
			'nameEN'			=> $input['nameEN'],
			'area'				=> $input['area'],
			'region_id'			=> $input['region_id'],
			'sector_id'			=> $input['sector_id'],
			'type_of_deal'		=> $input['type_of_deal'],
			'object_code'		=> $input['object_code'],
			'price'				=> $input['price'],
			'descriptionRU'		=> $input['descriptionRU'],
			'descriptionRO'		=> $input['descriptionRO'],
			'descriptionEN'		=> $input['descriptionEN'],
			'broker_id'			=> $input['broker_id']
		);
		$data_apartments = array(
			'number_of_rooms'		=> $input['number_of_rooms'],
			'ap_state_id'			=> $input['ap_state_id'],
			'housing_stock'			=> $input['housing_stock'],
			'floor'					=> $input['floor'],
			'total_floors'			=> $input['total_floors'],
			'number_of_balconies'	=> $input['number_of_balconies'],
			'number_of_bathrooms'	=> $input['number_of_bathrooms'],
			'parking_place_id'		=> $input['parking_place_id']
		);
		$data_advantages = $input['advantages'];
		// Удаляем массив input
		unset($input);
		// Проверяем, если есть запрос на изменение одной записи
		if (empty($uri3)) {
			// Записываем новый объект в БД
			$ci->db->insert('objects',$data_object);
			// Получаем идентификатор объекта
			$id=$ci->db->insert_id();
			// Вносим идентификатор объекта в массив данных аппартаментов
			$data_apartments['object_id'] = $id;
			// Записываем новые аппартаменты в БД
			$ci->db->insert($tblname, $data_apartments);
			// Получаем ключ аппартаментов
			$ap_id=$ci->db->insert_id();
		} else {
			// Получае ID объекта
			$id=$uri3;
			// Вносим изменения в Объекты
			$ci->db->where('ID',$id)->update('objects',$data_object);
			// Вносим изменения в аппартаменты
			$ci->db->where('object_id', $id)->update($tblname, $data_apartments);
		}
		// Работаем с преимуществами
		// Удаляем все сочетания преимуществ и объектов
		$ci->db->where('object', $id)->delete('object_advantage');
		// Записываем новые сочетания в базу
		foreach ($data_advantages as $advantage) {
			$ci->db->insert('object_advantage', array('object' => $id, 'advantage' => $advantage));
		}
		// Работаем с файлами
		foreach($files as $filename) {

			if(!empty($_FILES[$filename]['name'])) {
				$ci->upload->do_upload($filename);
				$resarr = $ci->upload->data();
				$file = $resarr['file_name'];

				if(strtolower($resarr['file_ext']) == '.jpg' || strtolower($resarr['file_ext']) == '.jpeg' || strtolower($resarr['file_ext']) == '.gif' || strtolower($resarr['file_ext']) == '.png')
				{
					$ci->db->where('ID',$id)->update('objects',array($filename=>$file));
				}
			}
		}
		// Работаем с галлереей
		if (isset($_SESSION['uploadedFiles'])) {
			foreach($_SESSION['uploadedFiles'] as $file) {
				$oldname = BASEPATH.'../public/upload/'.$file;
				$newname = BASEPATH.'../public/apartments/'.$file;
				rename($oldname, $newname);
				$ci->db->insert('gallery', array('object_id'=>$id, 'photo'=>$file));
			}
			unset($_SESSION['uploadedFiles']);
		}

		// В случае, если все прошло ок, возвращаем пользователя на страницу блога
		if (!empty($id)) {
			header("Location: /".ADM_CONTROLLER."/$tblname/");
			exit();
		}
	} else {
		$err.='<div style="padding:10px 0;color:#ff0000;">Все поля отмеченные * обязательны для заполения</div>';
	}
	return $err;
}

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary['photo'.$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

function save_data_for_houses($input=array(),$checker,$files,$tblname,$form,$multifiles) {
	// Подключаем стандартные модули
	$ci=&get_instance();

	// Получаем идентификатор одной записи
	$uri3=intval($ci->uri->segment(3));

	// Переменные для определения и вывода ошибок
	$erflag=false;
	$err='';

	// Проверяем если поля, отмеченные как важные заполнены
	foreach($checker as $val) {
		if (empty($input[$val])) {
			$erflag=true;
		}
	}

	// Делаем новую запись в объекты и дома
	if (!$erflag) {
			if (empty($uri3)) {
			// Формируем URI объекта
			$region 		= $ci->db->where('ID', $input['region_id'])->get('regions')->row_array();
			$sector 		= $ci->db->where('ID', $input['sector_id'])->get('regions')->row_array();
			$ru_uri_data 	= array();
			$ro_uri_data 	= array();
			$en_uri_data 	= array();
			if ($input['nameRU']) $ru_uri_data[] = trim($input['nameRU']);
			if ($input['nameRO']) $ro_uri_data[] = trim($input['nameRO']);
			if ($input['nameEN']) $en_uri_data[] = trim($input['nameEN']);
			if ($sector) {
				$ru_uri_data[] = $sector['nameRU'];
				$ro_uri_data[] = $sector['nameRO'];
				$en_uri_data[] = $sector['nameEN'];
			}
			if ($region) {
				$ru_uri_data[] = $region['nameRU'];
				$ro_uri_data[] = $region['nameRO'];
				$en_uri_data[] = $region['nameEN'];
			}
			if ($input['object_code']) {
				$ru_uri_data[] = $input['object_code'];
				$ro_uri_data[] = $input['object_code'];
				$en_uri_data[] = $input['object_code'];
			}
			$uriRU	= implode('_',$ru_uri_data);
			$uriRO 	= implode('_',$ro_uri_data);
			$uriEN 	= implode('_',$en_uri_data);
		} else {
			$uriRU = trim($input['uriRU']);
			$uriRO = trim($input['uriRO']);
			$uriEN = trim($input['uriEN']);
		}
		$uriRU 	= make_clean_url(transliteration($uriRU));
		$uriRO 	= make_clean_url(transliteration($uriRO));
		$uriEN 	= make_clean_url(transliteration($uriEN));
		// Формируем массивы данных
		$data_object = array(
			'uriRU'				=> $uriRU,
			'uriRO'				=> $uriRO,
			'uriEN'				=> $uriEN,
			'object_type_id'	=> $input['object_type_id'],
			'nameRU'			=> $input['nameRU'],
			'nameRO'			=> $input['nameRO'],
			'nameEN'			=> $input['nameEN'],
			'area'				=> $input['area'],
			'region_id'			=> $input['region_id'],
			'sector_id'			=> $input['sector_id'],
			'type_of_deal'		=> $input['type_of_deal'],
			'object_code'		=> $input['object_code'],
			'price'				=> $input['price'],
			'descriptionRU'		=> $input['descriptionRU'],
			'descriptionRO'		=> $input['descriptionRO'],
			'descriptionEN'		=> $input['descriptionEN'],
			'broker_id'			=> $input['broker_id']
		);
		$data_houses = array(
			'land_area_hundred'		=> $input['land_area_hundred'],
			'total_floors'			=> $input['total_floors'],
			'number_of_balconies'	=> $input['number_of_balconies'],
			'number_of_bathrooms'	=> $input['number_of_bathrooms'],
			'parking_place_id'		=> $input['parking_place_id']
		);
		$data_advantages = $input['advantages'];
		// Удаляем массив input
		unset($input);
		// Проверяем, если есть запрос на изменение одной записи
		if (empty($uri3)) {
			// Записываем новый объект в БД
			$ci->db->insert('objects',$data_object);
			// Получаем идентификатор объекта
			$id=$ci->db->insert_id();
			// Вносим идентификатор объекта в массив данных аппартаментов
			$data_houses['object_id'] = $id;
			// Записываем новые аппартаменты в БД
			$ci->db->insert($tblname, $data_houses);
			// Получаем ключ аппартаментов
			$ap_id=$ci->db->insert_id();
		} else {
			// Получае ID объекта
			$id=$uri3;
			// Вносим изменения в Объекты
			$ci->db->where('ID',$id)->update('objects',$data_object);
			// Вносим изменения в аппартаменты
			$ci->db->where('object_id', $id)->update($tblname, $data_houses);
		}
		// Работаем с преимуществами
		// Удаляем все сочетания преимуществ и объектов
		$ci->db->where('object', $id)->delete('object_advantage');
		// Записываем новые сочетания в базу
		foreach ($data_advantages as $advantage) {
			$ci->db->insert('object_advantage', array('object' => $id, 'advantage' => $advantage));
		}
		// Работаем с файлами
		foreach($files as $filename) {

			if(!empty($_FILES[$filename]['name'])) {
				$ci->upload->do_upload($filename);
				$resarr = $ci->upload->data();
				$file = $resarr['file_name'];

				if(strtolower($resarr['file_ext']) == '.jpg' || strtolower($resarr['file_ext']) == '.jpeg' || strtolower($resarr['file_ext']) == '.gif' || strtolower($resarr['file_ext']) == '.png')
				{
					$ci->db->where('ID',$id)->update('objects',array($filename=>$file));
				}
			}
		}
		// Работаем с галлереей
		if (isset($_SESSION['uploadedFiles'])) {
			foreach($_SESSION['uploadedFiles'] as $file) {
				$oldname = BASEPATH.'../public/upload/'.$file;
				$newname = BASEPATH.'../public/houses/'.$file;
				rename($oldname, $newname);
				$ci->db->insert('gallery', array('object_id'=>$id, 'photo'=>$file));
			}
			unset($_SESSION['uploadedFiles']);
		}

		// В случае, если все прошло ок, возвращаем пользователя на страницу блога
		if (!empty($id)) {
			header("Location: /".ADM_CONTROLLER."/$tblname/");
			exit();
		}
	} else {
		$err.='<div style="padding:10px 0;color:#ff0000;">Все поля отмеченные * обязательны для заполения</div>';
	}
	return $err;
}

function save_data_for_commercial_properties($input=array(),$checker,$files,$tblname,$form,$multifiles) {
	// Подключаем стандартные модули
	$ci=&get_instance();

	// Получаем идентификатор одной записи
	$uri3=intval($ci->uri->segment(3));

	// Переменные для определения и вывода ошибок
	$erflag=false;
	$err='';

	// Проверяем если поля, отмеченные как важные заполнены
	foreach($checker as $val) {
		if (empty($input[$val])) {
			$erflag=true;
		}
	}

	// Делаем новую запись в объекты и коммерческой недвижимости
	if (!$erflag) {
			if (empty($uri3)) {
			// Формируем URI объекта
			$region 		= $ci->db->where('ID', $input['region_id'])->get('regions')->row_array();
			$sector 		= $ci->db->where('ID', $input['sector_id'])->get('regions')->row_array();
			$ru_uri_data 	= array();
			$ro_uri_data 	= array();
			$en_uri_data 	= array();
			if ($input['nameRU']) $ru_uri_data[] = trim($input['nameRU']);
			if ($input['nameRO']) $ro_uri_data[] = trim($input['nameRO']);
			if ($input['nameEN']) $en_uri_data[] = trim($input['nameEN']);
			if ($sector) {
				$ru_uri_data[] = $sector['nameRU'];
				$ro_uri_data[] = $sector['nameRO'];
				$en_uri_data[] = $sector['nameEN'];
			}
			if ($region) {
				$ru_uri_data[] = $region['nameRU'];
				$ro_uri_data[] = $region['nameRO'];
				$en_uri_data[] = $region['nameEN'];
			}
			if ($input['object_code']) {
				$ru_uri_data[] = $input['object_code'];
				$ro_uri_data[] = $input['object_code'];
				$en_uri_data[] = $input['object_code'];
			}
			$uriRU	= implode('_',$ru_uri_data);
			$uriRO 	= implode('_',$ro_uri_data);
			$uriEN 	= implode('_',$en_uri_data);
		} else {
			$uriRU = trim($input['uriRU']);
			$uriRO = trim($input['uriRO']);
			$uriEN = trim($input['uriEN']);
		}
		$uriRU 	= make_clean_url(transliteration($uriRU));
		$uriRO 	= make_clean_url(transliteration($uriRO));
		$uriEN 	= make_clean_url(transliteration($uriEN));
		// Формируем массивы данных
		$data_object = array(
			'uriRU'				=> $uriRU,
			'uriRO'				=> $uriRO,
			'uriEN'				=> $uriEN,
			'object_type_id'	=> $input['object_type_id'],
			'nameRU'			=> $input['nameRU'],
			'nameRO'			=> $input['nameRO'],
			'nameEN'			=> $input['nameEN'],
			'area'				=> $input['area'],
			'region_id'			=> $input['region_id'],
			'sector_id'			=> $input['sector_id'],
			'type_of_deal'		=> $input['type_of_deal'],
			'object_code'		=> $input['object_code'],
			'price'				=> $input['price'],
			'descriptionRU'		=> $input['descriptionRU'],
			'descriptionRO'		=> $input['descriptionRO'],
			'descriptionEN'		=> $input['descriptionEN'],
			'broker_id'			=> $input['broker_id']
		);
		$data_commercial_properties = array(
			'property_type_id'		=> $input['property_type_id'],
			'floor'					=> $input['floor'],
			'number_of_bathrooms'	=> $input['number_of_bathrooms'],
			'parking_place_id'		=> $input['parking_place_id']
		);
		$data_advantages = $input['advantages'];
		// Удаляем массив input
		unset($input);
		// Проверяем, если есть запрос на изменение одной записи
		if (empty($uri3)) {
			// Записываем новый объект в БД
			$ci->db->insert('objects',$data_object);
			// Получаем идентификатор объекта
			$id=$ci->db->insert_id();
			// Вносим идентификатор объекта в массив данных аппартаментов
			$data_commercial_properties['object_id'] = $id;
			// Записываем новые аппартаменты в БД
			$ci->db->insert($tblname, $data_commercial_properties);
			// Получаем ключ аппартаментов
			$ap_id=$ci->db->insert_id();
		} else {
			// Получае ID объекта
			$id=$uri3;
			// Вносим изменения в Объекты
			$ci->db->where('ID',$id)->update('objects',$data_object);
			// Вносим изменения в аппартаменты
			$ci->db->where('object_id', $id)->update($tblname, $data_commercial_properties);
		}
		// Работаем с преимуществами
		// Удаляем все сочетания преимуществ и объектов
		$ci->db->where('object', $id)->delete('object_advantage');
		// Записываем новые сочетания в базу
		foreach ($data_advantages as $advantage) {
			$ci->db->insert('object_advantage', array('object' => $id, 'advantage' => $advantage));
		}
		// Работаем с файлами
		foreach($files as $filename) {

			if(!empty($_FILES[$filename]['name'])) {
				$ci->upload->do_upload($filename);
				$resarr = $ci->upload->data();
				$file = $resarr['file_name'];

				if(strtolower($resarr['file_ext']) == '.jpg' || strtolower($resarr['file_ext']) == '.jpeg' || strtolower($resarr['file_ext']) == '.gif' || strtolower($resarr['file_ext']) == '.png')
				{
					$ci->db->where('ID',$id)->update('objects',array($filename=>$file));
				}
			}
		}
		// Работаем с галлереей
		if (isset($_SESSION['uploadedFiles'])) {
			foreach($_SESSION['uploadedFiles'] as $file) {
				$oldname = BASEPATH.'../public/upload/'.$file;
				$newname = BASEPATH.'../public/commercial_properties/'.$file;
				rename($oldname, $newname);
				$ci->db->insert('gallery', array('object_id'=>$id, 'photo'=>$file));
			}
			unset($_SESSION['uploadedFiles']);
		}

		// В случае, если все прошло ок, возвращаем пользователя на страницу блога
		if (!empty($id)) {
			header("Location: /".ADM_CONTROLLER."/$tblname/");
			exit();
		}
	} else {
		$err.='<div style="padding:10px 0;color:#ff0000;">Все поля отмеченные * обязательны для заполения</div>';
	}
	return $err;
}

function save_data_for_lands($input=array(),$checker,$files,$tblname,$form,$multifiles) {
	// Подключаем стандартные модули
	$ci=&get_instance();

	// Получаем идентификатор одной записи
	$uri3=intval($ci->uri->segment(3));

	// Переменные для определения и вывода ошибок
	$erflag=false;
	$err='';

	// Проверяем если поля, отмеченные как важные заполнены
	foreach($checker as $val) {
		if (empty($input[$val])) {
			$erflag=true;
		}
	}

	// Делаем новую запись в объекты и апартаменты
	if (!$erflag) {
			if (empty($uri3)) {
			// Формируем URI объекта
			$region 		= $ci->db->where('ID', $input['region_id'])->get('regions')->row_array();
			$sector 		= $ci->db->where('ID', $input['sector_id'])->get('regions')->row_array();
			$ru_uri_data 	= array();
			$ro_uri_data 	= array();
			$en_uri_data 	= array();
			if ($input['nameRU']) $ru_uri_data[] = trim($input['nameRU']);
			if ($input['nameRO']) $ro_uri_data[] = trim($input['nameRO']);
			if ($input['nameEN']) $en_uri_data[] = trim($input['nameEN']);
			if ($sector) {
				$ru_uri_data[] = $sector['nameRU'];
				$ro_uri_data[] = $sector['nameRO'];
				$en_uri_data[] = $sector['nameEN'];
			}
			if ($region) {
				$ru_uri_data[] = $region['nameRU'];
				$ro_uri_data[] = $region['nameRO'];
				$en_uri_data[] = $region['nameEN'];
			}
			if ($input['object_code']) {
				$ru_uri_data[] = $input['object_code'];
				$ro_uri_data[] = $input['object_code'];
				$en_uri_data[] = $input['object_code'];
			}
			$uriRU	= implode('_',$ru_uri_data);
			$uriRO 	= implode('_',$ro_uri_data);
			$uriEN 	= implode('_',$en_uri_data);
		} else {
			$uriRU = trim($input['uriRU']);
			$uriRO = trim($input['uriRO']);
			$uriEN = trim($input['uriEN']);
		}
		$uriRU 	= make_clean_url(transliteration($uriRU));
		$uriRO 	= make_clean_url(transliteration($uriRO));
		$uriEN 	= make_clean_url(transliteration($uriEN));
		// Формируем массивы данных
		$data_object = array(
			'uriRU'				=> $uriRU,
			'uriRO'				=> $uriRO,
			'uriEN'				=> $uriEN,
			'object_type_id'	=> $input['object_type_id'],
			'nameRU'			=> $input['nameRU'],
			'nameRO'			=> $input['nameRO'],
			'nameEN'			=> $input['nameEN'],
			'area'				=> $input['area'],
			'region_id'			=> $input['region_id'],
			'sector_id'			=> $input['sector_id'],
			'type_of_deal'		=> 2,
			'object_code'		=> $input['object_code'],
			'price'				=> $input['price'],
			'descriptionRU'		=> $input['descriptionRU'],
			'descriptionRO'		=> $input['descriptionRO'],
			'descriptionEN'		=> $input['descriptionEN'],
			'broker_id'			=> $input['broker_id']
		);
		$data_lands = array(
			'land_property_type_id'		=> $input['land_property_type_id'],
			'area_type_id'				=> $input['area_type_id']
		);
		$data_advantages = $input['advantages'];
		// Удаляем массив input
		unset($input);
		// Проверяем, если есть запрос на изменение одной записи
		if (empty($uri3)) {
			// Записываем новый объект в БД
			$ci->db->insert('objects',$data_object);
			// Получаем идентификатор объекта
			$id=$ci->db->insert_id();
			// Вносим идентификатор объекта в массив данных аппартаментов
			$data_lands['object_id'] = $id;
			// Записываем новые аппартаменты в БД
			$ci->db->insert($tblname, $data_lands);
			// Получаем ключ аппартаментов
			$ap_id=$ci->db->insert_id();
		} else {
			// Получае ID объекта
			$id=$uri3;
			// Вносим изменения в Объекты
			$ci->db->where('ID',$id)->update('objects',$data_object);
			// Вносим изменения в аппартаменты
			$ci->db->where('object_id', $id)->update($tblname, $data_lands);
		}
		// Работаем с преимуществами
		// Удаляем все сочетания преимуществ и объектов
		$ci->db->where('object', $id)->delete('object_advantage');
		// Записываем новые сочетания в базу
		foreach ($data_advantages as $advantage) {
			$ci->db->insert('object_advantage', array('object' => $id, 'advantage' => $advantage));
		}
		// Работаем с файлами
		foreach($files as $filename) {

			if(!empty($_FILES[$filename]['name'])) {
				$ci->upload->do_upload($filename);
				$resarr = $ci->upload->data();
				$file = $resarr['file_name'];

				if(strtolower($resarr['file_ext']) == '.jpg' || strtolower($resarr['file_ext']) == '.jpeg' || strtolower($resarr['file_ext']) == '.gif' || strtolower($resarr['file_ext']) == '.png')
				{
					$ci->db->where('ID',$id)->update('objects',array($filename=>$file));
				}
			}
		}
		// Работаем с галлереей
		if (isset($_SESSION['uploadedFiles'])) {
			foreach($_SESSION['uploadedFiles'] as $file) {
				$oldname = BASEPATH.'../public/upload/'.$file;
				$newname = BASEPATH.'../public/lands/'.$file;
				rename($oldname, $newname);
				$ci->db->insert('gallery', array('object_id'=>$id, 'photo'=>$file));
			}
			unset($_SESSION['uploadedFiles']);
		}
		// В случае, если все прошло ок, возвращаем пользователя на страницу блога
		if (!empty($id)) {
			header("Location: /".ADM_CONTROLLER."/$tblname/");
			exit();
		}
	} else {
		$err.='<div style="padding:10px 0;color:#ff0000;">Все поля отмеченные * обязательны для заполения</div>';
	}
	return $err;
}
// Редидектим со страниц, которые не относятся к брокерам
function not_brokers_page()
{
	if($_SESSION['user_type'] == 3) {
		header("Location: /cp/apartments/");
		exit();
	}
}

function check_if_can_edit()
{
	if($_SESSION['edit'] != 'true') {
		header("Location: /cp/apartments_b/");
		exit();
	};
}

// Функция для получения данных по странице
function get_data_for_page($uri) {
	// Подключаем стандартные модули
	$ci=&get_instance();
	// Объявляем переменную страницы
	$page = $uri;
	// Опредеяем страницу
	if (!$page) {
		$page = '';
	}
	if ($page == 'index') {
		$page = '';
	}
	// Получаем язык
	$clang = strtoupper($_SESSION['lang']);
	// Делаем запрос в БД для получения данных по странице
	$ci->db->select("
			Title$clang as title,
			Text$clang as text,
			OptimizationTitle$clang as optimizationTitle,
			Keywords$clang as keywords,
			Description$clang as description,
			Image as image
	")->where('UriName'.$clang, $page);
	$q = $ci->db->get('TopMenu')->row();
	// Формируем массив данных по странице
	$data = array(
		'page_title'					=> $q->title,
		'keywords_for_layout'			=> $q->keywords,
		'description_for_layout'		=> $q->description,
		'text_for_layout'				=> $q->text,
		'optimization_title_for_layout' => $q->optimizationTitle,
		'image_for_layout'				=> $q->image
	);
	// Возвращаем полученные данные
	return $data;
}

// Функция суперАдмина
function superadmin_access() {
    if ($_SESSION['user_type'] != 4) {
        header("Location: /cp");
        exit();
    }
}

// Функция для дампинга
function dump($value='', $die=true) {
	echo '<pre>';
		echo '<br>';
		echo '<br>';
		echo '<br>';
		echo '<br>';
		echo '<br>';
		echo '<br>';
		echo '<br>';
		echo '<br>';
		echo '<br>';
		var_dump($value);
	echo '</pre>';
	if($die){
		die();
	}
}

/* Функция для Footer, отрисовываем хардкодом */
function footer_menu_go()
{
	// Первичные переменные
	$lang = $_SESSION['lang'];
	$footer_array = array();
	$ru_version = [
		FORSALE 	=> [
			'<li class="footer_list_item"><a class="footer_link" href="/ru/prodazha_kvartiry">Квартиры</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ru/prodazha_doma">Дома</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ru/prodazha_kommercheskaja_nedvizhimosti?flt=true&tods=1&cpt[]=2&so=0&obd=1">Офисы</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ru/prodazha_kommercheskaja_nedvizhimosti?flt=true&tods=1&cpt[]=1&so=0&obd=1">Коммерческого назначения</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ru/prodazha_kommercheskaja_nedvizhimosti?flt=true&tods=1&cpt[]=3&so=0&obd=1">Промышленного назначения</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ru/prodazha_zemelinye_uchastki">Земельные участки</a></li>'
		],
		FORRENT	=> [
			'<li class="footer_list_item"><a class="footer_link" href="/ru/arenda_kvartiry">Квартиры</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ru/arenda_doma">Дома</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ru/arenda_kommercheskaja_nedvizhimosti/?flt=true&toda=1&cpt[]=2&so=0&obd=1">Офисы</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ru/arenda_kommercheskaja_nedvizhimosti/?flt=true&toda=1&cpt[]=1&so=0&obd=1">Коммерческого назначения</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ru/arenda_kommercheskaja_nedvizhimosti/?flt=true&toda=1&cpt[]=3&so=0&obd=1">Промышленного назначения</a></li>'
		],
		COMPANY	=> [
			'<li class="footer_list_item"><a class="footer_link" href="/ru/o_nas">О нас</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ru/blog">Блог</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="javascript:void(0)">Вакансии</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ru/kontakty">Контакты</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ru/karta_sajta">Карта Сайта</a></li>'
		]
	];
	$ro_version = [
		FORSALE 	=> [
			'<li class="footer_list_item"><a class="footer_link" href="/ro/vanzare_apartamente">Apartamente</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ro/vanzare_case">Case</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ro/vanzare_proprietate_comerciala/?flt=true&tods=1&cpt[]=2&so=0&obd=1">Oficii</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ro/vanzare_proprietate_comerciala/?flt=true&tods=1&cpt[]=1&so=0&obd=1">Spații comerciale</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ro/vanzare_proprietate_comerciala/?flt=true&tods=1&cpt[]=3&so=0&obd=1">Spații industriale</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ro/vanzare_terenuri">Terenuri</a></li>'
		],
		FORRENT	=> [
			'<li class="footer_list_item"><a class="footer_link" href="/ro/chirie_apartamente">Apartamente</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ro/vanzare_case">Case</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ro/chirie_proprietate_comerciala/?flt=true&toda=1&cpt[]=2&so=0&obd=1">Oficii</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ro/chirie_proprietate_comerciala/?flt=true&toda=1&cpt[]=1&so=0&obd=1">Spații comerciale</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ro/chirie_proprietate_comerciala/?flt=true&toda=1&cpt[]=3&so=0&obd=1">Spații industriale</a></li>'
		],
		COMPANY	=> [
			'<li class="footer_list_item"><a class="footer_link" href="/ro/despre_noi">Despre Noi</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ro/blog">Blog</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="javascript:void(0)">Posturi vacante</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ro/contacte">Contacte</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/ro/harta_site-ului">Harta site-ului</a></li>'
		]
	];
	$en_version = [
		FORSALE 		=> [
			'<li class="footer_list_item"><a class="footer_link" href="/en/sale_apartments">Apartments</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/en/sale_houses">Houses</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/en/sale_commercial_properties/?flt=true&tods=1&cpt[]=2&so=0&obd=1">Offices</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/en/sale_commercial_properties/?flt=true&tods=1&cpt[]=1&so=0&obd=1">For commercial purposes</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/en/sale_commercial_properties/?flt=true&tods=1&cpt[]=3&so=0&obd=1">Industrial purpose</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/en/sale_lands">Land</a></li>'
		],
		FORRENT		=> [
			'<li class="footer_list_item"><a class="footer_link" href="/en/rent_apartments">Apartments</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/en/rent_houses">Houses</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/en/sale_commercial_properties/?flt=true&toda=1&cpt[]=2&so=0&obd=1">Offices</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/en/sale_commercial_properties/?flt=true&toda=1&cpt[]=1&so=0&obd=1">For commercial purposes</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/en/sale_commercial_properties/?flt=true&toda=1&cpt[]=3&so=0&obd=1">Industrial purpose</a></li>'
		],
		COMPANY	=> [
			'<li class="footer_list_item"><a class="footer_link" href="/en/about_us">About Us</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/en/blog">Blog</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="javascript:void(0)">Job Vacancies</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/en/contacts">Contacts</a></li>',
			'<li class="footer_list_item"><a class="footer_link" href="/en/sitemap">Sitemap</a></li>'
		]
	];
	// Через свитч получаем данные для footer_array
	switch ($lang) {
		case 'ru': $footer_array = $ru_version; break;
		case 'ro': $footer_array = $ro_version; break;
		case 'en': $footer_array = $en_version; break;
		default:   $footer_array = $ro_version; break;
	}
	// Прорисовываем футер
	foreach ($footer_array as $title => $links) {
		echo '<div class="footer_item">';
			echo '<div class="footer_title">'.$title.'</div>';
			echo '<ul class="footer_list">';
			foreach ($links as $link) {
				echo $link;
			}
			echo '</ul>';
		echo "</div>";
	}
}

// Функция для формирование ссылок при переключении языка
function create_lang_links()
{
	// Переменные функции
	$ci					= &get_instance();
	$lclang 			= strtolower($_SESSION['lang']);
	$clang 				= strtoupper($lclang);
	$actual_link 		= $_SERVER['REQUEST_URI'];
	$available_langs 	= array('ru', 'ro', 'en');
	$flags				= array('ru' => '/public/i/flag2.png', 'ro' => '/public/i/flag1.png', 'en' => '/public/i/flag3.png');
	$links 				= array();
	$dop_links          = [
		'ru/arenda_kommercheskaja_nedvizhimosti' => [
			'ro' => 'ro/chirie_proprietate_comerciala',
			'en' => 'en/rent_commercial_properties'
		],
		'ro/chirie_proprietate_comerciala' => [
			'ru' => 'ru/arenda_kommercheskaja_nedvizhimosti',
			'en' => 'en/rent_commercial_properties'
		],
		'en/rent_commercial_properties' => [
			'ru' => 'ru/arenda_kommercheskaja_nedvizhimosti',
			'ro' => 'ro/chirie_proprietate_comerciala'
		],
		'ru/prodazha_kommercheskaja_nedvizhimosti' => [
			'ro' => 'ro/vanzare_proprietate_comerciala',
			'en' => 'en/sale_commercial_properties'
		],
		'ro/vanzare_proprietate_comerciala' => [
			'ru' => 'ru/prodazha_kommercheskaja_nedvizhimosti',
			'en' => 'en/sale_commercial_properties'
		],
		'en/sale_commercial_properties' => [
			'ru' => 'ru/prodazha_kommercheskaja_nedvizhimosti',
			'ro' => 'ro/vanzare_proprietate_comerciala',
		],
		'ru/arenda_zemelinye_uchastki' => [
			'ro' => 'ro/chirie_terenuri',
			'en' => 'en/rent_lands'
		],
		'ro/chirie_terenuri' => [
			'ru' => 'ru/arenda_zemelinye_uchastki',
			'en' => 'en/rent_lands'
		],
		'en/rent_lands' => [
			'ru' => 'ru/arenda_zemelinye_uchastki',
			'ro' => 'ro/chirie_terenuri',
		],
		'ru/prodazha_zemelinye_uchastki' => [
			'ro' => 'ro/vanzare_terenuri',
			'en' => 'en/sale_lands'
		],
		'ro/vanzare_terenuri' => [
			'ru' => 'ru/prodazha_zemelinye_uchastki',
			'en' => 'en/sale_lands'
		],
		'en/sale_lands' => [
			'ru' => 'ru/prodazha_zemelinye_uchastki',
			'ro' => 'ro/vanzare_terenuri'
		],
        'ro/404' => [
            'ro' => 'ru/404',
            'en' => 'en/404'
        ],
        'ru/404' => [
            'ro' => 'ro/404',
            'en' => 'en/404'
        ],
        'en/404' => [
            'ru' => 'ru/404',
            'ro' => 'ro/404'
        ],
	];
	// Удаляем активный язык из массива
	$available_langs 	= array_diff($available_langs, ["$lclang"]);
	$available_langs	= array_values($available_langs);

	// В зависимости от запроса формируем ссылки
	if ($actual_link == '/' || !$ci->uri->segment(2))
	{
		// В случае, если у нас нету запроса на отдельную страницу (страницы каталога, блога и т.д.), меняем язык пользователя
		foreach($available_langs as $uri_lang)
		{
			echo '<a class="header_menu_link" href="/'.$uri_lang.'">'.strtoupper($uri_lang).'</a>';
		}
	}
	else
	{
		// Получаем доступные страницы из базы данных TopMenu
		$pages 		= $ci->db->get('TopMenu')->result_array();

		// Формируем массив данных с ссылками
		foreach ($pages as $page)
		{
			// Формируем название страницы, как ключ
			$key 	= $lclang.'/'.$page["UriName$clang"];
			$value 	= array();
			// Формируем значение для ключа и производим запись данных в основной массив
			foreach ($available_langs as $lang)
			{
				// Делаем с заглавных букв для того, чтоб получить в дальнейшем значение из массива данных по ссылкам с БД
				$lang_go_up 	= strtoupper($lang);
				$value[$lang] 	= $lang.'/'.$page["UriName$lang_go_up"];
			}
			// Заносим полученные ключ и значения в основной массив
			$links[$key] = $value;
		}
		// Добавляем доп линки в массив $links
		foreach ($dop_links as $key => $value) {
			$links[$key] = $value;
		}
		// Получаем активную ссылку без GET запроса
		$main_link = $ci->uri->uri_string();
		$main_link = trim($main_link, '/');
		// Получаем ГЕТ параметры если они присутствуют
		if (!empty($_GET))
		{
			$get_to_string = $_SERVER['QUERY_STRING'];
			$get_to_string = '?'.$get_to_string;
		} else {
			$get_to_string = '';
		}
		// Выводим ссылки
		if (!isset($links[$main_link])) {
			$main_link = $lclang.'/404';
		}
		foreach ($links[$main_link] as $key => $value)
		{
			echo '<a class="header_menu_link" href="/'.$value.$get_to_string.'">'.strtoupper($key).'</a>';
		}
	}
}

// Функция для формирования ссылок для объектов
function getObjectsHref($object_id, $lang_aac = false)
{
	// Переменные функции
	$ci	= &get_instance();
	if ($lang_aac) {
		$lclang = strtolower($lang_aac);
	} else {
		$lclang = strtolower($_SESSION['lang']);
	}
	$clang 	= strtoupper($lclang);
	// Языковые данные + формирование первой частик url
	$type_of_deal = [
		'ru' => [1 => 'arenda', 2 => 'prodazha'],
		'ro' => [1 =>'chirie', 2 =>'vanzare'],
		'en' => [1 => 'rent', 2 => 'sale']
	];
	$object_type = [
		'ru' => [1 => 'kvartiry', 2 => 'doma', 3 => 'kommercheskaja_nedvizhimosti', 4 => 'zemelinye_uchastki'],
		'ro' => [1 =>'apartamente', 2 =>'case', 3 => 'proprietate_comerciala', 4 => 'terenuri'],
		'en' => [1 => 'apartments', 2 => 'houses', 3 => 'commercial_properties', 4 => 'lands']
	];
	// Получаем информацию по объекту
	$object = $ci->db->where('ID', $object_id)->get('objects')->row_array();
	// Формируем ссылку
	$value_f =  $type_of_deal[$lclang][$object['type_of_deal']];
	$value_s = $object_type[$lclang][$object['object_type_id']];
	$uri = 'uri'.$clang;
	$first_segment =  implode('_', array($value_f, $value_s));
	$href = "/$lclang/$first_segment/{$object[$uri]}";
	return $href;
}

// Функция для получения мета данных для объекта
function get_data_for_object($uri) {
	// Переменные функции
	$ci					= &get_instance();
	$lclang 			= strtolower($_SESSION['lang']);
	$clang 				= strtoupper($lclang);
	// Получаем данные по Константам
	$const = getLangConstants();
	// Языковые данные
	$type_of_deal = [
		'ru' => [1 => $const->FORRENT->RU, 2 => $const->FORSALE->RU],
		'ro' => [1 => $const->FORRENT->RO, 2 => $const->FORSALE->RO],
		'en' => [1 => $const->FORRENT->EN, 2 => $const->FORSALE->EN]
	];
	$object_type = [
		'ru' => [1 => $const->APARTMENT->RU, 2 => $const->HOUSE->RU, 3 => $const->COMMERCIALPROP->RU, 4 => $const->LANDSINGLE->RU],
		'ro' => [1 => $const->APARTMENT->RO, 2 => $const->HOUSE->RO, 3 => $const->COMMERCIALPROP->RO, 4 => $const->LANDSINGLE->RO],
		'en' => [1 => $const->APARTMENT->EN, 2 => $const->HOUSE->EN, 3 => $const->COMMERCIALPROP->EN, 4 => $const->LANDSINGLE->EN]
	];
	// Получаем информацию по объекту
	$object = $ci->db->where("uri$clang", $uri)->get('objects')->row_array();
	// Получаем информацию по району и региону
	$region = $ci->db->where('ID', $object['region_id'])->get('regions')->row_array();
	$sector = $ci->db->where('ID', $object['sector_id'])->get('regions')->row_array();
	// Формируем данные для объекта
	$value 		= array();
	$value[] 	= $type_of_deal[$lclang][$object['type_of_deal']];
	$value[] 	= $object_type[$lclang][$object['object_type_id']];
	if ( !empty($object["name$clang"]) ) $value[]	= $object["name$clang"];
	if ( $object['object_type_id'] != 4 ) {
		if ( !empty($object['area']) ) $value[] = $object['area'].' m2';
	} else {
		$area = $ci->db->select("area_type.name$clang as area")
		->where('lands.object_id', $object['ID'])
		->join('area_type', 'lands.area_type_id = area_type.ID')
		->get('lands')->row_array();
		if ( !empty($object['area']) ) $value[] = $object['area'].' '.$area['area'];
	}
	if (!empty($sector)) $value[]= $sector["name$clang"];
	if (!empty($region)) $value[] = $region["name$clang"];
	if  (!empty($object['object_code'])) $value[] = $object['object_code'];
	// Формируем title
	$title = implode(' ', $value);
	// Формируем description
	$text   = $object["description$clang"];
	$length = strlen($text);
	$length = ($length > 160) ? 160 : $length;
	$description = '';
	for ($i = 0; $i < $length; $i++) {
		$description .= $text[$i];
	}
	// Возвращаем полученные данные
	return array('title' => $title, 'description' => $description );
}

// Функция для получения языковых url по объектам недвижимости
function create_lang_links_for_object()
{
	// Переменные функции
	$ci					= &get_instance();
	$lclang 			= strtolower($_SESSION['lang']);
	$clang 				= strtoupper($lclang);
	$uri_active			= $ci->uri->segment(3);
	$available_langs 	= array('ru', 'ro', 'en');
	$flags				= array('ru' => '/public/i/flag2.png', 'ro' => '/public/i/flag1.png', 'en' => '/public/i/flag3.png');
	$links 				= array();
	$type_of_deal = [
		'ru' => [1 => 'arenda', 2 => 'prodazha'],
		'ro' => [1 =>'chirie', 2 =>'vanzare'],
		'en' => [1 => 'rent', 2 => 'sale']
	];
	$object_type = [
		'ru' => [1 => 'kvartiry', 2 => 'doma', 3 => 'kommercheskaja_nedvizhimosti', 4 => 'zemelinye_uchastki'],
		'ro' => [1 =>'apartamente', 2 =>'case', 3 => 'proprietate_comerciala', 4 => 'terenuri'],
		'en' => [1 => 'apartments', 2 => 'houses', 3 => 'commercial_properties', 4 => 'lands']
	];

	// Удаляем активный язык из массива
	$available_langs 	= array_diff($available_langs, ["$lclang"]);
	$available_langs	= array_values($available_langs);

	// Получаем информацию по объекту
	$object = $ci->db->where("uri$clang", $uri_active)->get('objects')->row_array();

	// Рабочий скрипт для формирования ссылок на другие языки
	foreach ($available_langs as $lang)
	{
		$href = getObjectsHref($object['ID'], $lang);
		echo '<a class="header_menu_link" href="'.$href.'"><img src="'.strtoupper($lang).'" alt="'.$lang.'"/></a>';
	}
}

// Функция для получения ссылки на телефон нужного формата
function createPhoneLinkForBroker($phone_number)
{
	// Форматируем полученный номер
	$first 	= $phone_number[0];
	$old_pn = $phone_number;
	if ($first == '0') {
		$phone_number = substr($phone_number, 1);
	}
	echo '<a class="phone_link" onclick="goog_report_conversion(\'tel:8 (373) '.$phone_number.'\')" href="tel:+373'.$phone_number.'">'.$old_pn.'</a>';
}
// Функция для создания ссылки для звонка при нажатии на телефон, страница брокера
function createPhoneImgForBroker($phone_number, $html)
{
	// Форматируем полученный номер
	$first 	= $phone_number[0];
	$old_pn = $phone_number;
	if ($first == '0') {
		$phone_number = substr($phone_number, 1);
	}
	echo '<a onclick="goog_report_conversion(\'tel:8 (373) '.$phone_number.'\')" href="tel:+373'.$phone_number.'">'.$html.'</a>';
}
// Функция для получения URL для каталога
function getURlForCatalog($object_type_v, $type_of_deal_a)
{
	$lclang 			= strtolower($_SESSION['lang']);
	$clang 				= strtoupper($lclang);
	$value_1 			= (int) $object_type_v;
	if (!is_array($type_of_deal_a)) {
	 	$value_2 = 3;
	} else {
		if ($type_of_deal_a[0] == $type_of_deal_a[1]) {
			$value_2 = 3;
		} else {
			$value_2 = ($type_of_deal_a[0] == 'true') ? 1 : 2;
		}
	}
	$type_of_deal = [
		'ru' => [1 => 'arenda', 2 => 'prodazha', 3 => ''],
		'ro' => [1 =>'chirie', 2 =>'vanzare', 3 => ''],
		'en' => [1 => 'rent', 2 => 'sale', 3 => '']
	];
	$object_type = [
		'ru' => [1 => 'kvartiry', 2 => 'doma', 3 => 'kommercheskaja_nedvizhimosti', 4 => 'zemelinye_uchastki'],
		'ro' => [1 =>'apartamente', 2 =>'case', 3 => 'proprietate_comerciala', 4 => 'terenuri'],
		'en' => [1 => 'apartments', 2 => 'houses', 3 => 'commercial_properties', 4 => 'lands']
	];

	if ( $value_2 == 3 ) {
		$href = '/'.$lclang.'/'.$object_type[$lclang][$value_1];
	} else {
		$href = '/'.$lclang.'/'.$type_of_deal[$lclang][$value_2].'_'.$object_type[$lclang][$value_1];
	}
	return $href;
}

// Функция для получения кнопки вернуться обратно
function  getGoBackLink($ot_user)
{
	$lclang = strtolower($_SESSION['lang']);
	$clang 	= strtoupper($lclang);
	$ot 	= (int) $ot_user;
	$object_type = [
		'ru' => [1 => 'kvartiry', 2 => 'doma', 3 => 'kommercheskaja_nedvizhimosti', 4 => 'zemelinye_uchastki'],
		'ro' => [1 =>'apartamente', 2 =>'case', 3 => 'proprietate_comerciala', 4 => 'terenuri'],
		'en' => [1 => 'apartments', 2 => 'houses', 3 => 'commercial_properties', 4 => 'lands']
	];
	$href = '/'.$lclang.'/'.$object_type[$lclang][$ot];
	return $href;
}

// Функция для вывода кнопки фаворит
function favoriteGO()
{
	$lclang = strtolower($_SESSION['lang']);
	$clang 	= strtoupper($lclang);
	$favorite_links = [
		'ru' => 'izbrannoe',
		'ro' => 'favoritele_mele',
		'en' => 'favorite'
	];
	echo '<a class="icon_heart" href="/'.$lclang.'/'.$favorite_links[$lclang].'"></a>';
}

// Функция для формирования ссылки на поиск
function searchLinkGo()
{
	$lclang = strtolower($_SESSION['lang']);
	$clang 	= strtoupper($lclang);
	$favorite_links = [
		'ru' => 'poisk',
		'ro' => 'cautare',
		'en' => 'search'
	];
	echo '/'.$lclang.'/'.$favorite_links[$lclang];
}

// Функция для создания языковых линков для статьи в блоге
function create_lang_links_for_article()
{
	// Переменные функции
	$ci					= &get_instance();
	$lclang 			= strtolower($_SESSION['lang']);
	$clang 				= strtoupper($lclang);
	$actual_link 		= $_SERVER['REQUEST_URI'];
	$available_langs 	= array('ru', 'ro', 'en');
	$flags				= array('ru' => '/public/i/flag2.png', 'ro' => '/public/i/flag1.png', 'en' => '/public/i/flag3.png');
	$links 				= array();
	$uri2 				= $ci->uri->segment(2);
	$uri3 				= $ci->uri->segment(3);
	$uri4 				= $ci->uri->segment(4);
	// Удаляем активный язык из массива
	$available_langs 	= array_diff($available_langs, ["$lclang"]);
	$available_langs	= array_values($available_langs);

	// В зависимости от языков выводим ссылки
	foreach ($available_langs as $lang) {
		echo '<a class="header_menu_link" href="/'.$lang.'/'.$uri2.'/'.$uri3.'/'.$uri4.'"><img src="'.$flags[$lang].'" alt="'.$lang.'"/></a>';
	}
}

// Корректировка ориентации фотографии
function correctImageOrientation($filename) {
  	if (function_exists('exif_read_data')) {
    	$exif = exif_read_data($filename);
    	if($exif && isset($exif['Orientation'])) {
	      	$orientation = $exif['Orientation'];
	      	if($orientation != 1){
	        	$img = imagecreatefromjpeg($filename);
	        	$deg = 0;
	        	switch ($orientation) {
	          		case 3: $deg = 180; break;
	          		case 6: $deg = 270; break;
	          		case 8: $deg = 90; break;
	        	}
				if ($deg) {
					$img = imagerotate($img, $deg, 0);
				}
		        // then rewrite the rotated image back to the disk as $filename
		        imagejpeg($img, $filename, 95);
      		} // if there is some rotation necessary
    	} // if have the exif orientation info
  	} // if function exists
}

// Вывод мута данных по подаче правильного регионального языка
function correctLangMeta()
{
	// Переменные функции
	$ci					= &get_instance();
	$lclang 			= strtolower($_SESSION['lang']);
	$clang 				= strtoupper($lclang);
	// Ссылка на страницу
	$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	// Вывод
	if ($lclang == 'ro') {
		echo '<link rel="canonical" href="'.$actual_link.'" hreflang="'.$lclang.'" />'."\n";
	} else {
		echo '<link rel="alternate" href="'.$actual_link.'" hreflang="'.$lclang.'" />'."\n";
	}
}

// Получаем данные для страницы статьи в блоге
function get_data_for_article($uri) {
	// Подключаем стандартные модули
	$ci=&get_instance();
	// Получаем язык
	$clang = strtoupper($_SESSION['lang']);
	// Делаем запрос в БД для получения данных по странице
	$ci->db->select("
		otitle$clang as title,
		okeywords$clang as keywords,
		odescription$clang as description
	")->where('url_name', $uri);
	$q = $ci->db->get('blog')->row();
	// Формируем массив данных по странице
	$data = array(
		'title' 		=> $q->title,
		'keywords' 		=> $q->keywords,
		'description'	=> $q->description
	);
	// Возвращаем полученные данные
	return $data;
}

// Функция для получения Description Метаданных объекта
function getObjectDescription($ID)
{
	// Переменные функции
	$lclang = strtolower($_SESSION['lang']);
	$clang 	= strtoupper($lclang);
	$ID = (int) $ID;
	// Подключаем стандартные модули
	$ci=&get_instance();
	// Получаем данные по объекту
	$ci->db->select("
		object_type.name$clang as object_type,
		type_of_deal.name$clang as tod,
		object_type.ID as otID
	")
	->where('objects.ID', $ID)
	->join("object_type", "object_type.ID = objects.object_type_id", "left")
	->join('type_of_deal', "type_of_deal.ID = objects.type_of_deal", "left");
	$object = $ci->db->get('objects')->row();
	// Если данных нет, то останавливаем работу функции
	if (empty($object)) {
		return false;
	}
	// Формируем Description
	$description = $object->tod.' '.$object->object_type;
	// В случае, если у нас коммерческая недвижимость или земельные участки
	if ($object->otID == 3) {
		$ci->db->select("
			com_property_type.name$clang as cpt
		")->where("object_id", $ID)
		->join("com_property_type", "com_property_type.ID = commercial_properties.property_type_id", "left");
		$o_dop_data = $ci->db->get('commercial_properties')->row();
		$description .= ' '.$o_dop_data->cpt;
	}
	if ($object->otID == 4) {
		$ci->db->select("
			land_property_type.name$clang as lpt
		")->where("object_id", $ID)
		->join("land_property_type", "land_property_type.ID = lands.land_property_type_id", "left");
		$o_dop_data = $ci->db->get('lands')->row();
		$description .= ' '.$o_dop_data->lpt;
	}
	// Выводим полученый результат
	echo $description;
}

// Получаем фотографии для sitemap.xml
function getImgForSitemap($object_id, $object_type, $link, $gallery)
{
	// Переменные функции
	$lclang = strtolower($_SESSION['lang']);
	$clang 	= strtoupper($lclang);
	// Подключаем стандартные модули
	$ci=&get_instance();
	// Получаем тип объекта
	switch ($object_type) {
		case '1': $tblname = 'apartments';break;
		case '2': $tblname = 'houses'; break;
		case '3': $tblname = 'commercial_properties';break;
		case '4': $tblname = 'lands';break;
		default: $tblname = 'apartments';break;
	}
	if ($gallery) {
		$photo = $gallery[0]['photo'];
	} else  {
		$photo = false;
	}
	if ($photo) {
        $photo_file = newthumbs($photo, $tblname,384,250,"280_main", 1);
	} else {
		$photo_file = 'error 2';
	}

	if ($photo_file != 'error 2') {
		echo "<image:loc>$link"."$photo_file</image:loc>";
	}
}
// Функция для формирования ссылок для объектов
function getObjectsHrefV2($object, $lang_aac)
{
	// Переменные функции
	$ci	= &get_instance();
	$lclang = strtolower($lang_aac);
	$clang 	= strtoupper($lclang);
	// Языковые данные + формирование первой частик url
	$type_of_deal = [
		'ru' => [1 => 'arenda', 2 => 'prodazha'],
		'ro' => [1 =>'chirie', 2 =>'vanzare'],
		'en' => [1 => 'rent', 2 => 'sale']
	];
	$object_type = [
		'ru' => [1 => 'kvartiry', 2 => 'doma', 3 => 'kommercheskaja_nedvizhimosti', 4 => 'zemelinye_uchastki'],
		'ro' => [1 =>'apartamente', 2 =>'case', 3 => 'proprietate_comerciala', 4 => 'terenuri'],
		'en' => [1 => 'apartments', 2 => 'houses', 3 => 'commercial_properties', 4 => 'lands']
	];
	// Формируем ссылку
	$value_f =  $type_of_deal[$lclang][$object->type_of_deal];
	$value_s = $object_type[$lclang][$object->object_type_id];
	$uri_aac = 'uri'.$clang;
	$first_segment =  implode('_', array($value_f, $value_s));
	$href = "/$lclang/$first_segment/".$object->$uri_aac;
	return $href;
}

// Получаем фотографии и видео для объекта
function getObjectsXMLData($object_id, $object_type, $lang, $link, $object_name, $object_description, $gallery)
{
	// Переменные функции
	$lclang = strtolower($_SESSION['lang']);
	$clang 	= strtoupper($lclang);
	$lang = strtolower($lang);
	// Подключаем стандартные модули
	$ci=&get_instance();
	// Получаем тип объекта
	switch ($object_type) {
		case '1': $tblname = 'apartments';break;
		case '2': $tblname = 'houses'; break;
		case '3': $tblname = 'commercial_properties';break;
		case '4': $tblname = 'lands';break;
		default: $tblname = 'apartments';break;
	}
	// Работаем с фотографиями и видео
	if (!empty($gallery)) {
		foreach ($gallery as $value) {
			$mphile = newthumbs(@$value['photo'], $tblname, $width=850, $height=470, "850_main", 0,true);
			if ($mphile != 'error 2') {
				echo "<image:image>";
					echo "<image:loc>$link/$lang".$mphile."</image:loc>";
				echo "</image:image>";
			}
		}
		foreach ($gallery as $value) {
			if (!empty($value['video'])) {
				$mphile = newthumbs(@$value['photo'], $tblname, $width=850, $height=470, "850_main", 0,true);
				echo "<video:video>";
					echo "<video:title>";
						echo strip_tags($object_name);
					echo "</video:title>";
					echo "<video:description>";
						echo strip_tags($object_description);
					echo "</video:description>";
					echo "<video:thumbnail_loc>";
						echo "$link/$lang".$mphile;
					echo "</video:thumbnail_loc>";
					echo "<video:content_loc>";
						echo "$link/public/{$value['video']}";
					echo "</video:content_loc>";
					echo "<video:player_loc>";
						echo "$link/videotest/{$value['ID']}/";
					echo "</video:player_loc>";
				echo "</video:video>";
			}
		}
	}
}

// Функция для формаитирования массива
function transformGalleryToArray($gallery)
{
	$new_gallery =  array();
	foreach ($gallery as $value) {
		$new_gallery[$value['object_id']][] = $value;
	}
	return $new_gallery;
}

// Функция для вывода фото ОГ Объекта
function getMetaImgForObject()
{
	// Переменные функции
	$lclang = strtolower($_SESSION['lang']);
	$clang 	= strtoupper($lclang);
	$link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'];
	// Подключаем стандартные модули
	$ci=&get_instance();
	// Получаем объект
	$object = $ci->db->where('uri'.$clang, $ci->uri->segment(3))->get('objects')->row();
	switch ($object->object_type_id) {
		case '1': $tblname = 'apartments';break;
		case '2': $tblname = 'houses'; break;
		case '3': $tblname = 'commercial_properties';break;
		case '4': $tblname = 'lands';break;
		default: $tblname = 'apartments';break;
	}
	$photo = $ci->db->where('object_id', $object->ID)->order_by('Sorder ASC, ID DESC')->get('gallery')->row();
	// Получаем фотку
	if (!empty($photo)) {
		$photo_file = newthumbs(@$photo->photo, $tblname, $width=850, $height=470, "850_main", 0,true);
		if ($photo_file == 'error 2') {
			$photo_file = "/public/i/remark-logo.png";
		}
	} else {
		$photo_file = "/public/i/remark-logo.png";
	}
	echo '<meta property="og:image" content="'.$link.$photo_file.'" />';
	echo '<meta property="og:image:width" content="850" />';
	echo '<meta property="og:image:height" content="470" />';
}
// Функции для получения ссылок на большие категории
function getLinksForBig($tblname) {
    // Переменные функции
    $lclang = strtolower($_SESSION['lang']);
    $clang 	= strtoupper($lclang);
    switch ($tblname) {
        case 'apartments':
            $href_array = [
                'ru' => ['/ru/arenda_kvartiry', '/ru/prodazha_kvartiry'],
                'ro' => ['/ro/chirie_apartamente', '/ro/vanzare_apartamente'],
                'en' => ['/en/rent_apartments', '/en/sale_apartments']
            ];
            break;
        case 'houses':
            $href_array = [
                'ru' => ['/ru/arenda_doma', '/ru/prodazha_doma'],
                'ro' => ['/ro/chirie_case', '/ro/vanzare_case'],
                'en' => ['/en/rent_houses', '/en/sale_houses']
            ];
            break;
        case 'commercial_properties':
            $href_array = [
                'ru' => ['/ru/arenda_kommercheskaja_nedvizhimosti', '/ru/prodazha_kommercheskaja_nedvizhimosti'],
                'ro' => ['/ro/chirie_proprietate_comerciala', '/ro/vanzare_proprietate_comerciala'],
                'en' => ['/en/rent_commercial_properties', '/en/sale_commercial_properties']
            ];
        break;
    }
    $names_array = [FORRENT,FORSALE];
    $i = 0;
    foreach ($href_array[$lclang] as $href) {
        echo '<a class="btn_white" href="'.$href.'">'.$names_array[$i].'</a>';
        $i++;
    }
}
// Получаем ссылку на карандаш для Админа
function getPencilForAdmin()
{
	// Подключаем стандартные модули
	$ci=&get_instance();
	// Переменные функции
	$lclang = strtolower($_SESSION['lang']);
	$clang 	= strtoupper($lclang);
	$uri3 	= $ci->uri->segment(3);
	$link = '';
	// Проверяем если url существует
	if (!$uri3) {
		header('Location: /');
		exit();
	}
	// Производим поиск
	$object = $ci->db->where("uri$clang", $uri3)->get('objects')->result_array();
	if ($object) {
		// Получаем тип
		switch ($object['object_type_id']) {
			case '1': $link = 'apartments'; break;
			case '2': $link = 'houses'; break;
			case '3': $link = 'commercial_properties'; break;
			case '4': $link = 'lands'; break;
		}
		// Выводим ссылку
		echo '<a style="margin-left: 5px;" target="_blank" href="/cp/'.$link.'/'.$object['ID'].'/"><i class="fa fa-pencil"></i></a>';
	}
}
// Получаем ссылку на карандаш для
function getPencil()
{
	// Подключаем стандартные модули
	$ci=&get_instance();
	// Переменные функции
	$lclang = strtolower($_SESSION['lang']);
	$clang 	= strtoupper($lclang);
	$uri3 	= $ci->uri->segment(3);
	$link = '';
	// Проверяем если url существует
	if (!$uri3) {
		header('Location: /');
		exit();
	}
	// Производим поиск
	$object = $ci->db->where("uri$clang", $uri3)->get('objects')->row_array();
	if ($object) {
		// Получаем тип
		switch ($object['object_type_id']) {
			case '1': $link = 'apartments'; break;
			case '2': $link = 'houses'; break;
			case '3': $link = 'commercial_properties'; break;
			case '4': $link = 'lands'; break;
		}
		// Выводим ссылку
		if (isset($_SESSION['edit']) && $_SESSION['edit'] == 'true') {
			echo '<a style="margin-left: 5px;" target="_blank" href="/cp/'.$link.'/'.$object['ID'].'/"><i class="fa fa-pencil"></i></a>';
		} else {
			echo '<a style="margin-left: 5px;" target="_blank" href="/cp/'.$link.'_b/'.$object['ID'].'/"><i class="fa fa-pencil"></i></a>';
		}
	}
}
// Функция для получения доп КСС для попапа
function getDopCssForJsScript($position_pu)
{
	$position_pu = (int) $position_pu;
	$result = '';
	switch ($position_pu) {
		case '2': $result = 'aac_pup_right_bottom'; break;
		case '3': $result = 'aac_pup_right_top'; break;
		case '4': $result = 'aac_pup_right_middle'; break;
		case '5': $result = 'aac_pup_left_bottom'; break;
		case '6': $result = 'aac_pup_left_top'; break;
		case '7': $result = 'aac_pup_left_middle'; break;
		default: $result = 'aac_nothing'; break;
	}
	return $result;
}

// Функция дял получения ссылки на хлебную крошку по типу объекта
function getObjectBCforTOD($tblname)
{
	// Переменные функции
	$lclang = strtolower($_SESSION['lang']);
	$clang 	= strtoupper($lclang);
	$object_type = [
		'ru' => [1 => 'kvartiry', 2 => 'doma', 3 => 'kommercheskaja_nedvizhimosti', 4 => 'zemelinye_uchastki'],
		'ro' => [1 =>'apartamente', 2 =>'case', 3 => 'proprietate_comerciala', 4 => 'terenuri'],
		'en' => [1 => 'apartments', 2 => 'houses', 3 => 'commercial_properties', 4 => 'lands']
	];
	// Получаем номер в зависимости от типа объекта
	switch ($tblname) {
	case 'apartments': $object_type_id = 1; break;
	case 'houses': $object_type_id = 2; break;
	case 'commercial_properties': $object_type_id = 3; break;
	case 'lands': $object_type_id = 4; break;
	}
	// Формируем ссылку
	$link = '/'.$lclang.'/'.$object_type[$lclang][$object_type_id];
	// Форзвращаем полученное значение
	return $link;
}
// Функция для получения ссылки на страницу about_us
function getAboutUsLink()
{
	// Переменные функции
	$lclang = strtolower($_SESSION['lang']);
	$links = [
		'ru' => '/ru/o_nas',
		'ro' => '/ro/despre_noi',
		'en' => '/en/about_us'
	];
	// В зависимости от выбранного языка возвращаем значение
	echo $links[$lclang];
}
function getLangConstants()
{
	// Подключаем стандартные модули
	$ci=&get_instance();
	// Переменные функции
	$lclang = strtolower($_SESSION['lang']);
	$clang 	= strtoupper($lclang);
	// Получаем массив с константами
	$array = $ci->db->get('LangConstants')->result();
	// Конвертируем данные в необходиммый нам формат
	$new_std_class = new stdClass();
	foreach ($array as $value) {
		$name = $value->ConstantName;
		$new_std_class->$name = $value;
	}
	return $new_std_class;
}

function isAuthorized() {
	$ci=&get_instance();

	if(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == 1) {
		$ci->db->select('*');
		$query = $ci->db->where('ID', $_SESSION['user_id']);
		$query = $ci->db->get('SiteUser');

		if($query->num_rows() > 0 ) { 
			return true;
		} else { 
			return false;
		} 
	} else { 
		return false;
	} 
}

function isUser() {	
	if(isset($_SESSION['type']) && $_SESSION['type'] == 1) {
		return true;
	} else { 
		return false;
	} 
}

function displayCartOnHeader() {
	$total = 0;
	$count = 0;
	if (isset($_SESSION['cart']['total'])){
		$total = $_SESSION['cart']['total'];
	}
	if (isset($_SESSION['cart']['count'])){
		$count = $_SESSION['cart']['count'];
	}

	echo "<span>".$count."</span>".$total." ".TEXT_MDL;
}

function displayTotal($delivery=0) {
	$total = 0;
	$count = 0;
	if (isset($_SESSION['cart']['total'])){
		$total = $_SESSION['cart']['total'];
	}

	if ($delivery>0) {
		$total = $total + $delivery;
	}
	
	echo $total." ".TEXT_MDL;
}

function CalcTotal($delivery=0) {
	$total = 0;
	$count = 0;
	if (isset($_SESSION['cart']['total'])){
		$total = $_SESSION['cart']['total'];
	}

	if ($delivery>0) {
		$total = $total + $delivery;
	}

	return $total;
}

function Exchange ($price, $dollar=false) {
	$ci=&get_instance();

	if($dollar==true){
		
		$curs = $ci->db->select('USD')
		->order_by('Date','desc')
		->limit(1)->get('CursValutar')
		->row('USD');

		$price = $price * $curs;

		if(isset($_SESSION['discount']) && $_SESSION['discount'] > 0) {
			$discount = $price * $_SESSION['discount'] / 100;
			$price = $price - $discount;
		}

	}
	$price = number_format($price , 2 , "." , "");
	$price = floatval($price);
	return $price;
}

function displayNumberFormat($number, $zecimal=2) {
	echo number_format((float)$number, $zecimal, '.', ' ');
}

function categories_map($categories, $ParentCod = 0)
{
	$result = array();
    foreach ($categories as $category) {

        if ($category->ParentCod == $ParentCod) {

			$result[$category->Cod]['Title'] = $category->Title;
			$result[$category->Cod]['Cod'] = $category->Cod;
			$result[$category->Cod]['UriName'] = $category->UriName;
            $result[$category->Cod]['childs'] = categories_map($categories, $category->Cod);
		}
	}
	return $result;
}

function categories_map_without_keys($categories) {
	$result = array();
	$i=0;

	foreach ($categories as $category) {
		
		$result[$i] = $category;
		if(!empty($result[$i]['childs'])) {
			$result[$i]['childs'] = categories_map_without_keys($result[$i]['childs']);
		}
		$i++;
	}

	return $result;
}

if (!function_exists('ilabCrypt')) {
    function ilabCrypt($action, $string)
    {
        // Инициализируем даныне
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = "0jWLPvrkm0bASR}L_UpWi:TWzV3<%|(4~OglNl;+Z+G3C@l>AC)mW!g6w4C[LgMz";
        $secret_iv = '6)E=]7 nWq2SW,|P1RO}D92:1S=U-u9+(F-iy`]YN 5DmfDX|X]=Oqj4`-<gi73W';
        // Преобразуем данные
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        // Основной скрипт
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }
}

if (!function_exists('get_prefered_lang')) {
    function get_prefered_lang()
    {
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        switch ($lang) {
            case "ru":
                $_SESSION['lang'] = 'ru';
                break;
            case "ro":
                $_SESSION['lang'] = 'ro';
                break;
            case "en":
                $_SESSION['lang'] = 'en';
                break;
            default:
                $_SESSION['lang'] = 'ro';
                break;
        }
    }
}

if (!function_exists('get_lang')) {
    function get_lang($up = FALSE)
    {
        return ($up) ? strtoupper($_SESSION['lang']) : strtolower($_SESSION['lang']);
    }
}

if (!function_exists('throw_on_404')) {
    function throw_on_404()
    {
        header("HTTP/1.0 404 Not Found");
        show_404();
        exit();
    }
}

if (!function_exists('assign_language')) {
    function assign_language($lang = FALSE)
    {
        switch ($lang) {
            case "ru":
                $_SESSION['lang'] = 'ru';
                break;
            case "ro":
                $_SESSION['lang'] = 'ro';
                break;
            case "en":
                $_SESSION['lang'] = 'en';
                break;
            default:
                $_SESSION['lang'] = 'ro';
                break;
        }
    }
}
if (!function_exists('paymentHttpPost')) {
    function paymentHttpPost($url, $params)
    {
        $postData = http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        $output = curl_exec($ch);
        curl_close($ch);
    }
}

function diverse_array($vector) { 
    $result = array(); 
    foreach($vector as $key1 => $value1) 
        foreach($value1 as $key2 => $value2) 
            $result[$key2][$key1] = $value2; 
    return $result; 
}

if (!function_exists('getRealIpAddr')) {
    function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) // Определяем IP
            {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
}
?>
