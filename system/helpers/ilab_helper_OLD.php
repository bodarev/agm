<?
function create_form_by_array($form,$data) {
	foreach($form as $row) {
		$value=@$data[$row['name']];
		echo '<tr'.@$row['dop_tr'].'>
			<td width="200">'.$row['descr'].'</td>
			<td>';
			if ($row['type']=='text') {
				echo '<input name="data['.$row['name'].']" type="text" id="'.$row['name'].'" class="'.$row['class'].'" value="'.$value.'" />';
			} elseif ($row['type']=='textarea') {
				echo '<textarea name="data['.$row['name'].']" id="'.$row['name'].'" class="'.$row['class'].'"'.@$row['dop_style'].'>'.$value.'</textarea>';
			} elseif ($row['type']=='select') {
				echo '<select class="'.$row['class'].'" name="data['.$row['name'].']">';
					addSelect($row['source'],$row['sourcename'],$value);
				echo '</select>';
			} elseif ($row['type']=='checkbox') {
				if (!empty($value)) $md=' checked'; else $md='';
				echo '<input name="data['.$row['name'].']"'.$md.' type="checkbox" id="'.$row['name'].'" class="'.$row['class'].'" value="1" />';
			} elseif ($row['type']=='file') {
				echo '<input type="file" name="'.$row['name'].'">';
				if (!empty($value)) {
					echo '<img src="/public/'.$row['path'].'/'.$value.'" style="float:right;max-width:200px;">';
					if ($row['name']=='Banner') {
						echo '<br clear="all"><a style="float:right;margin-top:10px;" href="javascript:void(0);" id="bandel" onclick="delbanner()">Удалить</a>';
					}
				}
			} elseif ($row['type']=='catlist') {
				$cats=get_categories();
				echo '<select class="'.$row['class'].'" name="data['.$row['name'].']">';
					echo '<option value="0"></option>';
					build_cat_select($cats,0,$value,$row['exclude']);
					
				echo '</select>';
			}
			echo @$row['dop_data'];
			echo '</td>
		</tr>';
	}
}

function addSelect($table,$sourcename='Name',$value=0) {
	$dopcond='';
	$ci=&get_instance();
	//$q=$ci->db->order_by('Sorder ASC,ID DESC')->get($table)->result_array();
    if ($table=='TopMenu') {
        echo '<option value="0"></option>';
        $q=$ci->db->where('ParentID',0)->order_by('Sorder ASC,ID DESC')->get($table)->result_array();
    echo '<option'.$mod.' value=""></option>';
	/*foreach($q as $arr) {
		if ($arr['ID']==$value) $mod=' selected'; else $mod='';
		echo '<option'.$mod.' value="'.$arr['ID'].'">'.$arr[$sourcename].'</option>';
	}*/
}

function newthumbs($photo='',$dir='',$width=0,$height=0,$version=0,$zc=0)
{
	require_once(realpath('public').'/imres/phpthumb.class.php');
	//echo $dir;
	$result=is_dir(realpath('public/'.$dir).'/thumbs');
	if ($result) {
		$prevdir=$dir.'/thumbs';
	} else {
		if (mkdir(realpath('public/'.$dir).'/thumbs')) {
			$prevdir=$dir.'/thumbs';
		} else {
			return 'error 1';
		}
	}
	
	if (!empty($version)) {
		$result=is_dir(realpath('public/'.$dir).'/thumbs/version_'.$version);
		if ($result) {
			$prevdir=$dir.'/thumbs/version_'.$version;
		} else {
			if (mkdir(realpath('public/'.$dir).'/thumbs/version_'.$version)) {
				$prevdir=$dir.'/thumbs/version_'.$version;
			} else {
				return 'error 1';
			}
		}
	}
	$va1=explode('.',$photo);
	$ext=end($va1);
	
	$timg=realpath('public/'.$dir).'/'.$photo;
    $catimg=realpath('public/'.$prevdir).'/'.$photo;
	
	if (is_file($timg) && !is_file($catimg)) {
		$opath1=realpath('public/'.$dir).'/';
		$opath2=realpath('public/'.$prevdir).'/';
		$dest=$opath2.$photo;
		$source=$opath1.$photo;
		$phpThumb = new phpThumb();
		$phpThumb->setSourceFilename($source);
		if (!empty($width)) $phpThumb->setParameter('w', $width);
		if (!empty($height)) $phpThumb->setParameter('h', $height);
		if (!empty($height)) $phpThumb->setParameter('q', 100);
		if ($ext=='png') $phpThumb->setParameter('f', 'png');
		if (!empty($zc)) {
			$phpThumb->setParameter('zc', '1');
		}
		$phpThumb->setParameter('q', 100);
		if ($phpThumb->GenerateThumbnail())
		{
			if ($phpThumb->RenderToFile($dest))
			{
				$img='/public/'.$prevdir.'/'.$photo;
			} else {
				return 'error 3';
			}
		}
		   
	} elseif(is_file($catimg)) {
		$img='/public/'.$prevdir.'/'.$photo;
	} else {
		return 'error 2';
	}
	
	return $img;
}
?>