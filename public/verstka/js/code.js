$(document).ready(function(e) {
	/* mob_menu */
	$('#header').on('click', '.m_ctrl', function() {
		$('.mob_menu_wr').slideToggle(300);
	});
	$('.mob_menu_wr').on('click', '.close', function() {
		$('.mob_menu_wr').hide();
	});

	/* cat_ddown */
	$('.h_cat').on('click', '.ctrl', function() {
		$(this).siblings('.ddown').slideToggle(300);
	});

	/* mob_search */
	$('.search_ctrl_mob').on('click', function() {
		$('.sf_mob').slideToggle(300);
	});

	/* history_ddown */
	$(function() {
		$("#accordion").accordion({
			heightStyle: "content",
			collapsible: true,
			active: false,
			animate: false
		});
	});
	$('.history').children('.ddown').on('click', '.close', function() {
		$('.ddown_wr').hide();
	});
});