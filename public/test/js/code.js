$(document).ready(function(e) {
	/*h_menu_mob*/
	$('.m_ctrl').on('click', function() {
		$('.h_menu_mob_wr').toggle();
	});

	/*Form*/
	// $('input, select').styler({
	// 	selectPlaceholder: '',
	// 	selectVisibleOptions: 5
	// });

	/*popup*/
	// $('.open-popup-link').magnificPopup({
	// 	type: 'inline',
	// 	midClick: true,
	// 	showCloseBtn: false
	// });
	$('.open-popup-link').magnificPopup({
		type: 'inline',
		midClick: true,
		showCloseBtn: true,
		closeBtnInside: true,
		callbacks: {
			open: function() {
				$('input, select').styler({
					selectPlaceholder: '',
					selectVisibleOptions: 5
				});
			}
		}
	});


	/*tabs*/
	$('.tabs_wr').tabs();
});