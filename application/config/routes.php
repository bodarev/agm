<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'pages';
$route['404_override'] = 'pages/error_404';
$route['translate_uri_dashes'] = FALSE;

$route['ru'] = "pages/index";
$route['ro'] = "pages/index";
$route['en'] = "pages/index";

$route['ru/pages/contacts'] = "pages/contacts";
$route['ro/pages/contacts'] = "pages/contacts";
$route['en/pages/contacts'] = "pages/contacts";

$route['ru/pages/branches'] = "pages/branches";
$route['ro/pages/branches'] = "pages/branches";
$route['en/pages/branches'] = "pages/branches";

$route['ru/pages/brands'] = "pages/brands";
$route['ro/pages/brands'] = "pages/brands";
$route['en/pages/brands'] = "pages/brands";

$route['ru/pages/catalog'] = "pages/catalog";
$route['ro/pages/catalog'] = "pages/catalog";
$route['en/pages/catalog'] = "pages/catalog";

$route['ru/pages/catalog/(:any)'] = "pages/catalog";
$route['ro/pages/catalog/(:any)'] = "pages/catalog";
$route['en/pages/catalog/(:any)'] = "pages/catalog";

$route['ru/pages/search'] = "pages/search";
$route['ro/pages/search'] = "pages/search";
$route['en/pages/search'] = "pages/search";

$route['ru/pages/new-items'] = "pages/new_items";
$route['ro/pages/new-items'] = "pages/new_items";
$route['en/pages/new-items'] = "pages/new_items";

$route['ru/pages/promo'] = "pages/promo";
$route['ro/pages/promo'] = "pages/promo";
$route['en/pages/promo'] = "pages/promo";

$route['ru/pages/items'] = "pages/items";
$route['ro/pages/items'] = "pages/items";
$route['en/pages/items'] = "pages/items";

$route['ru/pages/product/(:any)'] = "pages/product";
$route['ro/pages/product/(:any)'] = "pages/product";
$route['en/pages/product/(:any)'] = "pages/product";

$route['ru/pages/cart'] = "pages/cart";
$route['ro/pages/cart'] = "pages/cart";
$route['en/pages/cart'] = "pages/cart";

$route['ru/pages/cart/:any'] = "pages/cart";
$route['ro/pages/cart/:any'] = "pages/cart";
$route['en/pages/cart/:any'] = "pages/cart";

$route['ru/pages/success/:any'] = "pages/success";
$route['ro/pages/success/:any'] = "pages/success";
$route['en/pages/success/:any'] = "pages/success";

$route['pages/subscribe'] = "pages/subscribe";
$route['pages/subscribe'] = "pages/subscribe";

$route['ru/pages/registration'] = "pages/registration";
$route['ro/pages/registration'] = "pages/registration";
$route['en/pages/registration'] = "pages/registration";

$route['ru/pages/signup-dealers'] = "pages/signup_dealers";
$route['ro/pages/signup-dealers'] = "pages/signup_dealers";
$route['en/pages/signup-dealers'] = "pages/signup_dealers";

$route['ru/pages/login'] = "pages/login";
$route['ro/pages/login'] = "pages/login";
$route['en/pages/login'] = "pages/login";

$route['ru/pages/restorepass'] = "pages/restorepass";
$route['ro/pages/restorepass'] = "pages/restorepass";
$route['en/pages/restorepass'] = "pages/restorepass";

$route['ru/pages/form_validation_restorepass/:any'] = "pages/form_validation_restorepass";
$route['ro/pages/form_validation_restorepass/:any'] = "pages/form_validation_restorepass";
$route['en/pages/form_validation_restorepass/:any'] = "pages/form_validation_restorepass";

$route['ru/pages/profile'] = "pages/profile";
$route['ro/pages/profile'] = "pages/profile";
$route['en/pages/profile'] = "pages/profile";

$route['ru/pages/profile/:any'] = "pages/profile";
$route['ro/pages/profile/:any'] = "pages/profile";
$route['en/pages/profile/:any'] = "pages/profile";

$route['ru/pages/logout'] = "pages/logout";
$route['ro/pages/logout'] = "pages/logout";
$route['en/pages/logout'] = "pages/logout";

$route['ru/pages/interview'] = "pages/interview";
$route['ro/pages/interview'] = "pages/interview";
$route['en/pages/interview'] = "pages/interview";

// ajax
$route['ru/pages/send_answers'] = "pages/send_answers";
$route['ro/pages/send_answers'] = "pages/send_answers";
$route['en/pages/send_answers'] = "pages/send_answers";

$route['ru/pages/send_password'] = "pages/send_password";
$route['ro/pages/send_password'] = "pages/send_password";
$route['en/pages/send_password'] = "pages/send_password";

$route['ru/pages/login_validation'] = "pages/login_validation";
$route['ro/pages/login_validation'] = "pages/login_validation";
$route['en/pages/login_validation'] = "pages/login_validation";

$route['ru/pages/change_date/:any'] = "pages/change_date";
$route['ro/pages/change_date/:any'] = "pages/change_date";
$route['en/pages/change_date/:any'] = "pages/change_date";

$route['ru/pages/registration_validation'] = "pages/registration_validation";
$route['ro/pages/registration_validation'] = "pages/registration_validation";
$route['en/pages/registration_validation'] = "pages/registration_validation";

$route['ru/pages/dealer_validation'] = "pages/dealer_validation";
$route['ro/pages/dealer_validation'] = "pages/dealer_validation";
$route['en/pages/dealer_validation'] = "pages/dealer_validation";

$route['pages/register_user/:any'] = "pages/register_user";

$route['pages/addToCart'] = "pages/addToCart";
$route['pages/addCount'] = "pages/plusMinusCount";

$route['ru/pages/order_validation'] = "pages/order_validation";
$route['ro/pages/order_validation'] = "pages/order_validation";
$route['en/pages/order_validation'] = "pages/order_validation";


$route['payment/generate'] = "payment/generate";
$route['payment/refund'] = "payment/refund";
$route['payment/answer'] = "payment/answer";
// test
$route['ru/pages/payment'] = "pages/payment";
$route['ro/pages/payment'] = "pages/payment";
$route['en/pages/payment'] = "pages/payment";

// topmenu

$route['ru/:any/:any/:any/:any'] = "pages/topmenu";
$route['ro/:any/:any/:any/:any'] = "pages/topmenu";
$route['en/:any/:any/:any/:any'] = "pages/topmenu";

$route['ru/:any/:any/:any'] = "pages/topmenu";
$route['ro/:any/:any/:any'] = "pages/topmenu";
$route['en/:any/:any/:any'] = "pages/topmenu";

$route['ru/:any/:any'] = "pages/topmenu";
$route['ro/:any/:any'] = "pages/topmenu";
$route['en/:any/:any'] = "pages/topmenu";

$route['ru/:any'] = "pages/topmenu";
$route['ro/:any'] = "pages/topmenu";
$route['en/:any'] = "pages/topmenu";

// admin
$route['cp'] = "admin/login";
$route['cp/(:any)'] = "admin/$1";
$route['cp/(:any)/(:any)'] = "admin/$1/$2";
$route['cp/(:any)/(:any)/(:any)'] = "admin/$1/$2/$3";
$route['cp/(:any)/(:any)/(:any)/(:any)'] = "admin/$1/$2/$3/$4";
$route['cp/(:any)/(:any)/(:any)/(:any)/(:any)'] = "admin/$1/$2/$3/$4/$5";
