<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Get_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getPage($url) {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->select('ID, Image, Title' . $clang . ' as PageTitle, OptimizationTitle' . $clang . ' as TabTitle , Keywords' . $clang . ' as Keywords, Description' . $clang . ' as Description, Text' . $clang . ' as PageText');
        $this->db->where('UriName', $url);
        $this->db->where('OnSite', 1);
        $query = $this->db->get('TopMenu');
        $result = $query->row_array();
        return $result;
    }

    public function getCategoryData($url) {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->select('ID, Cod, Image, Title' . $clang . ' as CategoryTitle, OptimizationTitle' . $clang . ' as Title , Keywords' . $clang . ' as Keywords, Description' . $clang . ' as Description, Text' . $clang . ' as PageText, has_product');
        $this->db->where('UriName', $url);
        $this->db->where('OnSite', 1);
        $query = $this->db->get('Catalog');
        $result = $query->row_array();
        return $result;
    }

    public function getBrands($category, $match, $cost, $pr) {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);
        $Title = 'Title'.$clang;

        $this->db->select('Brand');
        if($category != '') {
            $this->db->where('ParentCod', $category);
        }
        if($match != '') {
            $this->db->like($Title, $match);
        }
        if($pr != 0) {
            $this->db->where('Products.Promo', $pr);
        }
        switch ($cost) {
            case 1:
                $this->db->where('Price BETWEEN 10 AND 50');
                break;
            case 2:
                $this->db->where('Price BETWEEN 50 AND 200');
                break;
            case 3:
                $this->db->where('Price BETWEEN 200 AND 500');
                break;
            case 4:
                $this->db->where('Price >', 500);
                break;
        }
        $this->db->where('OnSite', 1);
        $this->db->where('Price >', 0);
        $this->db->where('TitleRO !=', '');
        $this->db->where('ParentCod >', 0);
        $this->db->group_by('Brand');
        $query = $this->db->get('Products');
        $result = $query->result();
        return $result;
    }

    public function getCategory($param) {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);
        $Title = 'Title'.$clang;

        $this->db->select('Catalog.Cod, Catalog.Title'.$clang.' as Title, Count(Products.Price) as Count');
        $this->db->join('Products', 'Products.ParentCod = Catalog.Cod');
        $this->db->where('Products.OnSite', 1);
        $this->db->where('Products.Price >', 0);
        if($param=='new') {
            $this->db->where('Products.New', 1);
        } else {
            $this->db->where('Products.Promo', 1);
        }
        $this->db->group_by('Products.ParentCod');
        $query = $this->db->get('Catalog');
        $result = $query->result();
        return $result;
    }

    public function getProducts($category, $match, $cost, $cat=0, $pr, $brand, $sortby, $nrpage, $new=0 ) {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);
        $Title = 'Title'.$clang;

        $nrpage--;
        $start = $nrpage*NR_PER_PAGE;

        $this->db->select('*, Title' . $clang . ' as Title,');
        if($category != '') {
            $this->db->where('ParentCod', $category);
        }
        if($match != '') {
            $this->db->like($Title, $match);
        }
        if($new != 0) {
            $this->db->where('Products.New', $new);
        }
        if($pr != 0) {
            $this->db->where('Products.Promo', $pr);
        }
        switch ($cost) {
            case 1:
                $this->db->where('Price BETWEEN 10 AND 50');
                break;
            case 2:
                $this->db->where('Price BETWEEN 50 AND 200');
                break;
            case 3:
                $this->db->where('Price BETWEEN 200 AND 500');
                break;
            case 4:
                $this->db->where('Price >', 500);
                break;
        }
        if($cat != 0) {
            $this->db->where('ParentCod', $cat);
        }
        if($brand != '') {
            $this->db->where('Brand', $brand);
        }
        $this->db->where('OnSite', 1);
        $this->db->limit(NR_PER_PAGE, $start);
        switch ($sortby) {
            case 1:
                $this->db->order_by('Title ASC');
                break;
            case 2:
                $this->db->order_by('Title DESC');
                break;
            case 3:
                $this->db->order_by('Price ASC');
                break;
            case 4:
                $this->db->order_by('Price DESC');
                break;
            default:
                $this->db->order_by('Title ASC');
        }
        $this->db->where('Price >', 0);
        $this->db->where('TitleRO !=', '');
        $this->db->where('ParentCod >', 0);
        $query = $this->db->get('Products');
        $result = $query->result();
        return $result;
        //return $this->db->last_query();
    }

    public function getRandProducts($category='', $limit ) {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);
        $Title = 'Title'.$clang;

        $this->db->select('*, Title' . $clang . ' as Title,');
        if(!empty($category)) {
            $this->db->where_in('ParentCod', $category);
        } 
        $this->db->where('OnSite', 1);
        $this->db->where('Price >', 0);
        $this->db->where('TitleRO !=', '');
        $this->db->where('ParentCod >', 0);
        $this->db->order_by('rand()');
        $this->db->limit($limit);
        $query = $this->db->get('Products');
        $result = $query->result();

        return $result;
    }

    public function getViewsProducts() {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);
        $Title = 'Title'.$clang;
        if(isset($_SESSION['views'])) {
            $arr = $_SESSION['views'];
        } else {
            $arr = null;
            return false;
        }

        $this->db->select('*, Title' . $clang . ' as Title,');
        $this->db->where('OnSite', 1);
        $this->db->where('Price >', 0);
        $this->db->where('TitleRO !=', '');
        $this->db->where('ParentCod >', 0);
        $this->db->where_in('Cod', $arr);
        $query = $this->db->get('Products');
        $result = $query->result();
        return $result;
    }
    public function getCartProducts($CodsArray='' ) {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);
        $Title = 'Title'.$clang;

        $this->db->select('*, Title' . $clang . ' as Title,');
        if($CodsArray != '') {
            $this->db->where_in('Cod', $CodsArray);
        }
        $this->db->where('OnSite', 1);
        $this->db->where('Price >', 0);
        $this->db->where('TitleRO !=', '');
        $this->db->where('ParentCod >', 0);
        $query = $this->db->get('Products');
        $result = $query->result();
        return $result;
    }
    public function getCartParentCod($CodsArray='' ) {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->distinct('ParentCod');
        $this->db->select('ParentCod');
        if($CodsArray != '') {
            $this->db->where_in('Cod', $CodsArray);
        }
        $this->db->where('OnSite', 1);
        $this->db->where('Price >', 0);
        $this->db->where('TitleRO !=', '');
        $this->db->where('ParentCod >', 0);
        $query = $this->db->get('Products');
        $result = $query->result_array();
        return $result;
    }

    public function getProductData($url='', $cod='' ) {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->select('Catalog.Title'.$clang.' as CategoryTitle, Catalog.UriName as CategoryUrl, Catalog.Cod as CategoryCod, Products.*, Products.Title' . $clang . ' as ProductTitle, Products.Description' . $clang . ' as ProductDesc, Products.OptimizationTitle' . $clang . ' as Title , Products.Keywords' . $clang . ' as Keywords, Products.Description' . $clang . ' as Description');
        $this->db->join('Catalog' , 'Catalog.Cod = Products.ParentCod');
        if($url != '') {
            $this->db->where('Products.UriName', $url);
        }
        if($cod != '') {
            $this->db->where('Products.Cod', $cod);
        }
        $this->db->where('Products.OnSite', 1);
        $this->db->where('Products.Price >', 0);
        $query = $this->db->get('Products');
        $result = $query->row_array();
        return $result;
    }

    function getCountProducts($category, $match, $cost, $cat=0, $pr, $brand, $new=0){
        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);
        $Title = 'Title'.$clang;

        $this->db->select('*');
        if($category != '') {
            $this->db->where('ParentCod', $category);
        }
        if($match != '') {
            $this->db->like($Title, $match);
        }
        if($new != 0) {
            $this->db->where('Products.New', $new);
        }
        if($pr != 0) {
            $this->db->where('Products.Promo', $pr);
        }
        switch ($cost) {
            case 1:
                $this->db->where('Price BETWEEN 10 AND 50');
                break;
            case 2:
                $this->db->where('Price BETWEEN 50 AND 200');
                break;
            case 3:
                $this->db->where('Price BETWEEN 200 AND 500');
                break;
            case 4:
                $this->db->where('Price >', 500);
                break;
        }
        if($cat != 0) {
            $this->db->where('ParentCod', $cat);
        }
        if($brand != '') {
            $this->db->where('Brand', $brand);
        }
        $this->db->where('OnSite', 1);
        $this->db->where('Price >', 0);
        $this->db->where('TitleRO !=', '');
        $this->db->where('ParentCod >', 0);
        $query = $this->db->get('Products');
        $count = $query->num_rows();
        return $count;
    }

    public function getBlocks($id) {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->select('image, name_' . $lclang . ' as BlockTitle ,  text_' . $lclang . ' as BlockText');
        $this->db->where('parent_id', $id);
        $this->db->where('view', '1');
        $this->db->order_by('Sorder');
        $query = $this->db->get('MenuBlocks');
        $result = $query->result_array();
        return $result;
    }

    public function getUserDate($email='', $pass='') {

        $this->db->select('*');
        if (!empty($email) && !empty($pass)) {
            $this->db->where('email', $email);
            $this->db->where('password', $pass);
        } else {
            $ID = $this->session->userdata('user_id');
            $this->db->where('ID', $ID);
        }
        $query = $this->db->get('SiteUser');
        $result = $query->row_array();
        return $result;
    }

    public function getUserDateByConfirm($confirm) {

        $this->db->select('*');
        $this->db->where('confirm', $confirm);
        $query = $this->db->get('SiteUser');
        $result = $query->row_array();
        return $result;
    }

    public function getOrdersByUser($UserID, $dates='') {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->select('Orders.*, Status.Name'.$clang.' as StatusName');
        $this->db->where('UserID', $UserID);
        if($dates !='') {
            $pizza = explode(' - ', $dates);
            $sd = date('Y-m-d', strtotime($pizza[0]));
            $ed = date('Y-m-d', strtotime($pizza[1]));
            $this->db->where('Date >=', $sd);
            $this->db->where('Date <=', $ed);
        }
        $this->db->from('Orders');
        $this->db->join('Status', 'Status.Value = Orders.status');
        $query = $this->db->get();
        $result = $query->result();

        foreach ($result as $value) {
            $items = $this->getItemsOrder($value->ID);
            $value->items = $items;
        }

        return $result;
    }

    public function getOrder($orderNr='') {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->select('Orders.*, Status.Name'.$clang.' as StatusName');
        $this->db->where('OrderNr', $orderNr);
        $this->db->from('Orders');
        $this->db->join('Status', 'Status.Value = Orders.status');
        $query = $this->db->get();
        $result = $query->row();

        $items = $this->getItemsOrder($result->ID);
        $result->items = $items;

        return $result;
    }

    public function getItemsOrder($id) {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->select('OrderItems.*, Products.Title'.$clang.' as NameProduct, Products.Image1, Products.Cod');
        $this->db->where('OrderItems.OrderID', $id);
        $this->db->from('OrderItems');
        $this->db->join('Products', 'Products.Cod = OrderItems.ItemCod');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getCity() {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->select('ID, Name' . $clang . ' as NameCity');
        $this->db->from('City');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function getBranches() {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->select('Address'.$clang.' as Address, Phone, Email, Type');
        $this->db->from('Branches');
        $this->db->order_by('Sorder', 'ASC');
        $this->db->order_by('ID', 'DESC');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getAgents() {

        $this->db->select('Agents.Image, Agents.District, Agents.Name, Agents.Phone');
        $this->db->from('Agents');
        $this->db->order_by('Agents.Sorder', 'ASC');
        $this->db->order_by('Agents.ID', 'DESC');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getDistricts() {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->select('ID, Name'.$clang.' as Name');
        $this->db->from('Districts');
        $this->db->order_by('Name', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getSiteUsers() {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->select('ID, companyname as Name');
        $this->db->from('SiteUser');
        $this->db->order_by("companyname", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function email_exist($key) {
        $this->db->where('email',$key);
        $query = $this->db->get('SiteUser');

        $count = $query->num_rows();
        return $count;
    }

    public function email_active($key) {
        $this->db->where('email',$key);
        $this->db->where('status !=', 0);
        $query = $this->db->get('SiteUser');

        $count = $query->num_rows();
        return $count;
    }

    public function can_log_in($type=1) {
        if($type==2) {
            $this->db->where('ID', $_SESSION['user_id']);
            $this->db->where('password', $this->input->post('currpassword'));
        } else {
            $this->db->where('email', $this->input->post('email'));
            $this->db->where('password', $this->input->post('password'));

        }
        $query = $this->db->get('SiteUser');

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function is_active($type = 1) {
        if($type==2) {
            $this->db->where('ID', $_SESSION['user_id']);
            $this->db->where('password', $this->input->post('currpassword'));
        } else {
            $this->db->where('email', $this->input->post('email'));
            $this->db->where('password', $this->input->post('password'));

        }
        $this->db->where('status !=', 0);

        $query = $this->db->get('SiteUser');

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function is_valid_key($key) {
        $this->db->where('confirm', $key);
        $query = $this->db->get('SiteUser');

        if($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }
/*
    public function get_uri_data_array($categories = [], $parentID = 0)
    {
        if (empty($categories)) {
            return [];
        }

        $result = [];

        foreach ($categories as $category) {
            if ($category->parentID == $parentID) {
                $result[$category->uri]['data'] = $category;
                if (check_children($categories, $category->ID)) {
                    $result[$category->uri]['children'] = $this->get_uri_data_array($categories, $category->ID);
                }
            }
        }

        return $result;
    }

    public function check_children() {}
    */
    public function getCategoryForMenu($cat=0) {
        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $result = [];

        $this->db->select('Catalog.Title'.$clang.' as Title, Catalog.Cod, Catalog.ParentCod, Catalog.UriName');
        if($cat !=0 ) {
            $this->db->where('Catalog.Cod', $cat);
            $this->db->or_where('Catalog.ParentCod', $cat);
        } 
        $this->db->where('Catalog.is_active', 1);
        //$this->db->or_where('Catalog.ParentCod', 0);

        $this->db->order_by('Catalog.Title'.$clang, 'Asc');
        $query = $this->db->get('Catalog')->result();
        $i=-1;

        $result = categories_map($query, 0);

        return $result;
    }

    public function getAutoProducts($match) {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);
        $Title = 'Title'.$clang;

        $this->db->select('*, Title' . $clang . ' as Title,');

        if($match != '') {
            $this->db->like($Title, $match);
        }

        $this->db->where('OnSite', 1);
        $this->db->where('Price >', 0);
        $this->db->where('TitleRO !=', '');
        $this->db->where('ParentCod >', 0);
        $this->db->limit(10);
        $query = $this->db->get('Products');
        $result = $query->result();
        return $result;
    }

    public function getInterviews() {
        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->select('*, Name' . $clang . ' as Name,');
        $result = $this->db->where('OnSite', 1)->order_by('Sorder', 'Asc')->order_by('ID', 'Desc')
        ->get('Interviews')->result();
        return $result;
    }

    public function getInterview($ID) {
        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->select('*, Name' . $clang . ' as Name,');
        $this->db->where('ID', $ID);
        $result = $this->db->get('Interviews')->row();
        return $result;
    }

    public function getQuestions($ID) {
        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->select('*, Name' . $clang . ' as Name,');
        $result = $this->db->where('InterviewID', $ID)
        ->where('OnSite', 1)->order_by('Sorder', 'ASC')->order_by('ID', 'Desc')->get('Questions')->result();
        return $result;
    }

    public function getAnswers($IDs) {
        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $this->db->select('*, Ans' . $clang . ' as Ans,');
        $result = $this->db->where_in('QuestionID', $IDs)
        ->order_by('ID', 'ASC')->get('Answers')->result();
        return $result;
    }

    public function getIp($user_ip, $interview_id) {    
        $this->db->select('*');
        $result = $this->db->where('user_ip', $user_ip)
        ->where('interview_id', $interview_id)
        ->get('InterviewsIP')->result();
        return $result;
    }
    

}
