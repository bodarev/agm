<?php

class Insert_model extends CI_Model {

    function insert_user($date) {

        $query= $this->db->insert('SiteUser', $date);
        $insert_id = $this->db->insert_id();

        if($query) {
            return $insert_id;
        } else {
            return 0;
        }

    }

    function insert_order($order, $items) {

        $query= $this->db->insert('Orders', $order);
        $insert_id = $this->db->insert_id();

        $first = rand(1, 9);
        $last = rand(1, 9);

        $orderNr = $first.$insert_id.$last;

        $data = array(
            'OrderNr' => $orderNr
        );

        $this->db->where('ID', $insert_id);
        $query =  $this->db->update('Orders', $data);

        foreach($items as $key=>$item) {
            $items[$key]['OrderNr'] = $orderNr;
            $items[$key]['OrderID'] = $insert_id;
        }

        $this->db->insert_batch('OrderItems', $items);

/*         $date = array(
            'OrderID' => $insert_id
        );

        $this->db->where('OrderNr', $order['OrderNr']);
        $query =  $this->db->update('OrderItems', $date); */

        if($query) {
            return $orderNr;
        } else {
            return 0;
        }

    }
    function insert_address($block) {

        $query = $this->db->insert_batch('Address', $block);

        if($query) {
            return true;
        } else {
            return false;
        }
    }

    function insert_ip($data) {

        $query = $this->db->insert('InterviewsIP', $data);

        if($query) {
            return true;
        } else {
            return false;
        }
    }
}

?>