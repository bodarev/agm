<?php

class Update_model extends CI_Model {

    function update_password($key) {
        $data = array(
            'email' => $this->input->post('email'),
            'password' => $key
        );
        $this->db->where('email', $this->input->post('email'));
        $query =  $this->db->update('SiteUser', $data);

        if($query) {
            return true;
        } else {
            return false;
        }
    }

    function update_user_date($date) {
        if(isset($date['confirm'])) {
            $this->db->where('confirm', $date['confirm']);
        } else {
            $this->db->where('ID', $_SESSION['user_id']);
        }
        $query =  $this->db->update('SiteUser', $date);

        if($query) {
            return true;
        } else {
            return false;
        }
    }

    function update_order($data, $nr_order) {
        $this->db->where('OrderNr', $nr_order);
        $query =  $this->db->update('Orders', $data);

        if($query) {
            return true;
        } else {
            return false;
        }
    }

    function update_count($data) {
        foreach ($data as $id ) {

            $this->db->where('ID', $id);
            $count =  $this->db->select('*')->get('Answers')->row()->Count;

            $this->db->where('ID', $id);
            $this->db->set('Count', $count+1);
            $query =  $this->db->update('Answers');    
        }
    }
}

?>