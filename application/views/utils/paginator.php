<? require realpath('libs/vendor').'/autoload.php';

use JasonGrimes\Paginator;

// Paginator Config
$p_page 		= (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
$p_page 		= ($p_page == 0) ? 1 : $p_page;
$p_per_page		= NR_PER_PAGE;
$p_offset		= $p_per_page * ($p_page - 1);
$count_results = $count;

$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
$string = '';
foreach ($_GET as $key => $value) {
    if($key != 'page') {
        $string .= $key.'='.$value.'&';
    }
}
$urlPattern = $uri_parts[0].'?'.$string.'page=(:num)';

if (!isset($count_results)) {
    $count_results = 0;
}
$paginator = new Paginator($count_results, $p_per_page, $p_page, $urlPattern);
$paginator->setMaxPagesToShow(5);
if ($paginator->getNumPages() > 1) {  ?>
    <div class="pagination pagination_wr">
    <div class="pg_nav">

        <? if ($paginator->getPrevUrl()) { ?>
        <a class="prev" href="<?php echo $paginator->getPrevUrl(); ?>"><?=LINK_PREV?></a>
        <? } ?>
        <? if ($paginator->getNextUrl()) { ?>
        <a class="next" href="<?php echo $paginator->getNextUrl(); ?>"><?=LINK_NEXT?></a>
        <? } ?>

    </div>  

    <div class="pg_num">

        <? foreach ($paginator->getPages() as $page) { ?>
        <? if ($page['url']){ ?>
            <a <? echo $page['isCurrent'] ? 'class="active"' : ''; ?> href="<?php echo $page['url']; ?>"><?php echo $page['num']; ?></a>
        <?  } else {  ?>
            <a class="disabled"><span><?=$page['num']?></span></a>
        <? } ?>
        <? } ?>

    </div>  
    </div>
<? } ?>