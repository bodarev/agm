<?
$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
?>
<style>
/* width */
::-webkit-scrollbar {
    width: 7px;
}
/* Track */
::-webkit-scrollbar-track {
    background: #f1f1f1; 
}
/* Handle */
::-webkit-scrollbar-thumb {
    background: #285299; 
}
/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
    background: #285299; 
}
</style>

<div class="tabs_wr1">
    <div class="tabs" style="display:none">
        <ul>
        </ul>
    </div>
    <div class="tab_content">
        <div id="tab_a1">
        <div class="" style="overflow:auto; width:100%; height:555px;">
            <div class="cols">
            <div class="col" id="col1">
            </div>
            <div class="col" id="col2">
            </div>
            <div class="col" id="col3">
            </div>
            </div>
        </div>
        </div>
    </div>
</div>

<script>

    $(document).ready(function(e) {

        var lang = "<?=$lclang?>";   
        var result = <? echo json_encode($categorii)?>;

        $.each(result, function (index, value) {
            elem = "<div class='title'><a href='/"+ lang +"/pages/catalog/" + value.UriName + "'>" + value.Title + "</a></div>";
            childsEach(value.childs);
            displayCategories(elem);  
        });

        function childsEach(childs) {
            if(childs.length !=0) {
                elem +="<ul>";
                $.each(childs, function (index, value) {
                    elem += "<li><a href='/"+ lang +"/pages/catalog/" + value.UriName + "'>" + value.Title + "</a>";
                    childsEach(value.childs);
                    elem += "</li>";
                });
                elem +="</ul>";
            } 
        }

        function displayCategories(elem) {
            h1 = $('div#col1 li').length;
            h2 = $('div#col2 li').length;
            h3 = $('div#col3 li').length;

            min = Math.min(h1, h2, h3);

            switch(min) {
            case h1:
            $( "div#col1" ).append( elem );
                break;
            case h2:
            $( "div#col2" ).append( elem );
                break;
            case h3:
            $( "div#col3" ).append( elem );
                break;
            }
        }    
    });
</script>

