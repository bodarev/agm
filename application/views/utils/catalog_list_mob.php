<?
$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
?>
<script>

$(document).ready(function(e) {

    var lang = "<?=$lclang?>";   
    var result = <? echo json_encode($categorii)?>;

    $.each(result, function (index, value) {
        elem = "<div class='head'>" + value.Title + "</div>";
        childsEach(value.childs);
        displayCategories(elem);  
    });

    function childsEach(childs) {
        elem +="<ul>";
        if(childs.length !=0) {
            $.each(childs, function (index, value) {
                elem += "<li><a href='/"+ lang +"/pages/catalog/" + value.UriName + "'>" + value.Title + "</a>";
                childsEach(value.childs);
                elem += "</li>";
            });
        }
        elem +="</ul>";
    }

    function displayCategories(elem) {
        $( ".h_cat_mob" ).append( elem );  
    } 
});
</script>
