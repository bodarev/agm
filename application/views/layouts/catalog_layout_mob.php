<?
$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
?>

<?
 $this->db->select('Title'.$clang.' as Title, Cod');
 $this->db->order_by('ID', 'DESC');
 $this->db->where('ParentCod', 0);
 $query = $this->db->get('Catalog');

foreach ($query->result() as $row) {

    $this->db->where('ParentCod', $row->Cod);
    $this->db->from('Catalog');
    $scount = $this->db->count_all_results();

    if ($scount > 0 ) {

        echo "<div class='head'>".$row->Title."</div>";
        echo "<div>";
        echo "<ul>";

        $this->db->select('Title'.$clang.' as Title, Cod, UriName');
        $this->db->order_by('ID', 'DESC');
        $this->db->where('ParentCod', $row->Cod);
        $query2 = $this->db->get('Catalog');

        foreach ($query2->result() as $item) {
            echo "<li><a href='/".$item->UriName."'>".$item->Title."</a></li>";
        }

        echo "</ul>";
        echo "</div>";
    }
}
?>