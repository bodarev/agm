<?
$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
?>
<div class="tabs_wr1">
    <div class="tabs" style="display:none">
        <ul>
        </ul>
    </div>
    <div class="tab_content">
        <div id="tab_a1">
        <div class="scroll-pane">
            <div class="cols">
            <div class="col">
            <?php

            // scoate nr de categorii parinte

            $this->db->from('Catalog T1');
            $this->db->where('T1.ParentCod', 0);
            $this->db->where("(select count(Cod) from Catalog T2 where T2.ParentCod = T1.Cod) >", 0);
            $count = $this->db->count_all_results();

            // calculam cate treb sa apara intr-o coloana

            $result = $count/3;

            $intreg = floor($result);      
            $fraction = $result - $intreg;

            if($fraction != 0) {
                $col = $intreg + 1;
            } else {
                $col = $intreg;
            }

            // scaotem categoriile parinte

            $this->db->select('Title'.$clang.' as Title, Cod');
            $this->db->order_by('ID', 'DESC');
            $this->db->where('ParentCod', 0);
            $query = $this->db->get('Catalog');

            // scoatem toate subcategoriile

            $i=0;

             foreach ($query->result() as $row) {

                if($i>=$col) {
                    echo "</div>";
                    echo "<div class='col'>";
                    $i=0;
                } 

                $this->db->where('ParentCod', $row->Cod);
                $this->db->from('Catalog');
                $scount = $this->db->count_all_results();

                // verificam daca ele sunt 

                if ($scount > 0 ) {

                    $i++;
        
                    echo "<div class='title'>".$row->Title."</div>";
                    echo "<ul>";

                    $this->db->select('Title'.$clang.' as Title, Cod, UriName');
                    $this->db->order_by('ID', 'DESC');
                    $this->db->where('ParentCod', $row->Cod);
                    $query2 = $this->db->get('Catalog');

                    foreach ($query2->result() as $item) {
                        echo "<li><a href='/".$item->UriName."'>".$item->Title."</a></li>";
                    }
                    
                    echo "</ul>";
                }
                
            } ?>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>