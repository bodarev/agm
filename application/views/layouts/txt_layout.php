<?
$uri1=addslashes($this->uri->segment(1));
$uri2=addslashes($this->uri->segment(2));
$uri3=addslashes($this->uri->segment(3));
$uri4=addslashes($this->uri->segment(4));
$uri5=addslashes($this->uri->segment(5));

$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);

$adline = "";
if(!empty($uri2))
    $adline .= $uri2."/";

if(!empty($uri3))
    $adline .= $uri3."/";

if(!empty($uri4))
    $adline .= $uri4."/";

if(!empty($uri5))
    $adline .= $uri5."/";


$main_uri='/'.$this->uri->uri_string().'/';
if (empty($uri2)) {
if ($lclang=='ru') $changelang='/ro/'; else $changelang='/ru/';
} else {
	if ($lclang=='ru') $changelang=str_replace('/ru/','/ro/',$main_uri); else $changelang=str_replace('/ro/','/ru/',$main_uri);
}

if(CalcTotal() >= VALUE_MINFREEDELIVERY) {
  $deliveryprice = 0;
} else {
  $deliveryprice = VALUE_DELIVERYPRICE;
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title><?=$tab_title?></title>
  <meta name="keywords" content="<?=$keywords_for_layout?>" />
  <meta name="description" content="<?=$description_for_layout?>" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <script src="/js/jquery-2.1.1.min.js"></script>
  <script src="/js/jquery-ui.min.js"></script>
  <script src="/js/jquery.formstyler.min.js"></script>
  <script src="/js/jquery.jscrollpane.min.js"></script>
  <script src="/js/jquery.mousewheel.js"></script>
  <script src="/js/chosen.jquery.min.js"></script>
  <script src="/js/code.js?v=<?=time()?>"></script>
  <script src="/js/MaskedInput.js" type="text/javascript"></script>
  <link rel="stylesheet" href="/css/jquery-ui.min.css">
  <link rel="stylesheet" href="/css/jquery-ui.structure.min.css">
  <link rel="stylesheet" href="/css/jquery.formstyler.css">
  <link rel="stylesheet" href="/css/jquery.jscrollpane.css">
  <link rel="stylesheet" href="/css/chosen.min.css">
  <link rel="stylesheet" href="/css/slick.css">
  <link rel="stylesheet" href="/css/bt_style.css?v=<?=time()?>">
  <link rel="stylesheet" href="/css/css.css?v=<?=time()?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!--[if lt IE 9]>
  <script src="/js/html5shiv.min.js"></script>
  <![endif]-->
  <script>
      (function($) {
          $(function() {
              $('input, select').styler({
                  selectPlaceholder: '',
                  selectVisibleOptions: 5
              });
              $('.switch input').styler('destroy');
              $('.switch1 input').styler('destroy');
          });
      })(jQuery);
  </script>
  <script>
      $(function(){
          $('.scroll-pane').jScrollPane({
              autoReinitialise: true,
              autoReinitialiseDelay: 300
          });
      });
  </script>
  <script>
      $(function(){
        $('.tabs_wr').tabs(
                {
                    active: <?= (isset($_SESSION['active_tab'])) ? $_SESSION['active_tab'] : 0 ?>
                }
            );
      });
  </script>
  <script>
      $(function(){
          $('.tabs_wr1').tabs({
              event: 'mouseover'
          }).addClass('ui-tabs-vertical ui-helper-clearfix');
          $('.tabs_wr1 li').removeClass('ui-corner-top').addClass('ui-corner-left');
      });
  </script>
 
    <script type='text/javascript'>
    (function(){ var widget_id = 'DXiCw47riU';var d=document;var w=window;function l(){
    var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
  <!-- Global site tag (gtag.js) - Google Ads: 792712608 --> 
  <script async src="https://www.googletagmanager.com/gtag/js?id=AW-792712608"></script> 
  <script> 
    window.dataLayer = window.dataLayer || []; function gtag()
    {dataLayer.push(arguments);}
    gtag('js', new Date()); gtag('config', 'AW-792712608'); 
  </script>
  <?= (isset($conversion_script)) ? $conversion_script : ''; ?>
</head>
<body>
<body>
<!--wrapper-->
<div id="wrapper">
  <!--header-->
    <?
    $top_menu=$this->db->where('OnTop',1)->order_by('Sorder ASC,ID DESC')->get('TopMenu')->result_array();
    $bot_menu=$this->db->where('BottomMenu',1)->order_by('Sorder ASC,ID DESC')->get('TopMenu')->result_array();
    ?>
  <div id="header">
    <div class="row v1">
      <div class="in">
        <div class="col v1">
          <div class="logo"><a href="/"><img src="/public/i/logo.png" alt="#"></a></div>
          <div class="h_cat">
            <div class="ctrl"><?=BUTTON_CATALOG?></div>
            <div class="ddown">
            <? 
              $this->load->view('utils/catalog_list');
            ?>
            </div>
          </div>
          <ul class="h_menu">
              <?
              $ad1 = "";
              foreach($top_menu as $row) {
                  if($uri3 == $row['UriName'] || $row['UriName'] == 'signup-dealers' ) $ad1 = " active";
                  else $ad1 = "";
                  if ($row['UriName'] == 'signup-dealers' && isset($_SESSION['type']) && $_SESSION['type']==3) {
                    $ad1 = " activea"; 
                    echo '<li class="'.$ad1.'"><a href="/'.$lclang.'/pages/profile/">'.LINK_QUICK_ORDER.'</a></li>';
                  } else {
                    echo '<li class="'.$ad1.'"><a href="/'.$lclang.'/pages/'.$row['UriName'].'/">'.$row['Title'.$clang].'</a></li>';
                  } 
              }
              ?>
          </ul>
          <div class="m_ctrl"></div>
        </div>
        <div class="col v2">
          <div class="search_ctrl_mob"></div>
          <div class="lang">
            <div class="head"><img src="/public/i/<?
                if($lclang == 'ru') { echo 'lang1.png'; }
                elseif($lclang == 'ro') { echo 'lang2.png'; }
                elseif($lclang == 'en') { echo 'lang3.png'; }
                ?>" alt="">
                <?
                if($lclang == 'ru') { echo 'Русский'; }
                elseif($lclang == 'ro') { echo 'Romana'; }
                elseif($lclang == 'en') { echo 'English'; }
                ?>
            </div>
            <div class="ddown">
              <a href="/ru/<?=$adline?>"><img src="/public/i/lang1.png" alt=""> Русский</a>
              <a href="/ro/<?=$adline?>"><img src="/public/i/lang2.png" alt=""> Romana</a>
              <a href="/en/<?=$adline?>"><img src="/public/i/lang3.png" alt=""> English</a>
            </div>
          </div>
          <div class="h_login">
              <?php if( isAuthorized() ) { ?>
                <a href="/<?=$lclang?>/pages/profile/"><img src="/public/i/ico_a1.png" alt=""> <?=$_SESSION['user_name']; ?></a>
                <div class="ddown">
                  <a href="/<?=$lclang?>/pages/profile/"><img src="/public/i/ico_a1.png" alt=""> <?=$_SESSION['user_name']; ?></a>
                  <a href="/<?=$lclang?>/pages/logout/"><img src="/public/i/logout.png" width="18" height="18" alt=""> <?=LINK_LOGOUT ?></a>
                </div>
              <?php } else { ?>
                <a href="/<?=$lclang?>/pages/login/"><img src="/public/i/ico_a1.png" alt=""> <?=LINK_LOGIN?></a>
              <?php } ?>
          </div>
          <div class="h_cart"><a href="/<?=$lclang?>/pages/cart/"> <? displayCartOnHeader() ?> </a></div>
        </div>
      </div>
    </div>
    <div class="row v2">
      <div class="in">
        <div class="search_form">
          <form method="GET" action="/<?=$lclang?>/pages/search">
            <input name="match" type="text" autocomplete="off" placeholder="<?=PLACEH_SEARCH?>">
            <button><?=BUTTON_SEARCH?></button>
          </form>
          <div class="search_result">
          </div>
        </div>
      </div>
    </div>
    <div class="search_form sf_mob">
      <form method="GET" action="/<?=$lclang?>/pages/search">
        <input name="match" type="text" autocomplete="off" placeholder="<?=PLACEH_SEARCH?>">
        <button><?=BUTTON_SEARCH?></button>
      </form>
      <div class="search_result">
      </div>
    </div>
    <div class="mob_menu_wr">
      <div class="mob_menu_in">
        <div class="close"></div>
        <div class="head">
          <div class="lang">
            <div class="head"><img src="/public/i/<?
                if($lclang == 'ru') { echo 'lang1.png'; }
                elseif($lclang == 'ro') { echo 'lang2.png'; }
                elseif($lclang == 'en') { echo 'lang3.png'; }
                ?>" alt="">
                <?
                if($lclang == 'ru') { echo 'Русский'; }
                elseif($lclang == 'ro') { echo 'Romana'; }
                elseif($lclang == 'en') { echo 'English'; }
                ?>
            </div>
            <div class="ddown">
              <a href="/ru/<?=$adline?>"><img src="/public/i/lang1.png" alt=""> Русский</a>
              <a href="/ro/<?=$adline?>"><img src="/public/i/lang2.png" alt=""> Romana</a>
              <a href="/en/<?=$adline?>"><img src="/public/i/lang3.png" alt=""> English</a>
            </div>
          </div>
          <div class="h_login">
              <?php if( isAuthorized() ) { ?>
                <a href="/<?=$lclang?>/pages/profile/"><img src="/public/i/ico_a1.png" alt=""> <?=$_SESSION['user_name']; ?></a>
                <div class="ddown">
                  <a href="/<?=$lclang?>/pages/profile/"><img src="/public/i/ico_a1.png" alt=""> <?=$_SESSION['user_name']; ?></a>
                  <a href="/<?=$lclang?>/pages/logout/"><img src="/public/i/logout.png" width="18" height="18" alt=""> <?=LINK_LOGOUT ?></a>
                </div>
              <?php } else { ?>
                <a href="/<?=$lclang?>/pages/login/"><img src="/public/i/ico_a1.png" alt=""> <?=LINK_LOGIN?></a>
              <?php } ?>
          </div>
          <div class="h_cart"><a href="/<?=$lclang?>/pages/cart/"> <? displayCartOnHeader() ?> </a></div>
          <div class="h_cat">
            <div class="ctrl"><?=BUTTON_CATALOG?></div>
            <div class="ddown">
              <div class="h_cat_mob">
              <? 
                $this->load->view('utils/catalog_list_mob');
              ?>
              </div>
            </div>
          </div>
        </div>
        <ul class="mob_menu">
            <?
            foreach($top_menu as $row) {
                if($uri3 == $row['UriName'] || $row['UriName'] == 'signup-dealers' ) $ad1 = " active";
                else $ad1 = "";
                if ($row['UriName'] == 'signup-dealers' && isset($_SESSION['type']) && $_SESSION['type']==3) {
                  $ad1 = " activea"; 
                  echo '<li class="'.$ad1.'"><a href="/'.$lclang.'/pages/profile/">'.LINK_QUICK_ORDER.'</a></li>';
                } else {
                  echo '<li class="'.$ad1.'"><a href="/'.$lclang.'/pages/'.$row['UriName'].'/">'.$row['Title'.$clang].'</a></li>';
                } 
            }
            ?>
        </ul>
        <div class="foot">
          <div class="mailer">
            <div class="title"><?=TEXT_SUBSCRIBE?></div>
            <div class="ok hide"><?=TEXT_SUBSCRIBEOK?></div>
            <div class="nok hide"><?=TEXT_SUBSCRIBENOK?></div>
            <form method="POST" class="subscribe" >
              <input type="email" name="subscribe" id="subscribe" placeholder="<?=PLACEH_SUBSCRIBE?>">
              <i class="fa fa-spinner fa-spin hide"></i><input type="submit" id="sub_button" value="<?=BUTTON_SUBSCRIBE?>">
            </form>
          </div>
          <div class="social">
          <a href="<?=SNETWORKS_VK?>"><img src="/public/i/social1.png" alt=""></a>
            <a href="<?=SNETWORKS_FB?>"><img src="/public/i/social2.png" alt=""></a>
            <a href="<?=SNETWORKS_IN?>"><img src="/public/i/social3.png" alt=""></a>
            <a href="<?=SNETWORKS_TW?>"><img src="/public/i/social4.png" alt=""></a>
            <a href="<?=SNETWORKS_YT?>"><img src="/public/i/social5.png" alt=""></a>
          </div>
        </div>
      </div>
    </div>
  </div><!--/header-->
  <!--main-->
  <div id="main" class="clearfix">
    <div class="warning2">
      <div class="in">
        <p><?=TEXT_INCONSTRUCTION?></p>
      </div>
    </div>
    <div class="c_wr7">
      <div class="in">
        <?php if (!empty($main_image)) { ?>
          <div class="thumb_b"><img src="/public/topmenu/<?=$main_image?>" alt=""></div>
        <?php } ?>
        <div class="b_info7">
          <? foreach( $blocks as $item) { ?>
            <div class="item">
              <?php if ($item['image']!='') { ?>
                <div class="thumb"><img src="/public/menublocks/<?=$item['image']?>" alt=""></div>
              <? } ?>
              <div class="descr">
                <h2 class="title1"><?=$item['BlockTitle']?></h2>
                <p><?=$item['BlockText']?></p>
              </div>
            </div>
         <? } ?>
        </div>
      </div>
    </div>
    <input type="text" hidden name="addtocartmsg" class="addtocartmsg" value="<?=TEXT_ADDTOCART?>">  
    <input type="text" hidden name="curency" class="curency" value="<?=TEXT_MDL?>">  
    <input type="text" hidden name="deliveryprice" class="deliveryprice" value="<?=$deliveryprice?>">
    <div class="rev_btn" style="display:<?= (isset($_SESSION['is_passed']) && $_SESSION['is_passed']==true) ? 'none' : 'block' ?>">
        <a href="/<?=$lclang?>/pages/interview">
            <i class="ico"></i>
            <span><?=TEXT_YOUROPINION?></span>
        </a>
    </div>  
  </div><!--/main-->
  <!--footer-->
  <div id="footer">
    <div class="in">
      <div class="row">
        <div class="col">
          <ul class="f_menu">
            <?php
            foreach($bot_menu as $row) {
                echo '<li><a href="/'.$lclang.'/pages/'.$row['UriName'].'/">'.$row['Title'.$clang].'</a></li>';
            }
            ?>
          </ul>
        </div>
        <div class="col">
          <div class="contacts">
            <p><a href="tel:'.<?=TEXT_CONTACTPHONE?>.'"><?=TEXT_CONTACTPHONE?></a></p>
            <p><a href="mailto:'<?=TEXT_EMAIL?>'"><?=TEXT_EMAIL?></a></p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <div class="copy">
            <?=date('Y').' '.TEXT_COPYRIGHT?>
            <br>
            <a href="http://www.ilab.md/" target="_blank" class="f-dev">
              <img src="/img/ilab_logo_gray.png" class="f-dev-img" alt="ilab.md">&nbsp;
              <?
              if($lclang == 'ru') {
                  echo 'Разработка сайта - ilab.md';
              } elseif ($lclang == 'ro') {
                  echo 'Elaborarea siteului - ilab.md';
              } else {
                  echo 'Website development - ilab.md';
              }
              ?>
				    </a>
          </div>
          <div class="mailer">
            <div class="title"><?=TEXT_SUBSCRIBE?></div>
            <div class="ok hide"><?=TEXT_SUBSCRIBEOK?></div>
            <div class="nok hide"><?=TEXT_SUBSCRIBENOK?></div>
            <form method="POST" class="subscribe" >
              <input type="email" name="subscribe" id="subscribe" placeholder="<?=PLACEH_SUBSCRIBE?>">
              <i class="fa fa-spinner fa-spin hide"></i><input type="submit" id="sub_button" value="<?=BUTTON_SUBSCRIBE?>">
            </form>
          </div>
          <div style="margin-top: 20px;">
            <img src="/img/visa.png" alt="visa" height="35px">
          </div>
        </div>
        <div class="col">
          <div class="social">
          <a href="<?=SNETWORKS_VK?>"><img src="/public/i/social1.png" alt=""></a>
            <a href="<?=SNETWORKS_FB?>"><img src="/public/i/social2.png" alt=""></a>
            <a href="<?=SNETWORKS_IN?>"><img src="/public/i/social3.png" alt=""></a>
            <a href="<?=SNETWORKS_TW?>"><img src="/public/i/social4.png" alt=""></a>
            <a href="<?=SNETWORKS_YT?>"><img src="/public/i/social5.png" alt=""></a>
          </div>
        </div>
      </div>
    </div>
  </div><!--/footer-->
</div><!--/wrapper-->
<script src="/js/slick.min.js"></script>
<script>
    $(document).ready(function(){
        $('.slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            appendArrows: '.slider_nav'
        });
        $('.carousel').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1001,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 601,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    });
</script>
 <script>
      $(function(){
          $('.h_cat_mob').accordion({
              header: '.head',
              heightStyle: 'content',
              active: false,
              collapsible: true
          });
      });
  </script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119826831-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
 
  gtag('config', 'UA-119826831-1');
</script>
</body>
</html>