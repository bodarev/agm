<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$_SERVER['HTTP_HOST']?> - Admin area</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/theme/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<link href="/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
<link href="/theme/assets/global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css"/>
<link href="/theme/assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="/theme/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="/theme/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="/theme/assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>
<link href="/theme/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<link href="/theme/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css"/>
<link href="/css/chosen.css" rel="stylesheet" type="text/css"/>
<link href="/css/colorpicker.css" rel="stylesheet" type="text/css"/>
<style>
.chosen-container {width:100%!important;}
.branch td:nth-child(2),
.branch td:nth-child(3) {color:#09128b;font-weight:bold;}
</style>
<!--[if lt IE 9]>
<script src="/theme/assets/global/plugins/respond.min.js"></script>
<script src="/theme/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="/theme/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/theme/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="/theme/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/theme/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/theme/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="/theme/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
<script src="/theme/assets/global/plugins/jquery-multi-select/js/jquery.quicksearch.js" type="text/javascript"></script>
<script src="/theme/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script src="/theme/assets/global/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.ru.js" type="text/javascript"></script>
<script src="/theme/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="/theme/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="/theme/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="/theme/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="/js/jquery.tablednd.js" type="text/javascript"></script>
<script src="/js/chosen.jquery.js" type="text/javascript"></script>
<script src="/js/colorpicker.js" type="text/javascript"></script>
<script src="/js/MaskedInput.js" type="text/javascript"></script>
<script src="/js/admin-code.js?v=<?=time()?>" type="text/javascript"></script>
<script src="/js/load.image.js" type="text/javascript"></script>
<link href="/css/jquery-ui2.css" rel="stylesheet">
<script>
jQuery(document).ready(function() {   
   // initiate layout and plugins
	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	QuickSidebar.init(); // init quick sidebar
	Demo.init(); // init demo features
	$(".dragger").tableDnD({
		onDrop: function(table, row) {
	        localsort();
	    }
	});
	$(".dragger2").tableDnD({
		onDrop: function(table, row) {
	        localsort2();
	    }
	});

	$(".dragger_loc").tableDnD({
		onDrop: function(table, row) {
	        localsort(table);
	    }
	});

	$('.red-stripe').click(function(e) {
		if (confirm('Вы уверены?')) {
		
		} else {
			e.preventDefault();
			return false;
		}
	});
	
	$('.chosen').chosen();
});
</script>
<style>
#fade-respond {	
	position: fixed;
    top: 70px;
    right: 100px;
    padding: 10px;
    border-radius: 7px !important;
    box-shadow: 3px 4px 2px #4c525138;
    display: none;
}
.portlet-title {cursor:pointer;}
.tablist td {vertical-align:middle!important;padding:5px!important;}
.portlet.box {margin-bottom:0;}
.btn.default.yellow-stripe {}
.btn.default.yellow-stripe.collapse {display:block;visibility:visible;}
.alertmsg {
	position: fixed;
	top: 70px;
	right: 100px;
	padding: 10px;
	background-color: #35aa47;
	color: #fff;
	border-radius:7px !important;
	box-shadow: 3px 4px 2px #4c525138;
	display:none;
}
</style>
<script>
$(document).ready(function() {
	 $('.date-picker').datepicker({
                orientation: "left",
                autoclose: true,
				language: 'ru-RU'
            });
});
</script>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content">

<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
      <img src="/public/i/logoiwh.png" alt="logo" style="width:95px; margin:9px 0 0 0;" class="logo-default">
		</div>
		<!-- END LOGO -->
		<!-- BEGIN HORIZANTAL MENU -->
		<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
		<!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) sidebar menu below. So the horizontal menu has 2 seperate versions -->
		<div class="hor-menu hor-menu-light hidden-sm hidden-xs">
			<ul class="nav navbar-nav">
				<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
				<?
				$uri2 = $this->uri->segment(2);
				
			$menu = array(
			"topmenu" => array('name'=>"Меню сайта",'ico'=>'<i class="icon-layers"></i>'),
          	"slider" => array('name'=>"Слаидер",'ico'=>'<i class="fa fa-sliders"></i>'),
          	"info"  => array('name'=>"Информационный блок",'ico'=>'<i class="fa fa-star"></i>'),
       	    "homecategory" => array('name'=>"Категорий на главной",'ico'=>'<i class="fa fa-star"></i>'),
       	    "hometabs" => array('name'=>"Табы на главной",'ico'=>'<i class="icon-folder"></i>'),
			"slidercategory" => array('name'=>"Слаидер с Категориями",'ico'=>'<i class="fa fa-sliders"></i>'),				
			"brands" => array('name'=>"Брэнды",'ico'=>'<i class="icon-paper-clip"></i>'),
       	   "branches"  => array('name'=>"Филиалы",'ico'=>'<i class="icon-pointer"></i>'),
      	    "agents"  => array('name'=>"Список агентов",'ico'=>'<i class="icon-users"></i>'),
      	    "districts" => array('name'=>"Районы",'ico'=>'<i class="icon-map"></i>'),
      	    "city"  => array('name'=>"Города",'ico'=>'<i class="icon-globe"></i>'),
          	"orders"  => array('name'=>"Заказы",'ico'=>'<i class="fa fa-archive"></i>'),
          	"dealers"  => array('name'=>"Дилеры",'ico'=>'<i class="icon-user"></i>'),
          	"users"  => array('name'=>"Пользователи",'ico'=>'<i class="icon-user"></i>'),
          	"interviews"  => array('name'=>"Опросы",'ico'=>'<i class="fa fa-book"></i>'),
          	"logs"  => array('name'=>"Логи",'ico'=>'<i class="fa fa-history"></i>'),
			"constants" => array('name'=>"Константы",'ico'=>'<i class="icon-wrench"></i>')
			);
					
				?>
			</ul>
		</div>
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<li class="dropdown dropdown-user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="icon-user"></i>
					<span class="username username-hide-on-mobile">
					<?=$_SESSION['login']?> </span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<li>
							<a href="/<?=ADM_CONTROLLER?>/admins/" class="olink"><i class="icon-user"></i>Админы</a>
						</li>
						<li>
							<a href="https://<?=$_SERVER['HTTP_HOST']?>/" target="_blank"><i class="icon-globe"></i><?=$_SERVER['HTTP_HOST']?></a>
						</li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-quick-sidebar-toggler">
					<a href="/<?=ADM_CONTROLLER?>/logout" class="dropdown-toggle">
					<i class="icon-logout"></i>
					</a>
				</li>
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<div class="page-sidebar navbar-collapse collapse">
			<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<li>&nbsp;</li>
				<?
        $delearcount=$this->db->select('count(*) as count')
            ->where('type', '2')
            ->where('status', '0')
            ->from('SiteUser')
            ->get()
			->row()->count;
			
        $ordercount=$this->db->select('count(*) as count')
            ->where('status', '0')
            ->from('Orders')
            ->get()
            ->row()->count;

				foreach($menu as $key=>$val) {
					if ($key==$uri2) $dop=' active'; else $dop='';
					echo '<li class="'.$dop.'">';
						echo '<a href="/'.ADM_CONTROLLER.'/'.$key.'/">';
							echo $val['ico'];
							echo '<span class="title">'.$val['name'].'</span>';
							if($key == 'dealers' && $delearcount > 0 ) {
							  echo "<span class='badge badge-roundless badge-danger'>+$delearcount</span>";
              				}
							if($key == 'orders' && $ordercount > 0 ) {
							  echo "<span class='badge badge-roundless badge-danger'>+$ordercount</span>";
              				}
						echo '</a>';
					echo '</li>';
				}
				?>
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->

	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<?
			$this->load->view($inner_view);
			?>
		</div>
	</div>
</div>
<div class="page-footer">
	<div class="page-footer-inner">
		 
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<div class="alertmsg">
</div>
<div id="fade-respond">
</div>

</body>
</html>