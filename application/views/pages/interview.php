<?
$uri1=addslashes($this->uri->segment(1));
$uri2=addslashes($this->uri->segment(2));
$uri3=addslashes($this->uri->segment(3));
$uri4=addslashes($this->uri->segment(4));
$uri5=addslashes($this->uri->segment(5));

$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
?>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/js/prettify.js"></script>
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/prettify.css" rel="stylesheet">

<div class="my-loader"><i class="fa fa-gear fa-spin" style="font-size:48px;"></i></div>
<form method="POST" action="/<?= $lclang ?>/pages/send_answers" class="interview_form">
<input type="text" name="user_ip" hidden value="<?=getRealIpAddr()?>">
<input type="text" name="interview_id" hidden value="<?=$interview->ID?>">
<div class="c_wr10">
  <div class="in">
    <div id="rootwizard">
      <div class="navbar">
        <div class="navbar-inner">
          <div class="container">
            <ul>
              <? $tab=1; ?>
              <? foreach ($questions as $question) { ?>
                <li><a href="#tab<?=$tab?>" data-toggle="tab"><?=$question->Name?></a></li>
                <? $tab++; ?>
              <?}?>
            </ul>
          </div>
        </div>
      </div>
      <div class="tab-content">
      <? $tab=1; ?>
        <? foreach ($questions as $question) { ?>
          <div class="tab-pane active" id="tab<?=$tab?>">
          <div class="question">
            <div class="title2"><?=$question->Name?></div>
            <? if($question->Multi == 0 ) { ?>
              <div class="b_info11">
                <? foreach($question->childs as $answer) { ?>
                  <label class="switch">
                    <input class="check__button" type="radio" name="<?=$question->ID?>" id="ch_a1" value="<?=$answer->id?>">
                    <span><?=$answer->name?></span>
                  </label>
                <?}?>
              </div>  
            <? } else {  ?>
              <div class="list1">
                <? $i=1; ?>
                <? foreach($question->childs as $answer) { ?>
                  <li><input class="check__button" type="checkbox" name="<?=$question->ID.'[]'?>" value="<?=$answer->id?>" id="ch_b<?=$i?>"><label for="ch_b<?=$i?>"><?=$answer->name?></label></li>
                <? $i++; ?>  
                <?}?>
              </div>
            <? } ?>
          </div>
          </div>
          <? $tab++; ?>
        <?}?>
        <ul class="wizard foot">
          <li class="previous"><a class="nav_btn nb_prev" href="javascript:;"><?=LINK_PREV?></a></li>
          <li class="prog">
            <div class="b_progress">
              <div class="bp_bar" style="">
                <div class="bp_num"></div>
              </div>
            </div>
          </li>
          <li class="next"><a class="nav_btn nb_next" href="javascript:;"><?=LINK_NEXT?></a></li>
          <li class="finish"><a class="nav_btn" href="javascript:;">Finish</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
</form>
<script>
$(document).ready(function() {
    $(window).load(function() {
			$(".my-loader").fadeOut("slow");
	  });

  	$('#rootwizard').bootstrapWizard(
    {
      onNext: function(tab, navigation, index) {
          var $current = index;
          if($('#tab'+$current+' .check__button').is(':checked')) { 

          } else {
            $(".tab-pane.active").addClass('shake');
            setTimeout(function(){
              $(".tab-pane.active").removeClass('shake');
            }, 1000);
            return false; 
          }
      },
      onTabShow: function(tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        $('#rootwizard').find('.bp_bar').css({width:$percent+'%'});
        $('#rootwizard').find('.bp_num').empty().append(parseFloat($percent).toFixed(2)+'%');
      }
  });
	$('#rootwizard .finish').click(function() {

    var isValid = false;

    $(".tab-pane").each(function(){
      var question = $(this);

      isValid=false;
      $(question).find("input.check__button").each(function() {
        var element = $(this);
        console.log(element);
        if (element.is(':checked')) {
          isValid = true;
        }
      });
    });

    if(isValid==true) {
      console.log('caseak');
      $( ".interview_form" ).submit();
    } else {
      $(".tab-pane.active").addClass('shake');
      setTimeout(function(){
        $(".tab-pane.active").removeClass('shake');
      }, 1000);
      return false;
    }
	});
});
</script>