<?
@session_start();
$uri1=addslashes($this->uri->segment(1));
$uri2=addslashes($this->uri->segment(2));
$uri3=addslashes($this->uri->segment(3));
$uri4=addslashes($this->uri->segment(4));
$uri5=addslashes($this->uri->segment(5));

$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
?>
<!-- <div class="m_address"> -->
<div class="">
  <div class="in">
<!--     <p><?=TEXT_HOTLINE?><br><span><?=TEXT_PHONE_HOTLINE?></span></p>
    <p><?=TEXT_ADDRESS?><br><a href="tel:<?=TEXT_CONTACTPHONE?>"><?=TEXT_CONTACTPHONE?></a>, <?=TEXT_EMAIL?></p>
 -->  

  <? foreach( $blocks as $item) { ?>
        <p><?=$item['BlockText']?></p>
  <? } ?>

  </div>
</div>
<div class="">
  <div class="in">
    <p>
    <?=TEXT_ONLINE24?>
    </p>
    <p>
    <?=TEXT_JURNAME?>
    <br/>
    <?=TEXT_JURADDRESS?>
    </p>
  </div>
</div>
<div class="map_wr">
  <div class="in">
    <div class="feedback">
      <div class="title"><?=TEXT_FEEDBACK?></div>
      <div class="ok hide"><?=TEXT_SENDMSG?></div>
      <form method="POST" class="sendmsg">
        <div class="f_row"><input type="text" name="name" id="name" placeholder="<?=PLACEH_YOURNAME?>"></div>
        <div class="f_row"><input type="email" name="semail" id="semail" placeholder="<?=PLACEH_EMAIL?>"></div>
        <div class="f_row"><textarea name="msg" id="msg" cols="30" rows="10" placeholder="<?=PLACEH_MESSAGE?>"></textarea></div>
        <div class="b_btn">
          <i class="fa fa-spinner fa-spin hide"></i><input name="sendbutton" id="sendbutton" type="submit" value="<?=BUTTON_SEND?>">
        </div>
      </form>
    </div>
  </div>
  <div id="map" class="map"></div>
  <? $pizza = explode(',', VALUE_COORDINATES); ?>
  <script>
      var map;
      function initMap() {
          map = new google.maps.Map(document.getElementById('map'), {
              center: new google.maps.LatLng(<?=VALUE_COORDINATES?>),
              zoom: 14,
          });
          image = '/public/i/marker.png',
              marker = new google.maps.Marker({
                  position: {
                      lat: <?=$pizza[0]?>,
                      lng: <?=$pizza[1]?>
                  },
                  map: map,
                  icon: image
              });
      }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBq5z4zkaz_1w14wyo9ERdG4KEIIbUViEQ&callback=initMap" async defer></script>
</div>
<div class="c_wr5">
  <div class="in">
    <h2 class="title1"><?=TEXT_OURBRANCES?></h2>
    <table cellpadding="0" cellspacing="0" class="table1">
      <?php
      $TEXT_RETAIL = TEXT_RETAIL;
      $TEXT_WHOLESALE = TEXT_WHOLESALE;

      foreach ($branches as $row) {
        if ($row->Type == 1) {
          $type = "<td><span class='label v1'>$TEXT_RETAIL</span></td>";
        } else if ($row->Type == 2) {
            $type = "<td><span class='label v2'>$TEXT_WHOLESALE</span></td>";
        } else if ($row->Type == 3) {
            $type = "<td><span class='label v1'>$TEXT_RETAIL</span><span class='label v2'>$TEXT_WHOLESALE</span></td>";
        }
          echo "<tr>
                  <td><strong>$row->Address</strong></td>
                  <td>$row->Phone, $row->Email</td>
                  $type
                </tr>";
                }
      ?>
    </table>
  </div>
</div>
<div class="c_wr6">
  <div class="in">
    <h2 class="title1"><?=TEXT_AGENTS?></h2>
    <div class="b_info6">
      <?php
      $TEXT_DISTRICT = TEXT_DISTRICT;
      foreach ($agents as $row) {
        $distaaray = json_decode($row->District, true);
        $diststring = '';  
        foreach ($distaaray as $item) {
          foreach ($districts as $value) {
            if ($value->ID == $item ) {
              $diststring .= $value->Name. ' ';
            }
          }
        }
          $src = newthumbs($row->Image, 'agents', 165, 165, '165x165x0', 0);
          echo "<div class='item'>
                  <div class='thumb'><img src='$src' alt=''></div>
                  <div class='title'>$row->Name</div>
                  <p>$TEXT_DISTRICT: $diststring</p>
                  <p><a href='tel:$row->Phone'>$row->Phone</a></p>
                </div>";
      }
      ?>
    </div>
  </div>
</div>
