<?
$uri1=addslashes($this->uri->segment(1));
$uri2=addslashes($this->uri->segment(2));
$uri3=addslashes($this->uri->segment(3));
$uri4=addslashes($this->uri->segment(4));
$uri5=addslashes($this->uri->segment(5));

$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
?>
<div class="tabs_wr_profile v2 ui-tabs ui-corner-all ui-widget ui-widget-content">
    <div class="tabs">
      <div class="in">
        <ul class="ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header">
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"><a class="ui-tabs-anchor" href="/<?=$lclang?>/pages/profile"><?=LINK_HISTORYORDERS?></a></li>
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"><a class="ui-tabs-anchor" href="/<?=$lclang?>/pages/profile/2"><?=LINK_VIEWEDPROD?></a></li>
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active"><a class="ui-tabs-anchor" href=""><?=LINK_PROFILE?></a></li>
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"><a class="ui-tabs-anchor" href="/<?=$lclang?>/pages/profile/4"><?=LINK_CHANGEPASS?></a></li>
        </ul>
      </div>
    </div>
    <div class="tab_content">
      <div id="tab_b3">
        <div class="in">
          <div class="form_wr">
            <div class="title"><?=TEXT_PROFILE?></div>
            <div class="title ok">
              <?php
                if(isset( $_SESSION['msg'])) {
                  echo  $_SESSION['msg'];
                  unset( $_SESSION['msg']);
                }
              ?>
            </div>
            <?php
            if( !empty( $this->input->post('fullname') ) ) {
              $fullname = $this->input->post('fullname');
            } else {
              $fullname = $user['Name'];
            }
            if( !empty( $this->input->post('district') ) ) {
              $districtID = $this->input->post('district');
            } else {
              $districtID = $user['DistrictID'];
            }
            if( !empty( $this->input->post('phone') ) ) {
              $phone = $this->input->post('phone');
            } else {
              $phone = $user['phone'];
            }
            if( !empty( $this->input->post('email') ) ) {
              $email = $this->input->post('email');
            } else {
              $email = $user['email'];
            }
            if( !empty( $this->input->post('fiscalcode') ) ) {
              $fiscalcode = $this->input->post('fiscalcode');
            } else {
              $fiscalcode = $user['fiscalcode'];
            }

            ?>
            <form class="" method="post" action="/<?=$lclang?>/pages/change_date/3">
              <div class="f_row">
                <div class="form_paceholder hide"><?=PLACEH_FULLNAME?></div>
                <input type="text" value="<?=$fullname?>" name="fullname" id="fullname" placeholder="<?=PLACEH_FULLNAME?> *">
                <span class="error"><?= $fullname_errore = form_error('fullname'); ?></span>
              </div>
              <div class="f_row">
                <div class="form_paceholder hide"><?=PLACEH_PHONE?></div>
                <input type="tel" value="<?=$phone?>" name="phone" id="phone" placeholder="<?=PLACEH_PHONE?> *" pattern="^\+373(\s+)?\(?(79|78|69|68|67|60)\)?(\s+)?[0-9]{3}-?[0-9]{3}$">
                <span class="error"><?= $phone_errore = form_error('phone'); ?></span>
              </div>
              <div class="f_row">
                <div class="form_paceholder hide"><?=PLACEH_EMAIL?></div>
                <input type="email" value="<?=$email?>" name="email" id="email" placeholder="<?=PLACEH_EMAIL?> *">
                <span class="error"><?= $email_errore = form_error('email'); ?></span>
              </div>
              <div class="f_row">
                <div class="form_paceholder hide"><?=PLACEH_FISCALCODE?></div>
                <input type="text" value="<?=$fiscalcode?>" name="fiscalcode" id="fiscalcode" placeholder="<?=PLACEH_FISCALCODE?> <?=PLACEH_FISCALCODE2?>">
                <span class="error"><?= $fiscalcode_errore = form_error('fiscalcode'); ?></span>
              </div>
              <div class="f_row">
                <select name="district" id="district">
                    <?php
                    foreach ($districts as $item) {
                        if($item->ID == $districtID) {
                          $attr = 'selected';
                        } else {
                          $attr = '';
                        }
                        echo "<option ".$attr." value='".$item->ID."'>".$item->Name."</option>";
                    }
                    ?>
                </select>
              </div>
              <div class="b_btn"><input type="submit" value="<?=BUTTON_SAVE?>"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
