<?
$uri1=addslashes($this->uri->segment(1));
$uri2=addslashes($this->uri->segment(2));
$uri3=addslashes($this->uri->segment(3));
$uri4=addslashes($this->uri->segment(4));
$uri5=addslashes($this->uri->segment(5));

$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
?>
<div class="form_wr">
    <div class="title"><?=TEXT_REGISTRATION?></div>
    <form method="post" action="/<?=$lclang?>/pages/registration_validation">
      <div class="f_row">
        <div class="form_paceholder hide"><?=PLACEH_FULLNAME?></div>
        <input value="<?=$this->input->post('fullname')?>" type="text" name="fullname" id="fullname" placeholder="<?=PLACEH_FULLNAME?> *">
        <span class="error"><?= $fullname_errore = form_error('fullname'); ?></span>
      </div>
      <div class="f_row">
        <div class="form_paceholder hide"><?=PLACEH_PHONE?></div>
        <input value="<?=$this->input->post('phone')?>" type="tel" name="phone" id="phone" pattern="^\+373(\s+)?\(?(79|78|69|68|67|60)\)?(\s+)?[0-9]{3}-?[0-9]{3}$">
        <span class="error"><?= $phone_errore = form_error('phone'); ?></span>
      </div>
      <div class="f_row">
        <div class="form_paceholder hide"><?=PLACEH_EMAIL?></div>
        <input value="<?=$this->input->post('email')?>" type="email" name="email" id="email" placeholder="<?=PLACEH_EMAIL?> *">
        <span class="error"><?= $email_errore = form_error('email'); ?></span>
      </div>
      <div class="f_row">
        <div class="form_paceholder hide"><?=PLACEH_FISCALCODE?></div>
        <input value="<?=$this->input->post('fiscalcode')?>" type="text" name="fiscalcode" id="fiscalcode" placeholder="<?=PLACEH_FISCALCODE?> <?=PLACEH_FISCALCODE2  ?>">
        <span class="error"><?= $district_errore = form_error('district'); ?></span>
      </div>
      <div class="f_row">
        <select name="district" id="district">
          <?php
            foreach ($districts as $item) {
              echo "<option value='".$item->ID."'>".$item->Name."</option>";
            }
          ?>
        </select>
      </div>
      <div class="f_row">
        <div class="form_paceholder hide"><?=PLACEH_ADDRESS?></div>
        <input value="<?=$this->input->post('legaladdress')?>" type="text" name="legaladdress" id="legaladdress" placeholder="<?=PLACEH_ADDRESS?> *">
        <span class="error"><?= $legaladdress_errore = form_error('legaladdress'); ?></span>
      </div>
      <div class="f_row">
        <div class="form_paceholder hide"><?=PLACEH_PASSWORD?></div>
        <input value="<?=$this->input->post('password')?>" type="password" name="password" id="password" placeholder="<?=PLACEH_PASSWORD?> *">
        <span class="error"><?= $pass_errore = form_error('password'); ?></span>
      </div>
      <div class="f_row">
        <div class="form_paceholder hide"><?=PLACEH_PASSWORDAGAIN?></div>
        <input value="<?=$this->input->post('passwordagain')?>" type="password" name="passwordagain" id="passwordagain" placeholder="<?=PLACEH_PASSWORDAGAIN?> *">
        <span class="error"><?= $passagain_errore = form_error('passwordagain'); ?></span>
      </div>
      <div class="b_btn"><input type="submit" value="<?=BUTTON_REGISTRATION?>"></div>
    </form>
  </div>
