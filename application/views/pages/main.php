<?
@session_start();
$uri1=addslashes($this->uri->segment(1));
$uri2=addslashes($this->uri->segment(2));
$uri3=addslashes($this->uri->segment(3));
$uri4=addslashes($this->uri->segment(4));
$uri5=addslashes($this->uri->segment(5));

$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
?>
<div class="c_wr">
  <div class="in">
    <div class="slider">
        <?
          $this->db->select('Photo'.$clang.' as Photo, PhotoLink'.$clang.' as PhotoLink');
          $this->db->order_by('Sorder', 'ASC');
          $this->db->order_by('ID', 'DESC');
          $query = $this->db->get('Slider');
          foreach ($query->result() as $row) {
              $src = newthumbs($row->Photo, 'slider', 1320, 322, '1320x322x0', 0);
              echo "<a href='".$row->PhotoLink."'><img src='$src' alt=''></a>";
          }
        ?>
    </div>
    <div class="slider_nav"></div>
  </div>
</div>
<div class="c_wr1">
  <div class="in">
    <div class="b_info">
        <?
        $this->db->select('Name'.$clang.' as Name, Description'.$clang.' as Description, Link'.$clang.' as Link , Image');
        $this->db->order_by('Sorder', 'ASC');
        $this->db->order_by('ID', 'DESC');
        $query = $this->db->get('Info');
        foreach ($query->result() as $row) {
            $src = newthumbs($row->Image, 'info', 57, 57, '57x57x1', 1);
            echo "<div class='item'>
                    <a href='$row->Link'>
                      <img src='$src' alt=''>
                      <div class='title'>$row->Name</div>
                      <p>$row->Description</p>
                    </a>
                  </div>";
        }
        ?>
    </div>
    <div class="b_info1">
        <?
        $this->db->select('Name'.$clang.' as Name, Description'.$clang.' as Description, Link'.$clang.' as Link , Image');
        $this->db->order_by('Sorder', 'ASC');
        $this->db->order_by('ID', 'DESC');
        $query = $this->db->get('HomeCategory');
        foreach ($query->result() as $row) {
            $src = newthumbs($row->Image, 'homecategory', 78, 84, '78x84x0', 0);
            echo "<div class='item'>
                    <a href='$row->Link'>
                      <img src='$src' alt=''>
                      <div class='title'>$row->Name</div>
                    </a>
                  </div>";
        }
        ?>
    </div>
  </div>
</div>
<?
$this->db->select('Name'.$clang.' as Name, ID');
$this->db->where('OnSite', 1);
$this->db->order_by('Sorder', 'ASC');
$this->db->order_by('ID', 'DESC');
$tabs = $this->db->get('HomeTabs')->result();
if(!empty($tabs)) {
?> 
<div class="tabs_wr">
    <div class="tabs">
      <div class="in">
        <ul>
          <? $i=0; foreach($tabs as $tab) { $i++; ?>
            <li><a href="#tab<?=$i?>"><?=$tab->Name?></a></li>
          <? } ?>
        </ul>
      </div>
    </div>
    <div class="tab_content">
      <? $i=0; foreach($tabs as $tab) { $i++; ?>
        <div id="tab<?=$i?>">
          <div class="in">
            <div class="b_info2">
              <?
              $this->db->select('Products.Title'.$clang.' as Title, Products.*');
              $this->db->join('Products', 'Products.Cod = TabProd.ProdID');
              $this->db->where('TabProd.TabID', $tab->ID);
              $this->db->order_by('TabProd.ID', 'asc');
              $products = $this->db->get('TabProd')->result();
              if(!empty($products)) { 
                foreach ($products as $product) { 
              ?>
                <div class="item">
                  <div class="thumb">
                  <?
                  if(!empty($product->Image1)) {
                      $src = 'data:image/jpeg;base64, '.$product->Image1;
                  } else {
                      $src = newthumbs('noicon.png', 'i', 218, 216, '218x216x1', 1);
                  }
                  ?>
                  <a href="/<?=$lclang?>/pages/product/<?= $product->UriName?>"><img src="<?=$src?>" alt=""></a>
                  <!-- <div class="label">%</div> -->
                  </div>
                  <div class="descr">
                  <div class="title"><a href="/<?=$lclang?>/pages/product/<?=$product->UriName?>"><?=$product->Title?></a></div>
                    <div class="b_buy">
                      <div class="price"><?=Exchange($product->Price,false).' '.TEXT_MDL?></div>
                      <div class="buy_wr">
                        <input type="number" name="" id="<?=$product->Cod?>" value="1" minvalue="<?=NR_ON_STOC?>">
                        <button class="buy"></button>
                      </div>
                      <div id="alertMsg">
                      </div>
                    </div>
                  </div>
                </div>
                <? } }?>
            </div>
          </div>
        </div>
      <? } ?>
    </div>
  </div>
</div>  
<? } ?>
<?
$this->db->select('Name'.$clang.' as Name, Url, Price, Photo');
$this->db->order_by('Sorder', 'ASC');
$this->db->order_by('ID', 'DESC');
$query = $this->db->get('SliderCategory')->result();
if(!empty($query)) {
?>  
<div class="c_wr2">
  <div class="in">
    <div class="b_info3 carousel">
      <? foreach ($query as $row) { ?>        
      <div class="item">
        <? $src = newthumbs($row->Photo, 'slidercategory', 250, 250, '250x250x0', 0); ?>
        <div class="thumb"><img src="<?=$src?>" alt=""></div>
        <div class="descr">
          <div class="head">
            <!-- <span>Техника для дома</span> -->
            <div class="title"><?=$row->Name?></div>
          </div>
          <div class="foot">
            <div class="price"><?=TEXT_FROM?> <span><?=$row->Price?></span> <?=TEXT_MDL?></div>
            <div class="b_lnk"><a href="<?=$row->Url?>"><?=TEXT_GOTO?></a></div>
          </div>
        </div>
      </div>
      <? } ?>
    </div>
  </div>
</div>
<? } ?>
<div class="c_wr4">
  <div class="in">
    <div class="b_info4">
        <?
        $this->db->select('Image, Name');
        $this->db->order_by('Sorder', 'ASC');
        $this->db->order_by('ID', 'DESC');
        $query = $this->db->get('Brands');
        foreach ($query->result() as $row) {
            if($row->Image!='') { ?>
              <div class='thumb'>
              <? $src = newthumbs($row->Image, 'brands', 55, 47, '55x47x0', 0); ?>  
              <a href='/<?=$lclang?>/pages/items/?cost=0&pr=0&brand=<?=$row->Name?>&sortby=1'>
                <img src='<?=$src?>' alt=''>
              </a>
              </div>
            <? }  
          }
        ?>
    </div>
  </div>
</div>
