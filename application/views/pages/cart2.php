<?
@session_start();
$uri1=addslashes($this->uri->segment(1));
$uri2=addslashes($this->uri->segment(2));
$uri3=addslashes($this->uri->segment(3));
$uri4=addslashes($this->uri->segment(4));
$uri5=addslashes($this->uri->segment(5));

$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
?>
<div class="tabs_wr_profile v2 ui-tabs ui-corner-all ui-widget ui-widget-content">
  <div class="tabs">
    <div class="in">
    <ul class="ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header">
        <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"><a class="ui-tabs-anchor" href="/<?=$lclang?>/pages/cart"><?=LINK_BASKETOFORDERS?></a></li>
        <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active"><a class="ui-tabs-anchor" href=""><?=LINK_ORDERING?></a></li>
      </ul>
    </div>
  </div>
  <div class="tab_content">
    <div id="tab_b2">
      <div class="in">
        <form method="POST" action="/<?=$lclang?>/pages/order_validation">
        <div class="order_wr">
          <div class="col">
          <?php
            if( !empty( $this->input->post('fullname') ) ) {
              $fullname = $this->input->post('fullname');
            } else {
              $fullname = $user['Name'];
            }
            if( !empty( $this->input->post('district') ) ) {
              $districtID = $this->input->post('district');
            } else {
              if(isset($_SESSION['sess_district'])) {
                $districtID = $_SESSION['sess_district'];
              } else {
                $districtID = $user['DistrictID'];
              }
            }
            if( !empty( $this->input->post('phone') ) ) {
              $phone = $this->input->post('phone');
            } else {
              $phone = $user['phone'];
            }
            if( !empty( $this->input->post('email') ) ) {
              $email = $this->input->post('email');
            } else {
              $email = $user['email'];
            }
            if( !empty( $this->input->post('street') ) ) {
              $street = $this->input->post('street');
            } else {
              $street = '';
            }
            if( !empty( $this->input->post('housenumber') ) ) {
              $housenumber = $this->input->post('housenumber');
            } else {
              $housenumber = '';
            }
            if( !empty( $this->input->post('commenttoorder') ) ) {
              $commenttoorder = $this->input->post('commenttoorder');
            } else {
              $commenttoorder = '';
            }
            if( !empty( $this->input->post('accept_terms') ) ) {
              $checked = 'checked';
            } else {
              $checked = '';
            }
            if( !empty( $this->input->post('r1') ) ) {
              $r1 = $this->input->post('r1');
            } else {
              $r1 = 2;
            }
            if( !empty( $this->input->post('r2') ) ) {
              $r2 = $this->input->post('r2');
            } else {
              $r2 = 2;
            }

            if($r1==2) {
              $value = 0;
            } else {
              if(CalcTotal() >= VALUE_MINFREEDELIVERY) {
                $value = 0;
              } else {
                $value = VALUE_DELIVERYPRICE;
              } 
            }

            ?>
              <fieldset>
                <legend><?=TEXT_DELIVERYADDRESS?></legend>
                <div class="form_wr">
                  <div class="cols">
                    <div class="f_row" style="width:100%">
                      <div class="form_paceholder hide"><?=PLACEH_FULLNAME?></div>
                      <input type="text" name="fullname" id="fullname" value="<?=$fullname?>" placeholder="<?=PLACEH_FULLNAME?>">
                      <span class="error"><?= $fullname_errore = form_error('fullname'); ?></span>
                    </div>
                  </div>
                  <div class="cols">
                    <div class="f_row">
                      <div class="form_paceholder hide"><?=PLACEH_EMAIL?></div>
                      <input type="email" name="email" value="<?=$email?>" id="email" placeholder="<?=PLACEH_EMAIL?>">
                      <span class="error"><?= $email_errore = form_error('email'); ?></span>
                    </div>
                    <div class="f_row">
                      <div class="form_paceholder hide"><?=PLACEH_PHONE?></div>
                      <input type="tel" name="phone" value="<?=$phone?>" id="phone" placeholder="<?=PLACEH_PHONE?>" pattern="^\+373(\s+)?\(?(79|78|69|68|67|60)\)?(\s+)?[0-9]{3}-?[0-9]{3}$">
                      <span class="error"><?= $phone_errore = form_error('phone'); ?></span>
                    </div>
                  </div>
                  <div class="cols">
                    <div class="f_row">
                      <select name="district" id="district">
                          <?php
                          foreach ($districts as $item) {
                              if($item->ID == $districtID) {
                                $attr = 'selected';
                              } else {
                                $attr = '';
                              }
                              echo "<option ".$attr." value='".$item->ID."'>".$item->Name."</option>";
                          }
                          ?>
                      </select>
                    </div>
                    <div class="f_row">
                      <div class="form_paceholder hide"><?=PLACEH_STREET?></div>
                      <input type="text" name="street" value="<?=$street?>" id="street" placeholder="<?=PLACEH_STREET?>">
                      <span class="error"><?= $street_errore = form_error('street'); ?></span>    
                    </div>
                  </div>  
                  <div class="f_row">
                    <div class="form_paceholder hide"><?=PLACEH_HOUSENUMBER?></div>
                    <input type="text" name="housenumber" id="housenumber" value="<?=$housenumber?>" placeholder="<?=PLACEH_HOUSENUMBER?>">
                    <span class="error"><?= $housenumber_errore = form_error('housenumber'); ?></span>      
                  </div>
                  <div class="f_row">
                    <div class="form_paceholder hide"><?=PLACEH_HOUSENUMBER?></div>
                    <textarea name="commenttoorder" id="commenttoorder" cols="30" rows="10" placeholder="<?=PLACEH_COMMENTTOOTDER?>"><?=$commenttoorder?></textarea></div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?=TEXT_DELIVERYINFORMATION?></legend>
                <div class="switch1 v1">
                  <label><input type="radio" value="1" name="r1" id="r1" <? if($r1==1) { echo 'checked';} ?> ><span><strong><?=TEXT_DELIVERY?></strong><?=TEXT_TERMPRICE." ".VALUE_DELIVERYPRICE.' '.TEXT_MDL?></span></label>
                  <label><input type="radio" value="2" name="r1" id="r1" <? if($r1==2) { echo 'checked';} ?> ><span><strong><?=TEXT_PICKUP?></strong><?=TEXT_TERMFREE?></span></label>
                </div>
              </fieldset>
              <fieldset>
                <legend><?=TEXT_PAYMENTINFORMATION?></legend>
                <div class="switch1 v1">
                  <label><input type="radio" value="1" name="r2" id="r2" <? if($r2==1) { echo 'checked';} ?>><span><strong><?=TEXT_PAYMENTONLINE?></strong><?=TEXT_BANKCARDS?></span></label>
                  <label><input type="radio" value="2" name="r2" id="r2" <? if($r2==2) { echo 'checked';} ?>><span><strong><?=TEXT_PAYMENTONDELIVERY?></strong><?=TEXT_PAYMENTTOCOURIER?></span></label>
                  <label><input type="radio" value="3" name="r2" id="r2" <? if($r2==3) { echo 'checked';} ?>><span><strong><?=TEXT_PAYMENTONDELIVERY?></strong><?=TEXT_PAYMENTCARD?></span></label>
                </div>
              </fieldset>
          </div>
          <div class="col">
            <div class="order_detail v1">
              <div class="info">
                <p class="p1"><?=TEXT_COSTOFGOODS?>: <span><? displayTotal() ?></span></p>
                <p class="p2"><?=TEXT_COSTOFDELIVERY?>: <span><?=$value.' '.TEXT_MDL?></span></p>
                <p class="p3"><?=TEXT_TOTALCOST?>: <span><? displayTotal($value)?></span></p>
              </div>
              <div id="accept_terms">
                <input type="checkbox" <?=$checked?> name="accept_terms" value="1" /> 
<!--                 <span><a href="#open-modal"><?=TEXT_ACCEPTTERM?></a></span><br>-->
                <span>
                  <a target="_blank" href="/<?=$lclang?>/pages/conditii-de-returnare-produs/"><?=TEXT_ACCEPTTERM?></a>
                  <br/>
                  <a target="_blank" href="/<?=$lclang?>/pages/securitat-a-informatiei-si-politica-de-securitate/"><?=TEXT_ACCEPTPOL?></a>
                </span>
                <br>
                <span class="error" style="display:none!important"><?= $accept_terms_errore = form_error('accept_terms'); ?></span>         
              </div>
              <div class="container">
                <div class="interior">
                </div>
              </div>
              <div id="open-modal" class="modal-window">
                <div>
                  <a href="#modal-close" title="Close" class="modal-close">Close</a>
                  <div><?=TEXT_TERM?></div>
                  </div>
              </div>
              <div class="b_btn"><button><?=BUTTON_CONFIRM?></button></div>
            </div>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="c_wr8">
  <div class="in">
    <h2 class="title1"><?=TEXT_RELATEDPRODUCTS?></h2>
    <div class="b_info2">
      <?foreach ($products as $value) { ?> 
      <div class="item">
        <div class="thumb">
            <?
            if(!empty($value->Image1)) {
                //$src = newthumbs($value->Image, 'products', 218, 216, '218x216x0', 0);
                $src = 'data:image/jpeg;base64, '.$value->Image1;
            } else {
                $src = newthumbs('noicon.png', 'i', 218, 216, '218x216x1', 1);
            }
            ?>
          <a href="/<?=$lclang?>/pages/product/<?=$value->UriName?>"><img src="<?=$src?>" alt=""></a>
          <!-- <div class="label">%</div> -->
        </div>
        <div class="descr">
          <div class="title"><a href="/<?=$lclang?>/pages/product/<?=$value->UriName?>"><?=$value->Title?></a></div>
          <div class="b_buy">
            <div class="price<?= $value->Cod ?>"><?=Exchange($value->Price,false).' '.TEXT_MDL ?></div>
            <div class="buy_wr">
              <input type="number" name="" id="<?=$value->Cod?>" value="1" min="1" max="<?=NR_ON_STOC?>">
              <button class="buy"></button>
            </div>
            <div id="alertMsg">
            </div>
          </div>
        </div>
      </div>
      <? } ?>
    </div>
  </div>
</div>
