<?
@session_start();
$uri1=addslashes($this->uri->segment(1));
$uri2=addslashes($this->uri->segment(2));
$uri3=addslashes($this->uri->segment(3));
$uri4=addslashes($this->uri->segment(4));
$uri5=addslashes($this->uri->segment(5));

$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
//dump_it($categorii);
?>
<div class="breadcrumbs">
    <div class="in">
    <ul>
        <? if ($isCat==0) {?>
            <li><a href="/"><?=LINK_HOME?></a></li>
            <li><a href=""><?=LINK_CATALOG?></a></li>
        <?} else {?>
            <li><a href="/"><?=LINK_HOME?></a></li>
            <li><a href="/<?=$lclang?>/pages/catalog"><?=LINK_CATALOG?></a></li>
        <?}?>
    </ul>
    </div>
</div>

<div class="m_head">
  <div class="in">
  </div>
</div>


<div class="m_cat">
  <div class="in">
    <div class="col" id="colp1">
    </div>
    <div class="col" id="colp2">
    </div>
    <div class="col" id="colp3">
    </div>
    <div class="col" id="colp4">
    </div>
  </div>
</div>
<?

//dump($categorii);
$categories_for_js = categories_map_without_keys($categorii);
?>
<script>
$(document).ready(function(e) {

    var lang = "<?=$lclang?>";   
    var iscat = "<?=$isCat?>";
    var result = <? echo json_encode($categories_for_js)?>;

    console.log(result);

    if(iscat > 0 ) {
        $.each(result, function (index, value) {
            elem = "<h1>"+value.Title+"</h1>";
            var contor =0;
            childsEach(value.childs, contor);
            displayCategories(elem, value.Cod, value.Title);
        });
    } else {
        $.each(result, function (index, value) {
            elem = "<div class='title'><a href='/"+ lang +"/pages/catalog/" + value.UriName + "'>" + value.Title + "</a></div>";
            childsEach(value.childs);
            displayCategories(elem, value.Cod, value.Title);
        });
    }
    function childsEach(childs, contor=0) {
        if(childs.length !=0) {
            if(iscat>0) {
                if (contor==0) {
                    clas = "simplecat";
                } else {
                    clas = "";
                }
            } else {
                clas = "";
            }
            elem +="<ul class='"+clas+"'>";
            $.each(childs, function (index, value) {
                elem += "<li><a href='/"+ lang +"/pages/catalog/" + value.UriName + "'>" + value.Title + "</a>";
                contor ++;
                childsEach(value.childs, contor);
                elem += "</li>";
            });
            elem +="</ul>";
        } 
    }

    function displayCategories(elem, cod, title) {
        if (iscat == 0) {
            h1 = $('div#colp1 li').length;
            h2 = $('div#colp2 li').length;
            h3 = $('div#colp3 li').length;
            h4 = $('div#colp4 li').length;

            min = Math.min(h1, h2, h3, h4);

            switch(min) {
            case h1:
            $( "div#colp1" ).append( elem );
                break;
            case h2:
            $( "div#colp2" ).append( elem );
                break;
            case h3:
            $( "div#colp3" ).append( elem );
                break;
            case h4:
            $( "div#colp4" ).append( elem );
                break;
            }
        } else {
            if(cod == iscat) {
                
                breadcrumbs = "<li>"+title+"</li>"; 
                $('.m_head .in').append(elem);
                $('.breadcrumbs .in ul').append(breadcrumbs);
            }
        }    
    }
});
</script>
