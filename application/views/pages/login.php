<?
$uri1=addslashes($this->uri->segment(1));
$uri2=addslashes($this->uri->segment(2));
$uri3=addslashes($this->uri->segment(3));
$uri4=addslashes($this->uri->segment(4));
$uri5=addslashes($this->uri->segment(5));
$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
?>
<div class="form_wr">
    <div class="title"><?=TEXT_LOGIN?></div>
    <form method="post" action="/<?=$lclang?>/pages/login_validation">
      <div class="f_row">
        <div class="form_paceholder hide"><?=PLACEH_EMAIL?></div>
        <input value="<?=$this->input->post('email')?>" type="text" name="email" id="email" placeholder="<?=PLACEH_EMAIL?> *">
        <span class="error"><?= $email_errore = form_error('email'); ?></span>
      </div>
      <div class="f_row">
        <div class="form_paceholder hide"><?=PLACEH_PASSWORD?></div>
        <input value="<?=$this->input->post('password')?>" type="password" name="password" id="password" placeholder="<?=PLACEH_PASSWORD?> *">
        <span class="error"><?= $pass_errore = form_error('password'); ?></span>
      </div>
      <div class="b_btn">
        <input type="submit" value="<?=BUTTON_LOGIN?>">
        <p><a href="/<?=$lclang?>/pages/restorepass"><?=LINK_RESTOREPASS?></a> | <a href="/<?=$lclang?>/pages/registration"><?=LINK_REGISTRATION?></a></p>
      </div>
    </form>
  </div>
