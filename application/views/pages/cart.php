<?
@session_start();
$uri1=addslashes($this->uri->segment(1));
$uri2=addslashes($this->uri->segment(2));
$uri3=addslashes($this->uri->segment(3));
$uri4=addslashes($this->uri->segment(4));
$uri5=addslashes($this->uri->segment(5));

$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);

if(CalcTotal() >= VALUE_MINFREEDELIVERY) {
  $value = 0;
} else {
  $value = VALUE_DELIVERYPRICE;
}

?>
<? if (CalcTotal() == 0 ) { ?>
<div class="warning">
  <div class="in">
    <p><?=TEXT_EMPTYCART?></p>
  </div>
</div>
<? } else { ?>
  <? $dis=''; if (CalcTotal() <= VALUE_MINSUMORDER ) { $dis = 'style="pointer-events: none; opacity:0.4;"'; ?>
  <div class="warning">
    <div class="in">
      <p><?=TEXT_MINSUMORDER?> – <strong><?=VALUE_MINSUMORDER?> <?=TEXT_MDL?></strong>. <?=TEXT_INYOURCART?>: <strong class="warningintotal"><? displayTotal() ?></strong></p>
    </div>
  </div>
  <? } ?>
<div class="tabs_wr_profile v2 ui-tabs ui-corner-all ui-widget ui-widget-content">
  <div class="tabs">
    <div class="in">
      <ul class="ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header">
        <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active"><a class="ui-tabs-anchor" href=""><?=LINK_BASKETOFORDERS?></a></li>
        <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"><a class="ui-tabs-anchor" <?=$dis?> href="/<?=$lclang?>/pages/cart/2"><?=LINK_ORDERING?></a></li>
      </ul>
    </div>
  </div>
  <div class="tab_content">
    <div id="tab_b1">
      <div class="in">
        <div class="order_wr">
          <div class="col">
            <form method="POST">
            <div class="b_info8">
              <? foreach ($productsoncart as $item) { ?>
                <div class="tr">
                  <div class="td">
                    <div class="thumb">
                      <?
                      if(!empty($item->Image1)) {
                          $src = 'data:image/jpeg;base64, '.$item->Image1;
                      } else {
                          $src = newthumbs('noicon.png', 'i', 52, 52, '52x52x1', 1);
                      }
                      ?>
                      <a href=""><img src="<?=$src?>" alt=""></a>
                    </div>
                  </div>
                  <div class="td">
                    <div class="title"><a href="/<?=$lclang?>/pages/product/<?=$item->UriName?>"><?=$item->Title?></a></div>
                    <div class="art"><span><?=TEXT_ARTICOL?>:</span> <?=$item->Cod?></div>
                  </div>
                  <div class="td"><input type="number" name="" id="<?=$item->Cod?>" value="<?=$_SESSION['cart']['items'][$item->Cod]['count']?>" min="1" max="<?=NR_ON_STOC?>"></div>
                  <div class="td"><div class="price" id="price<?= $item->Cod ?>"><?=Exchange( $_SESSION['cart']['items'][$item->Cod]['price'] * $_SESSION['cart']['items'][$item->Cod]['count']).' '.TEXT_MDL?></div></div>
                  <div class="td"><button formaction="/pages/delFromCart/<?=$item->Cod?>" id="<?=$item->Cod?>" class="del"></button></div>
                </div>   
              <? } ?>
            </div>
            </form>
          </div>
          <div class="col">
            <div class="order_detail">
              <div class="info">
                <p class="p1"><?=TEXT_COSTOFGOODS?>: <span><? displayTotal() ?></span></p>
                <p class="p2"><?=TEXT_COSTOFDELIVERY?>: <span><?=$value.' '.TEXT_MDL?></span></p>
                <p class="p3"><?=TEXT_TOTALCOST?>: <span><? displayTotal($value) ?></span></p>
              </div>
              <div class="b_btn" ><a <?=$dis?> href="/<?=$lclang?>/pages/cart/2"><button><?=BUTTON_COMPLETEORDER?></button></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<? } ?>
<div class="c_wr8">
  <div class="in">
    <h2 class="title1"><?=TEXT_RELATEDPRODUCTS?></h2>
    <div class="b_info2">
      <?foreach ($products as $value) { ?> 
      <div class="item">
        <div class="thumb">
            <?
            if(!empty($value->Image1)) {
                //$src = newthumbs($value->Image, 'products', 218, 216, '218x216x0', 0);
                $src = 'data:image/jpeg;base64, '.$value->Image1;
            } else {
                $src = newthumbs('noicon.png', 'i', 218, 216, '218x216x1', 1);
            }
            ?>
          <a href="/<?=$lclang?>/pages/product/<?=$value->UriName?>"><img src="<?=$src?>" alt=""></a>
          <!-- <div class="label">%</div> -->
        </div>
        <div class="descr">
          <div class="title"><a href="/<?=$lclang?>/pages/product/<?=$value->UriName?>"><?=$value->Title?></a></div>
          <div class="b_buy">
            <div class="price" id="price<?= $value->Cod ?>"> <?=Exchange($value->Price,false).' '.TEXT_MDL ?></div>
            <div class="buy_wr">
              <input type="number" name="" id="<?=$value->Cod?>" value="1" min="1" max="<?=NR_ON_STOC?>">
              <button class="buy"></button>
            </div>
            <div id="alertMsg">
            </div>
          </div>
        </div>
      </div>
      <? } ?>
    </div>
  </div>
</div>
