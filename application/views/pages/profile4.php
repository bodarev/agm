<?
$uri1=addslashes($this->uri->segment(1));
$uri2=addslashes($this->uri->segment(2));
$uri3=addslashes($this->uri->segment(3));
$uri4=addslashes($this->uri->segment(4));
$uri5=addslashes($this->uri->segment(5));

$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
?>
<div class="tabs_wr_profile v2 ui-tabs ui-corner-all ui-widget ui-widget-content">
    <div class="tabs">
      <div class="in">
        <ul class="ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header">
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"><a class="ui-tabs-anchor" href="/<?=$lclang?>/pages/profile"><?=LINK_HISTORYORDERS?></a></li>
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"><a class="ui-tabs-anchor" href="/<?=$lclang?>/pages/profile/2"><?=LINK_VIEWEDPROD?></a></li>
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"><a class="ui-tabs-anchor" href="/<?=$lclang?>/pages/profile/3"><?=LINK_PROFILE?></a></li>
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active"><a class="ui-tabs-anchor" href=""><?=LINK_CHANGEPASS?></a></li>
        </ul>
      </div>
    </div>
    <div class="tab_content">
      <div id="tab_b4">
        <div class="in">
          <div class="form_wr">
            <div class="title"><?=TEXT_CHANGEPASS?></div>
            <div class="title ok">
              <?php
                if(isset( $_SESSION['msg'])) {
                  echo  $_SESSION['msg'];
                  unset( $_SESSION['msg']);
                }
              ?>
            </div>
            <form method="post" action="/<?=$lclang?>/pages/change_date/4">
              <div class="f_row">
                <div class="form_paceholder hide"><?=PLACEH_CURRPASSWORD?></div>
                <input type="password" <input value="<?=$this->input->post('currpassword')?>" name="currpassword" id="currpassword" placeholder="<?=PLACEH_CURRPASSWORD?> *">
                <span class="error"><?= $currpass_errore = form_error('currpassword'); ?></span>
              </div>
              <div class="f_row">
                <div class="form_paceholder hide"><?=PLACEH_PASSWORD?></div>
                <input type="password" <input value="<?=$this->input->post('password')?>" name="password" id="password" placeholder="<?=PLACEH_PASSWORD?> *">
                <span class="error"><?= $pass_errore = form_error('password'); ?></span>
              </div>
              <div class="f_row">
                <div class="form_paceholder hide"><?=PLACEH_PASSWORDAGAIN?></div>
                <input type="password" <input value="<?=$this->input->post('passwordagain')?>" name="passwordagain" id="passwordagain" placeholder="<?=PLACEH_PASSWORDAGAIN?> *">
                <span class="error"><?= $passagain_errore = form_error('passwordagain'); ?></span>
              </div>
              <div class="b_btn"><input type="submit" value="<?=BUTTON_SAVE?>"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
