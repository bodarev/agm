<?
@session_start();
$uri1=addslashes($this->uri->segment(1));
$uri2=addslashes($this->uri->segment(2));
$uri3=addslashes($this->uri->segment(3));
$uri4=addslashes($this->uri->segment(4));
$uri5=addslashes($this->uri->segment(5));

$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
//dump_it($_SESSION);
?>
<div class="breadcrumbs">
<div class="in">
  <ul>
    <li><a href="/"><?=LINK_HOME?></a></li>
    <li><a href="/<?=$lclang?>/pages/catalog"><?=LINK_CATALOG?></a></li>
    <li><?=$title?></li>
  </ul>
</div>
</div>
<div class="m_head">
  <div class="in">
    <a href="" class="mh_back"></a>
    <h1><?=$title?></h1>
  </div>
</div>
<form method="GET" action="">

<? if (isset($_GET['match'])) { ?>
  <input name="match" type="text" hidden value="<?=$_GET['match']?>">
<? } ?>

<div class="sort_wr">
  <div class="in">
    <? if (!empty($category)) { ?>
      <div class="item">
        <label for="cat"><?=TEXT_CATEGORY?>:</label>
        <select name="cat" id="cat" class="v1">
          <option value=""><?=LINK_ALL?></option>
          <?
          foreach ($category as $item) { 
            if($item->Title != '') { ?> 
            <option <? if(isset($_GET['cat']) && $_GET['cat'] == $item->Cod) { echo " selected"; } ?> value='<?=$item->Cod?>'><?=$item->Title?> <span>(<?=$item->Count?>)</span></option>
          <? } 
          } ?>
        </select>
      </div>
    <?}?>
    <div class="item">
      <label for="cost"><?=TEXT_COST?>:</label>
      <select name="cost" id="cost" class="v1">
        <option value="0"><?=LINK_ALL?></option>
        <option <? if(isset($_GET['cost']) && $_GET['cost'] == 1) { echo " selected"; } ?> value="1">10 - 50 MDL</option>
        <option <? if(isset($_GET['cost']) && $_GET['cost'] == 2) { echo " selected"; } ?> value="2">50 - 200 MDL</option>
        <option <? if(isset($_GET['cost']) && $_GET['cost'] == 3) { echo " selected"; } ?> value="3">200 - 500 MDL</option>
        <option <? if(isset($_GET['cost']) && $_GET['cost'] == 4) { echo " selected"; } ?> value="4"> > 500 MDL</option>
      </select> 
    </div>
    <? if ($uri3 != 'promo') {?>
    <div class="item">
      <label for="pr"><?=TEXT_PROMOTION?>:</label>
      <select name="pr" id="pr" class="v1">
        <option <? if(isset($_GET['pr']) && $_GET['pr'] == 0) { echo " selected"; } ?> value="0"><?=LINK_ALL?></option>
        <option <? if(isset($_GET['pr']) && $_GET['pr'] == 1) { echo " selected"; } ?> value="1"><?=LINK_ONLY_PROMOTION?></option>
      </select>
    </div>
    <? } ?>
    <div class="item">
      <label for="brand"><?=TEXT_BRAND?>:</label>
      <select name="brand" id="brand" class="v1">
        <option value=""><?=LINK_ALL?></option>
        <?
        foreach ($brands as $item) { 
          if($item->Brand != '') { ?> 
          <option <? if(isset($_GET['brand']) && $_GET['brand'] == $item->Brand) { echo " selected"; } ?> value='<?=$item->Brand?>'><?=$item->Brand?></option>
        <? } 
        } ?>
      </select>
    </div>
    <div class="item">
      <label for="sortby"><?=TEXT_SORTBY?>:</label>
      <select name="sortby" id="sortby" class="v1">
        <option <? if(isset($_GET['sortby']) && $_GET['sortby'] == 1) { echo " selected"; } ?> value="1"><?=LINK_NAME?></option>
        <option <? if(isset($_GET['sortby']) && $_GET['sortby'] == 2) { echo " selected"; } ?> value="2"><?=LINK_NAME_INVERSE?></option>
        <option <? if(isset($_GET['sortby']) && $_GET['sortby'] == 3) { echo " selected"; } ?> value="3"><?=LINK_CHEAPER?></option>
        <option <? if(isset($_GET['sortby']) && $_GET['sortby'] == 4) { echo " selected"; } ?> value="4"><?=LINK_EXPENSIVE?></option>
      </select>
    </div>
  </div>
</div>
</form>
<div class="tabs_wr v1">
  <!-- ori dealer or nu-i logat -->
  <?php if (!isUser() ) { ?>
  <div class="tabs">
    <div class="in">
      <ul>
        <li><a href="#tab_b1" id="set_active_tab" data-tab="0"><?=LINK_RETAIL?></a></li>
        <li><a href="#tab_b2" id="set_active_tab" data-tab="1"><?=LINK_WHOLESALE?></a></li>
      </ul>
    </div>
  </div>
  <?php } ?>
  <div class="tab_content">
    <div id="tab_b1">
      <div class="in">
      <div class="b_info2">
        <? foreach ($products as $item) { ?>
        <div class="item">
          <div class="thumb">
            <?
            if(!empty($item->Image1)) {
                //$src = newthumbs($item->Image, 'products', 218, 216, '218x216x0', 0);
                $src = 'data:image/jpeg;base64, '.$item->Image1;
            } else {
                $src = newthumbs('noicon.png', 'i', 218, 216, '218x216x1', 1);
            }
            ?>
            <a href="/<?=$lclang?>/pages/product/<?=$item->UriName?>"><img src="<?=$src?>" alt=""></a>
            <!-- <div class="label">%</div> -->
          </div>
          <div class="descr">
            <div class="title"><a href="/<?=$lclang?>/pages/product/<?=$item->UriName?>"><?=$item->Title?></a></div>
            <div class="b_buy">
              <div class="price"><?=Exchange($item->Price,false)?> <?=TEXT_MDL?></div>
              <div class="buy_wr">
                <input type="number" name="" id="<?=$item->Cod?>" value="1" min="1" max="<?=NR_ON_STOC?>" >
                <button class="buy"></button>
              </div>
              <div id="alertMsg">
              </div>
            </div>
          </div>
        </div>
        <? } ?>
        </div>
        <? $this->load->view('utils/paginator'); ?>
      </div>
    </div>
    <!-- ori dealer or nu-i logat -->
    <?php if (!isUser() ) { ?>
    <div id="tab_b2">
      <? if( !isAuthorized() ) {  ?>   
        <div class="warning">
          <div class="in">
            <p><?=TEXT_TOACCESS?> <a href="/<?=$lclang?>/pages/login/"><strong><?=LINK_AUTHORIZATION?></strong></a> <?=TEXT_OR?> <a href="/<?=$lclang?>/pages/signup-dealers/"><strong><?=LINK_REGASDEALER?></strong></a></p>
          </div>
        </div>
      <? } ?>      

      <div class="in">
        <div class="b_info2">
          <? foreach ($products as $item) { ?>
          <div class="item">
            <div class="thumb">
              <?
              if(!empty($item->Image1)) {
                  //$src = newthumbs($item->Image, 'products', 218, 216, '218x216x0', 0);
                  $src = 'data:image/jpeg;base64, '.$item->Image1;
              } else {
                  $src = newthumbs('noicon.png', 'i', 218, 216, '218x216x1', 1);
              }
              ?>
              <a href="/<?=$lclang?>/pages/product/<?=$item->UriName?>"><img src="<?=$src?>" alt=""></a>
              <!-- <div class="label">%</div> -->
            </div>
            <div class="descr">
              <div class="title"><a href="/<?=$lclang?>/pages/product/<?=$item->UriName?>"><?=$item->Title?></a></div>
              <div class="b_buy">
                <div class="price">
                  <? if( !isAuthorized() ) {  ?>
                    <span>Dealer </span> 
                  <? } else {  ?>
                    <?=Exchange($item->PriceAngro,true)?>
                  <? }  ?>  
                  <?=TEXT_MDL?>
                </div>
                <div class="buy_wr">
                  <input type="number" name="" id="<?=$item->Cod?>" value="1" minvalue="<?=NR_ON_STOC?>">
                  <button class="buy"></button>
                </div>
                <div id="alertMsg">
                </div>
              </div>
            </div>
          </div>
          <? } ?>
        </div>
        <? $this->load->view('utils/paginator'); ?>
      </div>
    </div>
    <?php } ?>
  </div>
</div>
<script>
  $(document).ready(function(e) {

    $('form select').on('change', function(){
      $(this).closest('form').submit();
    });

  });
</script>
