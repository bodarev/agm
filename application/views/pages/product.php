<?
@session_start();
$uri1 = addslashes($this->uri->segment(1));
$uri2 = addslashes($this->uri->segment(2));
$uri3 = addslashes($this->uri->segment(3));
$uri4 = addslashes($this->uri->segment(4));
$uri5 = addslashes($this->uri->segment(5));

$lclang = strtolower($_SESSION['lang']);
$clang  = strtoupper($lclang);

unset($_SESSION['views'][$product['Cod']]);
$_SESSION['views'][$product['Cod']] = $product['Cod'];

$count  = count($_SESSION['views']);
$nrelem = 50;
if ($count > $nrelem) {
    $arr = array_slice($_SESSION['views'], $count - $nrelem, $nrelem, true);
    unset($_SESSION['views']);
    $_SESSION['views'] = $arr;
}
?>
<div class="breadcrumbs">
    <div class="in">
        <ul>
            <li><a href="/"><?= LINK_HOME ?></a></li>
            <li><a href="/<?= $lclang ?>/pages/catalog"><?= LINK_CATALOG ?></a>
            </li>
            <li>
                <a href="/<?= $lclang ?>/pages/catalog/<?= $product['CategoryUrl'] ?>"><?= $product['CategoryTitle'] ?></a>
            </li>
            <li><?= $title ?></li>
        </ul>
    </div>
</div>
<div class="b_detail">
    <div class="in">
        <div class="thumb thumb-slick">
            <?
            if (!empty($product['Image1'])) {
                $src1 = 'data:image/png;base64, '.$product['Image1'];
                if (!empty($product['Image2'])) {
                    $src2 = 'data:image/png;base64, '.$product['Image2'];
                    if (!empty($product['Image3'])) {
                        $src3 = 'data:image/png;base64, '.$product['Image3'];
                    }
                }
            } else {
                $src1 = newthumbs('noicon.png', 'i', 515, 408, '515x408x0', 0);
            }
            ?>
            <?php if (!empty($src1)): ?>
                <img src="<?= $src1 ?>">
            <?php endif; ?>

            <?php if (!empty($src2)): ?>
                <img src="<?= $src2 ?>">
            <?php endif; ?>

            <?php if (!empty($src3)): ?>
                <img src="<?= $src3 ?>">
            <?php endif; ?>
            <!-- <div class="label">%</div> -->
        </div>
        <div class="descr">
            <div class="txt">
                <h1><?= $title ?></h1>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <th><?= TEXT_ARTICOL ?>:</th>
                        <td><?= $product['Cod'] ?></td>
                    </tr>
                    <tr>
                        <th><?= TEXT_BALANCE ?>:</th>
                        <?
                        if ($product['Stoc'] == 1) {
                            $stock = TEXT_INSTOCK;
                        } else {
                            $stock = TEXT_OUTOFSTOCK;
                        }
                        ?>
                        <td><?= $stock ?></td>
                    </tr>
                    <tr>
                        <? if ($product['ProductDesc'] != '') { ?>
                            <th><?= TEXT_DESCRIPTION ?>:</th>
                            <td><?= $product['ProductDesc'] ?></td>
                        <? } ?>
                    </tr>
                </table>
            </div>
            <div class="b_buy">
                <div class="price v1"><?= TEXT_RETAILVALUE ?>:
                    <span><?= Exchange($product['Price'],false).' '.TEXT_MDL ?></span>
                </div>
                <? if (!isAuthorized() || isUser()) { ?>
                    <div class="price v2"><?= TEXT_ANGROVALUE ?>: <span
                                class="priceblur">Dealer <?= TEXT_MDL ?></span>
                    </div>
                <? } else { ?>
                    <div class="price v2"><?= TEXT_ANGROVALUE ?>:
                        <span><?= Exchange($product['PriceAngro'],true).' '
                                  .TEXT_MDL ?></span></div>
                <? } ?>
                <div class="buy_wr">
                    <input type="number" name="" id="<?= $product['Cod'] ?>"
                           value="1" min="1" max="<?= NR_ON_STOC ?>">
                    <button class="buy"></button>
                </div>
                <div id="alertMsg">
                </div>
            </div>
        </div>
        <!--  <div class="advert">
           <img src="/public/verstka/img/ico_c3.png" alt="">
           <p><strong>Сушилка бесплатно!</strong><br>Купите 5 сушилок и одну получите в подарок!</p>
         </div> -->
    </div>
</div>
<div class="c_wr8">
    <div class="in">
        <h2 class="title1"><?= TEXT_RELATEDPRODUCTS ?></h2>
        <div class="b_info2">
            <? foreach ($products as $value) { ?>
                <div class="item">
                    <div class="thumb">
                        <?
                        if (!empty($value->Image1)) {
                            //$src = newthumbs($value->Image, 'products', 218, 216, '218x216x0', 0);
                            $src = 'data:image/jpeg;base64, '.$value->Image1;
                        } else {
                            $src = newthumbs(
                                'noicon.png',
                                'i',
                                218,
                                216,
                                '218x216x1',
                                1
                            );
                        }
                        ?>
                        <a href="/<?= $lclang ?>/pages/product/<?= $value->UriName ?>"><img
                                    src="<?= $src ?>" alt=""></a>
                        <!-- <div class="label">%</div> -->
                    </div>
                    <div class="descr">
                        <div class="title"><a
                                    href="/<?= $lclang ?>/pages/product/<?= $value->UriName ?>"><?= $value->Title ?></a>
                        </div>
                        <div class="b_buy">
                            <div class="price"><?= Exchange($value->Price, false).' '
                                                   .TEXT_MDL ?></div>
                            <div class="buy_wr">
                                <input type="number" name=""
                                       id="<?= $value->Cod ?>" value="1" min="1"
                                       max="<?= NR_ON_STOC ?>">
                                <button class="buy"></button>
                            </div>
                            <div id="alertMsg">
                            </div>
                        </div>
                    </div>
                </div>
            <? } ?>
        </div>
    </div>
</div>
