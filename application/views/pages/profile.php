<?
$uri1=addslashes($this->uri->segment(1));
$uri2=addslashes($this->uri->segment(2));
$uri3=addslashes($this->uri->segment(3));
$uri4=addslashes($this->uri->segment(4));
$uri5=addslashes($this->uri->segment(5));

$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
?>
<div class="tabs_wr_profile v2 ui-tabs ui-corner-all ui-widget ui-widget-content">
    <div class="tabs">
      <div class="in">
        <ul class="ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header">
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active"><a class="ui-tabs-anchor" href=""><?=LINK_HISTORYORDERS?></a></li>
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"><a class="ui-tabs-anchor" href="/<?=$lclang?>/pages/profile/2"><?=LINK_VIEWEDPROD?></a></li>
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"><a class="ui-tabs-anchor" href="/<?=$lclang?>/pages/profile/3"><?=LINK_PROFILE?></a></li>
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"><a class="ui-tabs-anchor" href="/<?=$lclang?>/pages/profile/4"><?=LINK_CHANGEPASS?></a></li>
        </ul>
      </div>
    </div>
    <div class="tab_content">
      <div id="tab_b1">
        <div class="in">
          <div class="history_wr">
            <div class="head">
              <div class="info">
                  <form class="daterange" method="GET" action="">
                    <label for=""><?=TEXT_ORDERS.' '.TEXT_FROM?></label>
                    <? if(isset($_GET['dates'])) { $dates = $_GET['dates']; } else { $dates = date('01/01/Y').' - '.date('d/m/Y'); } ?>
                    <input type="text" id="dates" name="dates" value="<?=$dates?>" />
                  </form>
              </div>
              <!-- <div class="print"><a href=""><?=TEXT_PRINT?></a></div> -->
            </div>
            <div class="history">
              <div class="tbl_overflow">
                <table cellpadding="0" cellspacing="0">
                  <thead>
                  <tr>
                    <th><?=TEXT_NRORDER?></th>
                    <th><?=TEXT_STATUS?></th>
                    <th><?=TEXT_DATEADOPTION?></th>
                    <th><?=TEXT_DATECOMPLETION?></th>
                    <th><?=TEXT_DELIVERYADDRESS?></th>
                    <th><?=TEXT_AMOUNT?></th>
                    <th><?=TEXT_DELIVERY?></th>
                    <th><?=TEXT_MANAGER?></th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody id="accordion">
                  <? if (!empty($orders)) { foreach($orders as $order) { ?>    
                  <tr>
                    <td><?=$order->OrderNr?></td>
                    <td><?=$order->StatusName?></td>
                    <td><?=date('d.m.Y, H:i', strtotime($order->Date))?></td>
                    <td><?if ($order->FinalDate!=0)  { echo date('d.m.Y, H:i', strtotime($order->FinalDate)); } ?></td>
                    <td><?=$order->street?>, <?=$order->housenumber?></td>
                    <td><?=(float) $order->total?>&nbsp;<?=TEXT_MDL?></td>
                    <td><? if($order->delivery == 1) { echo TEXT_YES;} else  { echo TEXT_NO; } ?></td>
                    <td></td>
                    <td><button class="reload" id="<?=$order->ID?>"></button></td>
                  </tr>
                  <tr class="ddown_wr">
                    <td colspan="9">
                      <div class="ddown">

                        <div class="cols">
                          <div class="col">
                            <div class="b_info8">
                              <? foreach ($order->items as $item) {  ?>
                              <div class="tr">
                                <div class="td">
                                  <div class="thumb">
                                    <?
                                    if(!empty($item->Image1)) {
                                        $src = 'data:image/png;base64, '.$item->Image1;
                                    } else {
                                        $src = newthumbs('noicon.png', 'i', 52, 52, '515x408x0', 0);
                                    }
                                    ?>
                                    <a href=""><img src="<?=$src?>" alt=""></a>
                                  </div>
                                </div>
                                <div class="td">
                                  <div class="title"><a href=""><?=$item->NameProduct?></a></div>
                                  <div class="art"><span><?=TEXT_ARTICOL?>:</span> <?=$item->Cod?></div>
                                </div>
                                <div class="td"><?=$item->ItemCount?></div>
                                <div class="td"><div class="price"><?= (float) $item->ItemPrice?> <?=TEXT_MDL?></div></div>
                              </div>
                              <? } ?>
                             <!--  <div class="tr v1">
                                <div class="td"></div>
                                <div class="td">
                                  <div class="b_info10_wr">
                                    <div class="head">Сопутствующие товары:</div>
                                    <div class="b_info10">
                                      <a href=""><img src="img/img_a2.jpg" alt="">Заглушка для заварки кофейной гущи</a>

                                      <a href=""><img src="img/img_a8.jpg" alt="">Прибор 66F534</a>
                                    </div>
                                  </div>
                                </div>
                                <div class="td"></div>
                                <div class="td"></div>
                              </div> -->
                            </div>
                          </div>
                          <div class="col">
                            <div class="order_detail">
                              <div class="info">
                                <p><?=TEXT_COSTOFGOODS?>: <span><?=(float) $order->total?> <?=TEXT_MDL?></span></p>
                                <p><?=TEXT_COSTOFDELIVERY?>: <span><?=(float) $order->delivery_price?> <?=TEXT_MDL?></span></p>
                                <p><?=TEXT_TOTALCOST?>: <span><?=(float) $order->total + (float) $order->delivery_price ?> <?=TEXT_MDL?></span></p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <? } } else { ?> 
                    <tr><td colspan="9" align="center"><?=TEXT_EMPTYORDERS?></td></tr>
                  <? } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment-with-locales.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <script>
    var url = $(location).attr('href');
    var segments = url.split( '/' );
    var segment = segments[3];

    switch(segment) {
    case 'ro':
    moment.locale('ro');
    var apply = 'Aplica';
    var cancel = 'Anulare';
        break;
    case 'ru':
    moment.locale('ru');
    var apply = 'Применять';
    var cancel = 'Отмена';
        break
    case 'en':
    moment.locale('en');
    var apply = 'Apply';
    var cancel = 'Cancel';
        break;
    default:
    moment.locale('ro');
    var apply = 'Aplica';
    var cancel = 'Anulare';
    }

    $('input[name="dates"]').daterangepicker({
        locale: {
            applyLabel: apply,
            cancelLabel: cancel
        }
    });

    $(document).ready(function(e) {

      $('#dates').on('change', function(){
        $('form.daterange').submit();
      });

    });

  </script>
