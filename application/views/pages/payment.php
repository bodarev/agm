<?php
/**
 * Created by PhpStorm.
 * User: WhoAmI
 * Date: 29.03.2018
 * Time: 16:20
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Payment Generator</title>
</head>
<body>
<form style="display: none;" id="VictoriaBank" action="<?= $action_url ?>" method="post">
    <input value="<?= $amount ?>" name="AMOUNT"/>
    <input value="<?= $currency ?>" name="CURRENCY"/>
    <input value="<?= $rowsid ?>" name="ORDER"/>
    <input value="<?= $description ?>" name="DESC"/>
    <input value="<?= $merch_name ?>" name="MERCH_NAME"/>
    <input value="<?= $merch_url ?>" name="MERCH_URL"/>
    <input value="<?= $merchant ?>" name="MERCHANT"/>
    <input value="<?= $terminal ?>" name="TERMINAL"/>
    <input value="<?= $email ?>" name="EMAIL"/>
    <input value="<?= $tr_type ?>" name="TRTYPE"/>
    <input value="<?= $country ?>" name="COUNTRY"/>
    <input value="<?= $nonce ?>" name="NONCE"/>
    <input value="<?= $backref ?>" name="BACKREF"/>
    <input value="<?= $merch_gmt ?>" name="MERCH_GMT"/>
    <input value="<?= $timestamp ?>" name="TIMESTAMP"/>
    <input value="<?= $p_sign ?>" name="P_SIGN"/>
    <input value="<?= $lang ?>" name="LANG"/>
    <input value="<?= $merch_address ?>" name="MERCH_ADDRESS"/>
</form>
<script src="/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#VictoriaBank').submit();
    });
</script>
</body>
</html>
