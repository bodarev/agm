<?
$uri1   = addslashes($this->uri->segment(1));
$uri2   = addslashes($this->uri->segment(2));
$uri3   = addslashes($this->uri->segment(3));
$uri4   = addslashes($this->uri->segment(4));
$uri5   = addslashes($this->uri->segment(5));
$lclang = strtolower($_SESSION['lang']);
$clang  = strtoupper($lclang);
?>
<div class="c_wr9">
    <div class="in">
        <div class="autocomplete_content">
            <div class="search_form2">
                <input name="search" type="text" autocomplete="off"
                       placeholder="<?= PLACEH_SEARCH ?>">
                <div class="autocomplete_list">
                </div>
            </div>
        </div>
        <form method="POST">
            <div class="b_info5">
                <? foreach ($productsoncart as $item) { ?>
                    <div class="tr" id="p<?= $item->Cod ?>">
                        <div class="td">
                              <div class="thumb">
                                <?
                                if(!empty($item->Image1)) {
                                    $src = 'data:image/jpeg;base64, '.$item->Image1;
                                } else {
                                    $src = newthumbs('noicon.png', 'i', 52, 52, '52x52x1', 1);
                                }
                                ?>
                                <a href=""><img src="<?= $src ?>" alt=""></a>
                              </div>
                        </div>
                        <div class="td">
                            <div class="title"><a
                                        href="/<?= $lclang ?>/pages/product/<?= $item->UriName ?>"><?= $item->Title ?></a>
                            </div>
                        </div>
                        <?
                        if ($item->Stoc == 1) {
                            $stock = TEXT_INSTOCK;
                        } else {
                            $stock = TEXT_OUTOFSTOCK;
                        }
                        ?>
                        <div class="td"><span><?= TEXT_BALANCE ?>
                                :</span> <?= $stock ?></div>
                        <div class="td"><span><?= TEXT_ARTICOL ?>
                                :</span> <?= $item->Cod ?></div>
                        <div class="td">
                            <div class="price" id="price<?= $item->Cod ?>"><?= Exchange($_SESSION['cart']['items'][$item->Cod]['price'] * $_SESSION['cart']['items'][$item->Cod]['count']).' '
                                                   .TEXT_MDL ?></div>
                        </div>
                        <div class="td"><input type="number" name=""
                                               id="<?= $item->Cod ?>"
                                               value="<?= $_SESSION['cart']['items'][$item->Cod]['count'] ?>"
                                               min="1" max="<?= NR_ON_STOC ?>">
                        </div>
                        <div class="td">
                            <button formaction="/pages/delFromCart/<?= $item->Cod ?>"
                                    id="<?= $item->Cod ?>" class="del"></button>
                        </div>
                    </div>
                <? } ?>
            </div>
        </form>
        <form method="POST" action="/<?= $lclang ?>/pages/order_validation">
            <div class="final_comand">
                <textarea name="commenttoorder" id="commenttoorder"></textarea>
                <div>
                    <select name="district" id="district">
                        <?php
                        foreach ($districts as $item) {
                            if(isset($_SESSION['sess_district']) && $_SESSION['sess_district'] == $item->ID ) {
                                $sel = ' selected';
                            } else {
                                $sel = ' ';
                            }
                            echo "<option ".$sel." value='".$item->ID."'>".$item->Name."</option>";
                        }
                        ?>
                    </select>
                    <br/>
                    <br/>
                    <select name="dealer" id="dealer" data-placeholder="Dealer">
                        <?php
                        foreach ($dealers as $item) {
                            if(isset($_SESSION['sess_dealer']) && $_SESSION['sess_dealer'] == $item->ID ) {
                                $sel = ' selected';
                            } else {
                                $sel = ' ';
                            }

                            echo "<option ".$sel." value='" . $item->ID . "'>".$item->Name ."</option>";
                        }
                        ?>
                    </select>
                    <br/>
                    <br/>
                    <a style="pointer-events: none; opacity:0.4;"
                       href="/<?= $lclang ?>/pages/cart/2">
                        <button><?= BUTTON_COMPLETEORDER ?></button>
                    </a>
                </div>
            </div>
        </form>

    </div>
</div>
<script>
    $('body').on('change', '#dealer', function (event) {

        val = $(this).val();
        console.log(val);
        $.ajax({
            type: "POST",
            url: "/pages/setSess",
            data: {
                'val': val ,
                'type': 'dealer'
            },
            dataType: 'json',
            success: function (res) {
                if (res.error == true) {
                    console.log(res);
                } else {
                    console.log(res);
                    location.reload();  
                }
            },
            error: function (res) {
                console.log(res);
            }
        });       

    });

    $('body').on('change', '#district', function (event) {

        val = $(this).val();

        $.ajax({
            type: "POST",
            url: "/pages/setSess",
            data: {
                'val': val,
                'type': 'district'
            },
            dataType: 'json',
            success: function (res) {
                if (res.error == true) {
                    console.log(res);
                } else {
                    console.log(res);
                }
            },
            error: function (res) {
                console.log(res);
            }
        });       

    });

    $('body').on('click', '.autocomplete_item', function (event) {
        event.preventDefault();
        $('.autocomplete_list').hide();
        $('.autocomplete_content input').val('');

        let cod = $(this).attr('id');
        let curency = $(".curency").val();

        $.ajax({
            type: "POST",
            url: "/pages/addToCart",
            data: {
                'cod': cod,
                'count': 1
            },
            dataType: 'json',
            success: function (res) {
                if (res.error == true) {
                    console.log(res);
                } else {
                    $(".h_cart a").empty();
                    $(".h_cart a").append('<span>' + res.count + '</span> ' + res.total + ' ' + curency);
                    if (res.product.Image1 != '') {
                        src = "data:image/jpeg;base64, " + res.product.Image1;
                    } else {
                        src = "<?=newthumbs('noicon.png', 'i', 52, 52, '52x52x1', 1); ?>";
                    }
                    if (res.product.Stoc == 1) {
                        stock = "<?=TEXT_INSTOCK?>";
                    } else {
                        stock = "<?=TEXT_OUTOFSTOCK ?>";
                    }
                    if (res.newProduct) {
                        $('.b_info5').append('<div class="tr"><div class="td"><div class="thumb"><a href=""><img src="' + src + '" alt=""></a></div></div><div class="td"><div class="title"><a href="/<?=$lclang?>/pages/product/' + res.product.UriName + '">' + res.product.ProductTitle + '</a></div></div><div class="td"><span><?=TEXT_BALANCE?>: </span>' + stock + '</div><div class="td"><span><?=TEXT_ARTICOL?>: </span> ' + res.product.Cod + '</div><div class="td"><div class="price" id="price' + res.product.Cod + '">' + res.price + ' <?=TEXT_MDL?></div></div><div class="td"><input type="number" name="" id="' + res.product.Cod + '"' + '" value="' + res.qty + '" min="1" max="<?=NR_ON_STOC?>"></div><div class="td"><button formaction="/pages/delFromCart/'+res.product.Cod +'" id="' + res.product.Cod + '" class="del"></button></div></div>');
                        $('input, number').styler();
                    } else {
                        $('#price' + res.product.Cod).html((res.price * res.qty).toFixed(2) + ' ' + '<?=TEXT_MDL ?>');
                        $('#' + res.product.Cod, $('.b_info5')).val(function(i, oldval) {
                            return ++oldval;
                        });
                    }
                    $('.final_comand a').css({'pointer-events': 'auto', 'opacity': '1'});
                }
            },
            error: function (res) {
                console.log(res);
            }
        });
    });


        
    /* $(document).on('keyup', '.final_comand textarea', function(){ */
        sess= false;
        <? if (isset($_SESSION['cart']['total']) && $_SESSION['cart']['total']>0) { ?>
            sess= true;
        <? } ?>

        if ( sess == true ) {
            $('.final_comand a').css({'pointer-events': 'auto', 'opacity': '1'});
        } else {
            $('.final_comand a').css({'pointer-events': 'none', 'opacity': '0.4'});
        }
    /* }); */

</script>
