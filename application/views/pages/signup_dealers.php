<?
$uri1=addslashes($this->uri->segment(1));
$uri2=addslashes($this->uri->segment(2));
$uri3=addslashes($this->uri->segment(3));
$uri4=addslashes($this->uri->segment(4));
$uri5=addslashes($this->uri->segment(5));

$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
?>
<script>
    $(document).ready(function() {
        var max_fields      = 12; //maximum input boxes allowed
        var wrapper         = $(".wrapper"); //Fields wrapper
        var add             = $(".add"); //Add Link ID

        var x = 1;
        $(add).click(function(e){
            e.preventDefault();
            if(x < max_fields){
                x++;
                var prev = $('.wrapper input').size();
                prev++;
                $('.f_add').before('<div class="f_row"><input placeholder="<?=PLACEH_DELIVERYADDRESS?>" type="text" name="delivery[x'+prev+']"/></div>');
            }
        });
    });
</script>
<div class="m_register">
    <div class="in">
      <div class="head">
        </div>
      <div class="form_wr">
        <div class="title"><?=TEXT_REG_DEALER?></div>
        <form method="post" action="/<?=$lclang?>/pages/dealer_validation">
        <div class="f_row">
        <div class="form_paceholder hide"><?=PLACEH_FULLNAME?></div>
        <input value="<?=$this->input->post('fullname')?>" type="text" name="fullname" id="fullname" placeholder="<?=PLACEH_FULLNAME?> *">
        <span class="error"><?= $fullname_errore = form_error('fullname'); ?></span>
      </div>
<!--           <div class="f_row">
            <div class="form_paceholder hide"><?=PLACEH_ADMINISTRATOR?></div>
            <input value="<?=$this->input->post('administrator')?>" type="text" name="administrator" id="administrator" placeholder="<?=PLACEH_ADMINISTRATOR?> *">
            <span class="error"><?= $administrator_errore = form_error('administrator'); ?></span>
          </div>
          <div class="f_row">
            <div class="form_paceholder hide"><?=PLACEH_LEGALADDRESS?></div>
            <input value="<?=$this->input->post('legaladdress')?>" type="text" name="legaladdress" id="legaladdress" placeholder="<?=PLACEH_LEGALADDRESS?> *">
            <span class="error"><?= $legaladdress_errore = form_error('legaladdress'); ?></span>
          </div>
 -->          <div class="wrapper">
            <?
            $null = false;
            $delivery = $this->input->post('delivery');
            if (isset($delivery)) {
              $count = count($delivery);
              $j=0;
              for($i=1;$i<=$count;$i++) {

                if(!empty($delivery['x'.$i])) { $j++; ?>



                <div class="f_row">
                  <div class="form_paceholder hide"><?=PLACEH_DELIVERYADDRESS?></div>
                  <input value="<?=$delivery['x'.$i]?>" type="text" name="delivery[x<?=$j?>]" id="delivery[x<?=$j?>]" placeholder="<?=PLACEH_DELIVERYADDRESS?> <?if($j==1) { echo " *";}?>">
                  <span class="error"><?= $deliveryaddress_errore = form_error('delivery[x'.$j.']'); ?></span>
                </div>

                    <?
                }
              }
              if($j==0) {
                  $null = true;
              }
            } else {
                $null = true;
            } ?>

            <?if ($null) {  ?>

              <div class="f_row">
                <div class="form_paceholder hide"><?=PLACEH_DELIVERYADDRESS?></div>
                <input type="text" name="delivery[x1]" id="delivery" placeholder="<?=PLACEH_DELIVERYADDRESS?> *">
                <span class="error"><?= $deliveryaddress_errore = form_error('delivery[x1]'); ?></span>
              </div>

              <? } ?>

            <div class="f_add"><a href="" class="add"><?=LINK_ADDADDRESS?></a></div>
          </div>

          <div class="label"><?=TEXT_PAYMENTRESPONSIBLE?></div>
          <div class="cols">
            <div class="f_row">
              <div class="form_paceholder hide"><?=PLACEH_NAME?></div>
              <input value="<?=$this->input->post('resp_name')?>" type="text" name="resp_name" id="resp_name" placeholder="<?=PLACEH_NAME?> *">
              <span class="error"><?= $resp_name_errore = form_error('resp_name'); ?></span>
            </div>
            <div class="f_row">
              <div class="form_paceholder hide"><?=PLACEH_PHONE?></div>
              <input value="<?=$this->input->post('resp_phone')?>" type="tel" name="resp_phone" id="resp_phone" pattern="^\+373(\s+)?\(?(79|78|69|68|67|60)\)?(\s+)?[0-9]{3}-?[0-9]{3}$">
              <span class="error"><?= $resp_phone_errore = form_error('resp_phone'); ?></span>
            </div>
          </div>

          <div class="label"><?=TEXT_BOOKKEEPING?></div>
          <div class="cols">
            <div class="f_row">
              <div class="form_paceholder hide"><?=PLACEH_NAME?></div>
              <input value="<?=$this->input->post('book_name')?>" type="text" name="book_name" id="book_name" placeholder="<?=PLACEH_NAME?> *">
              <span class="error"><?= $book_name_errore = form_error('book_name'); ?></span>
            </div>
            <div class="f_row">
              <div class="form_paceholder hide"><?=PLACEH_PHONE?></div>
              <input value="<?=$this->input->post('book_phone')?>" type="tel" name="book_phone" id="book_phone" pattern="^\+373(\s+)?\(?(79|78|69|68|67|60)\)?(\s+)?[0-9]{3}-?[0-9]{3}$">
              <span class="error"><?= $book_phone_errore = form_error('book_phone'); ?></span>
            </div>
          </div>

          <div class="f_row">
            <div class="form_paceholder hide"><?=PLACEH_IDNP?></div>
            <input value="<?=$this->input->post('idnp')?>" type="text" name="idnp" id="idnp" placeholder="<?=PLACEH_IDNP?> *">
            <span class="error"><?= $idnp_errore = form_error('idnp'); ?></span>
          </div>
          <!-- <div class="f_row">
            <div class="form_paceholder hide"><?=PLACEH_VATCODE?></div>
            <input value="<?=$this->input->post('vatcode')?>" type="text" name="vatcode" id="vatcode" placeholder="<?=PLACEH_VATCODE?> *">
            <span class="error"><?= $vatcode_errore = form_error('vatcode'); ?></span>
          </div> -->
          <div class="f_row">
            <div class="form_paceholder hide"><?=PLACEH_ACCOUNT?></div>
            <input value="<?=$this->input->post('account')?>" type="text" name="account" id="account" placeholder="<?=PLACEH_ACCOUNT?> *">
            <span class="error"><?= $account_errore = form_error('account'); ?></span>
          </div>
          <div class="f_row">
            <div class="form_paceholder hide"><?=PLACEH_BANK?></div>
            <input value="<?=$this->input->post('bank')?>" type="text" name="bank" id="bank" placeholder="<?=PLACEH_BANK?> *">
            <span class="error"><?= $bank_errore = form_error('bank'); ?></span>
          </div>
          <div class="f_row">
            <select name="district" id="district">
              <?php
                foreach ($districts as $item) {
                  echo "<option value='".$item->ID."'>".$item->Name."</option>";
                }
              ?>
            </select>
          </div>
          <div class="f_row">
            <div class="form_paceholder hide"><?=PLACEH_EMAIL?></div>
            <input value="<?=$this->input->post('email')?>" type="text" name="email" id="email" placeholder="<?=PLACEH_EMAIL?> *">
            <span class="error"><?= $email_errore = form_error('email'); ?></span>
          </div>
          <div class="f_row">
            <div class="form_paceholder hide"><?=PLACEH_PHONE?></div>
            <input value="<?=$this->input->post('phone')?>" type="tel" name="phone" id="phone" pattern="^\+373(\s+)?\(?(79|78|69|68|67|60)\)?(\s+)?[0-9]{3}-?[0-9]{3}$">
            <span class="error"><?= $phone_errore = form_error('phone'); ?></span>
          </div>
          <div class="f_row">
            <div class="form_paceholder hide"><?=PLACEH_PASSWORD?></div>
            <input value="<?=$this->input->post('password')?>" type="password" name="password" id="password" placeholder="<?=PLACEH_PASSWORD?> *">
            <span class="error"><?= $pass_errore = form_error('password'); ?></span>
          </div>
          <div class="f_row">
            <div class="form_paceholder hide"><?=PLACEH_PASSWORDAGAIN?></div>
            <input value="<?=$this->input->post('passwordagain')?>" type="password" name="passwordagain" id="passwordagain" placeholder="<?=PLACEH_PASSWORDAGAIN?> *">
            <span class="error"><?= $passagain_errore = form_error('passwordagain'); ?></span>
          </div>
          <div class="b_btn">
            <input type="submit" value="<?=BUTTON_REGISTRATION?>"></div>
        </form>
      </div>
    </div>
  </div>