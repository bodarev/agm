<?
$uri1=addslashes($this->uri->segment(1));
$uri2=addslashes($this->uri->segment(2));
$uri3=addslashes($this->uri->segment(3));
$uri4=addslashes($this->uri->segment(4));
$uri5=addslashes($this->uri->segment(5));

$lclang=strtolower($_SESSION['lang']);
$clang=strtoupper($lclang);
?>
<div class="tabs_wr_profile v2 ui-tabs ui-corner-all ui-widget ui-widget-content">
    <div class="tabs">
      <div class="in">
        <ul class="ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header">
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"><a class="ui-tabs-anchor" href="/<?=$lclang?>/pages/profile"><?=LINK_HISTORYORDERS?></a></li>
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active"><a class="ui-tabs-anchor" href=""><?=LINK_VIEWEDPROD?></a></li>
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"><a class="ui-tabs-anchor" href="/<?=$lclang?>/pages/profile/3"><?=LINK_PROFILE?></a></li>
          <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"><a class="ui-tabs-anchor" href="/<?=$lclang?>/pages/profile/4"><?=LINK_CHANGEPASS?></a></li>
        </ul>
      </div>
    </div>
    <div class="tab_content">
      <div id="tab_b2">
        <div class="in">
        <div class="b_info2">
          <? if(!empty($products)) { ?>
          <? foreach ($products as $item) { ?>
          <div class="item">
            <div class="thumb">
              <?
              if(!empty($item->Image1)) {
                  //$src = newthumbs($item->Image, 'products', 218, 216, '218x216x0', 0);
                  $src = 'data:image/jpeg;base64, '.$item->Image1;
              } else {
                  $src = newthumbs('noicon.png', 'i', 218, 216, '218x216x1', 1);
              }
              ?>
              <a href="/<?=$lclang?>/pages/product/<?=$item->UriName?>"><img src="<?=$src?>" alt=""></a>
              <!-- <div class="label">%</div> -->
            </div>
            <div class="descr">
              <div class="title"><a href="/<?=$lclang?>/pages/product/<?=$item->UriName?>"><?=$item->Title?></a></div>
              <div class="b_buy">
                <div class="price"><?=Exchange($item->Price,false)?> <?=TEXT_MDL?></div>
                <div class="buy_wr">
                  <input type="number" name="" id="<?=$item->Cod?>" value="1" min="1" max="<?=NR_ON_STOC?>" >
                  <button class="buy"></button>
                </div>
                <div id="alertMsg">
                </div>
              </div>
            </div>
          </div>
          <? } 
           } else { 
            echo '<div class="head">'.TEXT_EMPTYVIEWS.'</div>';  
          } ?>
          </div>
          <? /* $this->load->view('utils/paginator'); */ ?>
        </div>
        </div>
      </div>
    </div>
  </div>
