<?
$uri3 = $this->uri->segment(3);

$head1 = 'Каталог';
$head2 = 'Редактирование Каталог';
$addnew = 'Товара';
$tblname = 'Catalog';
$headerloc = 'catalog';

$e_path = '/' . ADM_CONTROLLER . '/catalog/';
$delpath = '/' . ADM_CONTROLLER . '/del_' . $headerloc . '/';
$err = '';

$form1 = array(
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Ссылка *',
        'name' => 'UriName',
        'dop_data' => '<br /><div style="width: 97%; background-color: #FFFF00; border: 1px solid; padding: 5px;">
			без http://www и т.п. просто английская фраза, без пробелов, отражающая пункт меню, например: <br>
			<em style="font-size: 10px;">Haш подход: our-approach</em>
		</div>'
    ),
    array(
        'dop_tr'=>'Catalog',
        'dop_style'=>'',
        'type'=>'select',
        'class'=>'form-control',
        'descr'=>'Родительский раздел',
        'name'=>'ParentCod',
        'source'=>'Category',
        'sourcename'=>'TitleRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Название RU*',
        'name' => 'TitleRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Название RO*',
        'name' => 'TitleRO'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Название EN*',
        'name' => 'TitleEN'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'textarea',
        'class' => 'form-control ckeditor',
        'descr' => 'Описание RU',
        'name' => 'TextRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control ckeditor',
        'type' => 'textarea',
        'descr' => 'Описание RO',
        'name' => 'TextRO'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control ckeditor',
        'type' => 'textarea',
        'descr' => 'Описание EN',
        'name' => 'TextEN'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Cod *',
        'name' => 'Cod'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Model',
        'name' => 'Model'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Pret Promo',
        'name' => 'PricePromo'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Pret Angro',
        'name' => 'PriceAngro'
    ),
);
$form2 = array(
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Заголовок RU',
        'name' => 'OptimizationTitleRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Заголовок RO',
        'name' => 'OptimizationTitleRO'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Заголовок EN',
        'name' => 'OptimizationTitleEN'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Ключевые слова RU',
        'name' => 'KeywordsRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Ключевые слова RO',
        'name' => 'KeywordsRO'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Ключевые слова EN',
        'name' => 'KeywordsEN'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Описание RU',
        'name' => 'DescriptionRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Описание RO',
        'name' => 'DescriptionRO'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Описание EN',
        'name' => 'DescriptionEN'
    )
);

$Name = '';
$files=array('Image');
$checker = array('UriName', 'TitleRU', 'TitleRO', 'TitleEN', 'Cod');
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $erflag = false;

    foreach ($checker as $val) {
        if (empty($_POST['data'][$val])) $erflag = true;
    }

    if (!$erflag) {

        $data_array = $_POST['data'];

        if (empty($uri3)) {
            $this->db->insert($tblname, $data_array);
            $id = $this->db->insert_id();
        } else {
            $id = $uri3;
            $this->db->where('ID', $id)->update($tblname, $data_array);
        }

        foreach($files as $filename) {

            if(!empty($_FILES[$filename]['name'])) {

                $this->upload->do_upload($filename);
                $resarr = $this->upload->data();
                $file = $resarr['file_name'];

                if(strtolower($resarr['file_ext']) == '.jpg' || strtolower($resarr['file_ext']) == '.jpeg' || strtolower($resarr['file_ext']) == '.gif' || strtolower($resarr['file_ext']) == '.png')
                {
                    $this->db->where('ID',$id)->update($tblname,array($filename=>$file));
                }
            }
        }

        if (!empty($id)) {
            header("Location: /" . ADM_CONTROLLER . "/$headerloc/");
            exit();
        }
    } else {
        $err .= '<div style="padding:10px 0;color:#ff0000;">Все поля отмеченные * обязательны для заполения</div>';
    }
}

?>
  <script type="text/javascript">
    function toggleb() {
      $("#newb").toggle();
    }
  </script>
  <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
  <script>
    function localsort() {
      counter = 1;
      data = '';
      $.each($('.sorthold'), function () {
        $(this).html(counter);
        if (counter < 2) breaker = '';
        else breaker = '<>';
        data += breaker + $(this).attr('oid') + ':' + counter;
        counter++;
      });
      $.post('/<?=ADM_CONTROLLER?>/edit_table_order/', {
        data: data,
        table: '<?=$tblname?>'
      }, function (ret) {

      });
    }
  </script>
  <? if (empty($uri3)) { ?>
    <div class="page-bar">
      <ul class="page-breadcrumb">
        <li>
          <i class="fa fa-home"></i>
          <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
          <i class="fa fa-angle-right"></i>
        </li>
        <li>
          <a>
            <?= $head1 ?>
          </a>
        </li>
      </ul>
    </div>

    <?= $err ?>

      <form name="form1" method="POST" action="<?= $e_path ?>" enctype="multipart/form-data">
        <div class="portlet box">
          <div class="portlet-title">
            <div class="caption" style="color:#888;font-size:26px;">
              <?= $head1 ?>
            </div>
            <div class="tools">
              <a href="javascript:;" class="expand btn default yellow-stripe" style="width: auto !important; height: auto !important">
                <i class="fa fa-plus"></i>
                <span class="hidden-480"> Добавить
                  <?= $addnew ?>
                </span>
              </a>
            </div>
          </div>
          <div class="portlet-body" style="<? if (empty($err)) echo 'display:none;'; ?>">
            <div class="tabbable tabbable-custom">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1_1" data-toggle="tab">Общая информация</a>
                </li>
                <li>
                  <a href="#tab_1_2" data-toggle="tab">Служебная информация</a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                  <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover">
                      <?
                    create_form_by_array($form1, @$_POST['data']);
                    ?>
                        <!-- <tr>
                    <td>Изображение</td>
                    <td>
                      <div class="fileinput fileupload fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="max-width: 200px;">
                          <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                        <div>
                          <span class="btn default btn-file">
                            <span class="fileinput-new">Выбрать </span>
                            <span class="fileinput-exists">Изменить </span>
                            <input type="file" name="Image">
                          </span>
                          <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Отмена </a>
                          <span class="help-block">
                            Допустимые форматы
                            <code>jpg</code>
                            <code>png</code>
                            <code>gif</code>
                          </span>
                        </div>
                      </div>
                    </td>
                  </tr> -->
                        <tr>
                          <td>&nbsp;</td>
                          <td>
                            <button type="submit" class="btn green">
                              <i class="fa fa-check"></i> Добавить
                            </button>
                          </td>
                        </tr>
                    </table>
                  </div>
                </div>
                <div class="tab-pane" id="tab_1_2">
                  <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover">
                      <?
                    create_form_by_array($form2, @$_POST['data']);
                    ?>
                        <tr>
                          <td>&nbsp;</td>
                          <td>
                            <button type="submit" class="btn green">
                              <i class="fa fa-check"></i> Добавить
                            </button>
                          </td>
                        </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
      <br/>
      <?
    $checkb = $this->db->order_by('ParentCod')->get($tblname)->result_array();
    if (!empty($checkb)) {

        $ParentCod = '';

        foreach ($checkb as $barr)
        {
          if ($ParentCod != $barr['ParentCod']) {
            $ParentCod = $barr['ParentCod'];
            if($ParentCod!=0) {
              $result = $this->db->where('Cod', $ParentCod)->get('Category')->row()->TitleRU;
            } else {
              $result = 'FARA CATEGORIE';
            }
            if($ParentCod!=0) {
              echo '</table></div></div></div>';
            }
            echo '<div class="portlet box green">
              <div class="portlet-title">
                <div class="caption">'.$result.'</div>
                <div class="tools">
                  <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                </div>
            </div>';
          
            echo '<div class="portlet-body" style="display:none"><div class="table-scrollable">
            <table class="table table-striped table-bordered table-hover dataTable no-footer dragger">
              <tr class="heading nodrop nodrag">
                <th>Сортировка</th>
                <th width="30" style="text-align: center"><i class="fa fa-eye" aria-hidden="true"></i></th>
                <th>Название</th>
                <th width="220">Действия</th>
              </tr>';
          }

            echo "<tr>";
            echo "<td width='40' celpadding='5' cellspacing='10' oid='" . $barr['ID'] . "' align='center' class='sorthold'>";
            echo $barr['Sorder'];
            echo "</td>";
            $cmod = '';
            if (!empty($barr['OnSite'])) $cmod = ' checked';
            echo "<td><label><input type='checkbox'$cmod  value='" . $barr['ID'] . "' class='showsite'></label></td>";
            echo "<td><a href='$e_path" . $barr['ID'] . "/'>" . $barr['TitleRU'] . "</a>";
            echo "</td>";
            echo "<td align=''>";
            echo '<a href="' . $e_path . $barr['ID'] . '/" class="btn btn-xs default btn-editable green-stripe"><i class="glyphicon glyphicon-edit"></i> Изменить</a>';
                echo '<a href="' . $delpath . $barr['ID'] . '/" class="btn btn-xs default btn-editable red-stripe"><i class="glyphicon glyphicon-remove-circle"></i> Удалить</a>';
            echo "</td>";
            echo "</tr>";
        }
        // echo "</table>";
        // echo '</div>';
    }
} else {
    $query = $this->db->where('ID', $uri3)->get($tblname);
    $data = $query->row_array();
    ?>
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="/<?= ADM_CONTROLLER ?>/<?= $headerloc ?>/">
                <?= $head1 ?>
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a>Редактирование "
                <?= $data['TitleRU'] ?>"</a>
            </li>
          </ul>
        </div>
        <?= $err ?>

          <form name="form1" method="POST" action="<?= $e_path . $uri3; ?>/" enctype="multipart/form-data">
            <div class="tabbable tabbable-custom">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1_1" data-toggle="tab">Общая информация</a>
                </li>
                <li>
                  <a href="#tab_1_2" data-toggle="tab">Служебная информация</a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                  <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover">
                      <?
                create_form_by_array($form1, $data);
                ?>
                        <!--  <tr>
                <td>Изображение</td>
                <td>
                  <div class="fileinput fileupload fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="max-width: 200px;">
                        <?php if ($data['Image']  && file_exists('public/catalog/' .$data['Image'])): ?>
                          <img src="/public/catalog/<?=$data['Image']?>" id="thumb-img" alt="">
                        <?php else: ?>
                          <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                        <?php endif ?>
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                    <div>
                                            <span class="btn default btn-file">
                                              <span class="fileinput-new">Выбрать </span>
                                              <span class="fileinput-exists">Изменить </span>
                                              <input type="file" name="Image">
                                            </span>

                        <?php if ($data['Image']){ ?>
                          <a data-toggle="modal" href="#myModal<?=$data['ID']?>" class="btn btn-danger del_btn" ><i class="fa fa-trash"></i> Удалить </a>
                          <!-- Modal -->
                        <div class="modal fade theme-modal" id="myModal<?=$data['ID']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                          aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Подтвердить операцию</h4>
                              </div>
                              <div class="modal-body">
                                Вы действительно желаете удалить?
                              </div>
                              <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Отмена</button>
                                <button class="btn btn-success" type="button" onclick="toDeleteImg(this,'/cp/deleteImageElement/','<?=@$data['ID']?>', '<?=$tblname?>', '')">Удалить</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- Modal -->
                        <?php } else { ?>
                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Отмена </a>
                        <?php } ?>
                        <span class="help-block">Допустимые форматы
                          <code>jpg</code>
                          <code>png</code>
                          <code>gif</code>
                        </span>
                  </div>
                </div>
                </td>
                </tr> -->
                <tr>
                  <td></td>
                  <td>
                    <button type="submit" class="btn green">
                      <i class="fa fa-check"></i> Обновить
                    </button>
                  </td>
                </tr>
                </table>
              </div>
            </div>
            <div class="tab-pane" id="tab_1_2">
              <div class="table-scrollable">
                <table class="table table-striped table-bordered table-hover">
                  <?
                create_form_by_array($form2, $data);
                ?>
                    <tr>
                      <td>&nbsp;</td>
                      <td>
                        <button type="submit" class="btn green">
                          <i class="fa fa-check"></i> Обновить
                        </button>
                      </td>
                    </tr>
                </table>
              </div>
            </div>
            </div>
          </form>
          <br/>
          <? } ?>

            <script>
              $(document).ready(function () {
                $(document).on('change', '.showsite', function () {
                  if ($(this).is(':checked')) {
                    val = 1;
                  } else {
                    val = 0;
                  }
                  table = "<?=$tblname?>";
                  id = $(this).val();
                  $.get('/cp/setsite/' + id + '/' + val + '/' + table);
                });
              });
            </script>
            <style>
              .table>thead>tr>th,
              .table>tbody>tr>th,
              .table>tfoot>tr>th,
              .table>thead>tr>td,
              .table>tbody>tr>td,
              .table>tfoot>tr>td {
                vertical-align: inherit;
              }

              .portlet-head {
                border-bottom: 0;
                padding: 0 10px;
                margin-bottom: 0;
                color: #fff;
              }

              .portlet>.portlet-head>.tools {
                float: right;
                display: inline-block;
                padding: 12px 0 8px 0;
              }

              .portlet>.portlet-head>.tools>a {
                display: inline-block;
                height: 16px;
                margin-left: 5px;
                opacity: 1;
              }

              .portlet.box>.portlet-head>.tools>a.remove {
                background-image: url(/theme/assets/global/img/portlet-remove-icon-white.png);
                width: 11px;
              }

              .portlet>.portlet-head>.caption {
                float: left;
                display: inline-block;
                font-size: 18px;
                line-height: 18px;
                font-weight: 300;
                padding: 10px 0;
              }
            </style>