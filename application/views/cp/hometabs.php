<?
$uri3 = $this->uri->segment(3);

$head1 = 'Табы на главной';
$head2 = 'Редактирование';
$addnew = 'Таб';
$tblname = 'HomeTabs';
$headerloc = 'hometabs';

$e_path = '/' . ADM_CONTROLLER . '/' . $headerloc . '/';
$delpath = '/' . ADM_CONTROLLER . '/del_' . $headerloc . '/';
$err = '';

$form1 = array(
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Название RU*',
        'name' => 'NameRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Название RO*',
        'name' => 'NameRO'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Название EN*',
        'name' => 'NameEN'
    ),   
);
$files = array();
$checker = array('NameRU','NameRO','NameEN',);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $erflag = false;

    if (!$erflag) {
        foreach ($checker as $val) {
            if (empty($_POST['data'][$val])) $erflag = true;
        }
    }

    if (!$erflag) {
        $data_array = $_POST['data'];
        if (empty($uri3)) {
            $this->db->insert($tblname, $data_array);
            $id = $this->db->insert_id();
        } else {
            $id = $uri3;
            $this->db->where('ID', $id)->update($tblname, $data_array);
        }

        foreach ($files as $filename) {

            if (!empty($_FILES[$filename]['name'])) {
                $this->upload->do_upload($filename);
                $resarr = $this->upload->data();
                $file = $resarr['file_name'];

                if (strtolower($resarr['file_ext']) == '.jpg' || strtolower($resarr['file_ext']) == '.jpeg' || strtolower($resarr['file_ext']) == '.gif' || strtolower($resarr['file_ext']) == '.png') {
                    $this->db->where('ID', $id)->update($tblname, array($filename => $file));
                }
            }
        }

        if (!empty($id)) {
            header("Location: /" . ADM_CONTROLLER . "/$headerloc/");
            exit();
        }
    } else {
        $err .= '<div style="padding:10px 0;color:#ff0000;">Все поля отмеченные * обязательны для заполения</div>';
    }
}

?>
  <script type="text/javascript">
      function toggleb() {
          $("#newb").toggle();
      }
  </script>
  <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
  <script>
      function localsort() {
          counter = 1;
          data = '';
          $.each($('.sorthold'), function () {
              $(this).html(counter);
              if (counter < 2) breaker = ''; else breaker = '<>';
              data += breaker + $(this).attr('oid') + ':' + counter;
              counter++;
          });
          $.post('/<?=ADM_CONTROLLER?>/edit_table_order/', {data: data, table: '<?=$tblname?>'}, function (ret) {

          });
      }
  </script>
<? if (empty($uri3)) { ?>
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a><?= $head1 ?></a>
      </li>
    </ul>
  </div>

    <?= $err ?>

  <form name="form1" method="POST" action="<?= $e_path ?>" enctype="multipart/form-data">
    <div class="portlet box">
      <div class="portlet-title">
        <div class="caption" style="color:#888;font-size:26px;">
            <?= $head1 ?>
        </div>
        <div class="tools">
          <a href="javascript:;" class="expand btn default yellow-stripe" style="width: auto !important; height: auto !important">
            <i class="fa fa-plus"></i>
            <span class="hidden-480">
					                Добавить <?= $addnew ?>
                        </span>
          </a>
        </div>
      </div>
      <div class="portlet-body" style="<? if (empty($err)) echo 'display:none;'; ?>">
        <div class="table-scrollable">
          <table class="table table-striped table-bordered table-hover">
              <?
              create_form_by_array($form1, @$_POST['data']);
              ?>
            <tr>
              <td>&nbsp;</td>
              <td>
                <button type="submit" class="btn green"><i class="fa fa-check"></i> Добавить</button>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </form><br/>
    <?
    $checkb = $this->db->order_by('Sorder ASC, ID DESC')->get($tblname)->result_array();
    if (!empty($checkb)) {
        echo '<div class="table-scrollable">
		<table class="table table-striped table-bordered table-hover dataTable no-footer dragger">';
        echo '<tr class="heading nodrop nodrag">
			<th>Сортировка</th>
			<th style="width:40px; text-align: center"><i class="fa fa-eye" aria-hidden="true"></i></th>
			<th>Название</th>
			<th width="250">Действия</th>
		</tr>';

        foreach ($checkb as $barr) {
            echo "<tr>";
            echo "<td width=\"40\" celpadding=\"5\" cellspacing=\"10\" oid=\"" . $barr['ID'] . "\" align=\"center\" class=\"sorthold\">";
            echo $barr['Sorder'];
            echo "</td>";
            $cmod = $cmod1 = $cmod2 = '';
            if (!empty($barr['OnSite'])) $cmod = ' checked'; ?>
                <td>
                    <label>
                        <input data-table="<?=$tblname?>" data-col="OnSite" data-id="<?= $barr['ID'] ?>" class="check" type="checkbox" <?=$cmod?>>
                    </label>
                </td>
            
            <? echo "<td><a href=\"$e_path" . $barr['ID'] . "/\">" . $barr['NameRU'] . "</a>";
            echo "</td>";
            echo "<td align=\"center\">";
            echo '<a href="' . $e_path . $barr['ID'] . '/" class="btn btn-xs default btn-editable green-stripe"><i class="glyphicon glyphicon-edit"></i> Редактировать</a>';
            echo '<a href="' . $delpath . $barr['ID'] . '/" class="btn btn-xs default btn-editable red-stripe"><i class="glyphicon glyphicon-remove-circle"></i> Удалить</a>';
            echo "</td>";
            echo "</tr>";
        }
        echo "</table>";
        echo '</div>';
    }
} else {
    $query = $this->db->where('ID', $uri3)->get($tblname);
    $products = $this->db->select('TabProd.*, Products.TitleRU as Title')
    ->join('Products', 'Products.Cod = TabProd.ProdID')
    ->where('TabProd.TabID', $uri3)
    ->get('TabProd')
    ->result();

    $data = $query->row_array();
    ?>
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="/<?= ADM_CONTROLLER ?>/<?= $headerloc ?>/"><?= $head1 ?></a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a>Редактирование "<?= $data['NameRU'] ?>"</a>
      </li>
    </ul>
  </div>
    <?= $err ?>

  <form name="form1" method="POST" action="<?= $e_path . $uri3; ?>/" enctype="multipart/form-data">
    <div class="table-scrollable">
      <table class="table table-striped table-bordered table-hover">
          <?
          create_form_by_array($form1, $data);
          ?>
        <tr>
          <td>&nbsp;</td>
          <td>
            <button type="submit" class="btn green"><i class="fa fa-check"></i> Обновить</button>
          </td>
        </tr>
      </table>
    </div>
  </form>
  <br/>
  <table class="table table-striped table-bordered table-hover">
    <tbody>
        <? foreach ($products as $product) { ?>
        <tr>
            <td>
            <?=$product->Title?>
            </td>
            <td width="100">
                <a href="/cp/dellprodfromtab/<?=$uri3?>/<?=$product->ID?>" id="dellprod" class="btn btn-xs default btn-editable red-stripe"><i class="glyphicon glyphicon-remove-circle"></i> Удалить</a>
            </td>
        </tr>
        <? } ?>
    </tbody>
  </table>
  <br/>
    <div style="width:calc(100% - 150px);float:left;" class="autoform">
        <input type="text" name="match" class="form-control autocomp" autocomplete="off">
        <input id="prod_id" type="hidden" value="">
    </div>
    <div style="width:120px;float:left;margin-left:29px;"><button id="addprod" type="button" class="btn green"><i class="fa fa-check"></i> Добавить</button></div>
    <div class="search_result"> </div>
    <?
}
?>
<style>
.search_result {
    background: #fff;
    display: none;
    width: calc(100% - 150px);
    margin-top: 34px;
    padding: 5px 14px 14px 14px;
    -webkit-box-shadow: 0 0 13px rgba(0, 0, 0, 0.15);
    box-shadow: 0 0 13px rgba(0, 0, 0, 0.15);
    color: #000;
}
</style>