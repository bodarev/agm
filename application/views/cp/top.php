<?
$uri3 = $this->uri->segment(3);

$head1 = '200 самых популярных товаров';
$headerloc = 'top';

$e_path = '/' . ADM_CONTROLLER . '/' . $headerloc . '/';
$delpath = '/' . ADM_CONTROLLER . '/del_' . $headerloc . '/';
$err = '';
?>
  <style>
  .table tr td {
    vertical-align: middle !important;
}
</style>

  <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a><?= $head1 ?></a>
      </li>
    </ul>
  </div>

    <?= $err ?>
  
    <h2><?= $head1 ?></h2>
    <br>
    <?
     $query = $this->db->select('count(ItemCount) as total, ItemCod')
    ->group_by('ItemCod')
    ->order_by('total', 'desc')
    ->get('OrderItems')
    ->result();


    if (!empty($query)) { ?>
        <div class="">
        <table class="table table-striped table-bordered table-hover dataTable no-footer">
          <thead>
            <tr class="heading nodrop nodrag">
            <th>Имя</th>
            <th>Розничная цена</th>
            <th>Оптовая цена</th>
            </tr>
          </thead>
          <tbody>

        <? foreach ($query as $item) {
          
          $query = $this->db
          ->select('*')
          ->where('Cod', $item->ItemCod)
          ->get('Products')
          ->row();
        ?>  

        <tr>
          <td>
            <span><strong><?=$query->TitleRU?></strong></span><br>
            <span>Артикул: <?=$query->Cod?></span>
          </td>
          <td>
            <?=Exchange($query->Price, false)?> MDL
          </td>
          <td>
            <?=Exchange($query->PriceAngro, true)?> MDL
          </td>
        </tr>

        <? } ?>
        
            </tbody>
          <table>
        </div>
   <? } ?>