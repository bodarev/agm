<?
$uri3 = $this->uri->segment(3);

$head1 = 'Log';
$tblname = 'Log';
$headerloc = 'log';

$e_path = '/' . ADM_CONTROLLER . '/' . $headerloc . '/';
$delpath = '/' . ADM_CONTROLLER . '/del_' . $headerloc . '/';
$err = '';
?>
<script type="text/javascript">
    function toggleb() {
        $("#newb").toggle();
    }
</script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a><?= $head1 ?></a>
        </li>
    </ul>
</div>

<h3><?= $head1 ?> </h3>
<? $logs = $this->db->select('*')
    ->order_by('Date DESC')
    ->limit(50)
    ->get($tblname)
    ->result();
if (!empty($logs)) { ?>

<div class='bootstrap-table'>
    <table id='myTable' class='table table-hover table-bordered'>
        <thead>
            <tr class='heading'>
                <th scope='col' width='200px'>Date</th>
                <th scope='col' width='200px'>Действия</th>
                <th scope='col'>Сообщение</th>
                <th scope='col'>ip</th>
            </tr>
        </thead>
        <tbody>
        <? foreach ($logs as $item) { ?>
            <tr>
                <td><?=date('d.m.Y, H:i', strtotime($item->Date))?></td>
                <td><?=$item->Method?></td>
                <td><?=$item->Msg?></td>
                <td><?=$item->ip?></td>
            </tr>
        <? } ?>
        </tbody>
    </table>
</div>
<? } ?>
