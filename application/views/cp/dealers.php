<?
$uri3 = $this->uri->segment(3);

$head1 = 'Список Дилеров';
$head2 = 'Редактирование Дилера';
$addnew = 'Дилера';
$tblname = 'SiteUser';
$headerloc = 'dealers';

$e_path = '/' . ADM_CONTROLLER . '/' . $headerloc . '/';
$delpath = '/' . ADM_CONTROLLER . '/del_' . $headerloc . '/';
$err = '';

$form2 = array(
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'select',
        'descr' => 'Статус *',
        'name' => 'status',
        'source'=>'DealerStatus',
        'sourcename'=>'NameRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Администратор *',
        'name' => 'Name'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Название компании *',
        'name' => 'companyname'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Юридический адрес *',
        'name' => 'legaladdress'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Ответственный за оплаты: Имя *',
        'name' => 'resp_name'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Ответственный за оплаты: Телефон *',
        'name' => 'resp_phone'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Бухгалтерия: Имя *',
        'name' => 'book_name'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Бухгалтерия: Телефон *',
        'name' => 'book_phone'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Фискальный код *',
        'name' => 'fiscalcode'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Код НДС *',
        'name' => 'vatcode'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Р/С *',
        'name' => 'account'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Банк *',
        'name' => 'bank'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Телефон *',
        'name' => 'phone'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Email *',
        'name' => 'email'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'select',
        'descr' => 'Город *',
        'name' => 'DistrictID',
        'source'=>'Districts',
        'sourcename'=>'NameRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Код *',
        'name' => 'Cod'
    ),
);

$form1 = array(
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Имя *',
        'name' => 'Name'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Телефон *',
        'name' => 'phone'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Email *',
        'name' => 'email'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Фискальный код',
        'name' => 'fiscalcode'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'select',
        'descr' => 'Город *',
        'name' => 'DistrictID',
        'source'=>'Districts',
        'sourcename'=>'NameRU'
    ),
);

$checker2 = array('Name', 'companyname', 'legaladdress', 'fiscalcode', 'vatcode', 'account', 'bank' ,'phone', 'email', 'Cod');
$checker1 = array('Name' ,'phone', 'email' );
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $erflag = false;

    if (!$erflag) {
        if(isset($_POST['data']['type'])) {
            foreach ($checker2 as $val) {
                if (empty($_POST['data'][$val])) $erflag = true;
            }
        } else {
            foreach ($checker1 as $val) {
                if (empty($_POST['data'][$val])) $erflag = true;
            }
        }

        if(isset($_POST['delivery'])) {
          foreach ($_POST['delivery'] as $item) {
            if (empty($item)) {
                $erflag = true;
            }
          }
        }
    }

    if (!$erflag) {
        $data_array = $_POST['data'];
        if (!empty($uri3)) {
            $id = $uri3;
            $this->db->where('ID', $id)->update($tblname, $data_array);
        }

        foreach ($_POST['delivery'] as $val=>$item) {
            $data = array(
                    'Name' => $item
            );
            $this->db->where('DealerID', $id)->where('ID', $val)->update('Address', $data);
        }

        if (!empty($id)) {
            header("Location: /" . ADM_CONTROLLER . "/$headerloc/");
            exit();
        }
    } else {
        $err .= '<div style="padding:10px 0;color:#ff0000;">Все поля отмеченные * обязательны для заполения</div>';
    }
}

?>
  <script type="text/javascript">
      function toggleb() {
          $("#newb").toggle();
      }
  </script>
  <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
  <script>
      function localsort() {
          counter = 1;
          data = '';
          $.each($('.sorthold'), function () {
              $(this).html(counter);
              if (counter < 2) breaker = ''; else breaker = '<>';
              data += breaker + $(this).attr('oid') + ':' + counter;
              counter++;
          });
          $.post('/<?=ADM_CONTROLLER?>/edit_table_order/', {data: data, table: '<?=$tblname?>'}, function (ret) {

          });
      }
  </script>
<? if (empty($uri3)) { ?>
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a><?= $head1 ?></a>
      </li>
    </ul>
  </div>

    <?= $err ?>
  <h3><?= $head1 ?> </h3>
    <?
    $checkb = $this->db->select('Status.NameRU as StatusName, SiteUser.*')
        ->from($tblname)
        ->order_by('SiteUser.type DESC, SiteUser.status ASC')
        ->join('Status', 'Status.Value=SiteUser.status')
        ->where_in('type', array(2))
        ->order_by('ID', 'desc')
        ->get()
        ->result_array();
    if (!empty($checkb)) {

        echo "<div class='bootstrap-table'>
  <div class='fixed-table-toolbar'>
    <div class='pull-right search' style='margin-bottom: 15px'>
      <input class='form-control' type='text' placeholder='Search' id='myInput' onkeyup='myFunction()'>
    </div>
  </div>
  <table id='myTable' class='table table-hover table-bordered'>
  <thead>
    <tr class='heading'>
      <th scope='col'>Имя</th>
      <th scope='col' width='250'>Действия</th>
    </tr>
    </thead>
    <tbody>";

        foreach ($checkb as $barr) {
          if($barr['type']==2) {
            if ($barr['status'] == 0) {
                $color = 'yellow';
            } else if ($barr['status'] == 1) {
                $color = 'red';
            } else if ($barr['status'] == 2) {
                $color = 'green';
            }
            $type = 'Дилер';
          } else {
            $color ='';
            $type = 'пользователь';
          }
            echo "<tr>";
            echo "<td>
                    <p>
                      <a href='$e_path" . $barr['ID'] . "'>" . $barr['companyname'] ."</a>
                      <span class='btn blue btn-outline btn-circle btn-sm active' style='float: right'>".$type."</span>";
                      if($barr['type']==2) {
                          echo "<span class='btn " . $color . " btn-outline btn-circle btn-sm active' style='float: right'>" . $barr['StatusName'] . "</span>";
                      }
                    echo "</p>";
            echo "</td>";
            echo "<td align=\"center\">";
            echo '<a href="' . $e_path . $barr['ID'] . '/" class="btn btn-xs default btn-editable green-stripe"><i class="glyphicon glyphicon-edit"></i> Редактировать</a>';
            echo '<a href="' . $delpath . $barr['ID'] . '/" class="btn btn-xs default btn-editable red-stripe"><i class="glyphicon glyphicon-remove-circle"></i> Удалить</a>';
            echo "</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        echo '</div>';
    }
} else {
    $query = $this->db->where('ID', $uri3)->get($tblname);
    $data = $query->row_array();

    $address = $this->db->select('*')
        ->from('Address')
        ->where('DealerID', $uri3)
        ->get()
        ->result_array();

    ?>
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="/<?= ADM_CONTROLLER ?>/<?= $headerloc ?>/"><?= $head1 ?></a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a>Редактирование "<?= $data['Name'] ?>"</a>
      </li>
    </ul>
  </div>
    <?= $err ?>

  <form name="form1" method="POST" action="<?= $e_path . $uri3; ?>/" enctype="multipart/form-data">
    <div class="table-scrollable">
      <table class="table table-striped table-bordered table-hover">
          <?
          if($data['type']=='1') {
              create_form_by_array($form1, $data); ?>
              <tr>
                <td>
                  Активный
                </td>
                <td>
                  <?php
                    if ($data['status'] == '1') {
                        $cmod = ' checked';
                    } else {
                        $cmod = '';
                    }
                  ?>
                  <input type="checkbox" <?=$cmod?> name="status" value="<?=$data['ID']?>" class="form-control status">
                </td>
              </tr>
          <? } else {
              create_form_by_array($form2, $data); ?>
              <tr>
                <td>Адрес доставки *</td>
                <td>
                    <?php
                    foreach ($address as $item) {
                        echo "<input name='delivery[".$item['ID']."]' type='text' id='' class='form-control' value='".$item['Name']."'>";
                        echo "<br>";
                    }
                    ?>
                </td>
              </tr>
              <tr>
                <td>сгенерировать пароль</td>
                <td>
                    <div class="data-uniquecodegenerator">
                        <input type="text" readonly="" name="" value="" class="form-control musthave">
                        <span class="actionbutton"></span>
                    </div>
                </td>
              </tr>
        <?php  } ?>
        <tr>
          <td>&nbsp;</td>
          <td>
            <button type="submit" class="btn green"><i class="fa fa-check"></i> Обновить</button>
          </td>
        </tr>
      </table>
    </div>
  </form><br/>
    <?
}
?>

<script>
    $(document).ready(function () {

        $(document).on('change', '.status', function () {
            if ($(this).is(':checked')) {
                val = 1;
            } else {
                val = 0;
            }
            id = $(this).val();
            $.get('/cp/status/' + id + '/' + val + '/');
        });

        $(document).on('click', '.actionbutton', function () {
            var cod = Math.random().toString().slice(2,8);
            var id = <?=$uri3?>;
            $('.data-uniquecodegenerator input').val(cod);
            $.get('/cp/generatepass/' + cod + '/' + id + '/');
        });
    });

</script>

<script>
    function myFunction() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>
<style>
.data-uniquecodegenerator {
    padding-right: 34px;
    position: relative;
}
.data-uniquecodegenerator span.actionbutton {
    display: block;
    position: absolute;
    top: 0px;
    right: 0px;
    width: 34px;
    height: 34px;
    background: #eaeaea;
    border: 1px solid #e5e5e5;
    line-height: 32px;
    text-align: center;
    cursor:pointer;
}
.data-uniquecodegenerator span.actionbutton:before {
    display: inline;
    content: "\f013";
    font-family: "FontAwesome";
}
</style>