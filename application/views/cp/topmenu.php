<?
$uri3 = $this->uri->segment(3);

$head1 = 'Верхнее меню';
$head2 = 'Редактирование меню';
$addnew = 'пункт меню';
$tblname = 'TopMenu';
$tblname2 = 'MenuBlocks';
$headerloc = 'topmenu';

$e_path = '/' . ADM_CONTROLLER . '/topmenu/';
$delpath = '/' . ADM_CONTROLLER . '/del_' . $headerloc . '/';
$err = '';

$checker = array('TitleRU', 'UriName', 'TitleRO', 'TitleEN');

$form1 = array(
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Ссылка',
        'name' => 'UriName',
        'dop_data' => '<br /><div style="width: 97%; background-color: #FFFF00; border: 1px solid; padding: 5px;">
			без http://www и т.п. просто английская фраза, без пробелов, отражающая пункт меню, например: <br>
			<em style="font-size: 10px;">Haш подход: our-approach</em>
		</div>'
    ),
    array(
        'dop_tr'=>'',
        'dop_style'=>'',
        'type'=>'select',
        'class'=>'form-control',
        'descr'=>'Родительский раздел',
        'name'=>'ParentID',
        'source'=>'TopMenu',
        'sourcename'=>'TitleRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Заголовок RU*',
        'name' => 'TitleRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Заголовок RO*',
        'name' => 'TitleRO'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Заголовок EN*',
        'name' => 'TitleEN'
    ),
);
$form2 = array(
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Заголовок RU',
        'name' => 'OptimizationTitleRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Заголовок RO',
        'name' => 'OptimizationTitleRO'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Заголовок EN',
        'name' => 'OptimizationTitleEN'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Ключевые слова RU',
        'name' => 'KeywordsRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Ключевые слова RO',
        'name' => 'KeywordsRO'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Ключевые слова EN',
        'name' => 'KeywordsEN'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Описание RU',
        'name' => 'DescriptionRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Описание RO',
        'name' => 'DescriptionRO'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'text',
        'class' => 'form-control',
        'descr' => 'Описание EN',
        'name' => 'DescriptionEN'
    )
);

$Name = '';
$files=array('Image');
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $erflag = false;

    foreach ($checker as $val) {
        if (empty($_POST['data'][$val])) $erflag = true;
    }

    if (!$erflag) {

        $data_array = $_POST['data'];

        if (empty($uri3)) {
            $this->db->insert($tblname, $data_array);
            $id = $this->db->insert_id();
        } else {
            $id = $uri3;
            $this->db->where('ID', $id)->update($tblname, $data_array);
        }

        foreach($files as $filename) {

            if(!empty($_FILES[$filename]['name'])) {

                $this->upload->do_upload($filename);
                $resarr = $this->upload->data();
                $file = $resarr['file_name'];

                if(strtolower($resarr['file_ext']) == '.jpg' || strtolower($resarr['file_ext']) == '.jpeg' || strtolower($resarr['file_ext']) == '.gif' || strtolower($resarr['file_ext']) == '.png')
                {
                    $this->db->where('ID',$id)->update($tblname,array($filename=>$file));
                }
            }
        }

        $files_data = array();
        if (!empty($_FILES['image_block'])) {
            $this->load->library('upload');
            $files      = $_FILES['image_block'];
            $countFiles = count($files['name']);
            if ($countFiles > 0) {
                for ($i=0; $i <= $countFiles; $i++) {
                    if (!empty($files['name'][$i])) {
                        $_FILES['menufile']['name']     = $files['name'][$i];
                        $_FILES['menufile']['type']     = $files['type'][$i];
                        $_FILES['menufile']['tmp_name'] = $files['tmp_name'][$i];
                        $_FILES['menufile']['error']    = $files['error'][$i];
                        $_FILES['menufile']['size']     = $files['size'][$i];

                        $config = array(
                            'upload_path'   => "public/menublocks/$folder",
                            'allowed_types' => 'jpg|jpeg|png|gif',
                            'overwrite'     => false,
                            'max_size'      => '2048',
                            'remove_spaces' => true,
                            'encrypt_name'  => true
                        );
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('menufile')) {
                            $erflag = true;
                        }else{
                            $files_data[$i] = $this->upload->data();
                        }
                    }
                }
            }
        }

        if (!empty($_POST['block'])) {
            $insert = array();
            foreach ($_POST['block'] as $key => $value) {
                $data = array();
                foreach ($value as $lang => $text) {
                    $data["text_$lang"] = $text;
                }

                foreach ($_POST['b_title'][$key] as $lang => $title) {
                    $data["name_$lang"] = $title;
                }

                $data['Sorder']   = 1;
                $data['parent_id'] = $id;
                $data['image']     = !empty($files_data[$key]) ? $files_data[$key]['file_name'] : '';
                $insert[]          = $data;
            }
            $this->db->insert_batch('MenuBlocks', $insert);
        }

        $edit_files_data = array();
        if (!empty($_FILES['edit_image_block'])) {
            $this->load->library('upload');
            $files      = $_FILES['edit_image_block'];
            $countFiles = count($files['name']);
            $files_data = array();
            if ($countFiles > 0) {
                foreach ($files['name'] as $id_image => $value) {
                    if (!empty($files['name'][$id_image])) {
                        $_FILES['menufile']['name']     = $files['name'][$id_image];
                        $_FILES['menufile']['type']     = $files['type'][$id_image];
                        $_FILES['menufile']['tmp_name'] = $files['tmp_name'][$id_image];
                        $_FILES['menufile']['error']    = $files['error'][$id_image];
                        $_FILES['menufile']['size']     = $files['size'][$id_image];

                        $config = array(
                            'upload_path'   => "public/menublocks/$folder",
                            'allowed_types' => 'jpg|jpeg|png|gif',
                            'overwrite'     => false,
                            'max_size'      => '2048',
                            'remove_spaces' => true,
                            'encrypt_name'  => true
                        );
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('menufile')) {
                            $erflag = true;
                        }else{
                            $edit_files_data[$id_image] = $this->upload->data();
                        }
                    }
                }
            }
        }

        if (!empty($_POST['edit_blocks'])) {
            $update = array();
            foreach ($_POST['edit_blocks'] as $block_id => $langs) {
                $data          = array();
                $data['id']    = $block_id;
                if (!empty($edit_files_data[$block_id])) {
                    $data['image'] =  $edit_files_data[$block_id]['file_name'];
                }
                foreach ($langs as $lang => $fields) {
                    foreach ($fields as $field => $value) {
                        $data[$field.'_'.$lang] = $value;
                    }
                }
                $update[] = $data;
            }
            if (!empty($update)) {
                $this->db->update_batch('MenuBlocks', $update, 'id');
            }
        }


        if (!empty($id)) {
            header("Location: /" . ADM_CONTROLLER . "/$headerloc/");
            exit();
        }
    } else {
        $err .= '<div style="padding:10px 0;color:#ff0000;">Все поля отмеченные * обязательны для заполения</div>';
    }
}

?>
    <script type="text/javascript">
        function toggleb() {
            $("#newb").toggle();
        }
    </script>
    <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
    <script>
        function localsort() {
            counter = 1;
            data = '';
            $.each($('.sorthold'), function () {
                $(this).html(counter);
                if (counter < 2) breaker = ''; else breaker = '<>';
                data += breaker + $(this).attr('oid') + ':' + counter;
                counter++;
            });
            $.post('/<?=ADM_CONTROLLER?>/edit_table_order/', {data: data, table: '<?=$tblname?>'}, function (ret) {

            });
        }
        function localsort2() {
            counter2 = 1;
            data2 = '';
            $.each($('.sorthold2'), function () {
                $(this).html(counter2);
                if (counter2 < 2) breaker2 = ''; else breaker2 = '<>';
                data2 += breaker2 + $(this).attr('bid') + ':' + counter2;
                counter2++;
            });
            $.post('/<?=ADM_CONTROLLER?>/edit_table_order/', {data: data2, table: 'MenuBlocks'}, function (ret) {

            });
        }
    </script>
<? if (empty($uri3)) { ?>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a><?= $head1 ?></a>
            </li>
        </ul>
    </div>

    <?= $err ?>

    <form name="form1" method="POST" action="<?= $e_path ?>" enctype="multipart/form-data">
        <div class="portlet box">
            <div class="portlet-title">
                <div class="caption" style="color:#888;font-size:26px;">
                    <?= $head1 ?>
                </div>
                <div class="tools">
                  <a href="javascript:;" class="expand btn default yellow-stripe" style="width: auto !important; height: auto !important">
                    <i class="fa fa-plus"></i>
                    <span class="hidden-480">
					                Добавить <?= $addnew ?>
                        </span>
                  </a>
                </div>
            </div>
            <div class="portlet-body" style="<? if (empty($err)) echo 'display:none;'; ?>">
                <div class="tabbable tabbable-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1_1" data-toggle="tab">Общая информация</a>
                        </li>
                        <li>
                            <a href="#tab_1_2" data-toggle="tab">Служебная информация</a>
                        </li>
                        <li>
                            <a href="#tab_1_3" data-toggle="tab">Блоки</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1_1">
                            <div class="table-scrollable">
                                <table class="table table-striped table-bordered table-hover">
                                    <?
                                    create_form_by_array($form1, @$_POST['data']);
                                    ?>
                                    <tr>
                                    <td>Изображение</td>
                                    <td>
                                      <div class="fileinput fileupload fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="max-width: 200px;">
                                          <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                        <div>
                          <span class="btn default btn-file">
                            <span class="fileinput-new">Выбрать </span>
                            <span class="fileinput-exists">Изменить </span>
                            <input type="file" name="Image">
                          </span>
                                          <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Отмена </a>
                                          <span class="help-block">
                            Допустимые форматы
                            <code>jpg</code>
                            <code>png</code>
                            <code>gif</code>
                          </span>
                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <button type="submit" class="btn green"><i class="fa fa-check"></i> Добавить
                                            </button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_1_2">
                            <div class="table-scrollable">
                                <table class="table table-striped table-bordered table-hover">
                                    <?
                                    create_form_by_array($form2, @$_POST['data']);
                                    ?>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <button type="submit" class="btn green"><i class="fa fa-check"></i> Добавить
                                            </button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_1_3">
                          <tr>
                            <td colspan="2">
                              <div class="form-group row">
                                <div class="col-md-12">
                                  <div id="tab_images_uploader_container" onclick="menuBlock(this)" class="text-align-reverse margin-bottom-10" style="position:relative; display:inline-block; ">
                                    <a id="tab_images_uploader_pickfiles" href="javascript:;" class="btn yellow">
                                      <i class="fa fa-plus"></i> Добавить блок </a>
                                  </div>
                                </div>

                                <div class="col-md-12">
                                  <div class="added__container" style="margin-bottom: 15px;"> </div>
                                </div>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="4">
                              <button type="submit" class="btn green">
                                <i class="fa fa-check"></i> Добавить
                              </button>
                            </td>
                          </tr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form><br/>
    <?
        $result = $this->db->order_by('Sorder ASC, ID DESC')->get($tblname)->result_array();
        if (!empty($result)) { 
    ?>
    <div class="table-scrollable">
	<table class="table table-striped table-bordered table-hover dataTable no-footer dragger">
        <tr class="heading nodrop nodrag">
		    <th>Сортировка</th>
			<th>Название</th>
			<th colspan="2"></th>
			<th width="300">Действия</th>
		</tr>

        <? foreach ($result as $barr) { ?>
        <tr>
            <td width="40" celpadding="5" cellspacing="10" oid="<?= $barr['ID'] ?>" align="center" class="sorthold">
                <?=$barr['Sorder']?>
            </td>
            <td><a href="<?=$e_path. $barr['ID']?>/"><?= $barr['TitleRU']?></a></td>
            <?
            $cmod = $cmod1 = $cmod2 = '';
            if (!empty($barr['OnSite'])) $cmod = ' checked';
            if (!empty($barr['OnTop'])) $cmod1 = ' checked';
            if (!empty($barr['BottomMenu'])) $cmod2 = ' checked';
            ?>
<!--        <td width='175'><label>
                <input type="checkbox" <?=$cmod ?>  data-table="<?=$tblname?>" data-col="OnSite" data-id="<?= $barr['ID'] ?>" class="check">Выводить на саите
            </label></td> -->
            <td width='175'><label>
                <input type="checkbox" <?=$cmod1 ?> data-table="<?=$tblname?>" data-col="OnTop" data-id="<?= $barr['ID'] ?>" class="check"> Выводить в шапке
            </label></td>
            <td width='185'><label>
                <input type="checkbox" <?=$cmod2 ?> data-table="<?=$tblname?>" data-col="BottomMenu" data-id="<?= $barr['ID'] ?>" class="check"> Выводить в подвале
            </label></td>
            <td align="">
            <a target="_blank" href="/ru/pages/<?= $barr['UriName'] ?>/" class="btn btn-xs default btn-editable blue-stripe"><i class="glyphicon glyphicon-eye-open"></i> Перейти</a>
            <a href="<?= $e_path . $barr['ID'] ?>/" class="btn btn-xs default btn-editable green-stripe"><i class="glyphicon glyphicon-edit"></i> Изменить</a>
            <? if ($barr['System'] == 1) { ?> 
                <a href="<?= $delpath . $barr['ID'] ?>/" class="btn btn-xs default btn-editable red-stripe"><i class="glyphicon glyphicon-remove-circle"></i> Удалить</a>
            <? } ?>
            </td>
        </tr>
        <? } ?>
    </table>
    </div>
    <? }
} else {
    $query = $this->db->where('ID', $uri3)->get($tblname);
    $data = $query->row_array();
    $form1[0]['dop_tr'] = ' style="display:none;"';
    ?>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/<?= ADM_CONTROLLER ?>/<?= $headerloc ?>/"><?= $head1 ?></a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a>Редактирование "<?= $data['TitleRU'] ?>"</a>
            </li>
        </ul>
    </div>
    <?= $err ?>

    <form name="form1" method="POST" action="<?= $e_path . $uri3; ?>/" enctype="multipart/form-data">
        <div class="tabbable tabbable-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab">Общая информация</a>
                </li>
                <li>
                    <a href="#tab_1_2" data-toggle="tab">Служебная информация</a>
                </li>
              <li>
                <a href="#tab_1_3" data-toggle="tab">Блоки</a>
              </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover">
                            <?
                            create_form_by_array($form1, $data);
                            ?>
                          <tr>
                            <td>Изображение</td>
                            <td>
                              <div class="fileinput fileupload fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="max-width: 200px;">
                                    <?php if ($data['Image']  && file_exists('public/topmenu/' .$data['Image'])): ?>
                                      <img src="/public/topmenu/<?=$data['Image']?>" id="thumb-img" alt="">
                                    <?php else: ?>
                                      <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                    <?php endif ?>
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                <div>
                                            <span class="btn default btn-file">
                                              <span class="fileinput-new">Выбрать </span>
                                              <span class="fileinput-exists">Изменить </span>
                                              <input type="file" name="Image">
                                            </span>

                                    <?php if ($data['Image']){ ?>
                                      <a data-toggle="modal" href="#myModal<?=$data['ID']?>" class="btn btn-danger del_btn" ><i class="fa fa-trash"></i> Удалить </a>
                                      <!-- Modal -->
                                      <div class="modal fade theme-modal" id="myModal<?=$data['ID']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                              <h4 class="modal-title">Подтвердить операцию</h4>
                                            </div>
                                            <div class="modal-body">
                                              Вы действительно желаете удалить?
                                            </div>
                                            <div class="modal-footer">
                                              <button data-dismiss="modal" class="btn btn-default" type="button">Отмена</button>
                                              <button class="btn btn-success" type="button" onclick="toDeleteImg(this, '/cp/deleteImageElement/','<?=@$data['ID']?>', '<?=$tblname?>', '')">Удалить</button>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <!-- Modal -->
                                    <?php } else { ?>
                                      <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Отмена </a>
                                    <?php } ?>
                                  <span class="help-block">Допустимые форматы <code>jpg</code> <code>png</code> <code>gif</code></span>
                                </div>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>
                              <button type="submit" class="btn green"><i class="fa fa-check"></i> Обновить
                              </button>
                            </td>
                          </tr>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="tab_1_2">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover">
                            <?
                            create_form_by_array($form2, $data);
                            ?>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Обновить
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="tab_1_3">
                  <div class="form-group row">
                    <div class="col-md-12">
                      <div id="tab_images_uploader_container" onclick="menuBlock(this)" class="text-align-reverse margin-bottom-10" style="position:relative; display:inline-block; ">
                        <a id="tab_images_uploader_pickfiles" href="javascript:;" class="btn yellow">
                          <i class="fa fa-plus"></i> Добавить блок </a>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="added__container" style="margin-bottom: 15px;"> </div>
                    </div>

                    <div class="col-md-12">
                      <div id="fade-respond" class=""></div>
                    </div>

                      <?php $menu_blocks = $this->db->where('parent_id', $uri3)->order_by('Sorder asc, id desc')->get('MenuBlocks')->result_array(); ?>
                      <?php if (!empty($menu_blocks)) { ?>

                        <div class="col-md-12">
                          <table class="table table-striped table-bordered" style="margin: 0; margin-top: -1px;">
                            <thead>
                            <tr>
                              <!--<th>Сортировка</th>-->
                              <th style="width:40px; text-align:center;"><i class="fa fa-sort" aria-hidden="true"></i></th>
                              <th style="width:40px; text-align: center"><i class="fa fa-eye" aria-hidden="true"></i></th>
                              <th>Блоки</th>
                              <th style="width:40px;"></th>
                            </tr>
                            </thead>

                            <tbody class="dragger2">
                            <?php foreach ($menu_blocks as $item) { ?>
                              <tr id="<?=$item['id']?>">
                                <td width="40" celpadding="5" cellspacing="10" bid="<?=$item['id']?>" align="center" class="sorthold2">
                                    <?=$item['Sorder']?>
                                </td>
                                <td class="t-border" style="width:30px;">
                                    <?php if ($item['view'] == 1){
                                        $cmod4 ='checked';
                                    } else {
                                        $cmod4 ='';
                                    } ?>
                                  <input <?=$cmod4?> data-table="<? echo 'MenuBlocks'; ?>" data-col="view" data-id="<?=$item['id']?>" class="check" type="checkbox">
                                </td>
                                <td class="nodrop nodrag">
                                  <div class="portlet box blue-hoki">
                                    <div class="portlet-title">
                                      <div class="caption">
                                        <i class="fa fa-cogs"></i> <?=$item['name_ru']?>
                                      </div>
                                      <div class="tools">
                                        <a href="javascript:;" class="expand"></a>
                                      </div>
                                    </div>
                                    <div class="portlet-body" style="display: none">
                                      <div class="row">
                                        <div class="col-md-12" style="margin-bottom:15px;">
                                          <div class="fileinput fileupload fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 150px;">
                                                <?php if ($item['image']  && file_exists('public/menublocks/' .$item['image'])): ?>
                                                  <img src="/public/menublocks/<?=$item['image']?>" id="thumb-img" alt="">
                                                <?php else: ?>
                                                  <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                                <?php endif ?>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                            <div>
                                            <span class="btn default btn-file">
                                              <span class="fileinput-new">Выбрать </span>
                                              <span class="fileinput-exists">Изменить </span>
                                              <input type="file" name="edit_image_block[<?=$item['id']?>]">
                                            </span>

                                                <?php if ($item['image']){ ?>
                                                  <a data-toggle="modal" href="#myModal<?=$item['id']?>" class="btn btn-danger del_btn" ><i class="fa fa-trash"></i> Удалить </a>
                                                  <!-- Modal -->
                                                  <div class="modal fade theme-modal" id="myModal<?=$item['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                      <div class="modal-content">
                                                        <div class="modal-header">
                                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                          <h4 class="modal-title">Подтвердить операцию</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                          Вы действительно желаете удалить?
                                                        </div>
                                                        <div class="modal-footer">
                                                          <button data-dismiss="modal" class="btn btn-default" type="button">Отмена</button>
                                                          <button class="btn btn-success" type="button" onclick="toDeleteImg(this,'/cp/deleteImageElement/','<?=@$item['id']?>', 'MenuBlocks', '')">Удалить</button>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <!-- Modal -->
                                                <?php } else { ?>
                                                  <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Отмена </a>
                                                <?php } ?>
                                              <span class="help-block">Допустимые форматы <code>jpg</code> <code>png</code> <code>gif</code></span>
                                            </div>
                                          </div>
                                        </div>
                                          <?php $lang_arr = array('ru'=>0, 'ro'=>0, 'en'=>2);?>
                                          <?php foreach ($lang_arr as $key => $value){ ?>
                                            <div class="col-md-4 lang-area" id="field_<?=$key?>" style="margin-bottom:15px;">
                                              <input name="edit_blocks[<?=$item['id']?>][<?=$key?>][name]" value="<?=$item["name_$key"]?>" placeholder="Р—Р°РіРѕР»РѕРІРѕРє" class="form-control">
                                            </div>
                                          <?php } ?>
                                          <?php foreach ($lang_arr as $key => $value){ ?>
                                            <div class="col-md-4 lang-area" id="field_<?=$key?>">
                                          <textarea name="edit_blocks[<?=$item['id']?>][<?=$key?>][text]" class="form-control ckeditor">
                                              <?=$item["text_$key"]?>
                                          </textarea>
                                            </div>
                                          <?php } ?>
                                      </div>
                                    </div>
                                  </div>
                                </td>

                                <td style="text-align: center;">
                                  <a class="btn btn-xs default btn-editable red-stripe" href="/cp/del_block/<?=$uri3?>/<?=$item['id']?>"><i class="fa fa-trash-o "></i>  Удалить</a>
                                </td>
                              </tr>
                            <?php } ?>
                            </tbody>
                            <tbody>
                            <tr>
                              <td colspan="2"></td>
                              <td colspan="2">
                                <button type="submit" class="btn green">
                                  <i class="fa fa-check"></i> Обновить
                                </button>
                              </td>
                            </tr>
                            </tbody>
                          </table>
                        </div>

                      <?php } else {  ?>
                    <div class="col-md-12">
                      <button type="submit" class="btn green">
                        <i class="fa fa-check"></i> Обновить
                      </button>
                      <?php } ?>

                  </div>
                </div>
            </div>
        </div>
    </form><br/>
<? } ?>
<style>
  .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    vertical-align: inherit;
  }
  .portlet-head {
    border-bottom: 0;
    padding: 0 10px;
    margin-bottom: 0;
    color: #fff;
  }
  .portlet > .portlet-head > .tools {
    float: right;
    display: inline-block;
    padding: 12px 0 8px 0;
  }

  .portlet > .portlet-head > .tools > a {
    display: inline-block;
    height: 16px;
    margin-left: 5px;
    opacity: 1;
  }

  .portlet.box > .portlet-head > .tools > a.remove {
    background-image: url(/theme/assets/global/img/portlet-remove-icon-white.png);
    width: 11px;
  }
  .portlet > .portlet-head > .caption {
    float: left;
    display: inline-block;
    font-size: 18px;
    line-height: 18px;
    font-weight: 300;
    padding: 10px 0;
  }
  </style>
