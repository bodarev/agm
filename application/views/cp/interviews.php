<?
$uri3 = $this->uri->segment(3);
$uri4 = $this->uri->segment(4);

$head1 = 'Опросы';
$head2 = 'Вопросы';
$addnew1 = 'Опрос';
$addnew2 = 'Вопрос';
if($uri3 !='') {
    $tblname = 'Questions';
} else {
    $tblname = 'Interviews';
}
$headerloc = 'interviews';

$s_path = '/' . ADM_CONTROLLER . '/statistic/';
$e_path = '/' . ADM_CONTROLLER . '/' . $headerloc . '/';
$delpath = '/' . ADM_CONTROLLER . '/del_' . $headerloc . '/';
$err = '';

$form1 = array(
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Название RU*',
        'name' => 'NameRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Название RO*',
        'name' => 'NameRO'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Название EN*',
        'name' => 'NameEN'
    ),   
);
$files = array();
$checker = array('NameRU','NameRO','NameEN',);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $erflag = false;

    if (!$erflag) {
        foreach ($checker as $val) {
            if (empty($_POST['data'][$val])) $erflag = true;
        }
    }

    if (!$erflag) {
        $data_array = $_POST['data'];

        if (empty($uri3)) {
            $this->db->insert('Interviews', $data_array);
            $id = $this->db->insert_id();
        } else {
            if (empty($uri4)) {
                if(isset($_POST['interview'])) {
                    $id = $uri3;
                    $this->db->where('ID', $id)->update('Interviews', $data_array);
                } else {
                    $data_array['InterviewID']=$uri3;
                    $this->db->insert('Questions', $data_array);
                    $id = $this->db->insert_id();
                }
            } else {
                $id = $uri4;
                $this->db->where('ID', $id)->where('InterviewID', $uri3)->update('Questions', $data_array);
            }
        }

        // verificam daca sunt date noi in block

        if ( !empty($_POST['new'])) {

            $result = diverse_array($_POST['new']);

            foreach ($result as $item) {
                $new[] = array(
                    'QuestionID' => $id,
                    'AnsRU' => $item['AnsRU'],
                    'AnsRO' => $item['AnsRO'],
                    'AnsEN' => $item['AnsEN'],
                );
            }

            $this->db->insert_batch('Answers', $new);
        }

        // verificam daca sunt date vechi in block

        if ( !empty($_POST['old'])) {

            $result = diverse_array($_POST['old']);

            foreach ($result as $item) {
                $old = array(
                    'AnsRU' => $item['AnsRU'],
                    'AnsRO' => $item['AnsRO'],
                    'AnsEN' => $item['AnsEN'],
                );

                $this->db->where('ID', $item['ID']);
                $this->db->where('QuestionID', $id);
                $this->db->update('Answers', $old);
            }
        }

        if (!empty($id)) {
            header("Location: /" . ADM_CONTROLLER . "/$headerloc/" . "$uri3/"  );
            exit();
        }
    } else {
        $err .= '<div style="padding:10px 0;color:#ff0000;">Все поля отмеченные * обязательны для заполения</div>';
    }
}

if($uri3!=''){
    $interview = $this->db->where('ID', $uri3)->get('Interviews')->row_array();
    $questions = $this->db->where('InterviewID', $uri3)->order_by('Sorder ASC, ID DESC')->get('Questions')->result();
    if($uri4!='') {
        $question = $this->db->where('ID', $uri4)->where('InterviewID', $uri3)->get('Questions')->row_array();
        $answers = $this->db->where('QuestionID', $uri4)->order_by('ID', 'asc')->get('Answers')->result_array();
        $answers = (isset($_POST['old']))?diverse_array($_POST['old']):$answers;
    }
} else {
    $interviews = $this->db->order_by('Sorder ASC, ID DESC')->get('Interviews')->result_array();
}

?>
  <script type="text/javascript">
      function toggleb() {
          $("#newb").toggle();
      }
  </script>
  <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
  <script>
      function localsort() {
          counter = 1;
          data = '';
          table = '<?=$tblname?>';
          $.each($('.sorthold'), function () {
              $(this).html(counter);
              if (counter < 2) breaker = ''; else breaker = '<>';
              data += breaker + $(this).attr('oid') + ':' + counter;
              counter++;
          });
          $.post('/<?=ADM_CONTROLLER?>/edit_table_order/', {data: data, table: table}, function (ret) {

          });
      }
  </script>

<div class="page-bar">
    <ul class="page-breadcrumb">
    <li>
        <i class="fa fa-home"></i>
        <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <? if($uri3!='') {?>
            <a href="/<?= ADM_CONTROLLER ?>/<?= $headerloc ?>/"><?= $head1 ?></a>
            <i class="fa fa-angle-right"></i>
        <?} else {?>
            <a><?= $head1 ?></a>
        <?}?>    
    </li>
    <? if($uri3!='') {?>
        <li>
            <? if($uri4!='') {?>
                <a href="/<?= ADM_CONTROLLER ?>/<?= $headerloc ?>/<?= $uri3 ?>/">Опрос - <?= $interview['NameRU'] ?></a>
                <i class="fa fa-angle-right"></i>
            <?} else {?>
                <a>Опрос - <?= $interview['NameRU'] ?></a>
            <?}?>
        </li>
        <? if($uri4!='') {?>
            <li>
                <a>Вопрос - <?= $question['NameRU'] ?></a>
            </li>
        <?}?>
    <?}?>
    </ul>
</div>

<?= $err ?>

<? if (empty($uri3)) { ?>
 
    <form name="form1" method="POST" action="<?= $e_path ?>" enctype="multipart/form-data">
        <div class="portlet box">
        <div class="portlet-title">
            <div class="caption" style="color:#888;font-size:26px;">
                <?= $head1 ?>
            </div>
            <div class="tools">
            <a href="javascript:;" class="expand btn default yellow-stripe" style="width: auto !important; height: auto !important">
                <i class="fa fa-plus"></i>
                <span class="hidden-480">
                                        Добавить <?= $addnew1 ?>
                            </span>
            </a>
            </div>
        </div>
        <div class="portlet-body" style="<? if (empty($err)) echo 'display:none;'; ?>">
            <div class="">
            <table class="table table-striped table-bordered table-hover">
                <?
                create_form_by_array($form1, @$_POST['data']);
                ?> 
                <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="interview" hidden type="text" value="">
                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Добавить</button>
                </td>
                </tr>
            </table>
            </div>
        </div>
        </div>
    </form>
    <br/>

    <? if (!empty($interviews)) { ?>
        <div class="">
		<table class="table table-striped table-bordered table-hover dataTable no-footer dragger">
            <tr class="heading nodrop nodrag">
                <th>Сортировка</th>
                <th style="width:40px; text-align: center"><i class="fa fa-eye" aria-hidden="true"></i></th>
                <th>Название</th>
                <th width="350">Действия</th>
            </tr>

        <? foreach ($interviews as $interview) { ?> 
            <tr>
                <td width="40" celpadding="5" cellspacing="10" oid="<?=$interview['ID']?>" align="center" class="sorthold">
                    <?=$interview['Sorder']?>
                </td>
                <? $cmod = (!empty($interview['OnSite'])) ? ' checked' : '' ;?>
                <td>
                    <label>
                        <input data-table="<?=$tblname?>" data-col="OnSite" data-id="<?= $interview['ID'] ?>" class="check" type="checkbox" <?=$cmod?>>
                    </label>
                </td>
                <td>
                    <a href="<?=$e_path.$interview['ID']?>/"><?=$interview['NameRU']?></a>
                </td>
                <td align="center">
                    <a href="<?=$s_path . $interview['ID'] ?>/" class="btn btn-xs default btn-editable blue-stripe"><i class="fa fa-bar-chart"></i> Статистика</a>
                    <a href="<?=$e_path . $interview['ID'] ?>/" class="btn btn-xs default btn-editable green-stripe"><i class="glyphicon glyphicon-edit"></i> Редактировать</a>
                    <a href="<?=$delpath . $interview['ID'] ?>/" class="btn btn-xs default btn-editable red-stripe"><i class="glyphicon glyphicon-remove-circle"></i> Удалить</a>
                </td>
            </tr>
        <?}?>
        </table>
        </div>
    <?}?>
<? } else { ?>
    <? if (empty($uri4)) { ?>

        <h3>Опрос - <?=$interview['NameRU']?></h3>
        <br/>

        <!-- forma pentru edit interview -->
        <form name="form1" method="POST" action="<?= $e_path . $uri3; ?>/" enctype="multipart/form-data">
            <div class="">
            <table class="table table-striped table-bordered table-hover">
                <?
                create_form_by_array($form1, $interview);
                ?>     
                <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="interview" hidden type="text" value="">
                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Обновить</button>
                </td>
                </tr>
            </table>
            </div>
        </form>
        
        <!-- forma pentru add question -->
        <form name="form1" method="POST" action="<?= $e_path . $uri3;?>/" enctype="multipart/form-data">
            <div class="portlet box">
            <div class="portlet-title">
                <div class="caption" style="color:#888;font-size:26px;">
                    <?= $head2 ?>
                </div>
                <div class="tools">
                <a href="javascript:;" class="expand btn default yellow-stripe" style="width: auto !important; height: auto !important">
                    <i class="fa fa-plus"></i>
                    <span class="hidden-480"> Добавить <?= $addnew2 ?></span>
                </a>
                </div>
            </div>
            <div class="portlet-body" style="<? if (empty($err)) echo 'display:none;'; ?>">
                <div class="">
                <table class="table table-striped table-bordered table-hover">
                    <?
                    create_form_by_array($form1, @$_POST['question']);
                    ?>
                    <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input name="question" hidden type="text" value="">
                        <button type="submit" class="btn green"><i class="fa fa-check"></i> Добавить</button>
                    </td>
                    </tr>
                </table>
                </div>
            </div>
            </div>
        </form>

        <br/>

        <?if (!empty($questions)) { ?>
            <div class="">
            <table class="table table-striped table-bordered table-hover dataTable no-footer dragger">
                <tr class="heading nodrop nodrag">
                    <th>Сортировка</th>
                    <th style="width:40px; text-align: center"><i class="fa fa-eye" aria-hidden="true"></i></th>
                    <th style="width:40px; text-align: center">Много<br>ответов</th>
                    <th >Название</th>
                    <th width="250">Действия</th>
                </tr>

            <? foreach ($questions as $question) { ?> 
                <tr>
                    <td width="40" celpadding="5" cellspacing="10" oid="<?=$question->ID ?>" align="center" class="sorthold">
                        <?=$question->Sorder?>
                    </td>
                    <? $cmod = (!empty($question->OnSite)) ? ' checked' : '' ;?>
                    <td>
                        <label>
                            <input data-table="<?=$tblname?>" data-col="OnSite" data-id="<?= $question->ID ?>" class="check" type="checkbox" <?=$cmod?>>
                        </label>
                    </td>
                    <? $cmod = (!empty($question->Multi)) ? ' checked' : '' ;?>
                    <td>
                        <label>
                            <input data-table="<?=$tblname?>" align="center" data-col="Multi" data-id="<?= $question->ID ?>" class="check" type="checkbox" <?=$cmod?>>
                        </label>
                    </td>
                    <td>
                        <a href="<?=$e_path. $uri3 .'/'.$question->ID?>/"><?=$question->NameRU?></a>
                    </td>
                    <td align="center">
                        <a href="<?=$e_path . $uri3 .'/'. $question->ID ?>/" class="btn btn-xs default btn-editable green-stripe"><i class="glyphicon glyphicon-edit"></i> Редактировать</a>
                        <a href="<?=$delpath . $uri3 .'/'. $question->ID ?>/" class="btn btn-xs default btn-editable red-stripe"><i class="glyphicon glyphicon-remove-circle"></i> Удалить</a>
                    </td>
                </tr>
            <?}?>
            </table>
            </div>
        <?}?>
    <? } else { ?>

        <h3>Вопрос - <?=$question['NameRU']?></h3>
        <br/>

        <!-- forma pentru edit question -->
        <form name="form1" method="POST" action="<?= $e_path . $uri3 .'/'. $uri4 ; ?>/" enctype="multipart/form-data">
            <div class="">
            <table class="table table-striped table-bordered table-hover">
                <?
                create_form_by_array($form1, $question);
                ?>
                <tr>
                    <td width="200">
                        Список ответов
                    </td>
                    <td>
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <td colspan="2">
                                    <button type="submit" id="addtext" class="btn green"><i class="fa fa-plus"></i></button>
                                </td>
                            </tr>
                            <? if (!empty($answers)) { ?>
                                <? foreach ($answers as $answer) { ?>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-4 col-sm-12"> 
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="sizing-addon6">RU</span>
                                                        <input type="text" name="old[AnsRU][]" class="form-control" value="<?=$answer['AnsRU']?>">
                                                        </input>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12">    
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="sizing-addon6">RO</span>
                                                        <input type="text" name="old[AnsRO][]" class="form-control" value="<?=$answer['AnsRO']?>">
                                                        </input>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12">    
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="sizing-addon6">EN</span>
                                                        <input type="text" name="old[AnsEN][]" class="form-control" value="<?=$answer['AnsEN']?>">
                                                        </input>
                                                    </div>
                                                </div>
                                            </div>
                                            <input hidden name="old[ID][]" type="text" value="<?=$answer['ID']?>">
                                        </td>
                                        <td width="30">
                                            <button type="submit" data-id='<?=$answer['ID']?>' data-table="Answers" id="remove" class="btn red">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </td>
                                    </tr>
                                <? } ?>
                            <? } ?>
                        </table>    
                    </td>
                </tr>  
                <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="question" hidden type="text" value="">
                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Обновить</button>
                </td>
                </tr>
            </table>
            </div>
        </form>
    <?}?>
<?}?>
<style>
.ms-container {
    width:100% !important;
}
.input-group {
    padding:5px;
}
</style>