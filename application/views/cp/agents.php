<?
$uri3 = $this->uri->segment(3);

$head1 = 'Список агентов';
$head2 = 'Редактирование агентов';
$addnew = 'агента';
$tblname = 'Agents';
$headerloc = 'agents';

$e_path = '/' . ADM_CONTROLLER . '/' . $headerloc . '/';
$delpath = '/' . ADM_CONTROLLER . '/del_' . $headerloc . '/';
$err = '';

$form1 = array(
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Имя*',
        'name' => 'Name'
    ),
    array(
        'dop_tr'=>'',
        'dop_style'=>'',
        'type'=>'checkbox',
        'class'=>'form-control',
        'descr'=>'Районы *',
        'name'=>'District',
        'source'=>'Districts',
        'sourcename'=>'NameRU'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Телефон*',
        'name' => 'Phone'
    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Login',
        'name' => 'Login',
        'dop_data' => '<span class="help-block">обязательно <code> E-mail</code></span>'

    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'class' => 'form-control',
        'type' => 'text',
        'descr' => 'Password',
        'name' => 'Password',
        'dop_data' => '<span class="help-block">Пароль должно содержать <code>не менее 6 символов</code></span>'

    ),
    array(
        'dop_tr' => '',
        'dop_style' => '',
        'type' => 'file',
        'class' => 'form-control',
        'path' => 'agents',
        'descr' => 'Фото*',
        'name' => 'Image',
        'dop_data' => '<span class="help-block">Допустимый размер <code>165px x 165px</code></span>'

    ),
);
$files = array('Image');
$checker = array('Name', 'Phone', 'District');
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $erflag = false;

    $array = array();

    $this->db->select('ID');
    $query = $this->db->get('Districts');

    foreach ($query->result() as $row)
    {
      if (isset($_POST['checkbox_a'][$row->ID])) {
          $array[] = $_POST['checkbox_a'][$row->ID];
      }
    }

    $json = json_encode($array);
    $_POST['data']['District'] = $json;

   // verificam daca a incarcat fisierele img
    if ($_FILES['Image']['size'] == 0 &&
        empty($uri3) ) {
      $erflag = true;
    }

    if (!$erflag) {
        foreach ($checker as $val) {
            if (empty($_POST['data'][$val])) $erflag = true;
        }
    }

    if (!$erflag) {
        $data_array = $_POST['data'];
        $data = array (
            'email' => $data_array['Login'],
            'password' => $data_array['Password'],
            'Name' => $data_array['Name'],
            'phone' => $data_array['Phone'],
            'status' => 2,
            'type' => 3
        );
        if (empty($uri3)) {
            $this->db->insert($tblname, $data_array);
            $this->db->insert('SiteUser', $data);
            $id = $this->db->insert_id();
        } else {
            $id = $uri3;
            $this->db->where('ID', $id)->update($tblname, $data_array);
            $result = $this->db->select('*')->where('email', $data['email'])->get('SiteUser')->row();
            if(!empty($result)) {
                $this->db->where('email', $data['email'])->update('SiteUser', $data);
            } else {
                $this->db->insert('SiteUser', $data);
            }
        }

        foreach ($files as $filename) {

            if (!empty($_FILES[$filename]['name'])) {
                $this->upload->do_upload($filename);
                $resarr = $this->upload->data();
                $file = $resarr['file_name'];

                if (strtolower($resarr['file_ext']) == '.jpg' || strtolower($resarr['file_ext']) == '.jpeg' || strtolower($resarr['file_ext']) == '.gif' || strtolower($resarr['file_ext']) == '.png') {
                    $this->db->where('ID', $id)->update($tblname, array($filename => $file));
                }
            }
        }

        if (!empty($id)) {
            header("Location: /" . ADM_CONTROLLER . "/$headerloc/");
            exit();
        }
    } else {
        $err .= '<div style="padding:10px 0;color:#ff0000;">Все поля отмеченные * обязательны для заполения</div>';
    }
}

?>
  <script type="text/javascript">
      function toggleb() {
          $("#newb").toggle();
      }
  </script>
  <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
  <script>
      function localsort() {
          counter = 1;
          data = '';
          $.each($('.sorthold'), function () {
              $(this).html(counter);
              if (counter < 2) breaker = ''; else breaker = '<>';
              data += breaker + $(this).attr('oid') + ':' + counter;
              counter++;
          });
          $.post('/<?=ADM_CONTROLLER?>/edit_table_order/', {data: data, table: '<?=$tblname?>'}, function (ret) {

          });
      }
  </script>
<? if (empty($uri3)) { ?>
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a><?= $head1 ?></a>
      </li>
    </ul>
  </div>

    <?= $err ?>

  <form name="form1" method="POST" action="<?= $e_path ?>" enctype="multipart/form-data">
    <div class="portlet box">
      <div class="portlet-title">
        <div class="caption" style="color:#888;font-size:26px;">
            <?= $head1 ?>
        </div>
        <div class="tools">
          <a href="javascript:;" class="expand btn default yellow-stripe" style="width: auto !important; height: auto !important">
            <i class="fa fa-plus"></i>
            <span class="hidden-480">
					                Добавить <?= $addnew ?>
                        </span>
          </a>
        </div>
      </div>
      <div class="portlet-body" style="<? if (empty($err)) echo 'display:none;'; ?>">
        <div class="table-scrollable">
          <table class="table table-striped table-bordered table-hover">
              <?
              create_form_by_array($form1, @$_POST['data']);
              ?>
            <tr>
              <td>&nbsp;</td>
              <td>
                <button type="submit" class="btn green"><i class="fa fa-check"></i> Добавить</button>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </form><br/>
    <?
    $checkb = $this->db->order_by('Sorder ASC, ID DESC')->get($tblname)->result_array();
    if (!empty($checkb)) {
        echo '<div class="table-scrollable">
		<table class="table table-striped table-bordered table-hover dataTable no-footer dragger">';
        echo '<tr class="heading nodrop nodrag">
			<th>Сортировка</th>
			<th width="80">Фото</th>
			<th>Имя</th>
			<th width="250">Действия</th>
		</tr>';

        foreach ($checkb as $barr) {
            $District = '';
            $distarray = json_decode($barr['District'], true);
            $this->db->select('NameRU');
            $this->db->where_in('ID', $distarray);
            $query = $this->db->get('Districts');
            foreach ($query->result() as $row) {
                $District .= $row->NameRU.' ';
            }
            echo "<tr>";
            echo "<td width=\"40\" celpadding=\"5\" cellspacing=\"10\" oid=\"" . $barr['ID'] . "\" align=\"center\" class=\"sorthold\">";
            echo $barr['Sorder'];
            echo "</td>";
            if(!empty($barr['Image'])) {
                $src = newthumbs($barr['Image'], 'agents', 82, 82, '82x82x0', 0);
            } else {
                $src = newthumbs('noicon.png', 'i', 82, 82, '82x82x1', 1);
            }
            echo "<td><img src='$src' ></td>";
            echo "<td>
                    <p><a href=\"$e_path" . $barr['ID'] . "/\">" . $barr['Name'] . "</a></p>
                    <p>Районы : " . $District . "</p>
                    <p>" . $barr['Phone'] . "</p> ";
            echo "</td>";
            echo "<td align=\"center\">";
            echo '<a href="' . $e_path . $barr['ID'] . '/" class="btn btn-xs default btn-editable green-stripe"><i class="glyphicon glyphicon-edit"></i> Редактировать</a>';
            echo '<a href="' . $delpath . $barr['ID'] . '/" class="btn btn-xs default btn-editable red-stripe"><i class="glyphicon glyphicon-remove-circle"></i> Удалить</a>';
            echo "</td>";
            echo "</tr>";
        }
        echo "</table>";
        echo '</div>';
    }
} else {
    $query = $this->db->where('ID', $uri3)->get($tblname);
    $data = $query->row_array();
    ?>
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="/<?= ADM_CONTROLLER ?>/<?= $headerloc ?>/"><?= $head1 ?></a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a>Редактирование "<?= $data['Name'] ?>"</a>
      </li>
    </ul>
  </div>
    <?= $err ?>

  <form name="form1" method="POST" action="<?= $e_path . $uri3; ?>/" enctype="multipart/form-data">
    <div class="table-scrollable">
      <table class="table table-striped table-bordered table-hover">
          <?
          create_form_by_array($form1, $data);
          ?>
        <tr>
          <td>&nbsp;</td>
          <td>
            <button type="submit" class="btn green"><i class="fa fa-check"></i> Обновить</button>
          </td>
        </tr>
      </table>
    </div>
  </form><br/>
    <?
}
?>