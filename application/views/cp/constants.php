<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<i class="fa fa-home"></i>
				<a href="/<?=ADM_CONTROLLER?>/topmenu/">Главная</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a>Константы</a>
			</li>
		</ul>
	</div>
<?
echo "<h3 class=\"page-title\">Константы</h3>";

$array = array('PAGE', 'BUTTON', 'INFO' , 'PLACEH' , 'LINK', 'TEXT', 'MSG', 'EMAIL', 'SNETWORKS', 'VALUE');

echo "<form id=\"form1\" name=\"form1\" method=\"post\" action=\"/".ADM_CONTROLLER."/constants/\">";

foreach($array as $item) {

    echo '<div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption">
            <i class="fa fa-cogs"></i> '.$item.'
          </div>
          <div class="tools">
            <a href="javascript:;" class="expand"></a>
          </div>
        </div>
        <div class="portlet-body" style="display: none">
          <div class="table-scrollable">
            <table class="table">';

  if ($item=='PAGE') {
      $query = $this->db->order_by('ConstantName ASC')->where_in('ID', array('1','2','3','65','66', '67') )->get('LangConstants')->result_array();
  } else {
      $query = $this->db->order_by('ConstantName ASC')->like('ConstantName', $item, 'after' )->get('LangConstants')->result_array();
  }
  if(!empty($query))
  {
    foreach($query as $carr)
    {
      $desc = $carr['ConstantName'];
      if(!empty($carr['Description'])) {
          $desc .= " (".$carr['Description'].")";
      }
      echo '<tr>';
        echo "<tr><td colspan='3' style='background-color: #eee'>".$desc."</td></tr>";
        echo "<td>";
          if (!empty($carr['ckeditor'])) $dopclass='ckeditor'; else $dopclass='form-control';
          echo "<textarea name=\"ru[".$carr['ID']."]\" rows=\"3\" class=\"$dopclass\">".$carr['RU']."</textarea>";
        echo "</td>";
        echo "<td>";
          if (!empty($carr['ckeditor'])) $dopclass='ckeditor'; else $dopclass='form-control';
          echo "<textarea name=\"ro[".$carr['ID']."]\" rows=\"3\" class=\"$dopclass\">".$carr['RO']."</textarea>";
        echo "</td>";
        echo "<td>";
        if (!empty($carr['ckeditor'])) $dopclass='ckeditor'; else $dopclass='form-control';
        echo "<textarea name=\"en[".$carr['ID']."]\" rows=\"3\" class=\"$dopclass\">".$carr['EN']."</textarea>";
        echo "</td>";
      echo "</tr>";
    }
  }
  echo '</table>';
  echo '</div>';
  echo '</div>';
  echo '</div>';
}
echo "<br /><button type=\"submit\" class=\"btn green\"><i class=\"fa fa-check\"></i> Обновить</button>";
echo "</form>";
?>