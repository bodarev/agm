<?
    $uri3 = $this->uri->segment(3);
    $head1 = 'Опросы';
    $head2 = 'Статистика';
    $headerloc = 'interviews';
    if (!empty($uri3)) { 
    $Interview = $this->db->where('ID', $uri3)->get('Interviews')->row();
    $Questions = $this->db->where('InterviewID', $uri3)->where('OnSite', 1)->get('Questions')->result();
    
    $idsQuestion = array_map(function($item){
        return $item->ID;
    }, $Questions);

    $Answers = $this->db->where_in('QuestionID', $idsQuestion)->order_by('QuestionID', 'ASC')->get('Answers')->result();

    foreach ($Questions as $question) {
        $total = 0;
        foreach ($Answers as $answer) {
            if ($question->ID == $answer->QuestionID) {
                $question->childs[] = (object) array('id' => $answer->ID , 'name' => $answer->AnsRU, 'count' => $answer->Count);
                $total += $answer->Count;
            }
        }
        $question->total = $total;
    }
?>
<div class="page-bar">
    <ul class="page-breadcrumb">
    <li>
        <i class="fa fa-home"></i>
        <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <a href="/<?= ADM_CONTROLLER ?>/<?= $headerloc ?>/"><?= $head1 ?></a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <a><?=$head2?> "<?=$Interview->NameRU?>"</a>
    </li>
    </ul>
</div>
<h3><?=$head2?> "<?=$Interview->NameRU?>"</h3>
<div class="portlet">
    <div class="portlet-body">
        <? foreach ($Questions as $question ) { ?>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet yellow-crusta box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-question"></i>
                                <?=$question->NameRU?>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <? foreach($question->childs as $answer) { ?>
                                <? $percent = (($question->total > 0 )) ? ($answer->count*100)/$question->total : 0; ?>
                                <div class="row static-info">
                                    <div class="col-md-10 name">
                                        <?=$answer->name?>
                                        <div class="b_progress">
                                            <div class="bp_bar" style="width: <?displayNumberFormat($percent)?>%;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 value">
                                        <? displayNumberFormat($percent) ?>% (<?=$answer->count?>)
                                    </div>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
        <? } ?>    
    </div>
</div>
<?} else { ?>
    <h5>Error<h5>
<? } ?>
<style>
.bp_bar {
    background: #e12c63;
    height: 100%;
    position: relative;
}
.b_progress {
    background: #e0e7ef;
    width: 100%;
    max-width: 100%;
    height: 8px;
}
.value {
    padding-top:14px;
}
</style>