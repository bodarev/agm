<?php
/**
 * Created by PhpStorm.
 * User: WhoAmI
 * Date: 01.12.2017
 * Time: 13:32
 */
/* Переменные страницы */
$title = 'Админы';
$a_path = '/' . ADM_CONTROLLER . '/add_admin/';
$e_path = '/' . ADM_CONTROLLER . '/admin/';
$cp_path = '/' . ADM_CONTROLLER . '/change_password_admin/';
$del_path = '/' . ADM_CONTROLLER . '/delete_admin/';

/* Формируем запрос на получение данных по FAQ */
$admins = $this->db->where('AdminType', '2')->get('admins')->result();

/* Получаем данные, которые ввел пользователь */
$login = '';
$password = '';
$password_check = '';
$err_flag = false;
if (isset($_SESSION['user_form_data'])) {
	$login = $_SESSION['user_form_data']['login'];
	$password = $_SESSION['user_form_data']['password'];
	$password_check = $_SESSION['user_form_data']['password_check'];
	$err_flag = true;
	unset($_SESSION['user_form_data']);
}

?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a><?= $title ?></a>
        </li>
    </ul>
</div>
<br>

<?php // Отображаем сообщения пользователю ?>
<?php if (isset($_SESSION['success'])) : ?>
    <div class="alert alert-success alert-dismissable fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<?= $_SESSION['success']; ?>
    </div>
	<?php unset($_SESSION['success']); ?>
<?php endif; ?>
<?php if (isset($_SESSION['error'])) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<?php foreach ($_SESSION['error'] as $error) : ?>
			<?= $error ?>
            <br/>
		<?php endforeach; ?>
    </div>
	<?php unset($_SESSION['error']); ?>
<?php endif; ?>

<form method="POST" action="<?= $a_path; ?>" enctype="multipart/form-data">
    <div class="portlet box">
        <div class="portlet-title">
            <div class="caption" style="color:#888;font-size:26px;">
				<?= $title; ?>
            </div>
            <div class="tools" style="display:none;">
                <a href="javascript:;" class="expand"></a>
            </div>
            <div class="actions">
                <a class="btn default yellow-stripe">
                    <i class="fa fa-plus"></i>
                    <span class="hidden-480">Добавить</span>
                </a>
            </div>
        </div>
        <div class="portlet-body" style="<? if (!$err_flag) echo 'display:none;'; ?>">
            <div class="tabbable tabbable-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_1_1" data-toggle="tab">Общая информация</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1_1">
                        <div class="table-scrollable">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <td width="200">Логин *</td>
                                    <td>
                                        <input type="text" name="Login" class="form-control" value="<?= $login ?>" required>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="200">Пароль *</td>
                                    <td>
                                        <input type="password" name="Pass" class="form-control" value="<?= $password ?>" required>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="200">Подтвердите пароль *</td>
                                    <td>
                                        <input type="password" name="Pass_check" class="form-control" value="<?= $password_check ?>" required>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="200">&nbsp;</td>
                                    <td>
                                        <button type="submit" class="btn green aac_button_add">
                                            <i class="fa fa-check"></i> Добавить
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


<div class="table-scrollable">
    <table class="table table-striped table-bordered table-hover dataTable no-footer">
        <thead>
        <tr class="heading nodrop nodrag">
            <th>Login</th>
            <th width="100">Удалить</th>
        </tr>
        </thead>
        <tbody>
		<?php foreach ($admins as $admin): ?>
            <tr>
                <td style="vertical-align: middle"><?= $admin->Login ?></td>
                <td class="text-center" width="100">
                    <a href="<?= $del_path . $admin->ID . '/' ?>" class="btn btn-danger" onclick="return confirmDelete();"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
		<?php endforeach; ?>
        </tbody>
    </table>
</div>


<br>
<?php // Отображаем сообщения пользователю ?>
<?php if (isset($_SESSION['success_cpa'])) : ?>
    <div class="alert alert-success alert-dismissable fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<?= $_SESSION['success_cpa']; ?>
    </div>
	<?php unset($_SESSION['success_cpa']); ?>
<?php endif; ?>
<?php if (isset($_SESSION['error_cpa'])) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<?php foreach ($_SESSION['error_cpa'] as $error) : ?>
			<?= $error ?>
            <br/>
		<?php endforeach; ?>
    </div>
	<?php unset($_SESSION['error_cpa']); ?>
<?php endif; ?>

<h3 class=\"page-title\">Смена пароля</h3>
<form method="post" action="<?= $cp_path ?>">
    <div class="table-scrollable">
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <td width="200">Старый пароль</td>
                <td><input type="password" name="oldp" class="form-control"/></td>
            </tr>
            <tr>
                <td width="200">Новый пароль</td>
                <td><input type="password" name="newp" class="form-control"/></td>
            </tr>
            <tr>
                <td>Подтверждение пароля</td>
                <td><input type="password" name="newpc" class="form-control"/></td>
            </tr>
            <tr>
                <td align="right"></td>
                <td>
                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Обновить</button>
                </td>
            </tr>
        </table>
    </div>
</form>
<script type="text/javascript">
	/* Функция для подтверждения удаления */
	function confirmDelete() {
		return confirm("Вы точно уверены?");
	}
</script>