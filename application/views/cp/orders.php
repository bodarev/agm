<?
$uri3 = $this->uri->segment(3);

$head1     = 'Заказы';
$head2     = 'Редактирование Заказы';
$addnew    = 'Заказы';
$tblname   = 'Orders';
$headerloc = 'orders';

$e_path  = '/'.ADM_CONTROLLER.'/'.$headerloc.'/';
$delpath = '/'.ADM_CONTROLLER.'/del_'.$headerloc.'/';
$err     = '';

$form1 = array(
    array(
        'dop_tr'     => '',
        'dop_style'  => '',
        'class'      => 'form-control',
        'type'       => 'checkbox2',
        'descr'      => 'Тип*',
        'name'       => 'Type',
        // tabela
        'source'     => 'Types',
        // coloana
        'sourcename' => 'Name',
    ),
    array(
        'dop_tr'    => '',
        'dop_style' => '',
        'class'     => 'form-control',
        'type'      => 'text',
        'descr'     => 'Адрес RU*',
        'name'      => 'AddressRU',
    ),
    array(
        'dop_tr'    => '',
        'dop_style' => '',
        'class'     => 'form-control',
        'type'      => 'text',
        'descr'     => 'Адрес RO*',
        'name'      => 'AddressRO',
    ),
    array(
        'dop_tr'    => '',
        'dop_style' => '',
        'class'     => 'form-control',
        'type'      => 'text',
        'descr'     => 'Адрес EN*',
        'name'      => 'AddressEN',
    ),
    array(
        'dop_tr'    => '',
        'dop_style' => '',
        'class'     => 'form-control',
        'type'      => 'text',
        'descr'     => 'Телефон*',
        'name'      => 'Phone',
    ),
    array(
        'dop_tr'    => '',
        'dop_style' => '',
        'class'     => 'form-control',
        'type'      => 'text',
        'descr'     => 'Email*',
        'name'      => 'Email',
    ),
);
?>
<style>
    .table tr td {
        vertical-align: middle !important;
    }
</style>

<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<? if (empty($uri3)) { ?>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a><?= $head1 ?></a>
            </li>
        </ul>
    </div>

    <?= $err ?>

    <h2><?= $head1 ?></h2>
    <br>

    <?php // Отображаем сообщения пользователю ?>
    <?php if (isset($_SESSION['success'])) : ?>
        <div class="alert alert-success alert-dismissable fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?= $_SESSION['success']; ?>
        </div>
        <?php unset($_SESSION['success']); ?>
    <?php endif; ?>
    <?php if (isset($_SESSION['error'])) : ?>
        <div class="alert alert-danger alert-dismissable fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php foreach ($_SESSION['error'] as $error) : ?>
                <?= $error ?>
                <br/>
            <?php endforeach; ?>
        </div>
        <?php unset($_SESSION['error']); ?>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet green-meadow box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Отчет по заказам
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row static-info">
                        <div class="col-md-5 name">
                            Минимальная сумма покупки:
                        </div>
                        <div class="col-md-7 value">
                            <?
                            $this->db->select_min('total');
                            $min = $this->db->get('Orders')->row(
                            )->total;
                            displayNumberFormat($min);
                            ?>
                            <small>MDL</small>
                        </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name">
                            Максимальная сумма покупки:
                        </div>
                        <div class="col-md-7 value">
                            <?
                            $this->db->select_max('total');
                            $max = $this->db->get('Orders')->row(
                            )->total;
                            displayNumberFormat($max);
                            ?>
                            <small>MDL</small>
                        </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name">
                            Средний чек:
                        </div>
                        <div class="col-md-7 value">
                            <?
                            $this->db->select_sum('total');
                            $sum = $this->db->get('Orders')->row()->total;

                            $count = $this->db->count_all_results('Orders');
                            if($count > 0 ) {
                                $media = $sum / $count;
                            } else {
                                $media = $sum;
                            }
                            displayNumberFormat($media);
                            ?>
                            <small>MDL</small>
                        </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name">
                            Количество заказов:
                        </div>
                        <div class="col-md-7 value">
                            <?
                            echo $this->db->count_all_results('Orders');
                            ?>
                        </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name">
                            200 самых популярных товаров:
                        </div>
                        <div class="col-md-7 value">
                            <a target="_blank" href="/cp/top/">Нажмите здесь</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <a href="/cp/export_xml" target="_blank" style="float:right" class="btn green"><i class="icon-users"></i> генерировать XML</a>
    <br/>
    <br/>
    <?
    $checkb = $this->db->select('Orders.*, Status.NameRU as StatusName')->join(
        'Status',
        'Orders.Status = Status.Value'
    )->order_by('Date DESC')->get($tblname)->result_array();
    if (!empty($checkb)) {
        echo '<div class="table-scrollable">
        <table class="table table-striped table-bordered table-hover dataTable no-footer">
        <thead>
        <tr class="heading nodrop nodrag">
        <th>Номер</th>
			<th>Дата заявки</th>
			<th>Имя</th>
			<th>Адрес доставки</th>
			<th>Статус</th>
			<th width="150">Действия</th>
        </tr>
        </thead>';

        foreach ($checkb as $barr) {
            $items = $this->db
                ->select('OrderItems.*, Products.TitleRU as ProductName')
                ->join('Products', 'Products.Cod = OrderItems.ItemCod')
                ->where('OrderItems.OrderID', $barr['ID'])
                ->get('OrderItems')
                ->result_array();


            switch ($barr['status']) {
                case 0:
                    $class = 'badge badge-roundless badge-info';
                    break;
                case 1:
                    $class = 'badge badge-roundless badge-danger';
                    break;
                case 2:
                    $class = 'badge badge-roundless badge-success';
                    break;
                case 3:
                    $class = 'badge badge-roundless badge-warning';
                    break;
            }

            $name = $barr['Name'];
            echo "<tr>";
            echo '<td><a href="'.$e_path.$barr['ID'].'/">'.$barr['OrderNr']
                 .'</a></td>';
            echo "<td>".date('d.m.Y', strtotime($barr['Date'])).", ".date(
                    'H:i',
                    strtotime($barr['Date'])
                )."</td>";
            echo "<td>";
            echo "<span>".$name."</span>";
            echo "</td>";
            echo "<td>";
            echo "<span>".$barr['street']."</span>
                    <span>".$barr['housenumber']."</span>";
            echo "</td>";
            echo "<td><span class='".$class."'>".$barr['StatusName']
                 ."</status></td>";
            echo "<td align=\"center\">";
            echo '<a href="'.$e_path.$barr['ID']
                 .'/" class="btn btn-xs default btn-editable green-stripe"><i class="glyphicon glyphicon-edit"></i> Детали</a>';
            ?>    
            </td>
            </tr>
        <? } ?>
        </table>
        </div>
    <? }
} else {
    $query = $this->db
        ->select(
            'Orders.*, Status.NameRU AS StatusName, Districts.NameRU as DistrictName, SiteUser.Name as agentName'
        )
        ->join('SiteUser', 'SiteUser.ID = Orders.agentID', 'left')
        ->join('Status', 'Status.Value = Orders.Status', 'left')
        ->join('Districts', 'Districts.ID = Orders.DistrictID', 'left')
        ->where('Orders.ID', $uri3)
        ->get($tblname);
    $data  = $query->row_array();
    ?>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="/<?= ADM_CONTROLLER ?>/topmenu/">Главная</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/<?= ADM_CONTROLLER ?>/<?= $headerloc ?>/"><?= $head1 ?></a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a>Детали заказ номер "<?= $data['OrderNr'] ?>"</a>
            </li>
        </ul>
    </div>
    <?= $err ?>

    <h3>
        <i class="fa fa-shopping-cart"></i>
        Заказ #<?= $data['OrderNr'] ?>
        | <?= date('d.m.Y, H:i', strtotime($data['Date'])) ?>
        <a style="margin-bottom: 3px; float:right;" href="/cp/orders/"
           class="btn btn-xs default btn-editable yellow-stripe">
            <i class="fa fa-angle-left"></i> Назад
        </a>
    </h3>
    <div class="portlet">
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow-crusta box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Информация о заказе
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Заказ #:
                                </div>
                                <div class="col-md-7 value">
                                    <?= $data['OrderNr'] ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Дата:
                                </div>
                                <div class="col-md-7 value">
                                    <?= date(
                                        'd.m.Y, H:i',
                                        strtotime($data['Date'])
                                    ) ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Итог:
                                </div>
                                <div class="col-md-7 value">
                                    <? $itog = (float)($data['total']
                                        + (float)$data['delivery_price']);
                                        displayNumberFormat($itog);         
                                    ?>
                                    MDL
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    агент AGM:
                                </div>
                                <div class="col-md-7 value">
                                    <?= $data['agentName'] ?>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Статус:
                                </div>
                                <div class="col-md-7 value">
                                    <select name="status" id="status"
                                            class="<?= $data['ID'] ?>">
                                        <?php
                                        $query = $this->db->select('*')
                                                          ->get('Status')
                                                          ->result();

                                        foreach ($query as $item) { ?>
                                            <option id="<?= $item->Value ?>" <? if ($item->Value
                                                                                    == $data['status']
                                            ) {
                                                echo 'selected';
                                            } ?> >
                                                <?= $item->NameRU ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Возврат:
                                </div>
                                <div class="col-md-7 value">
                                    <?=$data['refund']?> MDL
                                    <?php if ($data['payment'] == 1 && $data['status'] == 2 && $data['refund']==0 ) : ?>
                                        <? $crypted = ilabCrypt('encrypt', $data['OrderNr'] );
                                            $refund_href = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . '/payment/refund?q=' . rawurlencode($crypted) . '&a=';
                                        ?>
                                        <a href="javascript:;" style="float:right"
                                        class="ilab_refund_payment btn btn-md red btn-editable"
                                            data-link="<?= $refund_href ?>" data-price="<?= $data['total']+$data['delivery_price'] ?>">
                                            Возврат
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue-hoki box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Информация о
                                покупателе
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Имя:
                                </div>
                                <div class="col-md-7 value">
                                    <?= $data['Name'] ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Email:
                                </div>
                                <div class="col-md-7 value">
                                    <?= $data['email'] ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Телефон:
                                </div>
                                <div class="col-md-7 value">
                                    <?= $data['phone'] ?>
                                </div>
                            </div>

                            <?php if (!empty($data['comment'])): ?>
                                <div class="row static-info">
                                    <div class="col-md-5 name">
                                        Заметки к заказу:
                                    </div>
                                    <div class="col-md-7 value">
                                        <?= $data['comment'] ?>
                                    </div>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet green-meadow box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Адес Доставки
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-12 value">
                                    <?= $data['Name'] ?><br>
                                    <?= $data['street'] ?>
                                    <?= $data['housenumber'] ?><br>
                                    <?= $data['DistrictName'] ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet grey-cascade box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Товары
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>
                                            Название
                                        </th>
                                        <th>
                                            Цена
                                        </th>
                                        <th>
                                            Кол-во
                                        </th>
                                        <th>
                                            Итог
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $products = $this->db
                                        ->select(
                                            'OrderItems.ItemCod, OrderItems.ItemCount, OrderItems.ItemPrice as PriceCurrent, Products.*'
                                        )
                                        ->join(
                                            'Products',
                                            'Products.Cod = OrderItems.ItemCod'
                                        )
                                        ->where('OrderID', $data['ID'])
                                        ->get('OrderItems')->result_array(); ?>

                                    <?php foreach ($products as $item): ?>
                                        <tr style="vertical-align: middle;">
                                            <td>
                                                <a target="_blank"
                                                   href="/ru/pages/product/<?= $item['UriName'] ?>">
                                                    <?= $item['TitleRU'] ?></a>
                                            </td>
                                            <td>
                                                <? 
                                                    $suma =  Exchange($item['PriceCurrent'],false);
                                                    displayNumberFormat($suma);         
                                                
                                                ?>
                                                <small>MDL</small>
                                            </td>
                                            <td>
                                                <?= $item['ItemCount'] ?>
                                            </td>
                                            <td>
                                                <?= (int)$item['ItemCount']
                                                    * Exchange(
                                                        $item['PriceCurrent'], false
                                                    ) ?>
                                                <small>MDL</small>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                    <div class="well">
                        <div class="row static-info align-reverse">
                            <div class="col-md-8 name">
                                Стоимость заказа:
                            </div>
                            <div class="col-md-3 value">
                                <?
                                    displayNumberFormat($data['total']);         
                                ?>
                                <small>MDL</small>
                            </div>
                        </div>
                        <div class="row static-info align-reverse">
                            <div class="col-md-8 name">
                                Доставка:
                            </div>
                            <div class="col-md-3 value">
                                <?
                                    displayNumberFormat($data['delivery_price']);         
                                ?>
                                <small>MDL</small>
                            </div>
                        </div>
                        <div class="row static-info align-reverse">
                            <div class="col-md-8 name">
                                Итог:
                            </div>
                            <div class="col-md-3 value">
                                <?
                                    displayNumberFormat($data['total']+ $data['delivery_price']);
                                ?>
                                <small>MDL</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? } ?>

<script type="text/javascript">
    $(document).ready(function () {
        
        $('body').on('click', '.ilab_refund_payment', function (event) {
            console.log('refund_start');
            event.preventDefault();
            var self = $(this);
            var link = self.attr('data-link');
            var price = self.attr('data-price');
            if (confirm('Вы точно уверены, что хотите оформить возврат средств?')) {
                var a = prompt('Введите сумму для возврата');
                a = parseFloat(a);
                if (!isNaN(a)) {
                    if (a > parseFloat(price)) {
                        alert('Сумма к возврату не может превышать стоимость товаров');
                        return false;
                    }
                    if (a <= 0) {
                        alert('Сумма к возврату не может быть меньше либо равной 0 (нулю)');
                        return false;
                    }
                    var new_href = link + a;
                    window.location.href = new_href;
                } else {
                    alert('Введенные Вами данные не прошли валидацию. Просим Вас при введении суммы, ввести числовое значение.');
                    return false;
                }
            }
            return false;
        });
    });
</script>

