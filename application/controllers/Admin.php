<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    private $msg = 'Данные успешно сохранены';

    function __construct()
    {
        parent::__construct();

        @session_start();
        header('Content-type: text/html; charset=utf-8');
        $this->load->helper('url');

        $il  = @$_SESSION['islogged'];
        $ll  = @$_SESSION['login'];
        $uid = @$_SESSION['admin_id'];

        if (empty($il) || empty($ll) || empty ($uid)) {
            $cc = $this->uri->segment(1);
            $ac = $this->uri->segment(2);

            if ($ac == 'importCategory') {
                $this->importCategory();
            }
            if ($ac == 'importProducts') {
                $this->importProducts();
            }
            if ($ac == 'updateCategory') {
                $this->updateCategory();
            }
            if ($ac == 'updateBrendList') {
                $this->updateBrendList();
            }
            if ($ac == 'importDealers') {
                $this->importDealers();
            }
            if ($ac == 'importCursValutar') {
                $this->importCursValutar();
            }
            if ($ac == 'export_xml') {
                $this->export_xml();
            }
            if ($ac == 'setExportedOrders') {
                $this->setExportedOrders();
            }
            if ($ac == 'setExistProducts') {
                $this->setExistProducts();
            }

            if ($cc != 'cp' || $ac != 'login') {
                header("Location: /cp/login");
                exit();
            }
        }
    }

    public function index()
    {
        header("Location: /cp/topmenu/");
    }

    public function login()
    {

        $fldata = '';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $check_user = $this->db->where('Login', $_POST['login'])->where(
                'Pass',
                md5($_POST['pass'])
            )->get('admins')->row_array();
            if (!empty($check_user)) {
                $_SESSION["login"]    = $_POST['login'];
                $_SESSION["islogged"] = true;
                $_SESSION["admin_id"]  = $check_user['ID'];
                header("Location: /cp/topmenu");
            } else {
                $fldata = "неправильный логин/пароль";
            }
        }

        //echo $this->session->userdata('islogged');

        $this->load->view('/cp/login', array('fldata' => $fldata));
    }

    function logout()
    {
        $_SESSION['admin_id'] = 0;
        header("Location: /cp/login");
    }

    function constants()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            while (list($key, $val) = each($_POST['ru'])) {
                $val = addslashes($val);
                $this->db->where('ID', $key)->update(
                    'LangConstants',
                    array('RU' => $val)
                );
            }
            while (list($key, $val) = each($_POST['ro'])) {
                $val = addslashes($val);
                $this->db->where('ID', $key)->update(
                    'LangConstants',
                    array('RO' => $val)
                );
            }
            while (list($key, $val) = each($_POST['en'])) {
                $val = addslashes($val);
                $this->db->where('ID', $key)->update(
                    'LangConstants',
                    array('EN' => $val)
                );
            }
        }

        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/constants')
        );
    }

    function edit_table_order()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (!empty($_POST['data'])) {
                $vals = explode('<>', $_POST['data']);
                if (empty($_POST['field'])) {
                    $field = 'Sorder';
                } else {
                    $field = $_POST['field'];
                }
                foreach ($vals as $value) {
                    $dt = explode(':', $value);
                    $this->db->where('ID', $dt[0])->update(
                        $_POST['table'],
                        array($field => $dt[1])
                    );
                }
            }
        }
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function topmenu()
    {
        $config['upload_path']   = realpath("public/topmenu");
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['encrypt_name']  = true;
        $this->load->library('upload', $config);

        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/topmenu')
        );
    }

    function menu()
    {
        $config['upload_path']   = realpath("public/menu");
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['encrypt_name']  = true;
        $this->load->library('upload', $config);

        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/menu')
        );
    }

    function del_topmenu()
    {
        $uri3 = $this->uri->segment(3);
        if (!empty($uri3)) {
            $this->deleteImageBeforeDelRec('TopMenu');
            $this->db->where('ID', $uri3)->delete('TopMenu');
        }
        header("Location: /cp/topmenu/");
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function check() {
        $table = $this->uri->segment(3);
        $col = $this->uri->segment(4);
        $id = $this->uri->segment(5);
        $val = $this->uri->segment(6);
        $this->db->where('ID', $id)->update($table, array($col => $val));
    }

    function setblock()
    {
        $uri3 = $this->uri->segment(3);
        $uri4 = $this->uri->segment(4);
        $this->db->where('ID', $uri3)->update(
            'MenuBlocks',
            array('view' => $uri4)
        );
    }

    public function deleteImageElement()
    {
        $id      = (int)$this->input->post('id');
        $table   = $this->input->post('table');
        $folder  = strtolower($table);
        $name    = $this->input->post('name');
        $del_img = !empty($name) ? $name : 'image';
        $path    = $this->db->select('Image')->from($table)->where('id', $id)
                            ->get()->row()->Image;
        $sql     = $this->db->where('id', $id)->update(
            $table,
            array(
                $del_img => '',
            )
        );
        if ($this->db->affected_rows() != 1) {
            echo json_encode(
                array('msg' => 'Во время изменений, произошла ошибка')
            );
        } else {
            unlink('public/'.$folder.'/'.$path);
            echo json_encode(array('msg' => 'Удалено успешно'));
        }
    }

    function del_block()
    {
        $uri3 = $this->uri->segment(3);
        $uri4 = $this->uri->segment(4);
        if (!empty($uri3) && !empty($uri4)) {
            $this->deleteImageBeforeDelRec('MenuBlocks');
            $this->db->where('id', $uri4)->delete('MenuBlocks');
        }
        header("Location: /cp/topmenu/".$uri3);
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function slider()
    {
        $config['upload_path']   = realpath("public/slider");
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['encrypt_name']  = true;
        $this->load->library('upload', $config);

        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/slider')
        );
    }

    function del_slider()
    {
        $uri3 = $this->uri->segment(3);
        if (!empty($uri3)) {
            $this->db->where('ID', $uri3)->delete('Slider');
        }
        header("Location: /cp/slider/");
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function sliderCategory()
    {
        $config['upload_path']   = realpath("public/slidercategory");
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['encrypt_name']  = true;
        $this->load->library('upload', $config);

        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/slidercategory')
        );
    }

    function del_sliderCategory()
    {
        $uri3 = $this->uri->segment(3);
        if (!empty($uri3)) {
            $this->db->where('ID', $uri3)->delete('SliderCategory');
        }
        header("Location: /cp/slidercategory/");
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function brands()
    {
        $config['upload_path']   = realpath("public/brands");
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['encrypt_name']  = true;
        $this->load->library('upload', $config);

        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/brands')
        );
    }

    function del_brands()
    {
        $uri3 = $this->uri->segment(3);
        if (!empty($uri3)) {
            $this->deleteImageBeforeDelRec('Brands');
            $this->db->where('ID', $uri3)->delete('Brands');
        }
        header("Location: /cp/brands/");
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function info()
    {
        $config['upload_path']   = realpath("public/info");
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['encrypt_name']  = true;
        $this->load->library('upload', $config);

        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/info')
        );
    }

    function del_info()
    {
        $uri3 = $this->uri->segment(3);
        if (!empty($uri3)) {
            $this->deleteImageBeforeDelRec('Info');
            $this->db->where('ID', $uri3)->delete('Info');
        }
        header("Location: /cp/info/");
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function homecategory()
    {
        $config['upload_path']   = realpath("public/homecategory");
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['encrypt_name']  = true;
        $this->load->library('upload', $config);

        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/homecategory')
        );
    }

    function del_HomeCategory()
    {
        $uri3 = $this->uri->segment(3);
        if (!empty($uri3)) {
            $this->deleteImageBeforeDelRec('HomeCategory');
            $this->db->where('ID', $uri3)->delete('HomeCategory');
        }
        header("Location: /cp/homecategory/");
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function hometabs()
    {
        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/hometabs')
        );
    }

    function del_HomeTabs()
    {
        $uri3 = $this->uri->segment(3);
        if (!empty($uri3)) {
            $this->db->where('ID', $uri3)->delete('HomeTabs');
        }
        header("Location: /cp/hometabs/");
    }

    function dellprodfromtab()
    {
        $uri3 = $this->uri->segment(3);
        $uri4 = $this->uri->segment(4);
        if (!empty($uri3) && !empty($uri4)) {
            $this->db->where('ID', $uri4)->delete('TabProd');
        }
        header("Location: /cp/hometabs/".$uri3);
    }

    function addprodtotabs()
    {
        $id    = $this->input->post('id');
        $tabid = $this->input->post('tabid');
        $data  = array(
            'ProdID' => $id,
            'TabID'  => $tabid,
        );
        $query = $this->db->select('*')
        ->where('ProdID', $id)
        ->where('TabID', $tabid)
        ->get('TabProd')->result();

        if(empty($query)) {
            $this->db->insert('TabProd', $data);
            $err = false;
            $msg = $this->msg;
        } else {
            $err = true;
            $msg = 'Такой товар уже существует';
        }

        $this->alert($msg, $err);
    }

        /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

        function interviews()
        {
            $this->load->view(
                'layouts/cp_layout',
                array('inner_view' => 'cp/interviews')
            );
        }
    
        function del_interviews()
        {
            $uri3 = $this->uri->segment(3);
            $uri4 = $this->uri->segment(4);
            if($uri4!=''){
                $this->db->where('ID', $uri4)->delete('Questions');
                $this->db->where('QuestionID', $uri4)->delete('Answers');
                header("Location: /cp/interviews/".$uri3);
            } else {
                if (!empty($uri3)) {
                    $result = $this->db->where('InterviewID', $uri3)->get('Questions')->result();

                    $idsQuestion = array_map(function($item){
                        return $item->ID;
                    }, $result);

                    $this->db->where('ID', $uri3)->delete('Interviews');
                    $this->db->where('InterviewID', $uri3)->delete('Questions');
                    $this->db->where_in('QuestionID', $idsQuestion)->delete('Answers');
                } 
                header("Location: /cp/interviews/");
            }
        }


        function statistic()
        {
            $this->load->view(
                'layouts/cp_layout',
                array('inner_view' => 'cp/statistic')
            );
        }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    public function alert($msg='', $error=false){ 
        echo json_encode(array('error' => $error, 'msg' => $msg));  
        exit();  
    } 

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function branches()
    {
        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/branches')
        );
    }

    function del_branches()
    {
        $uri3 = $this->uri->segment(3);
        if (!empty($uri3)) {
            $this->db->where('ID', $uri3)->delete('Branches');
        }
        header("Location: /cp/branches/");
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function orders()
    {
        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/orders')
        );
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function top()
    {
        $this->load->view('layouts/cp_layout', array('inner_view' => 'cp/top'));
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function agents()
    {
        $config['upload_path']   = realpath("public/agents");
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['encrypt_name']  = true;
        $this->load->library('upload', $config);

        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/agents')
        );
    }

    function del_agents()
    {
        $uri3 = $this->uri->segment(3);
        if (!empty($uri3)) {
            $this->deleteImageBeforeDelRec('Agents');
            $email = $this->db->select('Login')->where('ID', $uri3)->get(
                'Agents'
            )->row()->Login;
            $this->db->where('ID', $uri3)->delete('Agents');
            $this->db->where('email', $email)->delete('SiteUser');
        }
        header("Location: /cp/agents/");
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function dealers()
    {
        $config['upload_path']   = realpath("public/dealers");
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['encrypt_name']  = true;
        $this->load->library('upload', $config);

        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/dealers')
        );
    }

    function del_dealers()
    {
        $uri3 = $this->uri->segment(3);
        if (!empty($uri3)) {
            $this->db->where('ID', $uri3)->delete('SiteUser');
        }
        header("Location: /cp/dealers/");
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function users() {
        $config['upload_path']   = realpath("public/users");
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['encrypt_name']  = true;
        $this->load->library('upload', $config);

        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/users')
        );
    }

    function del_users() {
        $uri3 = $this->uri->segment(3);
        if (!empty($uri3)) {
            $this->db->where('ID', $uri3)->delete('SiteUser');
        }
        header("Location: /cp/users/");
    }

   /**
     * Страница пользователей Админки
     */
    public function admins()
    {
        $main_name = 'admins';
        
        $this->load->view('layouts/cp_layout', array('inner_view' => 'cp/' . $main_name));
    }
    
    /**
     * Добавить нового пользователя Админки
     */
    public function add_admin()
    {
        // Проверяем если POST запрос
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            header("HTTP/1.0 404 Not Found");
            show_404();
            exit();
        }
        
        // Исполняющий скрипт
        try {
            // Стандартная инициализация
            $tblname = 'admins';
            
            // Массив для ошибок
            $errors = array();
            
            // Получаем данные из POST запроса
            $login = $_POST['Login'];
            $password = $_POST['Pass'];
            $password_check = $_POST['Pass_check'];
            
            // Валидируем полученные данные
            // Проверяем Логин
            if (strlen($login) < 4) {
                $errors[] = 'Логин должен быть не менее 4 символов';
            }
            
            $login_check = $this->db->where('Login', $login)->get('admins')->result();
            if (!empty($login_check)) {
                $errors[] = 'Данный логин уже зарегистрирован в системе!';
            }
            // Проверяем Пароль
            if (strlen($password) < 4) {
                $errors[] = 'Пароль должен быть не менее 4 символов';
            }
            
            if ($password !== $password_check) {
                $errors[] = 'Пароли не совпадают!';
            }
            
            // Проверяем массив ошибок
            if (!empty($errors)) {
                throw new Exception('Ошибка при заполнении формы');
            }
            
            // Формируем массив для добавления в БД
            $data = array(
                'Login' => $login,
                'Pass' => md5($password)
            );
            
            // Записываем в БД
            if (!$this->db->insert($tblname, $data)) {
                throw new Exception('Ошибка записи данных в таблицу ' . $tblname);
            }
            
            // Сообщаем пользователю об успешной записи в БД
            $_SESSION['success'] = 'Вы успешно добавили нового админа!';
        } catch (Exception $e) {
            $_SESSION['user_form_data'] = array(
                'login' => $login,
                'password' => $password,
                'password_check' => $password_check
            );
            $errors[] = 'Выброшено исключение : ' . $e->getMessage();
            $_SESSION['error'] = $errors;
        }
        header('Location:/cp/admins/');
        exit();
    }
    
    /**
     * Изменить пароль
     */
    public function change_password_admin()
    {
        // Проверяем если POST запрос
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            header("HTTP/1.0 404 Not Found");
            show_404();
            exit();
        }
        
        // Исполняющий скрипт
        try {
            // Стандартная инициализация
            $tblname = 'admins';
           
            // Массив для ошибок
            $errors = array();
            
            // Получаем идентификатор пользователя
            $admin_id = $_SESSION['admin_id'];
            
            // Получаем данные  из POST запроса
            $old_password = $_POST['oldp'];
            $password = $_POST['newp'];
            $password_check = $_POST['newpc'];
            
            // Валидируем полученные данные
            // Проверяем данные по старому паролю
            $user = $this->db->where('ID', $admin_id)->where('Pass', md5($old_password))->get('admins')->row();
            if (empty($user)) {
                $errors[] = 'Неверный пароль!';
            }
            
            // Проверяем новый Пароль
            if (strlen($password) < 4) {
                $errors[] = 'Пароль должен быть не менее 4 символов';
            }
            
            if ($password !== $password_check) {
                $errors[] = 'Новые Пароли не совпадают!';
            }
            
            // Проверяем массив ошибок
            if (!empty($errors)) {
                throw new Exception('Ошибка при заполнении формы');
            }
            
            // Формируем массив для добавления в БД
            $data = array(
                'Pass' => md5($password)
            );
            
            // Записываем в БД
            if (!$this->db->where('ID', $admin_id)->update($tblname, $data)) {
                throw new Exception('Ошибка записи данных в таблицу ' . $tblname);
            }
            
            // Сообщаем пользователю об успешной записи в БД
            $_SESSION['success_cpa'] = 'Вы успешно изменили пароль!';
        } catch (Exception $e) {
            $errors[] = 'Выброшено исключение : ' . $e->getMessage();
            $_SESSION['error_cpa'] = $errors;
        }
        header('Location:/cp/admins/');
        exit();
    }
    
    /**
     * Удалить пользователя Админки
     */
    public function delete_admin($id = 0)
    {
        // Стандартная инициализация
        $main_name = 'admins';
        $tblname = 'admins';
        
        // Приводим данные к типу int
        $id = (int)$id;
        
        // Проверяем, если данный пользователь есть в БД
        $admin = $this->db->where('ID', $id)->get($tblname)->row();
        if (empty($admin) || $admin->AdminType == 1) {
            header("HTTP/1.0 404 Not Found");
            show_404();
            exit();
        }
        
        // Исполняющий скрипт
        try {
            
            // Удаляем запись в БД
            if (!$this->db->delete($tblname, array('ID' => $id))) {
                throw new Exception('Ошибка удаления данных из таблицы ' . $tblname);
            }
            
            // Сообщаем пользователю об успешном удалении из БД
            $_SESSION['success'] = 'Вы успешно удалили админа!';
        } catch (Exception $e) {
            $errors[] = 'Выброшено исключение :' . $e->getMessage();
            $_SESSION['error'] = $errors;
        }
        header('Location: /cp/' . $main_name . '/');
        exit();
    }

    function status()  {
        $uri3 = $this->uri->segment(3);
        $uri4 = $this->uri->segment(4);
        $this->db->where('ID', $uri3)->update(
            'SiteUser',
            array('status' => $uri4)
        );
    }

    function generatepass()  {
        $uri3 = $this->uri->segment(3);
        $uri4 = $this->uri->segment(4);
        $this->db->where('ID', $uri4)->update(
            'SiteUser',
            array('password' => $uri3)
        );
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function logs() {
        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/logs')
        );
    }


    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function districts()
    {
        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/districts')
        );
    }

    function del_districts()
    {
        $uri3 = $this->uri->segment(3);
        if (!empty($uri3)) {
            $this->db->where('ID', $uri3)->delete('Districts');
        }
        header("Location: /cp/districts/");
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function city()
    {
        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/city')
        );
    }

    function del_city(){
        $uri3 = $this->uri->segment(3);
        if (!empty($uri3)) {
            $this->db->where('ID', $uri3)->delete('City');
        }
        header("Location: /cp/city/");
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function catalog() {
        $config['upload_path']   = realpath("public/catalog");
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['encrypt_name']  = 'TRUE';
        $this->load->library('upload', $config);

        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/catalog')
        );
    }

    function del_catalog() {
        $uri3 = $this->uri->segment(3);
        if (!empty($uri3)) {
            $this->db->where('ID', $uri3)->delete('Catalog');
        }
        header("Location: /cp/catalog/");
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function category() {
        $config['upload_path']   = realpath("public/category");
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['encrypt_name']  = true;
        $this->load->library('upload', $config);

        $this->load->view(
            'layouts/cp_layout',
            array('inner_view' => 'cp/category')
        );
    }

    function del_category(){
        $uri3 = $this->uri->segment(3);
        if (!empty($uri3)) {
            $this->deleteImageBeforeDelRec('Category');
            $this->db->where('ID', $uri3)->delete('Category');
        }
        header("Location: /cp/category/");
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    function deleteImageBeforeDelRec($table) {
        $uri3 = $this->uri->segment(3);
        $uri4 = $this->uri->segment(4);
        if ($table == 'MenuBlocks') {
            $path = $this->db->where('ID', $uri4)->select('Image')->get($table)
                             ->row()->Image;
        } else {
            $path = $this->db->where('ID', $uri3)->select('Image')->get($table)
                             ->row()->Image;
        }
        $folder = strtolower($table);
        unlink('public/'.$folder.'/'.$path);
    }

    function importCategory() {
        
        echo "<form action='' method='POST' enctype='multipart/form-data'>
        <input type='file' name='userFile'><br>
        <input type='submit' name='upload_btn' value='upload'>
        </form>"; 

        $upload_folder = 'public/xml/';
        $name = 'Structura_categoriilor';
        if ($_SERVER['REQUEST_METHOD']=='POST') {
        
            if ( isset($_FILES)) {
                $this->cleanDir(realpath($upload_folder));

                $info = pathinfo($_FILES['userFile']['name']);
                $ext = $info['extension'];
                $newname = $name.".".$ext; 

                $target = $upload_folder.$newname;
                move_uploaded_file( $_FILES['userFile']['tmp_name'], $target);

                $content = file_get_contents($upload_folder.$newname);

                $x = new SimpleXmlElement($content);
                $y = json_encode($x);
                $z = json_decode($y, true);
                foreach ($z['Categoria'] as $item) {
                    $text = preg_replace(
                        '~[^\pL\d]+~u',
                        '-',
                        $item['@attributes']['Denumirea_ro']
                    );
                    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
                    $text = preg_replace('~[^-\w]+~', '', $text);
                    $text = trim($text, '-');
                    $text = preg_replace('~-+~', '-', $text);
                    $text = strtolower($text);

                    if (empty($text)) {
                        $text = 'n-a';
                    } else {
                        $int  = (int)$item['@attributes']['Cod'];
                        $text = $int.'-'.$text;
                    }
                    
                    $feeds[] = array(
                        'TitleRO'   => $item['@attributes']['Denumirea_ro'],
                        'TitleRU'   => (isset($item['@attributes']['Denumirea_ru'])) ? $item['@attributes']['Denumirea_ru'] : $item['@attributes']['Denumirea_ro'],
                        'TitleEN'   => (isset($item['@attributes']['Denumirea_en'])) ? $item['@attributes']['Denumirea_en'] : $item['@attributes']['Denumirea_ro'],
                        'UriName'   => $text,
                        'Cod'       => $item['@attributes']['Cod'],
                        'ParentCod' => $item['@attributes']['ParentCod'],
                    );
                }

                $this->db->truncate('Catalog');

                $this->db->insert_batch('Catalog', $feeds);

                echo $msg = 'Categorii incarcate/modificate : '.count($feeds);

                $this->updateCategory();
                $this->updateBrendList();

            } else {
                echo $msg = 'No files';
            }
        } else {
            echo $msg = 'No POST data';
        }
        
        $data = array(
            'Method' => $name,
            'Msg' => $msg
        );

        $this->setLog($data);

        exit();
    }

    function importProducts() {

        set_time_limit(300);

        echo "<form action='' method='POST' enctype='multipart/form-data'>
        <input type='file' name='userFile'><br>
        <input type='submit' name='upload_btn' value='upload'>
        </form>"; 

        $upload_folder = 'public/xml/';
        $name = 'Marfuri'.time();
        if ($_SERVER['REQUEST_METHOD']=='POST') {
        
            if ( isset($_FILES)) {

              /*   header("Content-Type: text/XML; charset=utf-8");

                $headers = apache_request_headers();

                foreach ($headers as $header => $value) {
                    echo "$header: $value <br />\n";
                } */

                //$this->cleanDir(realpath($upload_folder));

                $info = pathinfo($_FILES['userFile']['name']);
                $ext = $info['extension'];
                $newname = $name.".".$ext; 

                $target = $upload_folder.$newname;

                $data = file_get_contents($_FILES['userFile']['tmp_name']);
                $encoding = mb_detect_encoding($data);
                //$data = utf8_encode($data);
                
            /*     dump($encoding, true);
            
                dump($data, false);

                //$file = iconv('iso-8859-1', 'utf-8' , $_FILES['userFile']['tmp_name']);
                //$file = iconv('ASCII', 'UTF-8//IGNORE', $_FILES['userFile']['tmp_name']);

                if($encoding != "UTF-8"){
                    $data = mb_convert_encoding($data, "UTF-8", $encoding); 
                }

                dump($data, false);
                die(); */

                //$config['upload_path'] = realpath("public/xml");
                //$config['allowed_types'] = 'xml|XML';
                //$config['encrypt_name'] = FALSE;

                //$this->load->library('upload', $config);

                //$this->upload->do_upload('userFile');

                move_uploaded_file( $_FILES['userFile']['tmp_name'] , $target);

                $this->db->select('Cod');
                $query = $this->db->get('Products');

                $cods = [];
                foreach ($query->result() as $row) {
                    $cods[] = $row->Cod;
                }

                $xml = new XMLReader;
                
                if($xml->open($target)) {

                    if($xml->read()){
                        
                        $countImportedProducts = 0;

                        while ($xml->read()) {
                            if ($xml->nodeType == XMLReader::ELEMENT) {
                                if ($xml->name == 'Marfa') {
                                    $text = preg_replace(
                                        '~[^\pL\d]+~u',
                                        '-',
                                        $xml->getAttribute('Denumire_ro')
                                    );
                                    $text = preg_replace('~[^-\w]+~', '', $text);
                                    $text = trim($text, '-');
                                    $text = preg_replace('~-+~', '-', $text);
                                    $text = strtolower($text);

                                    if (empty($text)) {
                                        $text = (int)$xml->getAttribute('Cod').'-n-a';
                                    } else {
                                        $int  = (int)$xml->getAttribute('Cod');
                                        $text = $int.'-'.$text;
                                    }

                                    $cod  = (int)$xml->getAttribute('Cod');

                                    if ($xml->getAttribute('Unitate_masura') == 'Buc' &&
                                    $xml->getAttribute('Unitate_Min_Vinzare')=='Set') {
                                        if( $xml->getAttribute('Cantitate_Set') != '') {
                                            $set = (int)$xml->getAttribute('Cantitate_Set');
                                        } else {
                                            $set = 1;
                                        }
                                    } else {
                                        $set = 1;
                                    }

                                    if($set==1) {
                                        if ($xml->getAttribute('Unitate_masura') == 'Buc' &&
                                            $xml->getAttribute('Unitate_Min_Vinzare')=='Cutii') {
                                            if( $xml->getAttribute('Cantitate_Cutie') != '') {
                                                $set = (int)$xml->getAttribute('Cantitate_Cutie');
                                            } else {
                                                $set = 1;
                                            }
                                        } else {
                                            $set = 1;
                                        }
                                    }
                                    
                                    if($xml->getAttribute('Denumire_ru') !='') {
                                        $den_ru = $xml->getAttribute('Denumire_ru');
                                    } else {
                                        $den_ru = $xml->getAttribute('Denumire_ro');
                                    }

                                    if($xml->getAttribute('Denumire_en') !='') {
                                        $den_en = $xml->getAttribute('Denumire_en');
                                    } else {
                                        $den_en = $xml->getAttribute('Denumire_ro');
                                    }

                                    $feeds = [
                                        'TitleRO'       => $xml->getAttribute('Denumire_ro'),
                                        'TitleRU'       => $den_ru,
                                        'TitleEN'       => $den_en,
                                        'UriName'       => $text,
                                        'Cod'           => (int)$xml->getAttribute('Cod'),
                                        'ParentCod'     => $xml->getAttribute('Categoria'),
                                        'Brand'         => $xml->getAttribute('Brend'),
                                        'Model'         => $xml->getAttribute('Model'),
                                        'New'           => $xml->getAttribute('MarfaNoua'),
                                        'UnitMeas'      => $xml->getAttribute('Unitate_masura'),
                                        'QtySet'        => $xml->getAttribute('Cantitate_Set'),
                                        'QtyBox'        => $xml->getAttribute('Cantitate_Cutie'),
                                        'UnitSales'     => $xml->getAttribute('Unitate_Min_Vinzare'),
                                        'DescriptionRO' => $xml->getAttribute('Descriere_ro'),
                                        'DescriptionRU' => $xml->getAttribute('Descriere_ru'),
                                        'DescriptionEN' => $xml->getAttribute('Descriere_en'),
                                        'Stoc'          => $xml->getAttribute('StocReal'),
                                        'Price'           => $xml->getAttribute('Pret_Retail')*$set,
                                        'PriceAngro'      => $xml->getAttribute('Pret_Angro')*$set,
                                        'PricePromo'      => $xml->getAttribute('Pret_Promo_Retail')*$set,
                                        'PricePromoAngro' => $xml->getAttribute('Pret_Promo')*$set,
                                        'Promo' => 0,
                                    ];

                                    if ($feeds['Stoc'] == 'in stoc') {
                                        $feeds['Stoc'] = 1;
                                    } else {
                                        $feeds['Stoc'] = 0;
                                    }

                                    do {
                                        $xml->read();
                                    } while ($xml->name == '#text');

                                    $i = 1;
                                    if ($xml->nodeType == XMLReader::ELEMENT
                                        && $xml->name == 'Picture_List'
                                    ) {
                                        do {
                                            $xml->read();
                                            if ($xml->nodeType == XMLReader::ELEMENT) {
                                                $feeds['Image'.$i++] = $xml->getAttribute(
                                                    'Binary'
                                                );
                                            }
                                        } while ($xml->name != 'Picture_List' && $i < 4);
                                    }

                                        if ($feeds['PricePromo'] > 0) {
                                            $feeds['Promo']      = 1;
                                        }

                                        $countImportedProducts++;

                                        if (!in_array($cod, $cods)) {
                                            $this->db->insert('Products', $feeds);
                                        } else {
                                            $this->db->where('Cod', $cod );
                                            $this->db->update('Products', $feeds);
                                        }
                                    }
                                } elseif ($xml->nodeType == XMLReader::END_ELEMENT
                                        && $xml->name == 'Marfuri'
                                ) {
                                echo $msg = 'Produse incarcate/modificate : '.$countImportedProducts;
                            }
                        }

                        $this->updateCategory();
                        $this->updateBrendList();

                    } else {
                        echo $msg = 'Fisierul are formatul incorect';
                    }
                } else {
                    echo $msg = 'Eroare la deschiderea fisierului';
                }        
            } else {
                echo $msg = 'No files';
            }
        } else {
            echo $msg = 'No POST data';
        }

        $data = array(
            'Method' => $name,
            'Msg' => $msg
        );

        $this->setLog($data);

        exit();

    }

    public function updateCategory() {

        // punem la toate 0 

        $this->db->set('has_product', 0);
        $this->db->update('Catalog');

        $query = $this->db->select('distinct(ParentCod)')
        ->where('OnSite', 1)
        ->where('Price >', 0)
        ->where('TitleRO !=', '')
        ->get('Products')
        ->result_array();
        $data = $query;

        $idsCategory = array_map(function($item){
            return $item['ParentCod'];
        }, $data);

        $this->db->set('has_product', 1);
        $this->db->set('is_active', 1);
        $this->db->where_in('Cod', $idsCategory);
        $this->db->update('Catalog');

        // posibil sa fie primele 2 categorii fara produse si doar a 3 are 

        $query = $this->db->select('distinct(ParentCod)')
        ->where('has_product', 1)
        ->get('Catalog')
        ->result_array();
        $data = $query;

        $idsCategory = array_map(function($item){
            return $item['ParentCod'];
        }, $data);

        $this->db->set('is_active', 1);
        $this->db->where_in('Cod', $idsCategory);
        $this->db->update('Catalog');

        echo '<br/>';
        echo 'Categorii cu produse : '.count($idsCategory);
    }

    public function updateImageProducts(){

        set_time_limit(300);

        $xml = new XMLReader;
        $xml->open('public/xml/Marfuri.XML');

        $count = 0;

        $xml->read();
        while ($xml->read()) {
            if ($xml->nodeType == XMLReader::ELEMENT) {

                if ($xml->name == 'Marfa') {
                    $cod = (int)$xml->getAttribute('Cod');
                } elseif ($xml->name == 'Picture') {
                    $image = $xml->getAttribute('Binary');

                    if ($image > '') {
                        $count++;

                        $this->db->reconnect();
                        $this->db->set('Image', $image);

                        $this->db->where('Cod', $cod);
                        $this->db->update('Products');

                    }
                }

            } elseif ($xml->nodeType == XMLReader::END_ELEMENT
                        && $xml->name == 'Marfuri'
            ) {
                echo '<br>';
                echo 'end update image';
                echo '<br>';
                echo $count.' imagini';
            }
        }
    }

    function updateBrendList(){

        $this->db->select('Name');
        $query = $this->db->get('Brands');

        $brands = array();
        foreach ($query->result() as $row) {
            $brands[] = $row->Name;
        }

        $this->db->distinct();
        $this->db->select('Brand');
        $query = $this->db->get('Products')->result();

        $new_brands = array();
        foreach ($query as $item) {
            if (!in_array($item->Brand, $brands)) {
                $new_brands[] = array('Name' => $item->Brand);
            }
        }

        if (!empty($new_brands)) {
            $this->db->insert_batch('Brands', $new_brands);

            echo '<br/>';
            echo $msg = 'Sau adaugat '.count($new_brands).' brenduri noi!';

            
            $data = array(
                'Method' => 'update brends',
                'Msg' => $msg
            );

            $this->setLog($data);
            
        }
    }

    function importDealers() {

        ini_set('max_execution_time', 6000);

        echo "<form action='' accept-charset='utf-8' method='POST' enctype='multipart/form-data'>
        <input type='file' name='userFile'><br>
        <input type='submit' name='upload_btn' value='upload'>
        </form>"; 

        $upload_folder = 'public/xml/';
        $name = 'Clients';
        if ($_SERVER['REQUEST_METHOD']=='POST') {
    
            if ( isset($_FILES)) {
                $this->cleanDir(realpath($upload_folder));

                $query = $this->db->select('Cod')
                ->where('Cod >', 0)
                ->where('status', 2)
                ->get('SiteUser')
                ->result_array();
                $data = $query;

                $dealerCods = array_map(function($item){
                    return $item['Cod'];
                }, $data);

                $query = $this->db->select('DealerID')
                ->distinct()
                ->get('Address')
                ->result_array();
                $data = $query;

                $dealerIds = array_map(function($item){
                    return $item['ID'];
                }, $data);

                $count=0;

                $info = pathinfo($_FILES['userFile']['name']);
                $ext = $info['extension'];
                $newname = $name.".".$ext; 

                $target = $upload_folder.$newname;
                move_uploaded_file( $_FILES['userFile']['tmp_name'], $target);

                $content = file_get_contents($upload_folder.$newname);

                $x = new SimpleXmlElement($content);
                $y = json_encode($x);
                $z = json_decode($y, true);
                foreach ($z['Client'] as $item) {

                    if ($item['@attributes']['Telefon'] != '' &&  strlen($item['@attributes']['Telefon']) > 7 ) {
                        $arr = str_split($item['@attributes']['Telefon']);
                        
                        if ($arr[0] == 0) {
                            $phone = '+373 ('.$arr[1].$arr[2].') '.$arr[3].$arr[4]
                                    .$arr[5].'-'.$arr[6].$arr[7];
                                    if(isset($arr[8])) {
                                        $phone = $phone.$arr[8];
                                    }
                        } else {
                            $phone = '+373 ('.$arr[0].$arr[1].') '.$arr[2].$arr[3]
                                    .$arr[4].'-'.$arr[5].$arr[6].$arr[7];
                        }
                    } else {
                        $phone = '';
                    }

                    $data = array(
                        'Name'         => $item['@attributes']['PersoanaContact'],
                        'companyname'  => $item['@attributes']['Denumirea'],
                        'Cod'          => (int)$item['@attributes']['Cod'],
                        'phone'        => $phone,
                        'email'        => $item['@attributes']['Email'],
                        'legaladdress' => '',
                        'status'       => 2,
                        'type'         => 2,
                        'Discount'     => $item['@attributes']['Reducere'],
                        'idnp'         => $item['@attributes']['IDNO'],
                    );

                    $count++;
                    
                    if(in_array((int)$item['@attributes']['Cod'], $dealerCods)){
                        $this->db->where('Cod', (int)$item['@attributes']['Cod']);
                        $this->db->update('SiteUser', $data);  
                        
                        $user_id = $this->db->select('*')->where('Cod', (int)$item['@attributes']['Cod'])->get('SiteUser')->row()->ID;

                    } else {
                        $this->db->insert('SiteUser', $data);
                        $user_id = $this->db->insert_id(); 
                    }
                    
                    $data=array(
                        'DealerID' => $user_id,
                        'Name' => $item['@attributes']['Adresa']
                    );

                    //bodarev

                    $this->db->where('DealerID', $user_id);
                    $this->db->delete('Address');
                    $this->db->insert('Address', $data);
/* 
                    if(in_array($user_id, $dealerIds)){
                        $this->db->where('DealerID', $user_id);
                        $this->db->update('Address', $data);  
                    } else {
                        $this->db->insert('Address', $data);
                    } */
                }

                echo $msg = 'Sau modificat datele pentru '.$count.' dealeri';

                /* if(isset($feeds)){
                    $this->db->insert_batch('SiteUser', $feeds);
                    echo $msg += '<br/>';    
                    echo $msg += 'Sau incarcat datele pentru '.count($feeds).' dealeri';
                } */

            } else {
                echo $msg = 'No files';
            }
        } else {
            echo $msg = 'No POST data';
        }

        $data = array(
            'Method' => $name,
            'Msg' => $msg
        );

        $this->setLog($data);
        
        exit();
    }

    function importCursValutar() {

            echo "<form action=''  accept-charset='utf-8' method='POST' enctype='multipart/form-data'>
        <input type='file' name='userFile'><br>
        <input type='submit' name='upload_btn' value='upload'>
        </form>"; 

        $upload_folder = 'public/xml/';
        $name = 'CursValutar';
        if ($_SERVER['REQUEST_METHOD']=='POST') {
    
            if ( isset($_FILES)) {
                $this->cleanDir(realpath($upload_folder));

                $info = pathinfo($_FILES['userFile']['name']);
                $ext = $info['extension'];
                $newname = $name.".".$ext; 

                $target = $upload_folder.$newname;
                move_uploaded_file( $_FILES['userFile']['tmp_name'], $target);

                $content = file_get_contents($upload_folder.$newname);

                $x = new SimpleXmlElement($content);
                $y = json_encode($x);
                $z = json_decode($y, true);
                foreach ($z['Curs'] as $item) {

                    $pizza = explode('.', $item['Data']); 
                    $DATE = $pizza[2]."/".$pizza[1]."/".$pizza[0]; 

                    if($item['Valuta']=='USD'){
                        $USD = $item['Valoare'];
                        $USD = floatval(preg_replace('/[^\d.]/', '.', $USD));
                    }
                }

                $feeds = array(
                    'Date' => $DATE,
                    'USD'  => $USD,
                    'EUR'  => 0,
                    'RON'  => 0,
                );

                $query = $this->db->select('*')->where('Date', $DATE)->get('CursValutar')->result();

                if(empty($query)) {
                    $this->db->insert('CursValutar', $feeds);
                } else {
                    $this->db->where('Date', $DATE);
                    $this->db->update('CursValutar', $feeds);
                }

                echo $msg = 'Sa incarcat cursul pentru data '.$DATE;
            } else {
                echo $msg = 'No files';
            }
        } else {
            echo $msg = 'No POST data';
        }

        $data = array(
            'Method' => $name,
            'Msg' => $msg
        );

        $this->setLog($data);
        
        exit();
    }

    function setExportedOrders() {

            echo "<form action=''  accept-charset='utf-8' method='POST' enctype='multipart/form-data'>
        <input type='file' name='userFile'><br>
        <input type='submit' name='upload_btn' value='upload'>
        </form>"; 

        $upload_folder = 'public/xml/';
        $name = 'orders';
        if ($_SERVER['REQUEST_METHOD']=='POST') {
    
            if ( isset($_FILES)) {
                $this->cleanDir(realpath($upload_folder));

                $info = pathinfo($_FILES['userFile']['name']);
                $ext = $info['extension'];
                $newname = $name.".".$ext; 

                $target = $upload_folder.$newname;
                move_uploaded_file( $_FILES['userFile']['tmp_name'], $target);

                $content = file_get_contents($upload_folder.$newname);

                $x = new SimpleXmlElement($content);
                $y = json_encode($x);
                $z = json_decode($y, true);

                foreach ($z['Order'] as $item) {
                    if(isset($item['@attributes'])) {
                        $OrderNrs[] = $item['@attributes']['order_number'];
                    } else {
                        $OrderNrs[] = $item['order_number'];
                    }
                }

                $this->db->where_in('OrderNr', $OrderNrs);
                $this->db->update('Orders', array('isExported' => 1, 'status' => 2) );  

                echo 'Sau exportat cu succes '.count($OrderNrs).' comenzi!';
            } else {
                echo "No files";
            }
        } else {
            echo 'No POST data';
        }
        exit();
    }

    function export_xml() {

        $filepath = 'public/export/orders.xml';

        $query = $this->db->select('Orders.*, Districts.NameRO as DistrictName, SiteUser.Cod')
        ->join('Districts', 'Districts.ID = Orders.DistrictID', 'left')
        ->join('SiteUser', 'SiteUser.ID = Orders.UserID', 'left')
        ->where('Orders.isExported', 0)
        ->get('Orders')
        ->result();

        $xml=new DOMDocument('1.0');
        $xml->formatOutput=true;

        $orders=$xml->createElement("orders");
        $xml->appendChild($orders);

        $count=0;

        foreach($query as $item) {

        $order=$xml->createElement('order');
        
        $order->setAttribute('Data', $item->Date);
        $orders->appendChild($order);

        $order->setAttribute('order_number', $item->OrderNr);
        $orders->appendChild($order);

        $order->setAttribute('Denumire', $item->Name);
        $orders->appendChild($order);

        $order->setAttribute('Cod', $item->Cod);
        $orders->appendChild($order);

        $order->setAttribute('Comment', $item->comment);
        $orders->appendChild($order);

        if($item->userType==1) {
            $order->setAttribute('type', 0);
            $orders->appendChild($order);
        } else {
            $order->setAttribute('type', 1);
            $orders->appendChild($order);  
        }

        $order->setAttribute('total', $item->total);
        $orders->appendChild($order);

        $products = $this->db
            ->select('OrderItems.ItemCod, OrderItems.ItemPrice, OrderItems.ItemCount, Products.*')
            ->join('Products','Products.Cod = OrderItems.ItemCod')
            ->where('OrderID', $item->ID)
            ->get('OrderItems')->result();

        foreach ($products as $product) {

            $produs=$xml->createElement("Produs");

            $produs->setAttribute('Model', $product->Model);
            $order->appendChild($produs);

            $produs->setAttribute('Denumire', $product->TitleRO);
            $order->appendChild($produs);

            $produs->setAttribute('Cantitatea', $product->ItemCount);
            $order->appendChild($produs);

            $produs->setAttribute('Unitatea_de_masura', $product->UnitSales);
            $order->appendChild($produs);

        }    

        $count++;

        }

        //header('Content-Disposition: attachment=false;filename=myFile.xml');
        header('Content-Type: text/xml; charset=utf-8');

        echo $xml->saveXML();

        /* $xml->save($filepath) or die();

        if(file_exists($filepath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            flush(); // Flush system output buffer
            readfile($filepath);
            exit;
        } */
        exit();
    }

    function setExistProducts() {

        echo "<form action=''  accept-charset='utf-8' method='POST' enctype='multipart/form-data'>
        <input type='file' name='userFile'><br>
        <input type='submit' name='upload_btn' value='upload'>
        </form>"; 

        $upload_folder = 'public/xml/';
        $name = 'MarfuriExpuse';
        if ($_SERVER['REQUEST_METHOD']=='POST') {

            if ( isset($_FILES)) {
                $this->cleanDir(realpath($upload_folder));

                $info = pathinfo($_FILES['userFile']['name']);
                $ext = $info['extension'];
                $newname = $name.".".$ext; 

                $target = $upload_folder.$newname;
                move_uploaded_file( $_FILES['userFile']['tmp_name'], $target);

                $content = file_get_contents($upload_folder.$newname);

                $x = new SimpleXmlElement($content);
                $y = json_encode($x);
                $z = json_decode($y, true);

                $ExistCodes = array();
                $ForDeletedCodes = array();
                foreach ($z['Marfa'] as $item) {
                    $ExistCodes[] = (int)$item['@attributes']['Cod'];
                } 

                $SiteCodes = $this->db->select('Cod')->get('Products')->result_array();

                foreach ($SiteCodes as $item) {
                    if(!in_array($item['Cod'], $ExistCodes)) {
                        $ForDeletedCodes[] = $item['Cod'];
                    }
                }

                $count = count($ForDeletedCodes);
                if(!empty($ForDeletedCodes)) {
                    $this->db->group_start();
                    $chunk = array_chunk($ForDeletedCodes,25);
                    foreach($chunk as $ForDeletedCodes)
                    {
                        $this->db->or_where_in('Cod', $ForDeletedCodes);
                    }
                    $this->db->group_end();
                    $this->db->delete('Products');

                    echo $count.' produse au fost sterse de pe site!';

                    $this->updateCategory();
                    $this->updateBrendList();
                } else {
                    echo 'Nu sunt produse de sters!';
                }

            } else {
                echo "No files";
            }
        } else {
            echo 'No POST data';
        }
        exit();
    }

    function cleanDir($dir) {
        $files = glob($dir."/*");
        $c = count($files);
        if (count($files) > 0) {
            foreach ($files as $file) {      
                if (file_exists($file)) {
                unlink($file);
                }   
            }
        }
    }
    
    function setLog($data) {

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $data['Date'] = date('Y-m-d H:i:s');
        $data['ip'] = $ip;

        $this->db->insert('Log', $data);
    }

    public function changeStatus() {

        $status = $this->input->post("status");
        $id     = $this->input->post("id");

        if ($status != 0) {
            $date = date('Y-m-d H:i:s');
        } else {
            $date = '0000-00-00 00:00:00';
        }

        $data = array(
            'status'    => $status,
            'FinalDate' => $date,
        );

        $this->db->where('ID', $id);
        $this->db->update('Orders', $data);

        echo json_encode(
            array(
                'error' => false,
            )
        );
        exit();
    }

        /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
    
        public function deleteRow() {
            $id      = (int)$this->input->post('id');
            $table   = $this->input->post('table');
    
            $this->db->where('ID', $id);
            $this->db->delete($table);
         
            if ($this->db->affected_rows() != 1) {
                $err = true;
                $msg = $this->msg;
            } else {
                $err = false;
                $msg = 'Удалено успешно';
            }
    
            $this->alert($msg, $err);
        }
}
