<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

	function __construct() {

		parent::__construct();
        $this->load->model('get_model');
        $this->load->model('update_model');
        $this->load->model("insert_model");

        $this->load->library('email');

        $this->load->library('session');

		if (empty($_SESSION['lang'])) {
			$_SESSION['lang']='ro';
		}
		$uri1=$this->uri->segment(1);
		if ($uri1=='ru') $_SESSION['lang']='ru';
		if ($uri1=='ro') $_SESSION['lang']='ro';
        if ($uri1=='en') $_SESSION['lang']='en';

        //$clang=strtoupper($_SESSION['lang']);
        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        define ( 'TEMPLATE', BASEPATH . '../application/views/layouts/template.html' );
		
		$q=$this->db->query("select $clang as value, ConstantName from LangConstants")->result_array();
		foreach($q as $row) {
			define($row['ConstantName'],$row['value']);
        }
        
        // definim variabila produse pe pagina
        define('NR_PER_PAGE', 8);
        define('NR_ON_STOC', 999999);
        define('LCLANG', $lclang);
        define('CLANG', $clang);
	}
	
	public function index() {
        //$products = $this->get_model->getRandProducts( '' , 4);
        //$products2 = $this->get_model->getRandProducts( '' , 8);
        $categorii = $this->get_model->getCategoryForMenu(0);
		$data = array(
			'tab_title' => MAINTITLE,
			'keywords_for_layout' => MKW,
			'description_for_layout' => MDESC,
            'page_content' => 'pages/main',
            //'products' => $products,
            //'products2' => $products2,
            'categorii' => $categorii
		);
		
		$this->load->view('layouts/main_layout',$data);
	}
	
	public function topmenu() {
		$uri3 = addslashes($this->uri->segment(3));

		if(!empty($uri3)) {

            $page = $this->get_model->getPage($uri3);
            $categorii = $this->get_model->getCategoryForMenu(0);

            if(empty($page)) {
                $this->error_404();
            } else {
                $blocks = $this->get_model->getBlocks($page['ID']);
                $data = array(
                    'tab_title' => $page['TabTitle'],
                    'keywords_for_layout' => $page['Keywords'],
                    'description_for_layout' => $page['Description'],
                    'main_image' => $page['Image'],
                    'blocks' => $blocks,
                    'categorii' => $categorii
                );
                $this->load->view('layouts/txt_layout',$data);
            }
        } else {
            $this->error_404();
        }
	}

	public function error_404() {
        $categorii = $this->get_model->getCategoryForMenu(0);
        $data = array(
            'tab_title' => NFTITLE,
            'keywords_for_layout' => NFKW,
            'description_for_layout' => NFDESC,
            'categorii' => $categorii
        );
        $data['page_content'] = 'pages/error_404';
        $this->load->view('layouts/main_layout',$data);
    }

    public function registration() {

        $page = $this->get_model->getPage('registration');
        $categorii = $this->get_model->getCategoryForMenu(0);
        $districts = $this->get_model->getDistricts();

        if(empty($page)) {
            $this->error_404();
        }

        $data = array(
            'tab_title' => $page['TabTitle'],
            'keywords_for_layout' => $page['Keywords'],
            'description_for_layout' => $page['Description'],
            'main_image' => $page['Image'],
            'page_content' => 'pages/registration',
            'districts' => $districts,
            'categorii' => $categorii
        );
        $this->load->view('layouts/main_layout',$data);
    }

    public function signup_dealers() {

        $page = $this->get_model->getPage('signup-dealers');
        $districts = $this->get_model->getDistricts();

        if(empty($page)) {
            $this->error_404();
        }

        $data = array(
            'tab_title' => $page['TabTitle'],
            'keywords_for_layout' => $page['Keywords'],
            'description_for_layout' => $page['Description'],
            'main_image' => $page['Image'],
            'page_content' => 'pages/signup_dealers',
            'districts' => $districts
        );
        $this->load->view('layouts/main_layout',$data);
    }

    public function login() {

        $page = $this->get_model->getPage('login');
        $categorii = $this->get_model->getCategoryForMenu(0);

        if(empty($page)) {
            $this->error_404();
        }

        $data = array(
            'tab_title' => $page['TabTitle'],
            'keywords_for_layout' => $page['Keywords'],
            'description_for_layout' => $page['Description'],
            'main_image' => $page['Image'],
            'page_content' => 'pages/login',
            'categorii' => $categorii
        );
        $this->load->view('layouts/main_layout',$data);
    }

    public function restorepass() {

        $page = $this->get_model->getPage('restorepass');
        $categorii = $this->get_model->getCategoryForMenu(0);

        if(empty($page)) {
            $this->error_404();
        }

        $data = array(
            'tab_title' => $page['TabTitle'],
            'keywords_for_layout' => $page['Keywords'],
            'description_for_layout' => $page['Description'],
            'main_image' => $page['Image'],
            'page_content' => 'pages/restorepass',
            'categorii' => $categorii
        );
        $this->load->view('layouts/main_layout',$data);
    }

    public function profile($type='') {

	    if($this->session->userdata('logged_in')) {

            $page = $this->get_model->getPage('profile');
            $categorii = $this->get_model->getCategoryForMenu(0);
            $user = $this->get_model->getUserDate();
            $districts = $this->get_model->getDistricts();
            $dealers = $this->get_model->getSiteUsers();

            if (empty($page)) {
                $this->error_404();
            }

            $data = array(
                'tab_title' => $page['TabTitle'],
                'keywords_for_layout' => $page['Keywords'],
                'description_for_layout' => $page['Description'],
                'main_image' => $page['Image'],
                'user' => $user,
                'districts' => $districts,
                'dealers' => $dealers,
                'categorii' => $categorii
            );

            if (!empty($type)) {
                $page_num = $type;
            } else {
                $page_num = addslashes($this->uri->segment(4));
            }
            // pentru agenti 
            if($this->session->userdata('type') == 3) {
                $page_num = 5;
            }
            switch ($page_num) {
                case '':
                case '1':
                    $dates = '';
                    if (isset($_GET['dates'])) {
                        $dates = $_GET['dates'];
                    }
                    $user_id = $_SESSION['user_id'];
                    $orders = $this->get_model->getOrdersByUser($user_id, $dates);

                    $data['page_content'] = 'pages/profile';
                    $data['orders'] = $orders;
                    $this->load->view('layouts/main_layout', $data);
                    break;
                case '2':
                    $products = $this->get_model->getViewsProducts();

                    $data['page_content'] = 'pages/profile2';
                    $data['products'] = $products;
                    $this->load->view('layouts/main_layout', $data);
                    break;
                case '3':
                    $data['page_content'] = 'pages/profile3';
                    $this->load->view('layouts/main_layout', $data);
                    break;
                case '4':
                    $data['page_content'] = 'pages/profile4';
                    $this->load->view('layouts/main_layout', $data);
                    break;
                case '5':
                    $IdArray = array();
                    $IdArray[0] = 0;
                    if (isset($_SESSION['cart']['items'])) { 
                        foreach ($_SESSION['cart']['items'] as $item) {
                            $IdArray[] = $item['cod'];
                        }
                    }
                    $productsoncart = $this->get_model->getCartProducts($IdArray);
                    $data['productsoncart'] = $productsoncart;
                    $data['page_content'] = 'pages/profile5';
                    $this->load->view('layouts/main_layout', $data);
                    break;
                default :
                    $this->error_404();
                    break;
            }
        } else {
            //$this->login();
            redirect(base_url(LCLANG."/pages/login"));
        }
    }

    public function logout() {
        unset($_SESSION['user_name']);
        unset($_SESSION['user_id']);
        unset($_SESSION['logged_in']);
        unset($_SESSION['type']);
        unset($_SESSION['district']);
        header("Location: /");
        exit();
    }

    public function alert($data) {
        $data['tab_title'] = MAINTITLE;
        $data['keywords_for_layout'] = MKW;
        $data['description_for_layout'] = MDESC;

        $this->load->view('layouts/alert_layout',$data);
    }

    public function contacts() {

        $page = $this->get_model->getPage('contacts');
        $categorii = $this->get_model->getCategoryForMenu(0);
        $branches = $this->get_model->getBranches();
        $agents = $this->get_model->getAgents();
        $districts = $this->get_model->getDistricts();
        $blocks = $this->get_model->getBlocks($page['ID']);

        if(empty($page)) {
            $this->error_404();
        }
        
        $data = array(
            'tab_title' => $page['TabTitle'],
            'keywords_for_layout' => $page['Keywords'],
            'description_for_layout' => $page['Description'],
            'main_image' => $page['Image'],
            'page_content' => 'pages/contacts',
            'branches' => $branches,
            'agents' => $agents,
            'blocks' => $blocks,
            'districts' => $districts,
            'categorii' => $categorii
        );
        $this->load->view('layouts/main_layout',$data);
    }

    public function interview() {

        $page = $this->get_model->getPage('interview');

        if(empty($page)) {
            $this->error_404();
        } else {

            $categorii = $this->get_model->getCategoryForMenu(0);

            $data = array(
                'tab_title' => $page['TabTitle'],
                'keywords_for_layout' => $page['Keywords'],
                'description_for_layout' => $page['Description'],
                'main_image' => $page['Image'],
                'categorii' => $categorii
            );

            $Interviews = $this->get_model->getInterviews();

            $idsInterview = array_map(function($item){
                return $item->ID;
            }, $Interviews);

            $curent_item ='';
            foreach($idsInterview as $item) {
                $result = $this->get_model->getIp( getRealIpAddr(), $item);

                if(empty($result) && $curent_item=='') {
                    $curent_item = $item;
                }
            }

            if(!empty($curent_item)){
                $Interview = $this->get_model->getInterview($curent_item);
            }

            if(empty($curent_item)) {
                $data['message'] = TEXT_ALREADYVOTED;
                $data['page_content'] = 'pages/displaymsg';
            } else {
                
                $Questions = $this->get_model->getQuestions($Interview->ID);

                $idsQuestion = array_map(function($item){
                    return $item->ID;
                }, $Questions);

                $Answers = $this->get_model->getAnswers($idsQuestion);
            
                foreach ($Questions as $question) {
                    $total = 0;
                    foreach ($Answers as $answer) {
                        if ($question->ID == $answer->QuestionID) {
                            $question->childs[] = (object) array('id' => $answer->ID , 'name' => $answer->Ans, 'count' => $answer->Count);
                            $total += $answer->Count;
                        }
                    }
                    $question->total = $total;
                }

                $data['interview'] = $Interview;
                $data['questions'] = $Questions;
                $data['page_content'] = 'pages/interview';
            }
            $this->load->view('layouts/main_layout',$data);
        }    
    }

    public function branches() {

        $page = $this->get_model->getPage('branches');
        $categorii = $this->get_model->getCategoryForMenu(0);
        $branches = $this->get_model->getBranches();
        $blocks = $this->get_model->getBlocks($page['ID']);

        if(empty($page)) {
            $this->error_404();
        }
        
        $data = array(
            'tab_title' => $page['TabTitle'],
            'page_title' => $page['PageTitle'],
            'page_text' => $page['PageText'],
            'keywords_for_layout' => $page['Keywords'],
            'description_for_layout' => $page['Description'],
            'main_image' => $page['Image'],
            'page_content' => 'pages/branches',
            'branches' => $branches,
            'blocks' => $blocks,
            'categorii' => $categorii
        );
        $this->load->view('layouts/main_layout',$data);
    }

    public function brands() {

        $page = $this->get_model->getPage('brands');
        $categorii = $this->get_model->getCategoryForMenu(0);
        $blocks = $this->get_model->getBlocks($page['ID']);

        if(empty($page)) {
            $this->error_404();
        }
        
        $data = array(
            'tab_title' => $page['TabTitle'],
            'page_title' => $page['PageTitle'],
            'page_text' => $page['PageText'],
            'keywords_for_layout' => $page['Keywords'],
            'description_for_layout' => $page['Description'],
            'main_image' => $page['Image'],
            'page_content' => 'pages/brands',
            'blocks' => $blocks,
            'categorii' => $categorii
        );
        $this->load->view('layouts/main_layout',$data);
    }

    public function catalog() {

        $uri4=$this->uri->segment(4);

        if(empty($uri4)) {
        
            $page = $this->get_model->getPage('catalog');
            $categorii = $this->get_model->getCategoryForMenu(0);

            if(empty($page)) {
                $this->error_404();
            }

            $data = array(
                'tab_title' => $page['TabTitle'],
                'keywords_for_layout' => $page['Keywords'],
                'description_for_layout' => $page['Description'],
                'main_image' => $page['Image'],
                'page_content' => 'pages/catalog',
                'categorii' => $categorii,
                'isCat' => 0
            );
            $this->load->view('layouts/main_layout',$data);
        } else {

            $cost = 0;
            $cat = 0;
            $pr = 0;
            $brand = '';
            $sortby = 1;
            $nrpage = 1;

            if($_SERVER['REQUEST_METHOD'] === 'GET') {
            
                if (isset($_GET['cost'])) {
                    $cost = $_GET['cost'];
                }
                if (isset($_GET['pr'])) {
                    $pr = $_GET['pr'];
                }
                if (isset($_GET['brand'])) {
                    $brand = $_GET['brand'];
                }
                if (isset($_GET['sortby'])) {
                    $sortby = $_GET['sortby'];
                }
                if (isset($_GET['page'])) {
                    $nrpage = $_GET['page'];
                }
            }

            $page = $this->get_model->getCategoryData($uri4);
            $categorii = $this->get_model->getCategoryForMenu(0);

            if(empty($page)) {
                $this->error_404();
            }

            // verificam daca e categorie parinte sau are produse 

            if($page['has_product']>0) {

                $brands = $this->get_model->getBrands($page['Cod'], '', $cost, $pr, $brand );
                $products = $this->get_model->getProducts( $page['Cod'], '', $cost, $cat, $pr, $brand, $sortby, $nrpage, 0 );
                $count = $this->get_model->getCountProducts($page['Cod'], '', $cost, $cat, $pr, $brand, 0 );

                $data = array(
                    'tab_title' => $page['CategoryTitle'],
                    'title' => $page['CategoryTitle'],
                    'keywords_for_layout' => $page['Keywords'],
                    'description_for_layout' => $page['Description'],
                    'main_image' => $page['Image'],
                    'page_content' => 'pages/catalog2',
                    'brands' => $brands,
                    'products' => $products,
                    'count' => $count,
                    'categorii' => $categorii
                );

                $this->load->view('layouts/main_layout',$data);

            } else {

                $codCategory = $page['Cod'];

                // scoatem subcategorile doar pentru o categorie 

                $page = $this->get_model->getPage('catalog');
                $categorii = $this->get_model->getCategoryForMenu(0);

                if(empty($page)) {
                    $this->error_404();
                }

                $data = array(
                    'tab_title' => $page['TabTitle'],
                    'keywords_for_layout' => $page['Keywords'],
                    'description_for_layout' => $page['Description'],
                    'main_image' => $page['Image'],
                    'page_content' => 'pages/catalog',
                    'categorii' => $categorii,
                    'isCat' => $codCategory
                );
                $this->load->view('layouts/main_layout',$data);
            }
        }
    }

    public function search() {
       
        if($_SERVER['REQUEST_METHOD'] === 'GET') {
            
            $match = '';
            $cost = 0;
            $cat = 0;
            $pr = 0;
            $brand = '';
            $sortby = 1;
            $nrpage = 1;

            if (isset($_GET['match'])) {
                $match = $_GET['match'];
            }
            if (isset($_GET['cost'])) {
                $cost = $_GET['cost'];
            }
            if (isset($_GET['pr'])) {
                $pr = $_GET['pr'];
            }
            if (isset($_GET['brand'])) {
                $brand = $_GET['brand'];
            }
            if (isset($_GET['sortby'])) {
                $sortby = $_GET['sortby'];
            }
            if (isset($_GET['page'])) {
                $nrpage = $_GET['page'];
            }
            
            $page = $this->get_model->getPage('search');
            $categorii = $this->get_model->getCategoryForMenu(0);
            
            $brands = $this->get_model->getBrands('', $match, $cost, $pr, $brand );
            $products = $this->get_model->getProducts( '', $match, $cost, $cat, $pr, $brand, $sortby, $nrpage,0 );
            $count = $this->get_model->getCountProducts('', $match, $cost, $cat, $pr, $brand, 0 );


            $data = array(
                'tab_title' => $page['TabTitle'],
                'title' => $match,
                'keywords_for_layout' => $page['Keywords'],
                'description_for_layout' => $page['Description'],
                'main_image' => $page['Image'],
                'page_content' => 'pages/catalog2',
                'brands' => $brands,
                'products' => $products,
                'count' => $count,
                'categorii' => $categorii
            );

            $this->load->view('layouts/main_layout',$data);
        }
    }

    public function new_items() {
    
        $match = '';
        $cost = 0;
        $cat = 0;
        $pr = 0;
        $brand = '';
        $sortby = 1;
        $nrpage = 1;

        if (isset($_GET['match'])) {
            $match = $_GET['match'];
        }
        if (isset($_GET['cost'])) {
            $cost = $_GET['cost'];
        }
        if (isset($_GET['cat'])) {
            $cat = $_GET['cat'];
        }
        if (isset($_GET['pr'])) {
            $pr = $_GET['pr'];
        }
        if (isset($_GET['brand'])) {
            $brand = $_GET['brand'];
        }
        if (isset($_GET['sortby'])) {
            $sortby = $_GET['sortby'];
        }
        if (isset($_GET['page'])) {
            $nrpage = $_GET['page'];
        }
        
        $page = $this->get_model->getPage('new-items');
        $categorii = $this->get_model->getCategoryForMenu(0);

        $brands = $this->get_model->getBrands('', $match, $cost, $pr, $brand );
        $category = $this->get_model->getCategory('new');
        $products = $this->get_model->getProducts( '', $match, $cost, $cat, $pr, $brand, $sortby, $nrpage, 1);
        $count = $this->get_model->getCountProducts('', $match, $cost, $cat, $pr, $brand , 1);


        $data = array(
            'tab_title' => $page['TabTitle'],
            'title' => $match,
            'keywords_for_layout' => $page['Keywords'],
            'description_for_layout' => $page['Description'],
            'main_image' => $page['Image'],
            'page_content' => 'pages/catalog2',
            'brands' => $brands,
            'products' => $products,
            'count' => $count,
            'category' => $category,
            'categorii' => $categorii
        );

        $this->load->view('layouts/main_layout',$data);
    }

    public function promo() {
    
        $match = '';
        $cost = 0;
        $cat = 0;
        $pr = 1;
        $brand = '';
        $sortby = 1;
        $nrpage = 1;

        if (isset($_GET['match'])) {
            $match = $_GET['match'];
        }
        if (isset($_GET['cost'])) {
            $cost = $_GET['cost'];
        }
        if (isset($_GET['cat'])) {
            $cat = $_GET['cat'];
        }
        if (isset($_GET['pr'])) {
            $pr = $_GET['pr'];
        }
        if (isset($_GET['brand'])) {
            $brand = $_GET['brand'];
        }
        if (isset($_GET['sortby'])) {
            $sortby = $_GET['sortby'];
        }
        if (isset($_GET['page'])) {
            $nrpage = $_GET['page'];
        }
        
        $page = $this->get_model->getPage('promo');
        $categorii = $this->get_model->getCategoryForMenu(0);

        $brands = $this->get_model->getBrands('', $match, $cost, $pr, $brand );
        $category = $this->get_model->getCategory('promo');
        $products = $this->get_model->getProducts( '', $match, $cost, $cat, $pr, $brand, $sortby, $nrpage, 0);
        $count = $this->get_model->getCountProducts('', $match, $cost, $cat, $pr, $brand , 0);


        $data = array(
            'tab_title' => $page['TabTitle'],
            'title' => $match,
            'keywords_for_layout' => $page['Keywords'],
            'description_for_layout' => $page['Description'],
            'main_image' => $page['Image'],
            'page_content' => 'pages/catalog2',
            'brands' => $brands,
            'products' => $products,
            'count' => $count,
            'category' => $category,
            'categorii' => $categorii
        );

        $this->load->view('layouts/main_layout',$data);
    }

    public function items() {
    
        $match = '';
        $cost = 0;
        $cat = 0;
        $pr = 0;
        $brand = '';
        $sortby = 1;
        $nrpage = 1;

        if (isset($_GET['match'])) {
            $match = $_GET['match'];
        }
        if (isset($_GET['cost'])) {
            $cost = $_GET['cost'];
        }
        if (isset($_GET['pr'])) {
            $pr = $_GET['pr'];
        }
        if (isset($_GET['brand'])) {
            $brand = $_GET['brand'];
        }
        if (isset($_GET['sortby'])) {
            $sortby = $_GET['sortby'];
        }
        if (isset($_GET['page'])) {
            $nrpage = $_GET['page'];
        }
        
        $page = $this->get_model->getPage('items');
        $categorii = $this->get_model->getCategoryForMenu(0);

        $brands = $this->get_model->getBrands('', $match, $cost, $pr, $brand );
        $products = $this->get_model->getProducts( '', $match, $cost, $cat, $pr, $brand, $sortby, $nrpage, 0);
        $count = $this->get_model->getCountProducts('', $match, $cost, $cat, $pr, $brand , 0);


        $data = array(
            'tab_title' => $page['TabTitle'],
            'title' => $match,
            'keywords_for_layout' => $page['Keywords'],
            'description_for_layout' => $page['Description'],
            'main_image' => $page['Image'],
            'page_content' => 'pages/catalog2',
            'brands' => $brands,
            'products' => $products,
            'count' => $count,
            'categorii' => $categorii
        );

        $this->load->view('layouts/main_layout',$data);
    }

    public function product() {

        $uri4=$this->uri->segment(4);

        if(!empty($uri4)) {

            $page = $this->get_model->getProductData($uri4, '');
            $products = $this->get_model->getRandProducts($page['CategoryCod'], 4);
            $categorii = $this->get_model->getCategoryForMenu(0);

            if(empty($page)) {
                $this->error_404();
            } else {

                $data = array(
                    'tab_title' => $page['ProductTitle'],
                    'title' => $page['ProductTitle'],
                    'keywords_for_layout' => $page['Keywords'],
                    'description_for_layout' => $page['Description'],
                    /* 'main_image' => $page['Image'], */
                    'page_content' => 'pages/product',
                    'product' => $page,
                    'products' => $products,
                    'categorii' => $categorii
                );
                $this->load->view('layouts/main_layout',$data);
            }
            
        } else {
            $this->error_404();
        }
    }

    public function cart($type='') {

        $page = $this->get_model->getPage('cart');
        
        if(empty($page)) {
            $this->error_404();
        }

        $IdArray = array();
        $IdArray[0] = 0;
        if (isset($_SESSION['cart']['items'])) { 
            foreach ($_SESSION['cart']['items'] as $item) {
                $IdArray[] = $item['cod'];
            }
        }

        $categorii = $this->get_model->getCategoryForMenu(0);
        $productsoncart = $this->get_model->getCartProducts($IdArray);
        $parentCods = $this->get_model->getCartParentCod($IdArray);
        
        $idsCategory = array_map(function($item){
            return $item['ParentCod'];
        }, $parentCods);

        //dump($idsCategory);

        $products = $this->get_model->getRandProducts( $idsCategory , 4);

        $data = array(
            'tab_title' => $page['TabTitle'],
            'keywords_for_layout' => $page['Keywords'],
            'description_for_layout' => $page['Description'],
            'main_image' => $page['Image'],
            'page_content' => 'pages/cart',
            'products' => $products,
            'categorii' => $categorii,
            'productsoncart' => $productsoncart
        );

        if (!empty($type)) {
            $page_num = $type;
        } else {
            $page_num = addslashes($this->uri->segment(4));
        }

        switch ($page_num) {
            case '':
            case '1':
                $data['page_content'] = 'pages/cart';
                $this->load->view('layouts/main_layout', $data);
                break;
            case '2':
                if($this->session->userdata('logged_in')) {
                    if ((isset($_SESSION['cart']['items']) && $_SESSION['cart']['total'] >= VALUE_MINSUMORDER) || getRealIpAddr() == '188.237.130.215') { 
                        $user = $this->get_model->getUserDate();
                        $districts = $this->get_model->getDistricts();
                        $data['districts'] = $districts;
                        $data['user'] = $user;
                        $data['page_content'] = 'pages/cart2';
                        $this->load->view('layouts/main_layout', $data);
                    } else {
                        redirect(base_url(LCLANG."/pages/cart"));
                    }
                } else {
                    $_SESSION['redirect'] = 'cart';
                    //$this->login();
                    redirect(base_url(LCLANG."/pages/login"));
                }
                break;
            default :
                $this->error_404();
                break;
        }
    }

    public function success($order_nr='', $email='', $fullname='') {

        $order_nr = addslashes($this->uri->segment(4));

        $order = $this->get_model->getOrder($order_nr);

        if(!empty($order)) {
            
            unset($_SESSION['cart']);

            $config['charset'] = 'utf-8';
            $config['mailtype'] = 'html';

            $this->email->initialize($config);

            $this->email->from('no-reply@'.$_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST']);
            $this->email->to($order->email);
            $this->email->subject(EMAIL_CONFIRMONLINE_SUBJECT);

            $products = '';
            foreach($order->items as $item) {
                $products .='<p>'.$item->NameProduct.'</p>';
            }

            $data = array(
                'list' => $products,
                'companydata' => TEXT_CMPANYDATA,
                'date' => date('d/m/Y'),
                'ordernumber' => $order_nr,
                'name' => $order->Name,
                'transactiontype' => ($order->payment==1)?'Visa/Mastercard':'CASH',
                'rrn' => $order->rrn,
                'code' => $order->approval,
                'fornumber' => $order->card_number,
                'total' => $order->total + $order->delivery_price
            );    

            $template = file_get_contents(TEMPLATE);
            foreach ($data as $key => $value) {
                $template = str_replace('{'.$key.'}', $value, $template);
            }    

            $this->email->message($template);
            $this->email->send();
        }

        $page = $this->get_model->getPage('cart');
        $categorii = $this->get_model->getCategoryForMenu(0);
        
        if(empty($page)) {
            $this->error_404();
        }

        $script = "<!-- Event snippet for Clic conversion page --> 
                    <script> 
                        gtag('event', 'conversion',
                            { 'send_to': 'AW-792712608/-rT0CMTv0YsBEKCr__kC', 'value': 1.0, 'currency': 'USD' }
                        ); 
                    </script>";

        $data = array(
            'tab_title' => $page['TabTitle'],
            'keywords_for_layout' => $page['Keywords'],
            'description_for_layout' => $page['Description'],
            'main_image' => $page['Image'],
            'categorii' => $categorii,
            'conversion_script' => $script
       );

        if($this->session->userdata('logged_in')) {
            if($order_nr != '') {
                $data['page_content'] = 'pages/success';
                $data['order_nr'] = $order_nr;
                $this->load->view('layouts/main_layout', $data);
            } else {
                $this->error_404();
            }
        } else {
            $this->error_404();
        }
    }

    public function success_interview() {
        $page = $this->get_model->getPage('interview');
        $categorii = $this->get_model->getCategoryForMenu(0);
        
        if(empty($page)) {
            $this->error_404();
        }

        $isexist = false;
        $Interviews = $this->get_model->getInterviews();

        $idsInterview = array_map(function($item){
            return $item->ID;
        }, $Interviews);


        $curent_item ='';
        foreach($idsInterview as $item) {
            $result = $this->get_model->getIp( getRealIpAddr(), $item);

            if(empty($result) && $curent_item=='') {
                $curent_item = $item;
            }
        }

        if(!empty($curent_item)){
            $isexist = true;
        }

        $data = array(
            'tab_title' => $page['TabTitle'],
            'keywords_for_layout' => $page['Keywords'],
            'description_for_layout' => $page['Description'],
            'main_image' => $page['Image'],
            'categorii' => $categorii,
            'message' => TEXT_CONGRATULATIONS2,
            'page_content' => 'pages/displaymsg',
            'isexist' => $isexist
       );
       $this->load->view('layouts/main_layout', $data);
    }

    /************ restabilirea parolei *************/

    public function send_password() {
        $this->form_validation_restorepass('click');
        if ($this->form_validation->run()) {

            $key = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 6)), 0, 6);

            $config['charset'] = 'utf-8';
            $config['mailtype'] = 'html';

            $this->email->initialize($config);

            $this->email->from('no-reply@'.$_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST']);
            $this->email->to($this->input->post('email'));
            $this->email->subject(EMAIL_SENDP_SUBJECT);

            $message = str_replace('{pass}',$key,EMAIL_SENDP_TEXT);

            $this->email->message($message);

            //Send and email to the user
            if ($this->update_model->update_password($key)) {
                if ($this->email->send()) {
                    $data = array(
                        'tip' => 'success',
                        'message' => MSG_SENDP,
                    );
                } else {
                    $data = array(
                        'tip' => 'error',
                        'message' => MSG_SENDP_ERROR,
                    );
                }
            } else {
                $data = array(
                    'tip' => 'error',
                    'message' => MSG_SENDP_ERROR,
                );
            }

            $this->alert($data);
        } else {
            $this->restorepass();
        }
    }

    public function form_validation_restorepass($action='') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', PLACEH_EMAIL, 'required|trim|valid_email|callback_email_exist|callback_email_active|xss_clean');

        //$this->form_validation->set_error_delimiters('<span class="alert alert-error">', '<i class="fa fa-close fa-lg"></i></span>');

        $this->form_validation->set_message('required', INFO_REQUIRED);
        $this->form_validation->set_message('valid_email', INFO_VALIDEMAIL);
        $this->form_validation->set_message('email_exist', INFO_EMAILEXIST);
        $this->form_validation->set_message('email_active', INFO_ISACTIVE);

        if ($action != 'click' ) {
            if (!$this->form_validation->run()) {
                $q = array();
                $q['email_errore'] = form_error('email');
                echo json_encode($q);
            }
        }
    }

    public function email_exist($key){
        $count = $this->get_model->email_exist($key);
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function email_active($key){
        $count = $this->get_model->email_active($key);
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    /************ login validation *************/

    public function login_validation() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', PLACEH_EMAIL, 'required|trim|valid_email|callback_validate_credentials|xss_clean');
        $this->form_validation->set_rules('password', PLACEH_PASSWORD, 'required|trim|xss_clean');

        //$this->form_validation->set_error_delimiters('<span class="alert alert-error">', '<i class="fa fa-close fa-lg"></i></span>');

        $this->form_validation->set_message('required', INFO_REQUIRED);
        $this->form_validation->set_message('valid_email', INFO_VALIDEMAIL);

        if ($this->form_validation->run()) {
            $result = $this->get_model->getUserDate($this->input->post('email'), $this->input->post('password'));

            $words = explode(' ', $result['Name']);
            $Surname = array_pop($words);
            $Name = implode(' ', $words);

            $sess =  array (
                'user_id' => $result['ID'],
                'user_name' => $Surname,
                'logged_in' => TRUE,
                'type' => $result['type'],
                'discount' => $result['Discount']
            );

            $this->session->set_userdata($sess);
            if(isset($_SESSION['redirect']) && $_SESSION['redirect'] == 'cart'){
                unset($_SESSION['redirect']);
                //$this->cart(2);
                redirect(base_url(LCLANG."/pages/cart/2"));
            } else {
                //$this->profile();
                if($result['type']==3) {
                    redirect(base_url(LCLANG."/pages/profile/5"));
                } else {
                    redirect(base_url(LCLANG."/pages/profile"));
                }
            }
        } else {
            $this->login();
            //redirect(base_url(LCLANG."/pages/login"));
        }
    }

    public function validate_credentials() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        if(!empty($email) && !empty($password))
        {
            if($this->get_model->can_log_in(1)) {
                if($this->get_model->is_active(1)) {
                    return true;
                } else {
                    $this->form_validation->set_message('validate_credentials', INFO_ISACTIVE);
                    return false;
                }
            } else {
                $this->form_validation->set_message('validate_credentials', INFO_VALIDCRED);
                return false;
            }
        } else {
            $this->form_validation->set_message('validate_credentials', '');
            return false;
        }
    }

    public function validate_current() {
        $id = $_SESSION['user_id'];
        $password = $this->input->post('currpassword');
        if(!empty($id) && !empty($password))
        {
            if($this->get_model->can_log_in(2)) {
                if($this->get_model->is_active(2)) {
                    return true;
                } else {
                    $this->form_validation->set_message('validate_credentials', INFO_ISACTIVE);
                    return false;
                }
            } else {
                $this->form_validation->set_message('validate_credentials', INFO_VALIDCRED);
                return false;
            }
        } else {
            $this->form_validation->set_message('validate_credentials', '');
            return false;
        }
    }

    /************* schimbam datele ****************/

    public function change_date() {
        $uri4 = addslashes($this->uri->segment(4));
        $this->form_validation_change('click', $uri4);
        if ($this->form_validation->run()) {

            if ($uri4 == 4) {
                $date = array(
                    'password' => $this->input->post('password')
                );
            } else if ($uri4 == 3) {
                $date = array(
                    'Name' => $this->input->post('fullname'),
                    'email' => $this->input->post('email'),
                    'phone' => $this->input->post('phone'),
                    'fiscalcode' => $this->input->post('fiscalcode'),
                    'DistrictID' => $this->input->post('district')
                );
            }

            if($this->update_model->update_user_date($date)) {
                $data = array(
                    'tip' => 'success',
                    'message' => MSG_UPD,
                );

                if ($uri4 == 4) { 
                    $sess_data = array(
                        'msg' => MSG_UPD,
                    );
                    $this->session->set_userdata($sess_data);
                }

                if ($uri4 == 3) {
                    $words = explode(' ', $this->input->post('fullname'));
                    $Surname = array_pop($words);
                    $Name = implode(' ', $words);

                    $sess_data = array(
                        'user_name' => $Surname,
                        'msg' => MSG_UPD,
                    );
                    $this->session->set_userdata($sess_data);
                }

            } else {
                $data = array(
                    'tip' => 'error',
                    'message' => MSG_UPD_ERROR,
                );
            }
            //$this->alert($data);
            redirect(base_url(LCLANG."/pages/profile/".$uri4));

        } else {
            $this->profile($uri4);
        }
    }

    public function form_validation_change($action='', $type='') {

        $this->load->library('form_validation');

        if($type=='4') {
            // schimbam parola
            $this->form_validation->set_rules('currpassword', PLACEH_CURRPASSWORD, 'required|trim|callback_validate_current|min_length[6]|xss_clean');
            $this->form_validation->set_rules('password', PLACEH_PASSWORD, 'required|trim|min_length[6]|xss_clean');
            $this->form_validation->set_rules('passwordagain', PLACEH_PASSWORDAGAIN, 'required|trim|matches[password]|min_length[6]|xss_clean');

            //$this->form_validation->set_error_delimiters('<span class="alert alert-error">', '<i class="fa fa-close fa-lg"></i></span>');

            $this->form_validation->set_message('required', INFO_REQUIRED);
            $this->form_validation->set_message('matches', INFO_MATCHES);
            $this->form_validation->set_message('min_length', INFO_MINLENGHT);
            $this->form_validation->set_message('validate_current', INFO_VALIDCURR);

        } else if ($type=='3') {
            // schimbam toate datele
            $this->form_validation->set_rules('fullname', PLACEH_FULLNAME, 'required|trim|xss_clean');
            $this->form_validation->set_rules('phone', PLACEH_PHONE, 'required|trim|xss_clean');
            $this->form_validation->set_rules('email', PLACEH_EMAIL, 'required|trim|valid_email|callback_email_exist|xss_clean');
            $this->form_validation->set_rules('district', 'District', 'required|xss_clean');

            //$this->form_validation->set_error_delimiters('<span class="alert alert-error">', '<i class="fa fa-close fa-lg"></i></span>');

            $this->form_validation->set_message('required', INFO_REQUIRED);
            $this->form_validation->set_message('min_length', INFO_MINLENGHT);
            $this->form_validation->set_message('valid_email', INFO_VALIDEMAIL);
            $this->form_validation->set_message('email_exist', INFO_EMAILEXIST);
        }

        if ($action != 'click' ) {
            if (!$this->form_validation->run()) {
                $q = array();
                // aici treb de verificat type daca o sa avem action = blur
                $q['pass_errore'] = form_error('password');
                $q['passagain_errore'] = form_error('passwordagain');
                echo json_encode($q);
            }
        }
    }

    /************ signup validation *************/

    public function registration_validation() {
        $this->form_validation_registration('click');
        if ($this->form_validation->run()) {
            //Generate a random key

            $key_conferm = md5(uniqid());

            $this->load->library('email');

            $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
            $this->email->set_header('Content-type', 'text/html');


            $this->email->from('no-reply@'.$_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST']);
            $this->email->to($this->input->post('email'));

            $this->email->subject(EMAIL_REG_SUBJECT);

            $href = (isset($_SERVER['HTTPS']) ? "https" : "http") . '://'.$_SERVER['HTTP_HOST']."/pages/register_user/".$key_conferm;

            $link = '<a target="_blank" href="'.$href.'">'.LINK_CLICK_HERE.'</a>';
            $message = str_replace('{link}',$link,EMAIL_REG_TEXT);

            $this->email->message($message);

            $arr = array(
                'RegDate' => date('Y-m-d'),
                'Name' => $this->input->post('fullname'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'fiscalcode' => $this->input->post('fiscalcode'),
                'DistrictID' => $this->input->post('district'),
                'legaladdress' => $this->input->post('legaladdress'),
                'password' => $this->input->post('password'),
                'confirm' => $key_conferm
            );

            $this->subscribe($this->input->post('email'), 1, false);

            //Send and email to the user
            if ($this->insert_model->insert_user($arr)) {
                if ($this->email->send()) {
                    $data = array(
                        'tip' => 'success',
                        'message' => MSG_REG,
                    );
                } else {
                    $data = array(
                        'tip' => 'error',
                        'message' => MSG_REG_ERROR,
                    );
                }
            } else {
                $data = array(
                    'tip' => 'error',
                    'message' => MSG_REG_ERROR,
                );
            }
            $this->alert($data);
        } else {
            $this->registration();
        }
    }

    public function form_validation_registration($action='') {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('fullname', PLACEH_FULLNAME, 'required|trim|xss_clean');
        $this->form_validation->set_rules('phone', PLACEH_PHONE, 'required|trim|xss_clean');
        $this->form_validation->set_rules('email', PLACEH_EMAIL, 'required|trim|valid_email|callback_is_unique|xss_clean');
        $this->form_validation->set_rules('district', 'District', 'required|xss_clean');
        $this->form_validation->set_rules('legaladdress', PLACEH_ADDRESS, 'required|trim|xss_clean');
        $this->form_validation->set_rules('password', PLACEH_PASSWORD, 'required|trim|min_length[6]|xss_clean');
        $this->form_validation->set_rules('passwordagain', PLACEH_PASSWORDAGAIN, 'required|trim|matches[password]|min_length[6]|xss_clean');

        //$this->form_validation->set_error_delimiters('<span class="alert alert-error">', '<i class="fa fa-close fa-lg"></i></span>');

        $this->form_validation->set_message('required', INFO_REQUIRED);
        $this->form_validation->set_message('valid_email', INFO_VALIDEMAIL);
        $this->form_validation->set_message('is_unique', INFO_ISUNIQUE);
        $this->form_validation->set_message('matches', INFO_MATCHES);
        $this->form_validation->set_message('min_length', INFO_MINLENGHT);

        if ($action == 'blur' ) {
            if (!$this->form_validation->run()) {
                $q = array();
                // aici trebuie sa adaugam toate campurile in caz ca vom avea action blur
                $q['email_errore'] = form_error('email');
                $q['pass_errore'] = form_error('password');
                $q['cpass_errore'] = form_error('cpassword');
                echo json_encode($q);
            }
        }
    }

    public function is_unique($key){
        $count = $this->get_model->email_exist($key);
        if ($count > 0) {
            return false;
        } else {
            return true;
        }
    }

        /************ order validation *************/

    public function order_validation() {
        $this->form_validation_order();

        if ($this->form_validation->run() || $_SESSION['type'] == 3 || getRealIpAddr() == '188.237.130.215') {

            if ($_SESSION['type'] == 3) {
                /* $sess_data = array(
                    'sess_dealer' => $this->input->post('dealer'),
                    'sess_district' => $this->input->post('district')
                );
                $this->session->set_userdata($sess_data);
 */
                $r1 = 1;
                $r2 = 2;
                $result = $this->db->select('*')->where('ID',$_SESSION['sess_dealer'])->get('SiteUser')->row();
                $fullname = $result->companyname;
                $email = $result->email;
                $phone = $result->phone;
                $userID = $result->ID;
                $agentID = $_SESSION['user_id'];
            } else {
                $r1 = $this->input->post('r1');
                $r2 = $this->input->post('r2');
                $fullname = $this->input->post('fullname');
                $email = $this->input->post('email');
                $phone = $this->input->post('phone');
                $agentID = 0;
                $userID = $_SESSION['user_id'];
            }

            $total = 0;
            //$order_nr = rand(1000, 9999999);

            $items =  array();
            foreach ($_SESSION['cart']['items'] as $item) {
                $total += ($item['price']*$item['count']);
                $items[] = array(
                    'ItemCod' => $item['cod'],
                    //'ItemPrice' => Exchange($item['price']),
                    'ItemPrice' => $item['price'],
                    'ItemCount' => $item['count'],
                    'OrderNr' => ''/* $order_nr */
                );
            }

            if($this->input->post('r1')==1){
                if(CalcTotal() >= VALUE_MINFREEDELIVERY) { 
                    $delivery_price = 0;
                } else {
                    $delivery_price = VALUE_DELIVERYPRICE;
                }
            } else {
                $delivery_price = 0;
            }

            //$total = $total + $delivery_price;
            $arr = array(
                'OrderNr' => '' /* $order_nr */,
                'Date' => date('Y-m-d H:i:s'),
                'Name' => $fullname,
                //'UserID' => $_SESSION['user_id'],
                'UserID' => $userID,
                'email' => $email,
                'phone' => $phone,
                'DistrictID' => $this->input->post('district'),
                'agentID' => $agentID,
                'street' => $this->input->post('street'),
                'housenumber' => $this->input->post('housenumber'),
                'comment' => $this->input->post('commenttoorder'),
                'delivery' => $r1,
                'delivery_price' => $delivery_price,
                'payment' => $r2,
                'total' => $total,
                'userType' => $_SESSION['type'],

            );

            $order_nr = $this->insert_model->insert_order($arr, $items);

            if ($order_nr > 0 ) {
         
                if($r2==1) {
                    $this->payment($order_nr);
                }

                $this->success($order_nr, $email, $fullname);

            } else {
                $data = array(
                    'tip' => 'error',
                    'message' => MSG_REG_ERROR,
                );
                $this->alert($data);
            }
        } else {
            $this->cart(2);
        }
    }

    public function payment($order_nr) {           
        $crypted = ilabCrypt('encrypt', $order_nr);
        $href_crypted = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . '/payment/generate?q=' . rawurlencode($crypted);
        redirect($href_crypted);
    }
    
    public function form_validation_order() {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('fullname', PLACEH_FULLNAME, 'required|trim|xss_clean');
        $this->form_validation->set_rules('phone', PLACEH_PHONE, 'required|trim|xss_clean');
        $this->form_validation->set_rules('email', PLACEH_EMAIL, 'required|trim|valid_email|xss_clean');
        $this->form_validation->set_rules('district', 'District', 'required|xss_clean');
        $this->form_validation->set_rules('street', PLACEH_STREET, 'required|xss_clean');
        $this->form_validation->set_rules('housenumber', PLACEH_HOUSENUMBER, 'required|xss_clean');
        $this->form_validation->set_rules('accept_terms','','trim|required|xss_clean|greater_than[0]');

        //$this->form_validation->set_error_delimiters('<span class="alert alert-error">', '<i class="fa fa-close fa-lg"></i></span>');

        $this->form_validation->set_message('required', INFO_REQUIRED);
        $this->form_validation->set_message('valid_email', INFO_VALIDEMAIL);
    }


    /****** inregistrarea unui client nou *******/

    public function register_user() {

        $key_conferm = addslashes($this->uri->segment(3));
        if (!empty($key_conferm)) {
            if ($this->get_model->is_valid_key($key_conferm)) {
                $date = array(
                    'confirm' => $key_conferm,
                    'status' => 1
                );
                if ($this->update_model->update_user_date($date)) {
                    $result = $this->get_model->getUserDateByConfirm($key_conferm);

                    $words = explode(' ', $result['Name']);
                    $Surname = array_pop($words);
                    $Name = implode(' ', $words);

                    $sess =  array (
                        'user_id' => $result['ID'],
                        'user_name' => $Surname,
                        'logged_in' => TRUE,
                        'type' => $result['type'],
                        'discount' => $result['Discount']
        
                    );

                    $this->session->set_userdata($sess);
                    //$this->login();
                    unset($_SESSION['redirect']);
                    redirect(base_url(LCLANG."/pages/cart/2"));
                } else {
                    $data = array(
                        'tip' => 'error',
                        'message' => MSG_INSERT_ERROR,
                    );
                    $this->alert($data);
                }
            } else {
                $data = array(
                    'tip' => 'error',
                    'message' => MSG_VALID_ERROR,
                );
                $this->alert($data);
            }
        } else {
            $this->error_404();
        }
    }

    /************ dealer validation *************/

    public function dealer_validation() {
        $this->form_validation_dealer('click');
        if ($this->form_validation->run()) {

            $arr = array(
                'RegDate' => date('Y-m-d'),
                /* 'companyname' => $this->input->post('companyname'),
                'Name' => $this->input->post('administrator'), */
                'Name' => $this->input->post('fullname'),

                /* 'legaladdress' => $this->input->post('legaladdress'), */

                'resp_name' => $this->input->post('resp_name'),
                'resp_phone' => $this->input->post('resp_phone'),

                'book_name' => $this->input->post('book_name'),
                'book_phone' => $this->input->post('book_phone'),

                /* 'fiscalcode' => $this->input->post('fiscalcode'),
                'vatcode' => $this->input->post('vatcode'), */

                'idnp' => $this->input->post('idnp'),
                'account' => $this->input->post('account'),
                'bank' => $this->input->post('bank'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'password' => $this->input->post('password'),
                'DistrictID' => $this->input->post('district'),
                'type' => '2'
            );

            $config['charset'] = 'utf-8';
            $config['mailtype'] = 'html';

            $this->email->initialize($config);

            $this->email->from('no-reply@'.$_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST']);
            $this->email->to($this->input->post('email'));
            $this->email->subject(EMAIL_REGD_SUBJECT);

            $this->email->message(EMAIL_REGD_TEXT);

            $this->subscribe($this->input->post('email'), 2, false);

            $id = $this->insert_model->insert_user($arr);
            if ($id>0) {

                $delivery = $this->input->post('delivery');
                $count = count($delivery);

                $block = array();
                for($i=1;$i<=$count;$i++) {
                    if(!empty($delivery['x'.$i])) {
                        $block[] = array(
                            'DealerID' => $id,
                            'Name' => $delivery['x'.$i]
                        );
                    }
                }

                if($this->insert_model->insert_address($block)) {

                    if($this->email->send()) {
                        $data = array(
                            'tip' => 'success',
                            'message' => MSG_REG_DEAL,
                        );
                    } else {
                        $data = array(
                            'tip' => 'error',
                            'message' => MSG_REG_ERROR,
                        );
                    }
                } else {
                    $data = array(
                        'tip' => 'error',
                        'message' => MSG_REG_ERROR,
                    );
                }
            } else {
                $data = array(
                    'tip' => 'error',
                    'message' => MSG_REG_ERROR,
                );
            }
            $this->alert($data);
        } else {
            $this->signup_dealers();
        }
    }

    public function form_validation_dealer($action='') {

        $i = 0;
        $new_arr = array();
        if(isset($_POST['delivery'])) {
            foreach ($_POST['delivery'] as $item) {
                if (!empty($item)) {
                    $i++;
                    $new_arr['x' . $i] = $item;
                }
            }
            $_POST['delivery'] = null;
            $_POST['delivery'] = $new_arr;
        }

        $this->load->library('form_validation');
/*         $this->form_validation->set_rules('companyname', PLACEH_COMPANYNAME, 'required|trim|xss_clean');
        $this->form_validation->set_rules('administrator', PLACEH_ADMINISTRATOR, 'required|trim|xss_clean');
        $this->form_validation->set_rules('legaladdress', PLACEH_LEGALADDRESS, 'required|trim|xss_clean');
 */        $this->form_validation->set_rules('fullname', PLACEH_FULLNAME, 'required|trim|xss_clean');
/*         $this->form_validation->set_rules('fiscalcode', PLACEH_FISCALCODE, 'required|trim|xss_clean');
 */        $this->form_validation->set_rules('resp_name', PLACEH_NAME, 'required|trim|xss_clean');
        $this->form_validation->set_rules('resp_phone', PLACEH_PHONE, 'required|trim|xss_clean');
        $this->form_validation->set_rules('book_name', PLACEH_NAME, 'required|trim|xss_clean');
        $this->form_validation->set_rules('book_phone', PLACEH_PHONE, 'required|trim|xss_clean');
/*         $this->form_validation->set_rules('vatcode', PLACEH_VATCODE, 'required|trim|xss_clean');
 */        $this->form_validation->set_rules('idnp', PLACEH_IDNP, 'required|trim|xss_clean');
        $this->form_validation->set_rules('account', PLACEH_ACCOUNT, 'required|trim|xss_clean');
        $this->form_validation->set_rules('bank', PLACEH_BANK, 'required|trim|xss_clean');
        $this->form_validation->set_rules('delivery[x1]', PLACEH_DELIVERYADDRESS, 'required|xss_clean');
        $this->form_validation->set_rules('phone', PLACEH_PHONE, 'required|trim|xss_clean');
        $this->form_validation->set_rules('email', PLACEH_EMAIL, 'required|trim|valid_email|callback_is_unique|xss_clean');
        $this->form_validation->set_rules('password', PLACEH_PASSWORD, 'required|trim|min_length[6]|xss_clean');
        $this->form_validation->set_rules('passwordagain', PLACEH_PASSWORDAGAIN, 'required|trim|matches[password]|min_length[6]|xss_clean');

        //$this->form_validation->set_error_delimiters('<span class="alert alert-error">', '<i class="fa fa-close fa-lg"></i></span>');

        $this->form_validation->set_message('required', INFO_REQUIRED);
        $this->form_validation->set_message('valid_email', INFO_VALIDEMAIL);
        $this->form_validation->set_message('is_unique', INFO_ISUNIQUE);
        $this->form_validation->set_message('matches', INFO_MATCHES);
        $this->form_validation->set_message('min_length', INFO_MINLENGHT);

        if ($action == 'blur' ) {
            if (!$this->form_validation->run()) {
                $q = array();
                // aici trebuie sa adaugam toate campurile in caz ca vom avea action blur
                $q['email_errore'] = form_error('email');
                $q['pass_errore'] = form_error('password');
                $q['cpass_errore'] = form_error('cpassword');
                echo json_encode($q);
            }
        }
    }

    /************ ajax cart *************/

    public function addToCart() {
        $cod = $this->input->post("cod");
        $count = $this->input->post("count");
        $newProduct = false;
        $product = $this->get_model->getProductData(null, $cod);
        if(empty($product)) {
            echo json_encode(array('error' => true, 'msg' => 'Нету такого товара'));
            exit();
        }

        if($count < 1 or $count > NR_ON_STOC ) {
            echo json_encode(array('error' => true, 'msg' => 'Nu hakeri'));
            exit();
        }

        if (isset($_SESSION['cart']['items'][$cod])) {
            $old_count = $_SESSION['cart']['items'][$cod]['count'];
            $count = $count + $old_count;
            $_SESSION['cart']['items'][$cod]['count'] = $count;
        } else {
            $newProduct = true;
            // adaugam produsul la array-ul cu produse 

            if(isset($_SESSION['type']) && ($_SESSION['type'] == 2 || $_SESSION['type'] == 3)){
                $price = Exchange($product['PriceAngro'],true);
            } else {
                $price = Exchange($product['Price'],false);
            }

            $temparray = array(
                'cod' => $cod,
                'count' => $count,
                'price' => $price
            );
            $_SESSION['cart']['items'][$cod] = $temparray;
        }

        $tempcount = 0;
        $tempsuma = 0;

        foreach ($_SESSION['cart']['items'] as $item) {
            $tempcount += $item['count'];
            $tempsuma += ($item['price']*$item['count']);
        }

        $_SESSION['cart']['count'] = $tempcount;
        $_SESSION['cart']['total'] = $tempsuma;

        $qty = $_SESSION['cart']['items'][$cod]['count'];
        $price = Exchange($product['PriceAngro'],true);

        echo json_encode(
            array(  'error' => false, 
                    'count' => $_SESSION['cart']['count'],
                    'total' => $_SESSION['cart']['total'],
                    'product' => $product,
                    'qty' => $qty,
                    'price' => $price,
                    'newProduct' => $newProduct,
            )
        );

        exit();
    }

    public function reloadOrder() {
        $orderID = $this->input->post("orderID");

        $items = $this->db->select('ItemCod, ItemCount')
        ->where('OrderID', $orderID)
        ->get('OrderItems')->result();

        unset($_SESSION['cart']);

        foreach ($items as $item) {

            $product = $this->get_model->getProductData(null, $item->ItemCod);

            if(!empty($product)) {

                $count = $item->ItemCount;
        
                if(isset($_SESSION['type']) && ( $_SESSION['type'] == 2 || $_SESSION['type']==3)){
                    $price = Exchange($product['PriceAngro'],true);
                } else {
                    $price = Exchange($product['Price'],false);
                }
    
                $temparray = array(
                    'cod' => $item->ItemCod,
                    'count' => $count,
                    'price' => $price
                );
                $_SESSION['cart']['items'][$item->ItemCod] = $temparray;
               
            }

        }

        $tempcount = 0;
        $tempsuma = 0;

        foreach ($_SESSION['cart']['items'] as $item) {
            $tempcount += $item['count'];
            $tempsuma += ($item['price']*$item['count']);
        }

        $_SESSION['cart']['count'] = $tempcount;
        $_SESSION['cart']['total'] = $tempsuma;

        echo json_encode(
            array(  'error' => false
            )
        );
        exit();
    }

    public function plusMinusCount() {
        $cod = $this->input->post("cod");
        $count = $this->input->post("count");
        $product = $this->get_model->getProductData(null, $cod);

        if(empty($product)) {
            echo json_encode(array('error' => true, 'msg' => 'Нету такого товара'));
            exit();
        }

        if($count < 1 or $count > NR_ON_STOC ) {
            echo json_encode(array('error' => true, 'msg' => 'Nu hakeri'));
            exit();
        }

        if (isset($_SESSION['cart']['items'][$cod])) {
                $_SESSION['cart']['items'][$cod]['count'] = $count;
        } else {
            echo json_encode(array('error' => true, 'msg' => 'Iar hakeresti'));
            exit();
        }

        $tempcount = 0;
        $tempsuma = 0;

        foreach ($_SESSION['cart']['items'] as $item) {
            $tempcount += $item['count'];
            $tempsuma += ($item['price']*$item['count']);
        }

        //$tempsuma = number_format((float) $tempsuma , 2, '.', '');

        $_SESSION['cart']['count'] = $tempcount;
        $_SESSION['cart']['total'] = $tempsuma;

        if($_SESSION['cart']['total'] < VALUE_MINSUMORDER || getRealIpAddr() == '188.237.130.215') {
            $display = 'show';
        } else {
            $display = 'hide';
        }

        echo json_encode(
            array(  'error' => false,
                    'count' => $_SESSION['cart']['count'],
                    'qty' => $count,
                    'total' => $_SESSION['cart']['total'],
                    'display' => $display,
                    'price' => $_SESSION['cart']['items'][$cod]['price'],
                    'cod' => $cod,
            )
        );
        exit();
    }

    public function getPrice() {

        echo json_encode(
            array(  'error' => false, 
                    'total' => $_SESSION['cart']['total'],
            )
        );
        exit();
    }

    public function delFromCart() {
        $uri3 = addslashes($this->uri->segment(3));

        if (!empty($uri3)) {
            unset( $_SESSION['cart']['items'][$uri3]);
            
            $tempcount = 0;
            $tempsuma = 0;
            
            foreach ($_SESSION['cart']['items'] as $item) {
                $tempcount += $item['count'];
                $tempsuma += ($item['price']*$item['count']);
            }
    
            $_SESSION['cart']['count'] = $tempcount;
            $_SESSION['cart']['total'] = $tempsuma;

            //$this->cart();
            if (isset($_SESSION['type']) && $_SESSION['type']==3) {
                redirect(base_url(LCLANG."/pages/profile"));
            } else {
                redirect(base_url(LCLANG."/pages/cart"));
            }
            
        } else {
            $this->error_404();
        }
    }

    public function subscribe($email='', $type=1, $ajax=true) {
        include realpath('public').'/mailchimp/src/MailChimp.php';
        $MailChimp  = new MailChimp('778747ffd7eeb24e4599a201efc40d9e-us18');

        if($type==1) {
            $listId     = 'b5f2af1553';
        } else{
            $listId     = '5fe32df08b';
        }
        if($email=='') {
            $email = $this->input->post("email");
            $status = 'pending';
        } else {
            $status = 'subscribed';
        }

        if ($ajax==true) {
            if($email=='') {
                echo json_encode(
                    array(  'error' => true)
                );
                exit();
            }
        }

        $result = $MailChimp->put("lists/".$listId."/members/" . md5($email), 
            array(
                'email_address' => $email,
                'status' => $status,
                'merge_fileds' => [
                    'FNAME' => '',
                    'LNAME' => ''
                ]
            )
        );

        if ($ajax==true) {
            echo json_encode(
                array(  'error' => false)
            );
            exit();
        }
        
    }

    public function sendmsg() {

        $semail = $this->input->post("semail");
        $name = $this->input->post("name");
        $msg = $this->input->post("msg");

        if($semail=='' || $name=='' || $msg=='') {
            echo json_encode(
                array(  'error' => true)
            );
            exit();
        }

        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';

        $this->email->initialize($config);

        $this->email->from($semail, $name);
        $this->email->to(TEXT_EMAIL);
        $this->email->subject('Feedback');

        $this->email->message($msg);

        if ($this->email->send()) {
            echo json_encode(
                array(  'error' => false)
            );
            exit();
        } else {
            echo json_encode(
                array(  'error' => true)
            );
        }    
        
    }

    public function getCategoryForMenu() {

        $cat = $this->input->post("cat");

        $result = $this->get_model->getCategoryForMenu($cat);

        echo json_encode(
            array(  'error' => false, 'result' => $result
            )
        );
        exit();
    }

    public function autocomplete() {

        $lclang=strtolower($_SESSION['lang']);
        $clang=strtoupper($lclang);

        $match = $this->input->post("match");
        $result = $this->get_model->getAutoProducts($match);
        
        $curs = $this->db->select('USD')
        ->order_by('Date','desc')
        ->limit(1)->get('CursValutar')
        ->row('USD');
        
        echo json_encode(
            array(  
                'error' => false, 
                'result' => $result, 
                'curs' => $curs,
                'type' => (isset($_SESSION['type']))?$_SESSION['type']:'1',
                'discount' => (isset($_SESSION['discount']))?$_SESSION['discount']:'0',
            )
        );
        exit();
    }

    public function setSess() {

        $identificator = 'sess_'.$this->input->post('type');

        $sess_data[$identificator]= $this->input->post('val');
        $this->session->set_userdata($sess_data);

        if($this->input->post('type')=='dealer'){
            $Discount = $this->db->select('Discount')
            ->where('ID', $this->input->post('val'))
            ->get('SiteUser')
            ->row()->Discount;
            
            $sess_data['discount']= $Discount;
            $this->session->set_userdata($sess_data);

            if (isset($_SESSION['cart'])) {
                foreach ($_SESSION['cart']['items'] as $cartitem) {
                    $price = $this->db->select('PriceAngro')
                    ->where('Cod', $cartitem['cod'])
                    ->get('Products')
                    ->row()->PriceAngro;

                    $newprice = Exchange($price, true);
                    $_SESSION['cart']['items'][$cartitem['cod']]['price'] = $newprice;
                }

                $tempcount = 0;
                $tempsuma = 0;

                foreach ($_SESSION['cart']['items'] as $item) {
                    $tempcount += $item['count'];
                    $tempsuma += ($item['price']*$item['count']);
                }

                $_SESSION['cart']['count'] = $tempcount;
                $_SESSION['cart']['total'] = $tempsuma;

            }
        }

        echo json_encode(
            array(  'error' => false
            )
        );
    }

    public function set_active_tab() {

        $tab = $this->input->post('tab');

        $_SESSION['active_tab'] = $tab;
        
        echo json_encode(
            array(  'error' => false, 'active_tab' => $_SESSION['active_tab']
            )
        );
    }

    public function send_answers() {
        if(!isset($_POST) || empty($_POST)) {
            $this->error_404();
        } else {
            foreach ($_POST as $key => $value) {
                switch ($key) {
                    case 'user_ip':
                        $user_ip = $value;
                        break;
                    case 'interview_id':
                        $interview_id = $value;
                        break;
                    default:
                        if(is_array($value)) {
                            foreach ($value as $key2 => $value2) {
                                $array[] = $value2;
                            }
                        } else {
                            $array[] = $value;
                        }
                }
            }

            $data = array(
                'user_ip' => $user_ip,
                'interview_id' => $interview_id,
                'date' => date('Y-m-d H:i:s')
            );
            $this->insert_model->insert_ip($data);
            $this->update_model->update_count($array);

            $Interviews = $this->get_model->getInterviews();

            $idsInterview = array_map(function($item){
                return $item->ID;
            }, $Interviews);

            $curent_item ='';
            foreach($idsInterview as $item) {
                $result = $this->get_model->getIp( getRealIpAddr(), $item);

                if(empty($result) && $curent_item=='') {
                    $curent_item = $item;
                }
            }

            if(empty($curent_item)) {
                $_SESSION['is_passed']=true;
            }

            $this->success_interview();
        }
    }


}
