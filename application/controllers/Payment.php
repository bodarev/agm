<?php
/**
 * Created by PhpStorm.
 * User: WhoAmI
 * Date: 29.03.2018
 * Time: 16:16
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller
{
    private $uri1;
    private $uri2;
    private $uri3;
    private $uri4;
    private $uri5;
    private $data;
    private $layout_path;
    private $lclang;
    private $clang;
    private $site_url;
    private $full_url;

    public function __construct()
    {
        parent::__construct();

        @session_start();

        header('Content-type: text/html; charset=utf-8');

        $this->uri1 = $this->uri->segment(1);
        $this->uri2 = $this->uri->segment(2);
        $this->uri3 = $this->uri->segment(3);
        $this->uri4 = $this->uri->segment(4);
        $this->uri5 = $this->uri->segment(5);

        if (empty($_SESSION['lang'])) {
            get_prefered_lang();
        }

        $this->page_id = 1;
        $this->layout_path = 'pages/payment';
        $this->lclang = get_lang(FALSE);
        $this->clang = get_lang(TRUE);
        $this->site_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . '://' . $_SERVER['HTTP_HOST'];
        $this->full_url = $this->site_url . $_SERVER['REQUEST_URI'];
        $this->data['lclang'] = $this->lclang;
        $this->data['clang'] = $this->clang;
        $this->data['uri1'] = $this->uri1;
        $this->data['uri2'] = $this->uri2;
        $this->data['uri3'] = $this->uri3;
        $this->data['uri4'] = $this->uri4;
        $this->data['uri5'] = $this->uri5;
    }

    private function _get_last_numbers_of_card($cardnumber)
    {
        return substr($cardnumber, -4);
    }

    public function index()
    {
        throw_on_404();
    }

    public function generate()
    {
        if (empty($_GET) || empty($_GET['q'])) {
            throw_on_404();
        }

        try {
            $q_crypted = rawurldecode($_GET['q']);
            $order_id = ilabCrypt('decrypt', $q_crypted);

            $result = $this->db->select('ItemCod')->where('OrderNr', $order_id)->get('OrderItems')->result();
            $cods = array_map(function($item){
                return $item->ItemCod;
            }, $result);

            $result2 = $this->db->select('ParentCod')->where_in('Cod', $cods)->get('Products')->result();
            $parent_cods = array_map(function($item){
                return $item->ParentCod;
            }, $result2);

            $result2 = $this->db->select('TitleRO')->where_in('Cod', $parent_cods)->get('Catalog')->result();

            $order_name = '';
            foreach($result2 as $item) {
                if($order_name=='') {
                    $order_name .= $item->TitleRO;
                } else {
                    $order_name .= ', '.$item->TitleRO;
                }
            }

            $rowsid = '';
            

            $order = $this->db
            ->select(
                'Orders.*, Status.NameRU AS StatusName, Districts.NameRU as DistrictName, SiteUser.Name as agentName'
            )
            ->join('SiteUser', 'SiteUser.ID = Orders.agentID', 'left')
            ->join('Status', 'Status.Value = Orders.Status', 'left')
            ->join('Districts', 'Districts.ID = Orders.DistrictID', 'left')
            ->where('Orders.OrderNr', $order_id)
            ->get('Orders')->row();

            if (empty($order)) {
                throw new Exception('Order Not Found');
            }

            include_once(realpath('application') . '/libraries/card_scripts/P_SIGN_ENCRYPT.PHP');

            $rowsid = $order->OrderNr;
            if (strlen($order->OrderNr) < 11) {
                for ($i = strlen($order->OrderNr); $i <= 11; $i++) {
                    $rowsid = "0" . $rowsid;
                }
            } 

            $now = date("YmdHis", time() - 7200);

            $order_price = $order->total + $order->delivery_price;

            $p_sign = P_SIGN_ENCRYPT($rowsid, $now, 0, (float) $order_price);

            $this->data['action_url'] = 'https://egateway.victoriabank.md/cgi-bin/cgi_link';
            $this->data['amount'] = (float) $order_price;
            $this->data['currency'] = 'MDL';
            $this->data['rowsid'] = $rowsid;
            $this->data['description'] = $order_name;
            $this->data['merch_name'] = 'Megatradecom SRL';
            $this->data['merch_url'] = $this->site_url;
            $this->data['merchant'] = '498000049802496';
            $this->data['terminal'] = '49802496';
            $this->data['email'] = $order->email;
            $this->data['tr_type'] = 0;
            $this->data['country'] = 'md';
            $this->data['nonce'] = '11111111000000011111';
            $this->data['backref'] = $this->site_url . '/'.strtolower($this->clang).'/pages/success/'.$order->OrderNr;
            $this->data['merch_gmt'] = 2;
            $this->data['timestamp'] = $now;
            $this->data['p_sign'] = $p_sign;
            $this->data['lang'] = strtolower($this->clang);
            $this->data['merch_address'] = 'RM, com. Truseni str. Calea Iesilor 28/2';

            $this->load->view($this->layout_path, $this->data);
        } catch (Exception $e) {
            throw_on_404();
        }
    }
    
    public function refund()
    {

        if (empty($_GET) || empty($_GET['q']) || empty($_GET['a'])) {
            throw_on_404();
        }

        try {
            $now = date("YmdHis", time() - 7200);

            $amount = rawurldecode($_GET['a']);
            $amount = floatval($amount);

            if ($amount <= 0) {
                throw new Exception('The amount can not be less than 0');
            }
            
            $q_crypted = rawurldecode($_GET['q']);
            $order_id = ilabCrypt('decrypt', $q_crypted);

            $order_name = '';
            $rowsid = '';

            $order = $this->db
            ->select(
                'Orders.*, Status.NameRU AS StatusName, Districts.NameRU as DistrictName, SiteUser.Name as agentName'
            )
            ->join('SiteUser', 'SiteUser.ID = Orders.agentID', 'left')
            ->join('Status', 'Status.Value = Orders.Status', 'left')
            ->join('Districts', 'Districts.ID = Orders.DistrictID', 'left')
            ->where('Orders.OrderNr', $order_id)
            ->get('Orders')->row();

            if (empty($order)) {
                throw new Exception('Order Not Found');
            }

            $order_price = $order->total + $order->delivery_price;

            if ($amount > $order_price) {
                throw new Exception('Amount and Price ERROR');
            }
            
            $rowsid = $order->OrderNr;
            if (strlen($order->OrderNr) < 11) {
                for ($i = strlen($order->OrderNr); $i <= 11; $i++) {
                    $rowsid = "0" . $rowsid;
                }
            }

            include_once(realpath('application') . '/libraries/card_scripts/P_SIGN_ENCRYPT.PHP');
            $p_sign = P_SIGN_ENCRYPT($rowsid, $now, 24, $amount);
            $params = array(
                "ORDER" => $rowsid,
                "AMOUNT" => $amount,
                "CURRENCY" => "MDL",
                "RRN" => $order->rrn,
                "INT_REF" => $order->int_ref,
                "TRTYPE" => 24,
                "TERMINAL" => '49802496',
                "TIMESTAMP" => $now,
                "NONCE" => '11111111000000011111',
                "P_SIGN" => $p_sign
            );

            paymentHttpPost("https://egateway.victoriabank.md/cgi-bin/cgi_link", $params);

            $_SESSION['success'] = 'Возврат по заказу #'. $order->OrderNr . ' оформлен успешно';
            redirect('/cp/orders');
        } catch (Exception $e) {
            $data_ilab = json_encode(array('error' => $e->getMessage()));
            $this->db->insert('post_data', array('message' => $data_ilab));
            throw_on_404();
        }
    }

    public function answer()
    {

        try {
            $this->load->library('email');

            $now = date("YmdHis", time() - 7200);
            $unixnow = time();

            $pd = $_POST; // post data

            $logData = array(
                'Date' => date('Y-m-d H:i'),
                'Msg' => json_encode($pd),
                'Method' => 'TERMINAL'
            );
            $this->db->insert('Log', $logData);

            include_once(realpath('application') . '/libraries/card_scripts/P_SIGN_DECRYPT.PHP');

            $desc = P_SIGN_DECRYPT($pd['P_SIGN'], $pd['ACTION'], $pd['RC'], $pd['RRN'], $pd['ORDER'], $pd['AMOUNT']);

            if ($desc !== "OK") {
                throw new Exception('Descrypt Fail');
            }

            // authorization 

            if (($pd['ACTION'] == 0) && ($pd['TRTYPE'] == 0)) {
                $order_id = $pd['ORDER'];
                $update_data = array(
                    'card_number' => $pd['CARD']
                );
                $order_id = (int)$order_id;

                $this->load->model('update_model');
                $this->update_model->update_order($update_data, $order_id);

                include_once(realpath('application') . '/libraries/card_scripts/P_SIGN_ENCRYPT.PHP');
                
                $p_sign = P_SIGN_ENCRYPT($pd['ORDER'], $now, 21, $pd['AMOUNT']);
                $params = array(
                    "ORDER" => $pd['ORDER'],
                    "AMOUNT" => $pd['AMOUNT'],
                    "CURRENCY" => $pd['CURRENCY'],
                    "RRN" => $pd['RRN'],
                    "INT_REF" => $pd['INT_REF'],
                    "TRTYPE" => 21,
                    "TERMINAL" => $pd['TERMINAL'],
                    "TIMESTAMP" => $now,
                    "NONCE" => $pd['NONCE'],
                    "P_SIGN" => $p_sign
                );

                paymentHttpPost("https://egateway.victoriabank.md/cgi-bin/cgi_link", $params);
            }

            // confirmation 

            if ($pd['ACTION'] == 0 && $pd['TRTYPE'] == 21) {
                $order_id = $pd['ORDER'];
                $update_data = array(
                    'status' => 3,
                    'approval' => $pd['APPROVAL'],
                    'rrn' => $pd['RRN'],
                    'int_ref' => $pd['INT_REF']
                );
                $order_id = (int)$order_id;

                $this->load->model('update_model');
                $this->update_model->update_order($update_data, $order_id);

                $order = $this->db
                ->select(
                    'Orders.*, Status.NameRU AS StatusName, Districts.NameRU as DistrictName, SiteUser.Name as agentName'
                )
                ->join('SiteUser', 'SiteUser.ID = Orders.agentID', 'left')
                ->join('Status', 'Status.Value = Orders.Status', 'left')
                ->join('Districts', 'Districts.ID = Orders.DistrictID', 'left')
                ->where('Orders.OrderNr', $order_id)
                ->get('Orders')->row();

                assign_language(strtolower($order->lang));

                $q=$this->db->query("select $clang as value, ConstantName from LangConstants")->result_array();
                foreach($q as $row) {
                    define($row['ConstantName'],$row['value']);
                }

                $order_name = TEXT_ORDER;
                $product_type_message = TEXT_ORDERMESSAGE;
                $product_type_subject = TEXT_ORDER;

                $user_name = $order->name;
                $email = $order->email;
                $price = $order->total + $order->delivery_price;
                $card = $order->card_number;

                $message = TEXT_CARDMESSAGESUCCESS;
                $subject = TEXT_CARDMESSAGESUBJECT;

                $switch_data = array(
                    '{%ORDERNAME%}' => $order_name,
                    '{%ORDERID%}' => $pd['ORDER'],
                    '{%USERNAME%}' => $user_name,
                    '{%EMAIL%}' => $email
                );

                foreach ($switch_data as $key => $value) {
                    $message = str_replace($key, $value, $message);
                    $subject = str_replace($key, $value, $subject);
                }

                $config['charset'] = 'utf-8';
                $config['mailtype'] = 'html';
                $this->email->initialize($config);

                $this->email->from('no-reply@' . $_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST']);
                $this->email->to($email);
                $this->email->subject($subject);
                $this->email->message($message);
                $this->email->send();

                $switch_data = array(
                    '{%USERNAME%}' => $user_name,
                    '{%PRICE%}' => $price,
                    '{%DATE%}' => date('H:i:s - d/m/Y'),
                    '{%ORDERID%}' => $pd['ORDER'],
                    '{%APPROVAL%}' => $pd['APPROVAL'],
                    '{%RNN%}' => $pd['RRN'],
                    '{%CARD%}' => $this->_get_last_numbers_of_card($card)
                );

                foreach ($switch_data as $key => $value) {
                    $product_type_message = str_replace($key, $value, $product_type_message);
                }

                $this->email->from('no-reply@' . $_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST']);
                $this->email->to($email);
                $this->email->subject($product_type_subject);
                $this->email->message($product_type_message);
                $this->email->send();

                exit();
            }

            // refund

            if ($pd['ACTION'] == 0 && $pd['TRTYPE'] == 24) {

                $order_id = $pd['ORDER'];
                $order_id = (int)$order_id;
                
                $order = $this->db
                ->select(
                    'Orders.*, Status.NameRU AS StatusName, Districts.NameRU as DistrictName, SiteUser.Name as agentName'
                )
                ->join('SiteUser', 'SiteUser.ID = Orders.agentID', 'left')
                ->join('Status', 'Status.Value = Orders.Status', 'left')
                ->join('Districts', 'Districts.ID = Orders.DistrictID', 'left')
                ->where('Orders.OrderNr', $order_id)
                ->get('Orders')->row();

                if($pd['AMOUNT']==$order->total+$order->delivery_price) {
                    $status = 5;
                } else {
                    $status = $oder->status;
                }

                $update_data = array(
                    'status' => $status,
                    'approval' => $pd['APPROVAL'],
                    'rrn' => $pd['RRN'],
                    'int_ref' => $pd['INT_REF'],
                    'refund' => $pd['AMOUNT']
                );
                
                $this->load->model('update_model');
                $this->update_model->update_order($update_data, $order_id);

                assign_language(strtolower($order->lang));

                $q=$this->db->query("select $clang as value, ConstantName from LangConstants")->result_array();
                foreach($q as $row) {
                    define($row['ConstantName'],$row['value']);
                }

                $config['charset'] = 'utf-8';
                $config['mailtype'] = 'html';
                $this->email->initialize($config);

                $subject = 'Возврат средств';
                $message = 'Был осуществлен возврат средств на карточку клиента';
                $message .= '<br>Сумма к возврату:' . $pd['AMOUNT'] . ' лей';
                $message .= '<br>Номер заказа:' . $order_id;

                $this->email->from('no-reply@' . $_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST']);
                $this->email->to(TEXT_EMAIL);
                $this->email->subject($subject);
                $this->email->message($message);
                $this->email->send();
            }
        } catch (Exception $e) {
            $data_ilab = json_encode(array('error' => $e->getMessage()));
            $this->db->insert('post_data', array('message' => $data_ilab));
            throw_on_404();
        }
    }
}