 /* Загрузка изображении с полем описания */
 function multiImageDescription(input, area=true) {

     input_name = 'multi_image';
     area_name = 'mini_description';

     if (input.files && input.files[0]) {

         $(input.files).each(function(i) {
             var fileExtension = ["image/gif", "image/jpeg", "image/png", "image/jpg"];
             var fileType = this["type"];
             var fileName = this["name"];
             var fileSize = parseInt(this["size"]) / 1000;
             var content = $(input).closest('.form-group').find('#multi-image');

             if (jQuery.inArray(fileType, fileExtension) == -1) {
                 alert('Error file extension');
                 return;
             }

             var reader = new FileReader();
             reader.readAsDataURL(this);

             reader.onload = function(e) {
                 content.show();

                 var dscp = parseInt($(content).find('tbody tr').length);

                 //lang = LANGS;
                 lang = ['en','ro','ru'];
                 var open_lang = $('#change-content').find('.active').attr('data-lang');

                 var textarea = "";
                 if (area == true) {
                     for (var i = lang.length - 1; i >= 0; i--) {
                         var none = lang[i] == open_lang ? 'display:block;' : 'display:none;';
                         textarea += '<textarea style="height:100px; ' + none + '" class="lang-area form-control" id="field_' + lang[i] + '" name="' + area_name + '[' + lang[i] + '][' + parseInt(dscp) + ']" class="form-control" id="dscp" rows="3"></textarea>';
                     };
                 }

                 var row = '<tr>' +
                     '<td>' +
                     '<a href="' + reader.result + '" class="fancybox-button" data-rel="fancybox-button">' +
                     '<img class="img-responsive" src="' + reader.result + '" alt="">' +
                     '<input type="hidden" id="hidden_related" name="' + input_name + '[]" value="' + reader.result + '">' +
                     '</a>' +
                     '</td>' +
                     '<td>' +
                     textarea +
                     '</td>' +
                     '<td>' +
                     '<a href="javascript:;" onclick="deleteLoadItem(this)" class="btn default btn-sm">' +
                     '<i class="fa fa-times"></i> Удалить </a> ' +
                     '</td>' +
                     '</tr>';

                 $(content).find("tbody").append(row);
                 $(content).closest('table').show();
                 i++;
             }
         });
     }
 }

 function multiImageDate(input, input_name, area_name){

     input_name = !input_name ? 'multi_image' : input_name;
     area_name  = !area_name ? 'mini_description' : area_name;

     if (input.files && input.files[0]) {
         $(input.files).each(function(i) {
             var fileExtension = ["image/gif", "image/jpeg", "image/png", "image/jpg"];
             var fileType = this["type"];
             var fileName = this["name"];
             var fileSize = parseInt(this["size"]) / 1000;
             var content = $(input).closest('.load__images_iner').find('#multi-image');

             if (jQuery.inArray(fileType, fileExtension) == -1) {
                 alert('Error file extension');
                 return;
             }

             if (fileSize > 2048) {
                 // alert(fileSize);
                 alert('Максимальный размер изображения 2МБ');
                 return;
             }

             var reader = new FileReader();
             reader.readAsDataURL(this);

             reader.onload = function(e) {
                 content.show();

                 var dscp = parseInt($(content).find('tbody tr').length);

                 var row = '<tr>'+
                     '<td>'+
                     '<a href="'+reader.result+'" class="fancybox-button" data-rel="fancybox-button">'+
                     '<img class="img-responsive" src="'+reader.result+'" alt="">'+
                     '<input type="hidden" id="hidden_related" name="'+input_name+'[]" value="'+reader.result+'">'+
                     '</a>'+
                     '</td>'+
                     '<td style="text-align: right">'+
                     '<a href="javascript:;" onclick="deleteLoadItem(this)" class="btn default btn-sm">'+
                     '<i class="fa fa-times"></i> Удалить </a> '+
                     '</td>'+
                     '</tr>';

                 $(content).find("tbody").append(row);

                 i++;
             }
         });
     }
 }

 function addTextBlock() {
    var i = $('.company__container').length;
    if (i==0) {
        index=1;
    }else{
        index=$('.company__container').eq(i-1).data('index')+1;
    }

    var htmlType = '<div style="margin-bottom:15px;" class="col-md-12 company__container company__container_' + index + ' portlet box blue-hoki" data-index="'+ index +'">' +
         '<div class="portlet-head"><div class="caption">Блок с описанием</div><div class="tools"> <a href="javascript:;" class="remove" style="cursor:pointer;" onclick="clearBlockMenu(this)"></a></div></div>' +
         '<div class="blocks__menu portlet-body form" style="padding: 15px !important;"></div>' +
         '</div><br>';
    $('.added__container').append(htmlType);

    showBlock(i, index);
    Metronic.init();
 }

 function showBlock(inputNum, index) {
    var val = 1;

    $('.company__container_'+index).find('.blocks__menu').html('');
    var ckNr=0;
    $('textarea.ckeditor').each(function(){
        if (!$(this).hasClass('ck__blocks')) {ckNr++;}
    });

    var ckReplace = [];
    var textarea  = '';
    var block     = '';
    var title     = '';

    //lang = LANGS;
     lang = ['en','ro','ru'];
    var open_lang = $('#change-content').find('.active').attr('data-lang');
    for (var i = lang.length - 1; i >= 0; i--) {
         var none = lang[i] == open_lang ? 'display:block;' : 'display:none;';
         title   += '<div class="col-md-12 lang-area" id="field_' + lang[i] + '" style="margin-bottom:15px; '+none+'"> <input name="b_title[' + inputNum + ']['+lang[i]+']" placeholder="Заголовок" class="form-control"> </div>';
         textarea   += '<div class="col-md-12 lang-area" id="field_' + lang[i] + '" style="'+none+'">'+
                        '<textarea name="block[' + inputNum + ']['+lang[i]+']" class="form-control ckeditor ck__blocks" style="min-height:150px;"></textarea>'+
                      '</div>';
    };
    var image = '<div class="col-md-12" style="margin-bottom:15px;"> <input name="image_block[' + inputNum + ']" type="file"> </div>';
    block = '<div class="row">' +
        image+
             title +
             textarea +
             '</div>';

    $('.company__container_'+index).find('.blocks__menu').html(block);

    ckBLocks = ckNr;
    $('textarea.ck__blocks').each(function(i){
        var id = $(this).attr('id');
        if (!id) {
            var id = 'ckeditor_' + (ckBLocks+i);
            $(this).attr('id', id);
            ckReplace.push(id);
        }else{
            var valueCkeditor = CKEDITOR.instances[id].getData();
            var id = 'ckeditor_' + (ckBLocks+i);
            $(this).attr('id', id);
            $("#" + id).val(valueCkeditor);
        }
    });

    $.each(ckReplace, function(k, v) {
         CKEDITOR.replace(v, {});
    });
 }

 function menuBlock() {
     var i = $('.company__container').length;
     if (i==0) {
         index=1;
     }else{
         index=$('.company__container').eq(i-1).data('index')+1;
     }

     var htmlType = '<div style="margin-bottom:15px;" class="col-md-12 company__container company__container_' + index + ' portlet box blue-hoki" data-index="'+ index +'">' +
         '<div class="portlet-head"><div class="caption">Блок с описанием</div><div class="tools"> <a href="javascript:;" class="remove" style="cursor:pointer;" onclick="clearBlockMenu(this)"></a></div></div>' +
         '<div class="blocks__menu portlet-body form" style="padding: 15px !important;"></div>' +
         '</div>';
     $('.added__container').append(htmlType);

     showBlockMenu(i, index);
     //Metronic.init();
 }

 function showBlockMenu(inputNum, index) {
     var val = 1;

     $('.company__container_'+index).find('.blocks__menu').html('');
     var ckNr=0;
     $('textarea.ckeditor').each(function(){
         if (!$(this).hasClass('ck__blocks')) {ckNr++;}
     });

     var ckReplace = [];
     var textarea  = '';
     var block     = '';
     var title     = '';

     //lang = LANGS;
     lang = ['en','ro','ru'];

     var image = '<div class="col-md-12" style="margin-bottom:15px;"> <input name="image_block[' + inputNum + ']" type="file"> </div>';

     block += '<div class="row">' + image+ '</div>';
     block += '<div class="row">';

     for (var i = lang.length - 1; i >= 0; i--) {
         title   = '<div class="col-md-4 lang-area" id="field_' + lang[i] + '" style="margin-bottom:15px;"> <input name="b_title[' + inputNum + ']['+lang[i]+']" placeholder="Заголовок '+lang[i]+'" class="form-control"> </div>';
         block += title;
     };

     block += '</div>';
     block += '<div class="row">';

     for (var i = lang.length - 1; i >= 0; i--) {
         textarea   = '<div class="col-md-4 lang-area" id="field_' + lang[i] + '">'+
             '<textarea name="block[' + inputNum + ']['+lang[i]+']" class="form-control ckeditor ck__blocks" style="min-height:150px;"></textarea>'+
             '</div>';
         block += textarea;
     };
     block += '</div>';

     $('.company__container_'+index).find('.blocks__menu').html(block);

     ckBLocks = ckNr;
     $('textarea.ck__blocks').each(function(i){
         var id = $(this).attr('id');
         if (!id) {
             var id = 'ckeditor_' + (ckBLocks+i);
             $(this).attr('id', id);
             ckReplace.push(id);
         }else{
             var valueCkeditor = CKEDITOR.instances[id].getData();
             var id = 'ckeditor_' + (ckBLocks+i);
             $(this).attr('id', id);
             $("#" + id).val(valueCkeditor);
         }
     });

     $.each(ckReplace, function(k, v) {
         CKEDITOR.replace(v, {});
     });
 }

 function clearBlockMenu(block){
    $(block).closest('.company__container').fadeOut(300, function(){
      $(block).closest('.company__container').remove();
    });
  }


 function deleteLoadItem(item) {
     var parent = $(item).closest('tbody');
     $(item).closest('tr').remove();
     if ($(parent).find('tr').length <= 0) {
         $(parent).closest('table').hide();
     }
 }