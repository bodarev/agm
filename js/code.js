$(document).ready(function(e) {
    $('.thumb-slick').slick({
        dots: false,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });

    $('#dealer').chosen();
	/* mob_menu */
	$('#header').on('click', '.m_ctrl', function() {
		$('.mob_menu_wr').slideToggle(300);
	});
	$('.mob_menu_wr').on('click', '.close', function() {
		$('.mob_menu_wr').hide();
	});

	/* cat_ddown */
	$('.h_cat').on('click', '.ctrl', function() {
		$(this).siblings('.ddown').slideToggle(300);
	});

	/* mob_search */
	$('.search_ctrl_mob').on('click', function() {
		$('.sf_mob').slideToggle(300);
	});

	/* history_ddown */
	$(function() {
		$("#accordion").accordion({
			heightStyle: "content",
			collapsible: true,
			active: false,
			animate: false
		});
	});
	$('.history').children('.ddown').on('click', '.close', function() {
		$('.ddown_wr').hide();
	});

    $('.form_wr .f_row input').on('blur', function ()  {
        if($(this).val() != '') {
            $(this).parent().find('div').removeClass('hide').addClass('show');
        } else {
            $(this).parent().find('div').removeClass('show').addClass('hide');
        }
    });

    var arr = [ "fullname", "phone", "email", "fiscalcode", "currpassword", "password", "passwordagain", "companyname", "administrator", "legaladdress" , "delivery", "resp_name", "resp_phone", "book_name", "book_phone", "vatcode", "account", "bank", "street", "housenumber", "idnp", "accept_terms" ];

    jQuery.each( arr, function( i, val ) {
        var curent = $( "#" + val );
        var value = curent.val();
        if(value != '') {
            curent.parent().find('div').removeClass('hide').addClass('show');
        } else {
            curent.parent().find('div').removeClass('show').addClass('hide');
        }
    });

    jQuery.each( arr, function( i, val ) {
        var curent = $( "#" + val );
        var error = $(curent).parent().find('span.error').html();

        if(error!='') {
            $(curent).css('border', '1px solid tomato');
        }

    });

    // afiseaza in input formatul telefonului
    window.onload = function () {
        MaskedInput({
            elm: document.getElementById('phone'), // select only by id
            format: '+373 (__) ___-___'
            , separator: '+373 ()-'
        });
        MaskedInput({
            elm: document.getElementById('resp_phone'), // select only by id
            format: '+373 (__) ___-___'
            , separator: '+373 ()-'
        });
        MaskedInput({
            elm: document.getElementById('book_phone'), // select only by id
            format: '+373 (__) ___-___'
            , separator: '+373 ()-'
        });
    };

    $(document).on('click', '.buy_wr .buy', function() {
        var cod = $(this).parent().find('input').attr('id');
        var count = $(this).parent().find('input').val();
        var curent = $(this);
        var curency = $(".curency").val();
        $.ajax({
            type: "POST",
            url: "/pages/addToCart",
            data: {
                'cod': cod,
                'count': count
            },
            dataType: 'json',
            success: function(res) {
                if (res.error == true) {
                    curent.parent().parent().find("#alertMsg").empty();
					curent.parent().parent().find("#alertMsg").append( res.msg );
                    curent.parent().parent().find("#alertMsg").show();
                    setTimeout(function(){
                        curent.parent().parent().find("#alertMsg").hide();
                    }, 3000);
                } else {
                    $(".h_cart a").empty();
					$(".h_cart a").append('<span>'+ res.count +'</span> '+ res.total + ' ' + curency );
                    curent.parent().parent().find("#alertMsg").empty();
                    curent.parent().parent().find("#alertMsg").append($(".addtocartmsg").val());
                    curent.parent().parent().find("#alertMsg").show();
                    setTimeout(function(){
                        curent.parent().parent().find("#alertMsg").hide();
                    }, 3000);
                    var url = $(location).attr('href');
                    var segments = url.split( '/' );
                    var segment = segments[5];
                    if(segment=='cart') {
                        location.reload();
                    }
                }
            },
            error: function(res){
                console.log(res);
            }
        });
    });

    $(document).on('keyup', '.order_wr .jq-number, .b_info5 .jq-number', function(){
        var count = $(this).parent().find('.jq-number__field input').val();
        var cod = $(this).parent().find('.jq-number__field input').attr('id');
        var curency = $(".curency").val();
        var deliveryprice = $(".deliveryprice").val();
        $.ajax({
            type: "POST",
            url: "/pages/plusMinusCount",
            data: {
                'cod': cod,
                'count': count,
            },
            dataType: 'json',
            success: function(res) {
                if (res.error == true) {
                    console.log(res);
                } else {
                    $('#price' + res.cod).html((res.price * res.qty).toFixed(2) + ' ' + 'MDL');

                    $(".h_cart a").empty();
					$(".h_cart a").append('<span>'+ res.count +'</span> '+ res.total + ' ' + curency );
                    $(".order_detail .info .p1 span").empty();
					$(".order_detail .info .p1 span").append(res.total + ' ' + curency);
                    $(".order_detail .info .p3 span").empty();
					$(".order_detail .info .p3 span").append((parseFloat(res.total)+Number(deliveryprice)).toFixed(2) + ' ' + curency);
                    if(res.display=='show') {
                        $(".warning").show();
                        $(".order_detail .b_btn a").css({"opacity":"0.4", "pointer-events":"none"});
                        $(".tabs ul li:last-child a").css({"opacity":"0.4", "pointer-events":"none"});
                        $(".warning .in .warningintotal").empty();
                        $(".warning .in .warningintotal").append(res.total + ' ' + curency);
                    } else {
                        $(".warning").hide();
                        $(".order_detail .b_btn a").css({"opacity":"1", "pointer-events":"auto"});
                        $(".tabs ul li:last-child a").css({"opacity":"1", "pointer-events":"auto"});
                    }
                }
            },
            error: function(res){
                console.log(res);
            }
        });
    });

    $(document).on('click', '.order_wr .jq-number__spin.minus, .b_info5 .jq-number__spin.minus, .order_wr .jq-number__spin.plus, .b_info5 .jq-number__spin.plus', function(){
        var self = $(this);
        var count = self.parent().find('.jq-number__field input').val();
        var cod = self.parent().find('.jq-number__field input').attr('id');
        var curency = $(".curency").val();
        var deliveryprice = $(".deliveryprice").val();

        $.post('/pages/plusMinusCount', {cod: cod, count: count}, function (data) {
            try {
                var result = JSON.parse(data);
                if (result.error) {
                    throw new Error(result);
                }
                $('#price' + result.cod).html((result.price * result.qty).toFixed(2) + ' ' + 'MDL');
                $(".h_cart a").empty();
                $(".h_cart a").append('<span>'+ result.count +'</span> '+ result.total + ' ' + curency );
                $(".order_detail .info .p1 span").empty();
                $(".order_detail .info .p1 span").append(result.total + ' ' + curency );
                $(".order_detail .info .p3 span").empty();
                $(".order_detail .info .p3 span").append((parseFloat(result.total)+Number(deliveryprice)).toFixed(2) + ' ' + curency);
                if(result.display=='show') {
                    $(".warning").show();
                    $(".order_detail .b_btn a").css({"opacity":"0.4", "pointer-events":"none"});
                    $(".tabs ul li:last-child a").css({"opacity":"0.4", "pointer-events":"none"});
                    $(".warning .in .warningintotal").empty();
                    $(".warning .in .warningintotal").append(result.total + ' ' + curency);
                } else {
                    $(".warning").hide();
                    $(".order_detail .b_btn a").css({"opacity":"1", "pointer-events":"auto"});
                    $(".tabs ul li:last-child a").css({"opacity":"1", "pointer-events":"auto"});
                }
            } catch (e) {
                console.log(e);
                return false;
            }
        });
    });

    $(document).on('submit', '.subscribe', function(event){

        var email = $(this).find('#subscribe').val();
        $('.mailer i').show();
        $('.mailer input#sub_button').css("pointer-events","none");

        $.ajax({
            type: "POST",
            url: "/pages/subscribe",
            data: {
                'email': email
            },
            dataType: 'json',
            success: function(res) {
                if (res.error == true) {
                    $('.mailer .title').hide();
                    $('.mailer .ok').hide();
                    $('.mailer .nok').hide();
                    $('.mailer .nok').show();
                } else {
                    $('.mailer .title').hide();
                    $('.mailer .nok').hide();
                    $('.mailer .ok').hide();
                    $('.mailer .ok').show();
                    $('.mailer form input#subscribe').val('');
                }
                $('.mailer i').hide();
                $('.mailer input#sub_button').css("pointer-events","auto");

            },
            error: function(res){
                console.log(res);
            }
        });

        event.preventDefault();
    });

    $(document).on('submit', '.sendmsg', function(event){

        var semail = $(this).find('#semail').val();
        var name = $(this).find('#name').val();
        var msg = $(this).find('#msg').val();

        $('.feedback input#sendbutton').css("pointer-events","none");

        $.ajax({
            type: "POST",
            url: "/pages/sendmsg",
            data: {
                'semail': semail,
                'name': name,
                'msg': msg
            },
            dataType: 'json',
            success: function(res) {
                if (res.error == true) {
                    $('.feedback .ok').hide();
                } else {
                    $('.feedback .ok').show();
                    $('.feedback form input#name').val('');
                    $('.feedback form input#semail').val('');
                    $('.feedback form #msg').val('');
                }
                $('.feedback input#sendbutton').css("pointer-events","auto");

            },
            error: function(res){
                console.log(res);
            }
        });

        event.preventDefault();
    });

    $(document).on('change', '#r1', function(){

        var curent = $(this).val();
        var curency = $(".curency").val();
        var deliveryprice = $(".deliveryprice").val();

        $.ajax({
            type: "POST",
            url: "/pages/getPrice",
            data: {},
            dataType: 'json',
            success: function(res) {
                if (res.error == true) {
                    console.log(res);
                } else {
                    if(curent==2) {
                        $(".order_detail .info .p1 span").empty();
                        $(".order_detail .info .p1 span").append(res.total + ' ' + curency);
                        $(".order_detail .info .p2 span").empty();
                        $(".order_detail .info .p2 span").append("0" + ' ' + curency);
                        $(".order_detail .info .p3 span").empty();
                        $(".order_detail .info .p3 span").append(res.total + ' ' + curency);
                    } else {
                        $(".order_detail .info .p1 span").empty();
                        $(".order_detail .info .p1 span").append(res.total + ' ' + curency);
                        $(".order_detail .info .p2 span").empty();
                        $(".order_detail .info .p2 span").append(deliveryprice + ' ' + curency);
                        $(".order_detail .info .p3 span").empty();
                        var curenttotal = ( parseFloat(res.total) + Number(deliveryprice) ).toFixed(2);
                        var curenttotal = parseFloat(curenttotal);
                        $(".order_detail .info .p3 span").append( curenttotal + ' ' + curency);
                    }
                }
            },
            error: function(res){
                console.log(res);
            }
        });
    });

    $('#accordion .reload').click(function(e) {
        e.stopPropagation();
        var orderID = $(this).attr('id');
        $.ajax({
            type: "POST",
            url: "/pages/reloadOrder",
            data: {
                orderID : orderID
            },
            dataType: 'json',
            success: function(res) {
                if (res.error == true) {
                    console.log(res);
                } else {
                    var segments = url.split( '/' );
                    var lang = segments[3];
                    document.location.href = '/' + lang + '/pages/cart';
                }
            },
            error: function(res){
                console.log(res);
            }
        });
    });

    $(function () {
		var nav = $('#header');
		var search = $('#header .row.v2');
		var logo = $('#header .logo img');
		var after = $('#header .row.v1 .in');
		var navHomeY = nav.offset().top;
		var isFixed = false;
        var $w = $(window);
        var width = $w.width();
        $('#header_relativ').css({
            height: '140px',
            display: 'none',
        });

        if(width > 1070) {
            $w.scroll(function () {
                var scrollTop = $w.scrollTop() - 200;
                var shouldBeFixed = scrollTop > navHomeY;
                if (shouldBeFixed && !isFixed) {
                    search.css({
                        display: 'none'
                    });
                    isFixed = true;
                    $('#header_relativ').css({
                        display: 'block',
                    });
                    logo.css({
                        height: '84px'
                    });
                    after.addClass('classafter1');

                    nav.css({
                        position: 'fixed'
                        , top: 0
                        , left: 0
                        , right: 0
                    });

                }
                else if (!shouldBeFixed && isFixed) {
                    search.css({
                        display: 'block'
                    });
                    logo.css({
                        height: 'auto'
                    });
                    after.removeClass('classafter1');
                    nav.css({
                        position: 'relative'
                    });
                    isFixed = false;
                    $('#header_relativ').css({
                        display: 'none',
                    });
                }
            });
        }

    });

    $(document).on('keyup', 'input[name="match"]', function(){

        var match = $(this).val();
        match = match.trim();
        var url = $(location).attr('href');
        var segments = url.split( '/' );
        var lang = segments[3];

        if(lang == '') {
            lang='ro';
        }

        if(match != '') {

            $.ajax({
                type: "POST",
                url: "/pages/autocomplete",
                data: {
                    match : match
                },
                dataType: 'json',
                success: function(data) {
                    if (data.error == true) {
                        console.log(data);
                    } else {
                        $('.search_result').empty();
                        $.each(data.result, function (index, value) {
                            if(value.Image1=='') {
                                image = '/public/i/thumbs/version_52x52x1/noicon.png';
                            } else {
                                image = 'data:image/jpeg;base64, ' + value.Image1;
                            }
                            if(data.type!=1){
                                price = value.PriceAngro * data.curs;
                                if(data.discount>0){
                                    dis = price * data.discount / 100;
                                    price = price - dis;
                                }
                            } else {
                                price = value.Price;
                            }
                            price = parseFloat(price).toFixed(2).replace(/[.,]00$/, "");
                            $('.search_result').append('<a href="/'+lang+'/pages/product/'+ value.UriName +'"><img src="'+image+'" alt=""><span class="title">'+ value.Title+'</span><span class="price"> '+ price +' MDL</span></a>');

                        });
                        $('.search_result').show();
                    }
                },
                error: function(data){
                    console.log(data);
                }
            });

        } else {
            $('.search_result').hide();
        }
    });


    $(document).on('keyup', 'input[name="search"]', function(){

        var match = $(this).val();
        match = match.trim();
        var url = $(location).attr('href');
        var segments = url.split( '/' );
        var lang = segments[3];

        if(match != '') {

            $.ajax({
                type: "POST",
                url: "/pages/autocomplete",
                data: {
                    match : match
                },
                dataType: 'json',
                success: function(data) {
                    if (data.error == true) {
                        console.log(data);
                    } else {
                        $('.autocomplete_list').empty();
                        $.each(data.result, function (index, value) {
                            $('.autocomplete_list').append('<a class="autocomplete_item" id="'+value.Cod+'" href="/"><span class="title">'+ value.Title+'</span></a>');

                        });
                        $('.autocomplete_list').show();
                    }
                },
                error: function(data){
                    console.log(data);
                }
            });

        } else {
            $('.autocomplete_list').hide();
        }
    });


    $('body').click(function(){
        //$('.search_result').empty();
        $('.search_result').hide();
    });

    $(document).on('click', '#set_active_tab', function () {
        tab = $(this).data('tab');

        $.ajax({
            type: "POST",
            url: "/pages/set_active_tab",
            data: {
                tab : tab
            },
            dataType: 'json',
            success: function(data) {
                if (data.error == true) {
                    console.log(data);
                } else {
                    console.log(data);
                }
            },
            error: function(data){
                console.log(data);
            }
        });
    });

    $(document).on('click', '.head .lang .head', function (event) {
        event.preventDefault();
        $(this).find('.down').show();
    });

});
