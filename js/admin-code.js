$(document).ready(function(e) {

    $(document).on('change', '.check', function () {
		if ($(this).is(':checked')) {
			val = 1;
		} else {
			val = 0;
		}
		table = $(this).data('table');
		col = $(this).data('col');
        id = $(this).data('id');
		$.get('/cp/check/' + table + '/' + col + '/' + id + '/' + val);
    });

    $('#multi_select').multiSelect({
        selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Search'>",
        selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Search'>",
        afterInit: function(ms){
          var that = this,
              $selectableSearch = that.$selectableUl.prev(),
              $selectionSearch = that.$selectionUl.prev(),
              selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
              selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
      
          that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
          .on('keydown', function(e){
            if (e.which === 40){
              that.$selectableUl.focus();
              return false;
            }
          });
      
          that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
          .on('keydown', function(e){
            if (e.which == 40){
              that.$selectionUl.focus();
              return false;
            }
          });
        },
        afterSelect: function(){
          this.qs1.cache();
          this.qs2.cache();
        },
        afterDeselect: function(){
          this.qs1.cache();
          this.qs2.cache();
        }
    });

    $(document).on('click', '#addtext', function (event) {
        $(this).parent().parent().parent().append(
        '<tr id="">' +
        '<td>'+
            '<div class="row">' +
            '<div class="col-md-4 col-sm-12">' + 
                '<div class="input-group">' +
                    '<span class="input-group-addon" id="sizing-addon6">RU</span>'+
                    '<input type="text" name="new[AnsRU][]" class="form-control">'+
                    '</input>'+
                '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-12">' +    
                '<div class="input-group">' +
                    '<span class="input-group-addon" id="sizing-addon6">RO</span>'+
                    '<input type="text" name="new[AnsRO][]" class="form-control">'+
                    '</input>'+
                '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-12">' +    
                '<div class="input-group">' +
                    '<span class="input-group-addon" id="sizing-addon6">EN</span>'+
                    '<input type="text" name="new[AnsEN][]" class="form-control">'+
                    '</input>'+
                '</div>' +
            '</div>' +
            '</div>' +
        '<td width="30">'+
            '<button type="submit" id="remove" class="btn red"><i class="fa fa-minus"></i></button>'+
        '</td>'+
        '</tr>');
        event.preventDefault();
    });


    $(document).on('click', '#remove', function (event) {

        if (confirm('Вы уверены?')) {

            var id = $(this).data('id');
            var table = $(this).data('table');
            if (id != null && table != null) {
                toDeleteRow(id, table);
            }
            $(this).parent().parent().remove();
            event.preventDefault(); 
    
        } else {
            
            event.preventDefault();
            return false;
        }
            
    });

    function toDeleteRow(id, table) {
        $.ajax({
            type: "POST",
            url: "/cp/deleteRow",
            data: {
                'id': id,
                'table': table
            },
            dataType: 'json',
            success: function(response) {
                console.log(response);
                displayAjaxResponse(response); 
            },
            error: function(response){
                console.log(response);
            }
        });
    }

    function displayAjaxResponse(response, element='') {
        if(element!='') {
            $(element).closest('.modal').fadeOut(100);
            $('.modal-backdrop').fadeOut(100);
        }
    
        $('#fade-respond').fadeIn(500).html(response.msg);
    
        if (response.error === true) {
            $('#fade-respond').removeClass().addClass('alert alert-danger');
        } else {
            if(element!='') {
                $(element).closest('.single-image').fadeOut(100);
                $(element).closest('.fileupload').find('#thumb-img').attr('src', '/public/i/no-image-found.jpg');
                $(element).closest('.fileupload').find('.del_btn').hide();
            }
            $('#fade-respond').removeClass().addClass('alert alert-success');
        }
        setTimeout(function() {
            $('#fade-respond').fadeOut(700);
        }, 2000);
    }

    // afiseaza in input formatul telefonului
    window.onload = function () {
        MaskedInput({
            elm: document.getElementById('phone'), // select only by id
            format: '+373 (__) ___-___'
            , separator: '+373 ()-'
        });
        MaskedInput({
            elm: document.getElementById('resp_phone'), // select only by id
            format: '+373 (__) ___-___'
            , separator: '+373 ()-'
        });
        MaskedInput({
            elm: document.getElementById('book_phone'), // select only by id
            format: '+373 (__) ___-___'
            , separator: '+373 ()-'
        });

        MaskedInput({
            elm: document.getElementById('Phone'), // select only by id
            format: '+373 (__) ___-___'
            , separator: '+373 ()-'
        });
    };

    $(document).on('change', '#status', function(){ 
       id = $(this).attr("class");
       status = $(this).children(":selected").attr("id");
       $.ajax({
        type: "POST",
        url: "/cp/changeStatus",
        data: {
            'status': status,
            'id': id
        },
        dataType: 'json',
        success: function(res) {
            if (res.error == true) {
                console.log(res);
            } else {
                console.log(res);
            }
        },
        error: function(res){
            console.log(res);
        }
        });
    });

        $(document).on('keyup', 'input[name="match"]', function(){
        
        var match = $(this).val();
        var url = $(location).attr('href');
        var segments = url.split( '/' );
        var lang = segments[3];

        if(match != '') {

            $.ajax({
                type: "POST",
                url: "/pages/autocomplete",
                data: {
                    match : match
                },
                dataType: 'json',
                success: function(data) {
                    if (data.error == true) {
                        console.log(data);
                    } else {
                        $('.search_result').empty();
                        $.each(data.result, function (index, value) {
                            $('.search_result').append('<a class="result_a" id="'+value.Cod+'" href="/"><span class="title">'+ value.Title+'</span></a><br>');

                        });
                        $('.search_result').show();
                    }
                },
                error: function(data){
                    console.log(data);
                }
            });
        
        } else {
            $('.search_result').hide(); 
        }
    });

    $(document).on('click', '.result_a', function(event){

        var id = $(this).attr('id');
        var text = $(this).text();
        
        $('input.autocomp').val('');
        $('input#prod_id').val('');

        $('input.autocomp').val(text);
        $('input#prod_id').val(id);

        $('.search_result').hide(); 

        event.preventDefault();
    });

        $(document).on('click', '#addprod', function(event){

            var id = $('.autoform').find('input#prod_id').val();

            var url = $(location).attr('href');
            var segments = url.split( '/' );
            var tabid = segments[5];

            if(id > '') {

                $.ajax({
                    type : 'POST',
                    url  : '/cp/addprodtotabs',
                    data : {
                        id : id,
                        tabid : tabid
                    },
                    dataType : 'json',
                    success : function(res) {
                        console.log(res);
                        $('.alertmsg').empty();
                        $('.alertmsg').append(res.msg);
                        $('.alertmsg').show();
                        setTimeout(function(){
                            $('.alertmsg').hide();
                            $('input.autocomp').val('');
                            location.reload();          
                        }, 2000);

                    },
                    error : function(res) {
                        console.log(res);
                        location.reload();
                    }
                });            
            }
            //event.preventDefault();
        });
});

function toDeleteImg(element, path, id, table, multi, name, intable) {
    $.ajax({
        type: "POST",
        url: path,
        data: {
            'id': id,
            'table': table,
            'name' : !name ? null : name
        },
        dataType: 'json',
        success: function(res) {
            if (res.msg === 'error') {
                $('#fade-respond').fadeIn(500).html(res.cause);
                $('#fade-respond').removeClass().addClass('alert-danger');
                setTimeout(function() {
                    $('#fade-respond').fadeOut(700);
                }, 1500);
            } else {
                $(element).closest('.fileupload').find('.modal').fadeOut(100);
                $('.modal-backdrop').fadeOut(100);

                $('#fade-respond').fadeIn(500).html(res.msg);
                $('#fade-respond').removeClass().addClass('alert alert-success');

                setTimeout(function() {
                    $('#fade-respond').fadeOut(700);
                }, 1500);

                $(element).closest('.fileupload').find('.modal').fadeOut(100);
                if (multi) {
                    $('#'+id).hide();
                } else {
                    if (name == 'swf') {
                        $(element).closest('.fileupload').find('#thumb-img').remove();
                        $('#swf-file').html('<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>');
                    }else{
                        $(element).closest('.fileupload').find('#thumb-img').attr('src', 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image');
                    }

                    $(element).closest('.fileupload').find('.del_btn').hide();
                }

            }
        },
        error: function(res){
            console.log(res);
        }
    });
}