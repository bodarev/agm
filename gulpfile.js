var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var minify = require('gulp-minify');
var rename = require('gulp-rename');
var notify = require('gulp-notify');
var del = require('del');


// source and distribution folder
var
    source = 'src/',
    dest = 'public/assets/';

// Bootstrap source
var bootstrap = {
        in: './node_modules/bootstrap-sass/'
	};
	
// Jquery source
var jquery = {
	in: './node_modules/jquery/dist/'
};

// fonts
var vendor = {
	in: [jquery.in + '**/*.min.js', bootstrap.in + 'assets/javascripts/**/*.min.js'],
	out: dest + 'js/vendor/'
};

var fonts = {
	in: [source + 'fonts/*.*', bootstrap.in + 'assets/fonts/**/*'],
	out: dest + 'fonts/'
};

 // css source file: .scss files
 var css = {
    in: source + 'bootstrap/main.scss',
    out: dest + 'css/',
    watch: source + 'bootstrap/**/*',
    sassOpts: {
        outputStyle: 'compressed',
        precision: 8,
        errLogToConsole: true,
        includePaths: [bootstrap.in + 'assets/stylesheets']
    }
};

var onError = function( err ) {
	console.log( 'An error occured:', err );
	this.emit( 'end' );
  }


gulp.task('clean-js', function () {
	return del([
		dest + 'js/*.js'
	]);
});
 
gulp.task('clean-css', function () {
	return del([
		dest + 'css/*.css'
	]);
});

gulp.task('pack-js', ['clean-js'], function () {	
	return gulp.src([source + 'js/main.js', source + 'js/module*.js'])
		.pipe(concat('bundle.js'))
		.pipe(minify({
			ext:{
				min:'.min.js'
				},
			noSource: true
		}))
		.pipe(gulp.dest(dest + 'js'))

		.pipe(notify('[pack-js] task completato'))
});
 
gulp.task('pack-css', ['clean-css'], function () {	
	return gulp.src([source + 'scss/main.scss', source + 'scss/custom.scss'])
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest(dest + 'css'))
		.pipe(concat('stylesheet.min.css'))
		.pipe(cleanCSS())
   .pipe(gulp.dest(dest + 'css'))

   .pipe(notify('[pack-css] task completato'))
});

gulp.task('pack-bootstrap', function () {
    return gulp.src(css.in)
		.pipe(sass(css.sassOpts))
		.pipe(rename('bootstrap.min.css'))
		.pipe(gulp.dest(css.out))
		
		.pipe(notify('[pack-bootstrap] task completato'))
});

gulp.task('vendor', function () {
    return gulp
        .src(vendor.in)
        .pipe(gulp.dest(vendor.out));
});

gulp.task('fonts', function () {
    return gulp
        .src(fonts.in)
		.pipe(gulp.dest(fonts.out))
});

gulp.task('default', ['pack-js', 'pack-css', 'pack-bootstrap', 'vendor', 'fonts']);

gulp.task('watch', function() {
 gulp.watch(source + 'js/**/*.js', ['pack-js']);
 gulp.watch(source + 'scss/**/*.scss', ['pack-css']);
 gulp.watch(css.watch, ['pack-bootstrap']);
});